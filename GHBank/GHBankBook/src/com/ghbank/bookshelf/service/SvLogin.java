package com.ghbank.bookshelf.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.BookshelfStartActivity;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;

public class SvLogin {

	public void login(final Activity ac, AQuery aq,final EditText username, EditText password) {
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
   		 	@Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if(json != null){
					NLog.w("Login","Login : " + json);
                	try {
                		if (!json.isNull("status") && json.getString("status").equals("1")) {
                			@SuppressWarnings("static-access")
							final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
                			SharedPreferences.Editor sPrefEditor = sPref.edit();

//            				sPrefEditor.putString( SnPreferenceVariable.USER_NAME
//									, !json.isNull("username") ? (json.getString("username").split(":").length > 0 ? json.getString("username").split(":")[0] : "") : "" );

							String userName = "";
							if(!json.isNull("username")){
								userName = json.getString("username");
							}

							sPrefEditor.putString( SnPreferenceVariable.USER_NAME,userName );

            				sPrefEditor.putString( SnPreferenceVariable.NICK_NAME, !json.isNull("nickname") ? json.getString("nickname") : "" );
            				sPrefEditor.putString( SnPreferenceVariable.GROUP, !json.isNull("group") ? json.getString("group") : "0" );
            				sPrefEditor.putString( SnPreferenceVariable.SESSION_ID, !json.isNull("session_id") ? json.getString("session_id") : "" );
            				sPrefEditor.putString( SnPreferenceVariable.USER_KEY, !json.isNull("user_key") ? json.getString("user_key") : "" );
							sPrefEditor.putBoolean( SnPreferenceVariable.IS_USER_LOGIN , !json.isNull("session_id"));
            				sPrefEditor.commit();

							if(sPrefEditor.commit()) {
								Toast.makeText(ac, "Login successful", Toast.LENGTH_LONG).show();
								Intent i = new Intent(ac, BookshelfStartActivity.class);
								ac.startActivity(i);
								ac.finish();
							} else {
								Toast.makeText(ac, "Login fail, sometime username or password is wrong", Toast.LENGTH_LONG).show();
							}
                		} else if( !json.isNull("user_key") && !json.getString("user_key").equals("")) {
							Toast.makeText(ac, "Login fail, sometime username or password is wrong", Toast.LENGTH_LONG).show();
	            		} else{
							Toast.makeText(ac, "Login fail, sometime username or password is wrong", Toast.LENGTH_LONG).show();
	            		}
                	} catch (JSONException e) { 
	           			Log.e("Login", e.getMessage());
	           		}
                } else {
                	Toast.makeText(ac, "Please check  your internet connection", Toast.LENGTH_LONG).show();
	            }
            }
		};
		cb.header("User-Agent", "android");
   		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
   		pairs.add(new BasicNameValuePair("t", "3"));
   		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
   		pairs.add(new BasicNameValuePair("os_type", UtConfig.PLATFORM));
   		pairs.add(new BasicNameValuePair("login", username.getText().toString()));
   		pairs.add(new BasicNameValuePair("password", password.getText().toString()));
 
   		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);
		
		aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	}
	
	  @SuppressWarnings("unused")
	private void hideSoftKeyboard(Activity ac ,EditText edt)
	    {
	        if(ac.getCurrentFocus()!=null && ac.getCurrentFocus() instanceof EditText){
	            InputMethodManager imm = (InputMethodManager)ac.getSystemService(Context.INPUT_METHOD_SERVICE);
	            imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
	        }
	    }
}
