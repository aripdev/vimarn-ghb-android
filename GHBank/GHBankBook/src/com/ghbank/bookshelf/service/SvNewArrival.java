package com.ghbank.bookshelf.service;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.ghbank.bookshelf.R;


public class SvNewArrival {

	public ArrayList<HashMap<String, String>> GetDataFeatureFromJSONObject(
			Context ctx, JSONObject json) {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

		if (json != null) {
			try {
				if (json.getString("status").equals("1")) {
					JSONArray c = json.getJSONArray("data");
					for (int i = 0; i < c.length(); i++) {
						HashMap<String, String> hash = new HashMap<String, String>();
						JSONObject json2 = c.getJSONObject(i);
						String id = json2.getString("id");
       					String name = json2.getString("name");
       					String image = json2.getString("image");
       					String st = json2.getString("status");
       					hash.put("id",id);
       					hash.put("name",name);
       					hash.put("image", image);	
       					hash.put("status", st);	
						arrayList.add(hash);
					}
				} else {
					Toast.makeText(
							ctx,
							ctx.getString(R.string.dg_internet_connection_check),
							Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {

			}
		} else {
			Toast.makeText(ctx,
					ctx.getString(R.string.dg_internet_connection_check),
					Toast.LENGTH_LONG).show();
		}
		return arrayList;
	}
}
