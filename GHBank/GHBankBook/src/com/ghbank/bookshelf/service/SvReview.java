package com.ghbank.bookshelf.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.adapter.AdtReviewAdapter;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.R;

public class SvReview {
	
	@SuppressWarnings("unused")
	private static String DB_PATH;
	
	public ArrayList<HashMap<String, String>> GetDataReviewFromJSONObject(Context ctx, JSONObject json) {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

		if (json != null) {
			try {
				if (json.getString("status").equals("1")) {
					JSONArray c = json.getJSONArray("review_all");
					for (int i = 0; i < c.length(); i++) {
						HashMap<String, String> hash = new HashMap<String, String>();
						JSONObject json2 = c.getJSONObject(i);
						
						String title = json2.getString("title");
						String desc = json2.getString("review");
						String date = json2.getString("datetime");
						String average = json2.getString("rating");
						
						hash.put("desc", desc);
						hash.put("title", title);
						hash.put("date", date);
						hash.put("average", average);
						arrayList.add(hash);
					}
				} else {
					Toast.makeText(ctx,ctx.getString(R.string.dg_internet_connection_check),Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {

			}
		} else {
			Toast.makeText(ctx,ctx.getString(R.string.dg_internet_connection_check),Toast.LENGTH_LONG).show();
		}
		return arrayList;
	}
	
	public void sendReview(final Activity ac,final AQuery aq,final View popupView,final ListView listReview,final String id, final EditText title, EditText detail, String rating, String ebookId)
	{
		DB_PATH = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/"+id+"/";
		 final ImageView pb = (ImageView) popupView.findViewById(R.id.progressBar_horizontal);
		 if(!pb.isShown()){
			 pb.setVisibility(View.VISIBLE);
			 listReview.setVisibility(View.GONE);
		 }
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
			
   		 	@Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if(json != null){
                	try {
                		if (!json.isNull("status") && json.getString("status").equals("1")) 
                		{
                			setReviewLoader(ac, aq, popupView, listReview, id);
                		}else if(!json.isNull("status") && json.getString("status").equals("2")) {
                			Toast.makeText(ac, "Please sign out and sign in again. Your account used by another device.", Toast.LENGTH_LONG).show();
                		}

                		Log.e("Error Status", json.getString("status"));
                	} catch (JSONException e) { 
	           			Log.e("Login", e.getMessage());
	           		}
                }
                else
                {            
                	Toast.makeText(ac, "Please check  your internet connection", Toast.LENGTH_LONG).show();
	            }
            }
		};
		@SuppressWarnings("static-access")
		final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		cb.header("User-Agent", "android");
   		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
   		Log.e("login", sPref.getString(SnPreferenceVariable.USER_NAME, ""));
   		Log.e("session_id", sPref.getString(SnPreferenceVariable.SESSION_ID, ""));
   		Log.e("id", ebookId);
   		Log.e("review_title", title.getText().toString());
   		Log.e("review_detail", detail.getText().toString());
   		Log.e("review_rating", rating!=null?rating:"0");
   		
   		pairs.add(new BasicNameValuePair("t", "3"));
   		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
   		pairs.add(new BasicNameValuePair("login", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
   		pairs.add(new BasicNameValuePair("session_id", sPref.getString(SnPreferenceVariable.SESSION_ID, "")));
   		pairs.add(new BasicNameValuePair("id", ebookId));
   		pairs.add(new BasicNameValuePair("review", "1"));
   		pairs.add(new BasicNameValuePair("review_title", title.getText().toString()));
   		pairs.add(new BasicNameValuePair("review_detail", detail.getText().toString()));
   		pairs.add(new BasicNameValuePair("review_rating", rating!=null?rating:"0"));
 
   		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);
		
		aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	}
	
	public void setReviewLoader(final Activity ac, AQuery aq,final View popupView,final ListView listReview, String id){
//		id = cat_id;
				
	    DB_PATH = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/"+id+"/";
	    final ImageView pb = (ImageView) popupView.findViewById(R.id.progressBar_horizontal);
	    
	   
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
    		@Override
        	public void callback(String url, JSONObject json,
        			AjaxStatus status) {
        		super.callback(url, json, status);
        		SvReview svBanner = new SvReview();
    	      	ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
    	      	arrayList = svBanner.GetDataReviewFromJSONObject(ac, json);
    	       if(arrayList!=null){
    	    	   if(pb.isShown()){
    	    		   pb.setVisibility(View.GONE);
    	    		   listReview.setVisibility(View.VISIBLE);
    	  		 }
    	    	  
    	       }
    	       AdtReviewAdapter adtReviw = new AdtReviewAdapter(ac, arrayList);
    	       if(adtReviw!=null){
    	    	   listReview.setAdapter(adtReviw);
    	    	   adtReviw.notifyDataSetChanged();
    	       }
        	}
    	};
    	 @SuppressWarnings("static-access")
		final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
    	 
    	cb.header("User-Agent", "android");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("t", "1"));    
        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE)); 
        //pairs.add(new BasicNameValuePair("test", "1")); 
        pairs.add(new BasicNameValuePair("preview", "1")); 
        pairs.add(new BasicNameValuePair("id", id));
        pairs.add(new BasicNameValuePair("username", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
        pairs.add(new BasicNameValuePair("pgroup", sPref.getString(SnPreferenceVariable.GROUP, "0")));
  
        HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);
        
        aq.image(R.id.progressBar_horizontal).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
    }
}
