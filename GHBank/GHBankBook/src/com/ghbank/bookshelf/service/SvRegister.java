package com.ghbank.bookshelf.service;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;

public class SvRegister {

	private static final String TAG = SvRegister.class.getSimpleName();

	public void register(final Activity ac, AQuery aq,final EditText username, EditText password, EditText name)
	{
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
			
   		 	@Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if(json != null){
					NLog.w(TAG,"Response : " + json);
                	try {
                		if (!json.isNull("status") && json.getString("status").equals("1")) 
                		{
//                			Intent data = new Intent();
//                            data.putExtra("username", !json.isNull("username")?json.getString("username"):"");
//                    		data.putExtra("nickname", !json.isNull("nickname")?json.getString("nickname"):"");
//                    		data.putExtra("group", !json.isNull("group")?json.getString("group"):"");
//                    		data.putExtra("key", !json.isNull("user_key")?json.getString("user_key"):"");
                		}
	           		
                	
	            		if( !json.isNull("message_error") && !json.getString("message_error").equals("") )
	            		{
	//            			setResult(RESULT_OK, data);
	            			Toast.makeText(ac, json.getString("message_error"), Toast.LENGTH_LONG).show();
//	            			hideSoftKeyboard(ac,username);
	            			ac.finish();
	            		}
                	} catch (JSONException e) { 
	           			Log.e("Login", e.getMessage());
	           		}
                }
                else
                {            
                	Toast.makeText(ac, "Please check  your internet connection", Toast.LENGTH_LONG).show();
	            }
            }
		};
		cb.header("User-Agent", "android");
   		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
   		pairs.add(new BasicNameValuePair("t", "3"));
   		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
   		pairs.add(new BasicNameValuePair("login", username.getText().toString()));
   		pairs.add(new BasicNameValuePair("password", password.getText().toString()));
   		pairs.add(new BasicNameValuePair("nickname", name.getText().toString()));
   		pairs.add(new BasicNameValuePair("register", "1"));

		NLog.w(TAG,"t : 3");
		NLog.w(TAG,"library : " +  UtConfig.LIBRARY_CODE);
		NLog.w(TAG,"login : " + username.getText().toString());
		NLog.w(TAG,"password : " + password.getText().toString());
		NLog.w(TAG,"nickname : " + name.getText().toString());
		NLog.w(TAG,"register : 1");

   		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);
		aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	}
}
