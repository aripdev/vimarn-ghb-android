package com.ghbank.bookshelf.service;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.ghbank.bookshelf.R;


public class SvBanner {
	public ArrayList<HashMap<String, String>> GetDataBannerFromJSONObject(Context ctx, JSONObject json) {
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

		if (json != null) {
			try {
				if (json.getString("status").equals("1")) {
					JSONArray c = json.getJSONArray("data");
					for (int i = 0; i < c.length(); i++) {
						HashMap<String, String> hash = new HashMap<String, String>();
						JSONObject json2 = c.getJSONObject(i);
						String id = json2.getString("id");
						String click = json2.getString("click");
						String picture = json2.getString("picture");
						String link = json2.getString("link");
						hash.put("id", id);
						hash.put("click", click);
						hash.put("picture", picture);
						hash.put("link", link);
						arrayList.add(hash);
					}
				} else {
					Toast.makeText(ctx,ctx.getString(R.string.dg_internet_connection_check),Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {

			}
		} else {
			Toast.makeText(ctx,ctx.getString(R.string.dg_internet_connection_check),Toast.LENGTH_LONG).show();
		}
		return arrayList;
	}
}
