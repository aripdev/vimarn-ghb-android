package com.ghbank.bookshelf;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ghbank.bookshelf.change.category.BookShelfViewCategory;
import com.ghbank.bookshelf.change.category.CategorySelectActivity;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.home.HomeBookshelfTypeShelf;
import com.ghbank.bookshelf.change.home.HomeBookshelfTypeSlide;
import com.ghbank.bookshelf.change.loaded.LoadedBookMainFragment;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.Utils;

import group.aim.framework.helper.ScreenSizeHelper;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class BookshelfStartActivity extends FragmentActivity {

    private FrameLayout slideshowContainer , tabsContainer;
    private LinearLayout layoutContainer;
    private ImageView navLeftButton , navLogo , navButtonFirst , navButtonSecond , navButtonThird;
    private static final int REQUEST_CATEGORY_INTENT_FROM_STARTUP = 9980;

    private final int HOME_TYPE_BANNER = 1;
    private final int HOME_TYPE_SHELF = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_book_shelf_start_activity);

        this.initialView();
        this.setValue();
        this.setSlideShowView();
        this.initialOnClick();
    }

    private void initialView() {
        this.layoutContainer = (LinearLayout) findViewById(R.id.new_main_book_shelf__container);
        this.tabsContainer = (FrameLayout) findViewById(R.id.new_main_book_shelf__content_container);
        this.navButtonFirst = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_first);
        this.navButtonSecond = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_second);
        this.navButtonThird = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_third);
        this.navLeftButton = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_left);
        this.navLogo = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_logo);
    }

    private void initialOnClick() {

        this.navButtonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NLog.w("BookShelfStartActivity","on button first clicked.");
                Intent categorySelectIntent = new Intent(BookshelfStartActivity.this , com.ghbank.bookshelf.BkShelfTutorial.class);
                startActivity(categorySelectIntent);
            }
        });

        this.navButtonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 NLog.w("BookShelfStartActivity","on button second clicked.");


                int centerOfButton = (navButtonFirst.getWidth() + navButtonSecond.getWidth() + navButtonThird.getWidth()) / 2;
                int paddingFromLeft = ScreenSizeHelper.getScreenWidth() - centerOfButton;

                Intent categorySelectIntent = new Intent(BookshelfStartActivity.this , CategorySelectActivity.class);
                categorySelectIntent.putExtra("POINT" , paddingFromLeft);

                startActivityForResult(categorySelectIntent , REQUEST_CATEGORY_INTENT_FROM_STARTUP);
            }
        });

        this.navButtonThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NLog.w("BookShelfStartActivity","on button third clicked.");
                Intent librarySelectIntent = new Intent(BookshelfStartActivity.this , LoadedBookMainFragment.class);
                startActivity(librarySelectIntent);
            }
        });

        this.navLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NLog.w("BookShelfStartActivity","on button user clicked.");
                final SharedPreferences sPref = getSharedPreferences(SnPreferenceVariable.PF_ARIP, Context.MODE_PRIVATE );
                if(sPref.getBoolean(SnPreferenceVariable.IS_USER_LOGIN,false)){
                    Intent librarySelectIntent = new Intent(BookshelfStartActivity.this, com.ghbank.bookshelf.BkShelfEditProfile.class);
                    startActivity(librarySelectIntent);
                }else {
                    Intent librarySelectIntent = new Intent(BookshelfStartActivity.this, com.ghbank.bookshelf.BkShelfLogIn.class);
                    startActivity(librarySelectIntent);
                }
            }
        });
    }

    private void setValue() {

        Drawable backgroundDrawable = ThemeResourceManager.getBackground1(this);
        Drawable helpIcon = ThemeResourceManager.getHelpIcon();
        Drawable myBookIcon = ThemeResourceManager.getMyBookIcon();
        Drawable categoryIcon = ThemeResourceManager.getCategoryIcon();
        Drawable loginIcon = ThemeResourceManager.getLoginIcon();
        Drawable logoIcon = ThemeResourceManager.getLogoIcon();

        Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
        if(headerBackground != null){
            ImageView headerView = (ImageView)findViewById(R.id.headerBackground);
            headerView.setImageDrawable(headerBackground);
        }

        if(backgroundDrawable != null) {
            this.layoutContainer.setBackground(backgroundDrawable);
        }else{
            this.layoutContainer.setBackgroundResource(R.drawable.bg1_1);
        }

        if(helpIcon != null)
            this.navButtonFirst.setImageDrawable(helpIcon);

        if(categoryIcon != null)
            this.navButtonSecond.setImageDrawable(categoryIcon);

        if(myBookIcon != null)
            this.navButtonThird.setImageDrawable(myBookIcon);

        if(logoIcon != null)
            this.navLogo.setImageDrawable(logoIcon);

        if(loginIcon != null)
            this.navLeftButton.setImageDrawable(loginIcon);

        this.navLeftButton.setVisibility(ThemeResourceManager.isThemeShowLogin(this) ? View.VISIBLE : View.GONE);
        this.navButtonFirst.setVisibility(ThemeResourceManager.isShowHelpIcon(this) ? View.VISIBLE : View.GONE);
        this.navButtonSecond.setVisibility(ThemeResourceManager.isShowCategoryIcon(this) ? View.VISIBLE : View.GONE);

    }

    private void setSlideShowView() {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if(ThemeResourceManager.getHomeType(this) == HOME_TYPE_BANNER) {
            transaction.replace(R.id.new_main_book_shelf__content_container, HomeBookshelfTypeSlide.newInstance());
        } else {
            transaction.replace(R.id.new_main_book_shelf__content_container, HomeBookshelfTypeShelf.newInstance());
        }

        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CATEGORY_INTENT_FROM_STARTUP && resultCode == RESULT_OK) {
            Intent categoryIntent = new Intent(this , BookShelfViewCategory.class);
            categoryIntent.putExtra("RESULT" , data.getExtras().getSerializable("RESULT"));
            startActivity(categoryIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TextView textUserName = (TextView)findViewById(R.id.textUserName);

        if(Utils.isTablet(BookshelfStartActivity.this)) {
            boolean isLoggedIn = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE)
                    .getBoolean(SnPreferenceVariable.IS_USER_LOGIN, false);

            if (isLoggedIn) {
                textUserName.setText(getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE)
                        .getString(SnPreferenceVariable.NICK_NAME, ""));
            } else {
                textUserName.setText("");
            }
        }else {
            textUserName.setText("");
        }
    }
}
