package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.service.SvEdit;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.utils.UtUtilities;

import java.io.File;

public class BkShelfEditProfile extends FragmentActivity {

	private SharedPreferences sPref;
	private LinearLayout lnScreen;
	private ImageView imgProfile;
	private TextView txtHeaderUser;
	private TextView txtHeaderUsername;
	private TextView btnSubmitName;
	private TextView btnSubmitPassword;
    private TextView btnCanel;
    private TextView btnSignout;
    private EditText edtNameOrEmail;
    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;
    
    private AQuery aq;
    private String edtCurrentUsername;
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_edit);
		
		final ActionBar actionBar = getActionBar();
	    actionBar.hide(); 
	    
	    aq = new AQuery(this);
	    initial();
	    
	    txtHeaderUsername.setText(sPref.getString(SnPreferenceVariable.NICK_NAME, "Anonymus"));
//	    edtNameOrEmail.setText(sPref.getString(SnPreferenceVariable.USER_NAME, "Anonymus"));
	    edtCurrentUsername = sPref.getString(SnPreferenceVariable.USER_NAME, "Anonymus");
	    File fBg = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1);
        if (fBg.exists()) {
        	if (Build.VERSION.SDK_INT >= 16) {

        		lnScreen.setBackground(new BitmapDrawable(getResources(),
            			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG3)));

        	} else {
        		lnScreen.setBackgroundDrawable(new BitmapDrawable(getResources(),
            			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG3)));
        	}

        }

		// Set background
//		Drawable bg = ThemeResourceManager.getBackgroundFromType(1);
//		if(bg != null){
//			lnScreen.setBackground(bg);
//		}

        File fIcon1 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1);
        if (fIcon1.exists()) {
        	imgProfile.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1));
        }
        
        btnSubmitName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(edtNameOrEmail!=null && edtNameOrEmail.length()>0){
				
					boolean isComplete = true;
					String msgError = "";
					
	                if(isComplete){
	                	new SvEdit().changeUserInfo(BkShelfEditProfile.this, aq, edtCurrentUsername, edtNameOrEmail, txtHeaderUsername, sPref.getString(SnPreferenceVariable.SESSION_ID, ""));
	                }else{
	                	Toast.makeText(BkShelfEditProfile.this, msgError, Toast.LENGTH_LONG).show();
	                }
				}else{
					Toast.makeText(BkShelfEditProfile.this, "Please completed all fields", Toast.LENGTH_LONG).show();
				}
			}
		});
        
        btnSubmitPassword.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if((edtOldPassword!=null && edtOldPassword.length()>0) && 
						(edtNewPassword!=null  && edtNewPassword.length()>0) &&
						(edtConfirmPassword!=null  && edtConfirmPassword.length()>0)){
				
					boolean isComplete = true;
					String msgError = "";
					
					if(!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())){
						msgError = "Your password doesn't match with comfirm password";
						isComplete = false;
					}
	              
	                 
	                if(isComplete){
	                	new SvEdit().changePassword(BkShelfEditProfile.this, aq, edtCurrentUsername, edtOldPassword, edtNewPassword, sPref.getString(SnPreferenceVariable.SESSION_ID, ""));
	                }else{
	                	Toast.makeText(BkShelfEditProfile.this, msgError, Toast.LENGTH_LONG).show();
	                }
				}else{
					Toast.makeText(BkShelfEditProfile.this, "Please completed all fields", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		btnCanel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		btnSignout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				displayLogoutConfirm();
    			
			}
		});
    }
    
    private void initial(){
    	sPref = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE );
    	lnScreen = (LinearLayout) findViewById(R.id.lay_edit);
	    imgProfile = (ImageView) findViewById(R.id.img_profile);
	    txtHeaderUser = (TextView) findViewById(R.id.txt_header_user);
	    txtHeaderUsername = (TextView) findViewById(R.id.txt_header_username);
	    btnSubmitName = (TextView) findViewById(R.id.txt_save_name);
	    btnSubmitPassword = (TextView) findViewById(R.id.txt_forgot);
	    btnCanel = (TextView) findViewById(R.id.txt_cancel);
	    btnSignout = (TextView) findViewById(R.id.txt_signout);
	    edtNameOrEmail = (EditText) findViewById(R.id.edt_name_or_email);
	    edtOldPassword = (EditText) findViewById(R.id.edt_old_password);
	    edtNewPassword = (EditText) findViewById(R.id.edt_new_password);
		edtConfirmPassword = (EditText) findViewById(R.id.edit_confirm_password);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		btnSubmitName.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_edit_save_name)+"</u></body></html>"));
		btnSubmitPassword.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_edit_save_password)+"</u></body></html>"));
		btnCanel.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_edit_cancel)+"</u></body></html>"));
		
		txtHeaderUser.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		txtHeaderUsername.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		btnSubmitName.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		btnSubmitPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    btnCanel.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    btnSignout.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    edtNameOrEmail.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    edtOldPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    edtNewPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtConfirmPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		
		txtHeaderUser.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		txtHeaderUsername.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		btnSubmitName.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		btnSubmitPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    btnCanel.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    btnSignout.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    edtNameOrEmail.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    edtOldPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    edtNewPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtConfirmPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		
    }
    
    private void displayLogoutConfirm(){
    	AlertDialog alertDialog = new AlertDialog.Builder( BkShelfEditProfile.this).create();

		// Setting Dialog Title
		alertDialog.setTitle(getString(R.string.txt_edit_dialog_title));
		
		// Setting Dialog Message
		alertDialog.setMessage(getString(R.string.txt_edit_dialog_message));

		// Setting OK Button
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.dg_ok), new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		        // Write your code here to execute after dialog closed
		        	SharedPreferences.Editor sPrefEditor = sPref.edit();
					sPrefEditor.putString( SnPreferenceVariable.USER_NAME, "" );
					sPrefEditor.putString( SnPreferenceVariable.NICK_NAME, "" );
					sPrefEditor.putString( SnPreferenceVariable.GROUP, "" );
					sPrefEditor.putString( SnPreferenceVariable.SESSION_ID, "" );
					sPrefEditor.putString( SnPreferenceVariable.USER_KEY, "" );
					sPrefEditor.putBoolean(SnPreferenceVariable.IS_USER_LOGIN,false);
					sPrefEditor.commit();
					
					Intent i = new Intent(BkShelfEditProfile.this,BkShelfLogIn.class);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					
					startActivity(i);
					finish();
					dialog.dismiss();
		        }
		});
		
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.txt_edit_cancel), new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        // Write your code here to execute after dialog closed
	        	dialog.dismiss();
	        }
			});
				// Showing Alert Message
				alertDialog.show();
				    }
}

