package com.ghbank.bookshelf.change.instance;

import group.aim.framework.basemodel.BaseArrayList;
import com.ghbank.bookshelf.change.model.CategoryModel;

/**
 * Created by narztiizzer on 12/15/2016 AD.
 */

public class ApplicationInstance {

    private static ApplicationInstance ourInstance;

    private BaseArrayList<CategoryModel> categoryInstance = null;

    private boolean isDeleteMode = false;

    public static ApplicationInstance getInstance() {
        if(ourInstance == null)
            ourInstance = new ApplicationInstance();
        return ourInstance;
    }

    public BaseArrayList getCategoryInstance() {
        return this.categoryInstance;
    }

    public void setCategoryInstance(BaseArrayList categoryInstance) {
        this.categoryInstance = categoryInstance;
    }

    public void setIsDeleteMode(boolean isDeleteMode) {
        this.isDeleteMode = isDeleteMode;
    }

    public boolean isDeleteMode() {
        return this.isDeleteMode;
    }
}
