package com.ghbank.bookshelf.change.tabs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringEscapeUtils;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.enumeration.UpdatePolicy;
import group.aim.framework.helper.ScreenSizeHelper;
import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.BookModel;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class TabShowItemFragment extends Fragment implements RequestURLListener {

    private LinearLayout itemList;
    private BaseArrayList mDataList = BaseArrayList.Builder(BookModel.class);
    private TextView mTextTitle;
    private boolean isChanging = false;


    public static TabShowItemFragment newInstance() {
        return new TabShowItemFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_tab_child , container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);
        this.requestItemList();
    }

    private void initialView(View view) {
        this.mTextTitle = (TextView) view.findViewById(R.id.new_tab_child__title);
        this.itemList = (LinearLayout) view.findViewById(R.id.new_tab_child__item_list);
    }

    private void setTitle(String title) {
        this.mTextTitle.setText(title);
    }

    private void requestItemList() {

        int type = 0;

        Bundle receiveArguments = getArguments();
        String target = receiveArguments.getString("TARGET");

        SharedPreferences sPref = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, Context.MODE_PRIVATE);
        String tab1 = sPref.getString(SnPreferenceVariable.THEME_TAB_1_TITLE,"HIGHLIGHT");
        String tab2 = sPref.getString(SnPreferenceVariable.THEME_TAB_2_TITLE,"FEATURE");
        String tab3 = sPref.getString(SnPreferenceVariable.THEME_TAB_3_TITLE,"NEW");

        if(target.equalsIgnoreCase(tab1)){
            type = 1;
        }else if(target.equalsIgnoreCase(tab2)){
            type = 2;
        }else if(target.equalsIgnoreCase(tab3)){
            type = 3;
        }


        setTitle(target);

        if(type != 0 && !this.isChanging)
            RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                    .addParameters("t" , 1)
                    .addParameters("type" , type)
                    .addParameters("test" , 1)
                    .addParameters("library" , UtConfig.LIBRARY_CODE)
                    .setRequestTag(target)
                    .setRequestURLListener(this)
                    .start();
    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {
        String unescapeResponse = StringEscapeUtils.unescapeJson(responseString).replace("\"\"" , "null");

        try {
            this.mDataList.updateFromJson(unescapeResponse, UpdatePolicy.ForceUpdate);
            this.setPreviewBook(this.mDataList);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {

    }

    private void setPreviewBook(final BaseArrayList<BookModel> dataList) {
        if(getActivity()!= null && !this.isChanging)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isChanging = true;
                    for(int index = 0 ; index < dataList.size() ; index++) {
                        final BookModel currentModel = (BookModel) dataList.get(index);
                        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT , LinearLayout.LayoutParams.MATCH_PARENT);
                        ImageView imagePreview = new ImageView(getActivity());
                        int paddingLeftRight = (int) ScreenSizeHelper.metrics.density * 16;
                        int paddingTopBottom = (int) ScreenSizeHelper.metrics.density * 8;
                        imagePreview.setPadding(paddingLeftRight , paddingTopBottom , paddingLeftRight , paddingTopBottom);
                        imagePreview.setAdjustViewBounds(true);
                        imagePreview.setLayoutParams(imageParams);
                        Glide.with(getActivity()).load(currentModel.getImage()).dontAnimate()
                                .placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                                .into(imagePreview);
                        imagePreview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
                                intent.putExtra("id_category", currentModel.getId() + "");
                                intent.putExtra("details", currentModel.getDescription());
                                startActivity(intent);
                                Log.d("SELECT PREVIEW" , "SELECT PREVIEW BOOK ID " + currentModel.getId());
                            }
                        });
                        itemList.addView(imagePreview);
                    }
                    isChanging = false;
                    Log.d("BOOK PREVIEW" , "ADD PREVIEW BOOK SUCCESS");
                }
            });
    }
}
