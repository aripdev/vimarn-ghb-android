package com.ghbank.bookshelf.change.tabs;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class TabFragment extends Fragment {

    private FrameLayout tabFirst , tabSecond , tabThird;
    private ImageView iconTabFirst , iconTabSecond , iconTabThird , iconTabPointer;
    private float initialMoveTo , mIndex = 1;
    private Drawable highlightTabIcon , highlightSelectTabIcon;
    private Drawable newTabIcon , newSelectTabIcon;
    private Drawable featureTabIcon , featureSelectTabIcon;

    private String[] tabTitle = new String[3];

    private LinearLayout tabView;
    public static TabFragment newInstance() {
        return new TabFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_tab_category , container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sPref = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, Context.MODE_PRIVATE);
        tabTitle[0] = sPref.getString(SnPreferenceVariable.THEME_TAB_1_TITLE,"HIGHLIGHT");
        tabTitle[1] = sPref.getString(SnPreferenceVariable.THEME_TAB_2_TITLE,"FEATURE");
        tabTitle[2] = sPref.getString(SnPreferenceVariable.THEME_TAB_3_TITLE,"NEW");

        this.initialView(view);
        this.setTabIcon();
        this.initialOnTabClick();
        this.initialPointer();
        this.setTab(setPreviewTab(tabTitle[0]));

    }

    private void initialView(View view) {
        tabView = (LinearLayout)view.findViewById(R.id.tabView);
        this.tabFirst = (FrameLayout) view.findViewById(R.id.tabFirst);
        this.tabSecond = (FrameLayout) view.findViewById(R.id.tabSecond);
        this.tabThird = (FrameLayout) view.findViewById(R.id.tabThird);

        this.iconTabFirst = (ImageView) view.findViewById(R.id.iconTabFirst);
        this.iconTabSecond = (ImageView) view.findViewById(R.id.iconTabSecond);
        this.iconTabThird = (ImageView) view.findViewById(R.id.iconTabThird);
        this.iconTabPointer = (ImageView) view.findViewById(R.id.new_tab_category__tab_pointer);

//        Drawable bg = ThemeResourceManager.getSmallIconCategoryBackground();
//        if(bg != null){
//            FrameLayout frame = (FrameLayout)view.findViewById(R.id.new_tab_category__tab_content);
//            frame.setBackground(bg);
//        }
//        bg = null;
    }

    private void initialPointer() {
        this.tabSecond.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                initialMoveTo = (float) ((tabView.getMeasuredWidth() / 2) - (tabSecond.getWidth() / 1.5));
                iconTabPointer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        initialMoveTo = initialMoveTo - (iconTabPointer.getWidth());
                        setPointer(initialMoveTo);
                        iconTabPointer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
                tabFirst.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void setPointer(float moveTo) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(iconTabPointer, View.TRANSLATION_X, moveTo);
        anim.setDuration(500);
        anim.start();
    }

    private void setTabIcon() {
        this.highlightTabIcon = ThemeResourceManager.getHighlightIcon();
        this.featureTabIcon = ThemeResourceManager.getFeatureIcon();
        this.newTabIcon = ThemeResourceManager.getNewIcon();

        this.highlightSelectTabIcon = ThemeResourceManager.getHighlightSelectedIcon();
        this.featureSelectTabIcon = ThemeResourceManager.getFeatureSelectedIcon();
        this.newSelectTabIcon = ThemeResourceManager.getNewSelectedIcon();

        this.iconTabFirst.setImageDrawable(this.highlightTabIcon);
        this.iconTabSecond.setImageDrawable(this.featureTabIcon);
        this.iconTabThird.setImageDrawable(this.newTabIcon);
    }

    private void setSelectTabIcon() {
        switch((int) this.mIndex) {
            case 1 :
                if(this.highlightSelectTabIcon != null)
                    this.iconTabFirst.setImageDrawable(this.highlightSelectTabIcon);
                break;
            case 2 :
                if(this.featureSelectTabIcon != null)
                    this.iconTabSecond.setImageDrawable(this.featureSelectTabIcon);
                break;
            case 3 :
                if(this.newSelectTabIcon != null)
                    this.iconTabThird.setImageDrawable(this.newSelectTabIcon);
                break;

            default : Log.d("RESET TAB ICON" , "INDEX NOT MATCH");
        }
    }

    private void resetTabIcon() {
        switch((int) this.mIndex) {
            case 1 : this.iconTabFirst.setImageDrawable(ThemeResourceManager.getHighlightIcon());
                break;
            case 2 : this.iconTabSecond.setImageDrawable(ThemeResourceManager.getFeatureIcon());
                break;
            case 3 : this.iconTabThird.setImageDrawable(ThemeResourceManager.getNewIcon());
                break;

            default : Log.d("RESET TAB ICON" , "INDEX NOT MATCH");
        }
    }

    private void initialOnTabClick() {
        this.tabFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIndex != 1) {
                    resetTabIcon();
                    mIndex = 1;
                    setSelectTabIcon();
                    float moveTo = (float) ((tabView.getMeasuredWidth() / 2) - (tabSecond.getWidth() / 1.5)) - (iconTabPointer.getWidth());
                    setTab(setPreviewTab(tabTitle[0]));
                    setPointer(moveTo);
                }
            }
        });

        this.tabSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIndex != 2) {
                    resetTabIcon();
                    mIndex = 2;
                    setSelectTabIcon();
                    //float moveTo = (ScreenSizeHelper.getScreenWidth() / 2) - (iconTabPointer.getWidth() / 2);
                    float moveTo = (float)((tabView.getMeasuredWidth() / 2) - (iconTabPointer.getWidth() / 2));
                    setTab(setPreviewTab(tabTitle[1]));
                    setPointer(moveTo);
                }
            }
        });

        this.tabThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NLog.e("TAB 3","TAB 3 Click");
                if(mIndex != 3) {
                    resetTabIcon();
                    mIndex = 3;
                    setSelectTabIcon();
                    float moveTo = (float) ((tabView.getMeasuredWidth() / 2) + (tabSecond.getWidth() / 1.5));
                    setTab(setPreviewTab(tabTitle[2]));
                    setPointer(moveTo);
                }
            }
        });
    }

    private void setTab(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.new_tab_category__tab_content , fragment);
        transaction.commit();
    }

    private TabShowItemFragment setPreviewTab(String target) {
        Bundle sendBundle = new Bundle();
        sendBundle.putString("TARGET" , target);
        TabShowItemFragment showItemFragment = TabShowItemFragment.newInstance();
        showItemFragment.setArguments(sendBundle);

        return  showItemFragment;
    }

}
