package com.ghbank.bookshelf.change.async;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by narztiizzer on 12/26/2016 AD.
 */

public class UnzipAsyncTask {
    public static void unzip(String... sourcePath) {
        new UnzipFile().execute(sourcePath);
    }

    private static boolean unpackZip(String path) {
        InputStream inputStream;
        ZipInputStream zipInputStream;
        try {
            String filename;
            File zipFile = new File(path);
            inputStream = new FileInputStream(path);
            zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
            ZipEntry zipEntry;
            byte[] buffer = new byte[1024];
            int count;

            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                filename = zipEntry.getName();
                if (zipEntry.isDirectory()) {
                    File fmd = new File(zipFile.getParent() + "/" + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(zipFile.getParent() + "/" + filename);

                while ((count = zipInputStream.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zipInputStream.closeEntry();
            }

            zipInputStream.close();
            return true;
        }
        catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static class UnzipFile extends AsyncTask<String , Void , Boolean> {
        private int unzipCount;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            for(int unzipIndex = 0 ; unzipIndex < params.length ; unzipIndex++) {
                if(unpackZip(params[unzipIndex])) {
                    this.unzipCount++;
                }
            }

            return this.unzipCount == params.length ? true : false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("Unzip AsyncTask" , (aBoolean ? "Success" : "Fail" ) + " to unzip file.");
        }
    }
}
