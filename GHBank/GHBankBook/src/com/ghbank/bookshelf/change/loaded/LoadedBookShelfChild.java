package com.ghbank.bookshelf.change.loaded;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arip.it.library.MuPDFActivity;
import com.bumptech.glide.Glide;

import java.io.File;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.helper.ScreenSizeHelper;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.instance.ApplicationInstance;
import com.ghbank.bookshelf.change.model.LoadedBookModel;

/**
 * Created by narztiizzer on 11/30/2016 AD.
 */

public class LoadedBookShelfChild extends Fragment {

    public static final int ICON_DELETE_RES_ID = 114720;

    private LinearLayout mShelfBookFirstRow , mShelfBookSecondRow , mShelfBookThirdRow;
    private int paddingLeftRight;
    private int paddingTopBottom;
    private BaseArrayList itemList;

    public static LoadedBookShelfChild newInstance() { return new LoadedBookShelfChild(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_book_shelf_type_shelf_child_page , container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);
        this.initialValue();
        this.setContent();
    }

    private void initialView(View view) {
        this.mShelfBookFirstRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_1);
        this.mShelfBookSecondRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_2);
        this.mShelfBookThirdRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_3);
    }

    private void initialValue() {
        this.paddingLeftRight = (int) ScreenSizeHelper.metrics.density * 4;
        this.paddingTopBottom = (int) ScreenSizeHelper.metrics.density * (ScreenSizeHelper.isTabletSize(getActivity()) ? 56 : 32);
    }

    @SuppressWarnings("ResourceType")
    private void setContent() {
        Bundle argumentBundle = getArguments();

        if(argumentBundle == null || argumentBundle.isEmpty())
            return;

        this.itemList = (BaseArrayList) argumentBundle.getSerializable("ITEM");
        for(int itemIndex = 0; itemIndex < itemList.size() ; itemIndex++) {
            int row = itemIndex < 3 ? 1 : (itemIndex < 6 ? 2 : 3);
            final LoadedBookModel currentBook = (LoadedBookModel) itemList.get(itemIndex);
            File coverImage = new File(currentBook.getImage().replace("\\" , ""));

            FrameLayout frameLayout = createFrameLayout();
            ImageView imageBook = createImageView();
            ImageView deleteIcon = createDeleteView();

            deleteIcon.setId(ICON_DELETE_RES_ID);
            deleteIcon.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.op_ic_cut));
            deleteIcon.setTag(currentBook);

//            frameLayout.setVisibility(View.VISIBLE);
//            imageBook.setVisibility(View.VISIBLE);
            deleteIcon.setVisibility(View.GONE);

            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoadedBookModel bookItem = (LoadedBookModel) v.getTag();
                    ((LoadedBookMainFragment) getActivity()).deleteBookFromId(bookItem.getId());
                    ((View) v.getParent()).setVisibility(View.INVISIBLE);
                }
            });

            Glide.with(getActivity()).load(Uri.fromFile(coverImage)).dontAnimate()
                    .placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                    .into(imageBook);
//            imageBook.setImageBitmap(BitmapFactory.decodeFile(coverImage.getPath()));

            imageBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!ApplicationInstance.getInstance().isDeleteMode()) {
                        if (currentBook.getCategory().indexOf("book.pdf") != -1) {
                            Uri uri = Uri.parse(currentBook.getCategory());
                            Intent intent = new Intent(getActivity(), MuPDFActivity.class);
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setData(uri);
                            intent.putExtra("bookid", currentBook.getId());
                            intent.putExtra("title", currentBook.getName());
                            getActivity().startActivity(intent);
                        }
                    }
                }
            });

            frameLayout.addView(imageBook);
            frameLayout.addView(deleteIcon);

            switch (row) {
                case 1 : this.mShelfBookFirstRow.addView(frameLayout);
                    break;
                case 2 : this.mShelfBookSecondRow.addView(frameLayout);
                    break;
                case 3 : this.mShelfBookThirdRow.addView(frameLayout);
                    break;

                default : Log.d("ROW ITEM" , "ROW FOR ITEM NOT MATCH");
            }

            if((itemIndex + 1) == itemList.size()) {
                LinearLayout selectRow = null;
                switch (row) {
                    case 1 : selectRow = this.mShelfBookFirstRow;
                        break;
                    case 2 : selectRow = this.mShelfBookSecondRow;
                        break;
                    case 3 : selectRow = this.mShelfBookThirdRow;
                        break;

                    default : Log.d("ROW ITEM" , "ROW FOR ITEM NOT MATCH");
                }

                if(selectRow != null)
                    checkChildOfRow(selectRow);
            }

            Log.d("ROW ITEM" , "FINISH TO ADD ITEM");
        }
    }

    private void checkChildOfRow(LinearLayout row)  {
        int lostChild = 3 - row.getChildCount();
        if(lostChild > 0) {
            for(int iLost = 0 ; iLost < lostChild ; iLost++) {
                LinearLayout.LayoutParams emptyFrameParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.MATCH_PARENT , 1f);
                ImageView imageBook = createImageView();
                imageBook.setLayoutParams(emptyFrameParam);
                row.addView(imageBook);
            }
        }
    }

    private FrameLayout createFrameLayout() {
        LinearLayout.LayoutParams frameViewParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.MATCH_PARENT , 1f);
        FrameLayout frameLayout = new FrameLayout(getActivity());
        frameLayout.setLayoutParams(frameViewParam);
        frameLayout.setPadding(paddingLeftRight , paddingLeftRight * 2 , paddingLeftRight , paddingTopBottom);
        return frameLayout;
    }

    private ImageView createImageView() {
        FrameLayout.LayoutParams previewParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT , FrameLayout.LayoutParams.WRAP_CONTENT);
        previewParams.gravity = Gravity.BOTTOM;
        ImageView imageView = new ImageView(getActivity());
        imageView.setLayoutParams(previewParams);
        imageView.setAdjustViewBounds(true);
        return imageView;
    }

    private ImageView createDeleteView() {
        FrameLayout.LayoutParams deleteViewParam = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT , FrameLayout.LayoutParams.WRAP_CONTENT);
        deleteViewParam.gravity = Gravity.CENTER;
        ImageView imageView = new ImageView(getActivity());
        imageView.setLayoutParams(deleteViewParam);
        imageView.setAdjustViewBounds(true);
        return imageView;
    }
}
