package com.ghbank.bookshelf.change.loaded;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.activity.SearchActivity;
import com.ghbank.bookshelf.change.category.BookShelfViewCategory;
import com.ghbank.bookshelf.change.category.CategorySelectActivity;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.home.HomeBookShelfTypeShelfViewPagerAdapter;
import com.ghbank.bookshelf.change.instance.ApplicationInstance;
import com.ghbank.bookshelf.change.model.LoadedBookModel;
import com.ghbank.bookshelf.change.model.UpdateLoadedBookModel;
import com.ghbank.bookshelf.db.DbBook;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.helper.ScreenSizeHelper;

/**
 * Created by narztiizzer on 11/29/2016 AD.
 */

public class LoadedBookMainFragment extends FragmentActivity implements RequestURLListener {

    private ViewPager mBookPager;
    private BaseArrayList<Fragment> pagerPageList = BaseArrayList.Builder(Fragment.class);
    private BaseArrayList<LoadedBookModel> loadedBookItem = BaseArrayList.Builder(LoadedBookModel.class);
    private BaseArrayList<UpdateLoadedBookModel> updateBookItem = BaseArrayList.Builder(UpdateLoadedBookModel.class);
    private ImageView mBackgroundFirstRow , mBackgroundSecondRow , mBackgroundThirdRow;
    private int paddingLeftRight , paddingTopBottom;
    private DbBookHandler bookHandler;
    private AQuery aQuery;
    private ImageView navLeftButton , navLogo , navButtonFirst , navButtonSecond , navButtonThird;
    private LinearLayout layoutContainer;
    private TextView textTitle;
    private FrameLayout frameBook;

    private static final int REQUEST_CATEGORY_INTENT_FROM_LOADED = 10000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.loaded_book_shelf_main_page);
        ApplicationInstance.getInstance().setIsDeleteMode(false);
        this.initialView();
        this.initialValue();
        this.setView();
        this.setAction();
        this.requestLoadedBook();
    }

    private void initialView() {
        this.layoutContainer = (LinearLayout) findViewById(R.id.home_book_shelf_type_shelf__container);
        this.mBookPager = (ViewPager) findViewById(R.id.home_book_shelf_type_shelf__pager);
        this.mBackgroundFirstRow = (ImageView) findViewById(R.id.home_book_shelf_type_shelf__bg_row_1);
        this.mBackgroundSecondRow = (ImageView) findViewById(R.id.home_book_shelf_type_shelf__bg_row_2);
        this.mBackgroundThirdRow = (ImageView) findViewById(R.id.home_book_shelf_type_shelf__bg_row_3);
        this.navButtonFirst = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_first);
        this.navButtonSecond = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_second);
        this.navButtonThird = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_right_third);
        this.navLeftButton = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_button_left);
        this.navLogo = (ImageView) findViewById(R.id.new_main_book_shelf__nav_bar_logo);
        this.frameBook = (FrameLayout) findViewById(R.id.loaded_book_shelf__frame_book);
        this.textTitle = (TextView) findViewById(R.id.textTitle);

        Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
        if(headerBackground != null){
            ImageView headerView = (ImageView)findViewById(R.id.headerBackground);
            headerView.setImageDrawable(headerBackground);
        }

    }

    private void initialValue() {
        this.aQuery = new AQuery(LoadedBookMainFragment.this);
        this.textTitle.setVisibility(View.GONE);
        this.navButtonFirst.setVisibility(ThemeResourceManager.isShowHelpIcon(this) ? View.VISIBLE : View.GONE);
        this.navButtonSecond.setVisibility(ThemeResourceManager.isShowCategoryIcon(this) ? View.VISIBLE : View.GONE);
    }

    private void setView() {
        Glide.with(this).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(this , R.drawable.custom_progress_loading))
                .into(this.mBackgroundFirstRow);
        Glide.with(this).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(this , R.drawable.custom_progress_loading))
                .into(this.mBackgroundSecondRow);
        Glide.with(this).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(this , R.drawable.custom_progress_loading))
                .into(this.mBackgroundThirdRow);

        Drawable background = ThemeResourceManager.getBackground1(this);
        Drawable searchIcon = ThemeResourceManager.getSearchIcon();
        Drawable deleteIcon = ThemeResourceManager.getDeleteIcon();
        Drawable categoryIcon = ThemeResourceManager.getCategoryIcon();
        Drawable logoIcon = ThemeResourceManager.getLogoIcon();
        Drawable backIcon = ThemeResourceManager.getBackIcon();

        if(background != null) {
            this.layoutContainer.setBackground(background);
        }else{
            this.layoutContainer.setBackgroundResource(R.drawable.bg1_1);
        }

        if(searchIcon != null)
            this.navButtonFirst.setImageDrawable(searchIcon);

        if(categoryIcon != null)
            this.navButtonSecond.setImageDrawable(categoryIcon);

        if(deleteIcon != null)
            this.navButtonThird.setImageDrawable(deleteIcon);

        if(logoIcon != null)
            this.navLogo.setImageDrawable(logoIcon);

        if(backIcon != null)
            this.navLeftButton.setImageDrawable(backIcon);

        if(ScreenSizeHelper.isTabletSize(this)) {
            this.paddingLeftRight = (int) ScreenSizeHelper.metrics.density * 16;
            this.paddingTopBottom = (int) ScreenSizeHelper.metrics.density * 8;

            this.mBookPager.setPadding(this.paddingLeftRight , 0 , this.paddingLeftRight , 0);
        }

        this.textTitle.setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));
        this.textTitle.setBackground(getResources().getDrawable(R.drawable.title_background));

        Log.d("SET SHELF" , "SET SHELF BAR");
    }

    private void setAction() {
        this.navLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.navButtonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent categorySelectIntent = new Intent(LoadedBookMainFragment.this , BkShelfTutorial.class);
//                startActivity(categorySelectIntent);
                Intent searchActivity = new Intent(LoadedBookMainFragment.this , SearchActivity.class);
                startActivity(searchActivity);

            }
        });

        this.navButtonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int centerOfButton = (navButtonFirst.getWidth() + navButtonSecond.getWidth() + navButtonThird.getWidth()) / 2;
                int paddingFromLeft = ScreenSizeHelper.getScreenWidth() - centerOfButton;

                Intent categorySelectIntent = new Intent(LoadedBookMainFragment.this , CategorySelectActivity.class);
                categorySelectIntent.putExtra("POINT" , paddingFromLeft);

                startActivityForResult(categorySelectIntent , REQUEST_CATEGORY_INTENT_FROM_LOADED);
            }
        });

        this.navButtonThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int iconVisible;
                if(ApplicationInstance.getInstance().isDeleteMode()) {
                    ApplicationInstance.getInstance().setIsDeleteMode(false);
                    iconVisible = View.INVISIBLE;
                } else {
                    ApplicationInstance.getInstance().setIsDeleteMode(true);
                    iconVisible = View.VISIBLE;
                }

                resetDeleteIcon(iconVisible);

                Log.d("DELETE" , "DELETE BOOK");
            }
        });
    }

    private void resetDeleteIcon(int visibleType) {
        LinearLayout rowFirst = (LinearLayout) mBookPager.getChildAt(mBookPager.getCurrentItem()).findViewById(R.id.home_book_shelf_type_shelf__shelf_row_1);
        LinearLayout rowSecond = (LinearLayout) mBookPager.getChildAt(mBookPager.getCurrentItem()).findViewById(R.id.home_book_shelf_type_shelf__shelf_row_2);
        LinearLayout rowThird = (LinearLayout) mBookPager.getChildAt(mBookPager.getCurrentItem()).findViewById(R.id.home_book_shelf_type_shelf__shelf_row_3);
        for(int i = 0 ; i < rowFirst.getChildCount() ; i ++) {
            View childViewFirstRow = rowFirst.getChildAt(i);
            View childViewSecondRow = rowSecond.getChildAt(i);
            View childViewThirdRow = rowThird.getChildAt(i);

            if(childViewFirstRow instanceof FrameLayout && childViewFirstRow != null) {
                //noinspection ResourceType
                childViewFirstRow.findViewById(LoadedBookShelfChild.ICON_DELETE_RES_ID).setVisibility(visibleType);
            }

            if(childViewSecondRow instanceof FrameLayout && childViewSecondRow != null) {
                //noinspection ResourceType
                childViewSecondRow.findViewById(LoadedBookShelfChild.ICON_DELETE_RES_ID).setVisibility(visibleType);
            }

            if(childViewThirdRow instanceof FrameLayout && childViewThirdRow != null) {
                //noinspection ResourceType
                childViewThirdRow.findViewById(LoadedBookShelfChild.ICON_DELETE_RES_ID).setVisibility(visibleType);
            }
        }
    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {
        Log.d("REQUEST BOOK" , "REQUEST ALL BOOK SUCCESS");
    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {
        Log.d("REQUEST BOOK" , "REQUEST ALL BOOK FAIL");
    }

    private void setLoadedBookPager() {
        this.pagerPageList.clear();
        int totalPage = this.loadedBookItem.size() / 9;
        if(totalPage == 0 || this.loadedBookItem.size() % (totalPage * 9) > 0)
            totalPage++;

        for(int pageIndex = 0 ; pageIndex < totalPage ; pageIndex++) {
            Bundle argumentBundle = new Bundle();
            BaseArrayList<LoadedBookModel> itemForPage = BaseArrayList.Builder(LoadedBookModel.class);
            int itemPerPage = this.loadedBookItem.size() < 9 ? this.loadedBookItem.size() : 9;
            for(int itemIndex = 0 ; itemIndex < itemPerPage ; itemIndex++) {
                LoadedBookModel currentBook = (LoadedBookModel) this.loadedBookItem.get(itemIndex);
                itemForPage.add(currentBook);
            }
            this.loadedBookItem.removeAll(itemForPage);

            argumentBundle.putSerializable("ITEM" , itemForPage);
            LoadedBookShelfChild page = LoadedBookShelfChild.newInstance();
            page.setArguments(argumentBundle);
            this.pagerPageList.add(page);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBookPager.setAdapter(new HomeBookShelfTypeShelfViewPagerAdapter(getSupportFragmentManager() , pagerPageList));
            }
        });
    }

    private void openBookHandler() {
        if(bookHandler == null)
            bookHandler = new DbBookHandler(LoadedBookMainFragment.this);

        bookHandler.open();
    }

    private void closeBookHandler() {
        if(bookHandler != null)
            bookHandler.close();
    }

    private void requestLoadedBook() {
        new RequestLoadedBook().execute();
    }

    private class RequestLoadedBook extends AsyncTask<Void, Void, BaseArrayList<LoadedBookModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            openBookHandler();
        }

        @Override
        protected BaseArrayList<LoadedBookModel> doInBackground(Void... params) {
            try{
                List<DbBook> data = bookHandler.sort(1);

                for (int i = 0; i < data.size(); i++) {
                    LoadedBookModel item = new LoadedBookModel();

                    item.setId(data.get(i).getId());
                    item.setName(data.get(i).getTitle());
                    item.setDatetime(data.get(i).getDate());
                    item.setDescription(data.get(i).getDetail());
                    item.setCategory(data.get(i).getCategory());
                    item.setImage(data.get(i).getImage());
                    item.setRevision(data.get(i).getRevision());

                    loadedBookItem.add(item);
                }
            }catch(Exception e){

            }

            return loadedBookItem;
        }

        @Override
        protected void onPostExecute(BaseArrayList<LoadedBookModel> result) {
            super.onPostExecute(result);
            closeBookHandler();

            if (isNetworkConnected()) {
                RequestUpdateBook(result);
            }else{
                setLoadedBookPager();
            }
        }
    }

    public void deleteBookFromId(String id) {
        new DeleteFile().execute(id);
    }

    public class DeleteFile extends AsyncTask<String, Void, Void>{
        private ProgressDialog progressDialog;
        @Override
        protected Void doInBackground(String... params) {
            String DB_PATH = UtConfig.DESIGN_PATH + params[0] + "/";
            File fileOrDirectory = new File(DB_PATH);
            deleteRecursive(fileOrDirectory);
            bookHandler.deleteComment(params[0]);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(LoadedBookMainFragment.this, null, "Deleting...");
            openBookHandler();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            closeBookHandler();
//            requestLoadedBook();
        }
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    private void RequestUpdateBook(final BaseArrayList<LoadedBookModel> compare){
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json,
                                 AjaxStatus status) {
                super.callback(url, json, status);
                if(json != null) {
                    try {
                        if (json.optString("status").equals("1")) {
                            JSONArray dataArray = json.optJSONArray("data");
                            if(dataArray != null && dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    UpdateLoadedBookModel updateItem = new UpdateLoadedBookModel();
                                    JSONObject dataObject = dataArray.getJSONObject(i);

                                    updateItem.setId(dataObject.getString("id"));
                                    updateItem.setRevision(dataObject.getString("re_vision"));

                                    updateBookItem.add(updateItem);
                                }
                            } else {
                                setLoadedBookPager();
                            }
                        } else {
                            Toast.makeText(LoadedBookMainFragment.this , "Please check your internet connection", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        Log.d("UPDATE LIBRARY" , "FAIL CAUSE : " + e.getMessage());
                    } catch (RuntimeException e) {
                        Log.d("UPDATE LIBRARY" , "FAIL CAUSE : " + e.getMessage());
                    } catch (Exception e) {
                        Log.d("UPDATE LIBRARY" , "FAIL CAUSE : " + e.getMessage());
                    }

                } else {
                    Toast.makeText(LoadedBookMainFragment.this , "Please check your internet connection", Toast.LENGTH_LONG).show();
                }
            }
        };
        cb.header("User-Agent", "android");

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("t", "1"));
        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
        pairs.add(new BasicNameValuePair("type", "4"));
        if (compare.size() > 0) {
            String gid = "";
            for (int i = 0; i < compare.size(); i++) {
                LoadedBookModel currentLoadedItem = (LoadedBookModel) compare.get(i);
                gid += currentLoadedItem.getId() + ",";
            }
            pairs.add(new BasicNameValuePair("gid", gid.substring(0 , gid.length() - 1)));
        }

        HttpEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(pairs, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);
        aQuery.ajax(UtSharePreferences.getPrefServerConfig(LoadedBookMainFragment.this), params, JSONObject.class, cb);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) LoadedBookMainFragment.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CATEGORY_INTENT_FROM_LOADED && resultCode == RESULT_OK) {
            Intent categoryIntent = new Intent(this , BookShelfViewCategory.class);
            categoryIntent.putExtra("RESULT" , data.getExtras().getSerializable("RESULT"));
            startActivity(categoryIntent);
        }
    }

}
