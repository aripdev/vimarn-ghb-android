package com.ghbank.bookshelf.change.helper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.ScreenUtils;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;

/**
 * Created by narztiizzer on 11/25/2016 AD.
 */

public class ThemeResourceManager {
    public static Drawable getBackgroundFromType(int typeId) {

        String backgroundName = "";
        switch (typeId) {
            case 1 : backgroundName = UtDesign.BG1;
                break;
            case 2 : backgroundName = UtDesign.BG2;
                break;
            case 3 : backgroundName = UtDesign.BG3;
                break;
            case 4 : backgroundName = UtDesign.BG4;
                break;
            default :
                Log.d("BG TYPE" , "BG TYPE NOT MATCH");
        }

        return requestDrawable(backgroundName);
    }

    public static Drawable getMyBookIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON3);
    }

    public static Drawable getUserIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.USER_ICON);
    }

    public static Drawable getPasswordIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.PASSWORD_ICON);
    }

    public static Drawable getDownloadIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON11);
    }

    public static Drawable getHelpIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON15);
    }

    public static Drawable getCategoryIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON2);
    }

    public static Drawable getHighlightIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON7);
    }

    public static Drawable getFeatureIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON8);
    }

    public static Drawable getNewIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON9);
    }

    public static Drawable getSearchIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON40);
    }

    public static Drawable getHighlightSelectedIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON42);
    }

    public static Drawable getFeatureSelectedIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON43);
    }

    public static Drawable getNewSelectedIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON44);
    }

    public static Drawable getLoginIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON1);
    }

    public static Drawable getLogoIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.LOGO1);
    }

    public static Drawable getRatingIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON12);
    }

    public static Drawable getBackIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON4);
    }

    public static Drawable getDeleteIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.ICON5);
    }

    public static Drawable getPointerTriangleIcon() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.THEME_TRI1_1);
    }

    public static int getHomeType(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getInt(SnPreferenceVariable.THEME_HOME_TYPE , 1);
    }

    public static boolean isThemeShowLogin(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_LOGIN , false);
    }

    public static boolean isShowHelpIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_02 , false);
    }

    public static boolean isShowCategoryIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_03 , false);
    }

    public static boolean isShowBackIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_04 , false);
    }

    public static boolean isShowHighlightIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_05 , false);
    }

    public static boolean isShowTextIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_06 , false);
    }

    public static boolean isShowNoteIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_07 , false);
    }

    public static boolean isShowBookmarkIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_08 , false);
    }

    public static boolean isShowOptionIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_09 , false);
    }

    public static boolean isShowContentIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_10 , false);
    }

    public static boolean isShowForgotPasswordButton(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_11 , false);
    }

    public static boolean isShowRegisterButton(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_12 , false);
    }

    public static boolean isShowRatingIcon(Context context) {
        return context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_13 , false);
    }

    public static int getPrimaryThemeColor(Context context) {
        String hexCodeColor = context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getString(SnPreferenceVariable.THEME_COLOR , "");
        return hexCodeColor.equalsIgnoreCase("") ? Color.TRANSPARENT : Color.parseColor(hexCodeColor);
    }

    public static int getSecondaryThemeColor(Context context) {
        String hexCodeColor = context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getString(SnPreferenceVariable.THEME_COLOR2 , "");
        return hexCodeColor.equalsIgnoreCase("") ? Color.TRANSPARENT : Color.parseColor(hexCodeColor);
    }

    public static int getPlaceholderColor(Context context) {
        String hexCodeColor = context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getString(SnPreferenceVariable.THEME_PLACEHOLDER_COLOR , "");
        return hexCodeColor.equalsIgnoreCase("") ? Color.TRANSPARENT : Color.parseColor(hexCodeColor);
    }

    public static int getFontColor(Context context) {
        String hexCodeColor = context.getSharedPreferences(SnPreferenceVariable.PF_ARIP, context.MODE_PRIVATE ).getString(SnPreferenceVariable.THEME_FONT_COLOR , "");
        return hexCodeColor.equalsIgnoreCase("") ? Color.GRAY : Color.parseColor(hexCodeColor);
    }

    public static Drawable getSmallIconCategoryBackground() {
        return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + UtDesign.SMALL_ICON_7);
    }

    private static Drawable requestDrawable(String drawableName) {
        if(drawableName.equalsIgnoreCase(""))
            return null;
        else
            return ImageHelper.createDrawableFromFile(UtConfig.DESIGN_PATH_THEME + drawableName);
    }

    public static Drawable getBackground1(Context context){
        String ratio = ScreenUtils.getScreenAspectRatio(context);
        int drawableId = R.drawable.bg1_1;
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if(ratio.equals(ScreenUtils.ASPECT_RATIO_16_9)){
            drawableId = R.drawable.bg1_5;
            drawable = ContextCompat.getDrawable(context, drawableId);
        }
        return drawable;
    }
}
