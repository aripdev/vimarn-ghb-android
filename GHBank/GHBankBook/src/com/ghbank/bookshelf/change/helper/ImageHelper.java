package com.ghbank.bookshelf.change.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by narztiizzer on 11/25/2016 AD.
 */

public class ImageHelper {
    public static Drawable createDrawableFromFile(String pathToImageFile) {
        return isExistsFileFromPath(pathToImageFile) ? Drawable.createFromPath(pathToImageFile) : null;
    }

    public static ByteArrayOutputStream createBitmapByteArrayOutputStreamFromResource(Context context , int resId) {
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), resId);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream;
    }

    private static boolean isExistsFileFromPath(String path) {
        return new File(path).exists();
    }
}
