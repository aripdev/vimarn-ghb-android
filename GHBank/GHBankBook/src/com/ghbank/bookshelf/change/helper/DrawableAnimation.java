package com.ghbank.bookshelf.change.helper;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

/**
 * Created by narztiizzer on 11/30/2016 AD.
 */

public class DrawableAnimation {
    public static Drawable createFromResource(Context context , int resId) {
        AnimationDrawable holder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            holder = (AnimationDrawable) context.getResources().getDrawable(resId, null);
        else
            holder = (AnimationDrawable) context.getResources().getDrawable(resId);
        holder.start();
        return holder;
    }
}
