package com.ghbank.bookshelf.change.slideshow;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.category.BookShelfViewCategory;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.CategoryModel;
import com.ghbank.bookshelf.change.model.SlideModel;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class SlideItemFragment extends Fragment {

    private ImageView slideImage;

    public static SlideItemFragment newInstance() {
        return new SlideItemFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_slide_show_item , null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        this.initialView(view);
        this.setView();
    }

    private void initialView(View view) {
        this.slideImage = (ImageView) view.findViewById(R.id.slideImage);
    }

    private void setView() {
        Bundle recieveArguments = getArguments();
        final SlideModel slideItem = (SlideModel) recieveArguments.getSerializable("ITEM");
        Glide.with(getActivity()).load(slideItem.getPicture().replace("\\" , "")).dontAnimate()
                .placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                .into(this.slideImage);
        this.slideImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (slideItem.getClickId()) {
                    case "0" : Log.d("SELECT SLIDE" , "NO ACTION FOR THIS ITEM");
                        break;
                    case "1" : openPreviewBook(slideItem.getId());
                        break;
                    case "2" :
                        break;
                    case "3" : openCategory(slideItem.getId());
                        break;
                    case "4" : openBrowser(slideItem.getLink());
                        break;
                    case "5" :
                        break;
                    default : Log.d("SELECT SLIDE" , "ACTION FOR THIS ITEM IS NOT MATCH");
                }
            }
        });
    }

    private void openPreviewBook(String bookId) {
        Intent intent = new Intent(getActivity() , BkShelfPreview.class);
        intent.putExtra("id_category", bookId + "");
        intent.putExtra("details", "");
        startActivity(intent);
    }

    private void openCategory(String categoryId) {
        CategoryModel openCategory = new CategoryModel();
        openCategory.setId(Long.parseLong(categoryId));

        Intent categoryIntent = new Intent(getActivity() , BookShelfViewCategory.class);
        categoryIntent.putExtra("RESULT" , openCategory);
        startActivity(categoryIntent);
    }

    private void openBrowser(String target) {
        Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
        intentBrowser.setData(Uri.parse(target));
        startActivity(intentBrowser);
    }

}
