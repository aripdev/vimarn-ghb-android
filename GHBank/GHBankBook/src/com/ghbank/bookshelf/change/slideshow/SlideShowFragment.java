package com.ghbank.bookshelf.change.slideshow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.ConfigurationJSONModelParams;
import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.enumeration.UpdatePolicy;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.model.SlideModel;
import com.ghbank.bookshelf.utils.UtConfig;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class SlideShowFragment extends Fragment implements RequestURLListener {

    private static final String REQUEST_SLIDESHOW_TAG = "SLIDESHOW";

    private ViewPager slideShowPager;

    public static SlideShowFragment newInstance() {
        return new SlideShowFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_slide_show_fragment , container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);

        ConfigurationJSONModelParams.getInstance().setEntriesFieldName("data");

        RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                .addParameters("t" , 2)
                .addParameters("test" , 1)
                .addParameters("library" , UtConfig.LIBRARY_CODE)
                .setRequestTag(REQUEST_SLIDESHOW_TAG)
                .setRequestURLListener(this)
                .start();
    }

    private void initialView(View view) {
        this.slideShowPager = (ViewPager) view.findViewById(R.id.slideshowPager);
    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {
        try {
            ArrayList<Fragment> slideItemFragment = new ArrayList<>();

            BaseArrayList list = BaseArrayList.Builder(SlideModel.class);
            list.updateFromJson(responseString.replace("\"\"" , "\"null\""), UpdatePolicy.ForceUpdate);

            for(int index = 0 ; index < list.size() ; index++) {
                Bundle sendBundle = new Bundle();
                sendBundle.putSerializable("ITEM" , (SlideModel) list.get(index));

                SlideItemFragment itemFragment = SlideItemFragment.newInstance();
                itemFragment.setArguments(sendBundle);
                slideItemFragment.add(itemFragment);
            }

            SlidshowPagerAdapter slideAdapter = new SlidshowPagerAdapter(getActivity().getSupportFragmentManager() , slideItemFragment);
            setSlideshowPager(slideAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(REQUEST_SLIDESHOW_TAG , "REQUEST " + REQUEST_SLIDESHOW_TAG + " SUCCESS");
    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {
        Log.d(REQUEST_SLIDESHOW_TAG , "REQUEST " + REQUEST_SLIDESHOW_TAG + " FAIL");
    }

    private void setSlideshowPager(final SlidshowPagerAdapter adapter) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                slideShowPager.setAdapter(adapter);
            }
        });
    }
}
