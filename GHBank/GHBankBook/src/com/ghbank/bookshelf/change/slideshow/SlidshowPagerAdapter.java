package com.ghbank.bookshelf.change.slideshow;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class SlidshowPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> slideshowList;

    public SlidshowPagerAdapter(FragmentManager fm , ArrayList<Fragment> slideshowList) {
        super(fm);
        this.slideshowList = slideshowList;
    }

    @Override
    public Fragment getItem(int position) {
        return this.slideshowList.get(position);
    }

    @Override
    public int getCount() {
        return slideshowList.size();
    }
}
