package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 12/15/2016 AD.
 */

public class CategoryModel extends BaseModel implements Serializable {
    @JSONVariable
    private long id;
    @JSONVariable
    private String name;
    @JSONVariable
    private String image;
    @JSONVariable
    private BaseArrayList image_new = BaseArrayList.Builder(BookPreviewItemModel.class);
    @JSONVariable
    private BaseArrayList subcategory = BaseArrayList.Builder(CategoryModel.class);

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseArrayList getImageNew() {
        return image_new;
    }

    public void setImageNew(BaseArrayList image) {
        this.image_new = image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public BaseArrayList getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(BaseArrayList subcategory) {
        this.subcategory = subcategory;
    }
}
