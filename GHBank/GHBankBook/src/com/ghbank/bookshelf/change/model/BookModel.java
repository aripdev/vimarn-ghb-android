package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 11/23/2016 AD.
 */

public class BookModel extends BaseModel implements Serializable {

    @JSONVariable
    private long id;
    @JSONVariable
    private String name;
    @JSONVariable
    private String image;
    @JSONVariable
    private String description;
    @JSONVariable
    private BaseArrayList image_new = BaseArrayList.Builder(BookPreviewItemModel.class);
    @JSONVariable
    private String status;
    @JSONVariable
    private boolean show_status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseArrayList getImageNew() {
        return image_new;
    }

    public void setImageNew(BaseArrayList image) {
        this.image_new = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isShow() {
        return show_status;
    }

    public void setShowStatus(boolean show_status) {
        this.show_status = show_status;
    }
}
