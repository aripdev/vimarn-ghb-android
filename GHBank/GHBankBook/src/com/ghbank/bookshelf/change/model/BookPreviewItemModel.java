package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 11/24/2016 AD.
 */

public class BookPreviewItemModel extends BaseModel implements Serializable {
    @JSONVariable
    private long width;
    @JSONVariable
    private long height;
    @JSONVariable
    private String url;

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
