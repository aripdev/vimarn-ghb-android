package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 11/23/2016 AD.
 */

public class SlideModel extends BaseModel implements Serializable {
    @JSONVariable
    private String id;
    @JSONVariable
    private String picture;
    @JSONVariable
    private String link;
    @JSONVariable
    private String click;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getClickId() {
        return click;
    }

    public void setClickId(String click) {
        this.click = click;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
