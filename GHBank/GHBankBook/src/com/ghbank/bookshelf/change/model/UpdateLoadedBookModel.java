package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 12/15/2016 AD.
 */

public class UpdateLoadedBookModel extends BaseModel implements Serializable {
    @JSONVariable
    private String id;
    @JSONVariable
    private String revision;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }
}
