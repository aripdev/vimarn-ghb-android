package com.ghbank.bookshelf.change.model;

import java.io.Serializable;

import group.aim.framework.annotation.JSONVariable;
import group.aim.framework.basemodel.BaseModel;

/**
 * Created by narztiizzer on 12/15/2016 AD.
 */

public class LoadedBookModel extends BaseModel implements Serializable {
    @JSONVariable
    private String id;
    @JSONVariable
    private String name;
    @JSONVariable
    private String datetime;
    @JSONVariable
    private String description;
    @JSONVariable
    private String category;
    @JSONVariable
    private String image;
    @JSONVariable
    private String revision;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }
}
