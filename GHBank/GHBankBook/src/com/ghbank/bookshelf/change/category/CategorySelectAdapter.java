package com.ghbank.bookshelf.change.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.CategoryModel;
import com.ghbank.bookshelf.utils.Utils;

import group.aim.framework.basemodel.BaseArrayList;

/**
 * Created by narztiizzer on 12/15/2016 AD.
 */

public class CategorySelectAdapter extends RecyclerView.Adapter<CategorySelectAdapter.CategoryViewHolder> {

    private BaseArrayList mCategoryItem = BaseArrayList.Builder(CategoryModel.class);
    private Context mContext;
    private BaseArrayList baseList;

    private boolean isParentCategoryShow;
    private boolean isChildCategoryShow;

    public CategorySelectAdapter(Context mContext, BaseArrayList mCategoryItem) {
        this.mContext = mContext;
        this.mCategoryItem = mCategoryItem;
        baseList = mCategoryItem;
        isParentCategoryShow = false;
        isChildCategoryShow = false;
    }

    @Override
    public CategorySelectAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(this.mContext).inflate(R.layout.new_book_shelf_select_category_child, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategorySelectAdapter.CategoryViewHolder holder, final int position) {

        final CategoryModel rootCategory = (CategoryModel) this.mCategoryItem.get(position);
        holder.categoryTitle.setText(Utils.decodeUnicode(rootCategory.getName()));
        holder.categoryTitle.setTag(rootCategory);
        // holder.categoryTitle.setTextColor(ThemeResourceManager.getFontColor(this.mContext));
        // holder.categoryTitle.setTextColor(Color.BLACK);
        Glide.with(this.mContext).load(rootCategory.getImage().replace("\\", ""))
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(this.mContext, R.drawable.custom_progress_loading))
                .into(holder.categoryIcon);

        holder.categoryIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isChildCategoryShow){
                    isParentCategoryShow = false;
                    mCategoryItem.clear();
                    mCategoryItem = baseList;
                    notifyDataSetChanged();
                    ((CategorySelectActivity) mContext).sendObjectBack(rootCategory);
                }else {
                    if (isParentCategoryShow) {
                        BaseArrayList childList = rootCategory.getSubcategory();
                        if (childList != null && childList.size() > 0) {
                            mCategoryItem.clear();
                            mCategoryItem = childList;
                            notifyDataSetChanged();
                            isChildCategoryShow = true;
                        } else {
                            isParentCategoryShow = false;
                            mCategoryItem.clear();
                            mCategoryItem = childList;
                            notifyDataSetChanged();
                            ((CategorySelectActivity) mContext).sendObjectBack(rootCategory);
                        }
                    } else {
                        BaseArrayList parentList = rootCategory.getSubcategory();
                        if (parentList != null && parentList.size() > 0) {
                            mCategoryItem.clear();
                            mCategoryItem = parentList;
                            notifyDataSetChanged();
                            isParentCategoryShow = true;
                        } else {
                            isParentCategoryShow = false;
                            mCategoryItem.clear();
                            mCategoryItem = baseList;
                            notifyDataSetChanged();
                            ((CategorySelectActivity) mContext).sendObjectBack(rootCategory);
                        }
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.mCategoryItem.size();
    }

    protected class CategoryViewHolder extends RecyclerView.ViewHolder {
        public ImageView categoryIcon;
        public TextView categoryTitle;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            this.categoryIcon = (ImageView) itemView.findViewById(R.id.new_book_shelf_select_category_child__thumbnail);
            this.categoryTitle = (TextView) itemView.findViewById(R.id.new_book_shelf_select_category_child__title);
        }
    }
}
