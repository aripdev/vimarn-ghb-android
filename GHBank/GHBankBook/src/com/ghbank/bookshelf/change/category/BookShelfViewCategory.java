package com.ghbank.bookshelf.change.category;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.activity.SearchActivity;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.loaded.LoadedBookMainFragment;
import com.ghbank.bookshelf.change.model.BookModel;
import com.ghbank.bookshelf.change.model.CategoryModel;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.Utils;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.ConfigurationJSONModelParams;
import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.enumeration.UpdatePolicy;
import group.aim.framework.helper.ScreenSizeHelper;

/**
 * Created by narztiizzer on 12/14/2016 AD.
 */

public class BookShelfViewCategory extends FragmentActivity implements RequestURLListener {

    private static final String TAG = BookShelfViewCategory.class.getSimpleName();

    private static final String REQUEST_CATEGORY_ITEM_TAG = "REQUEST_CATEGORY_ITEM_TAG";
    private static final int REQUEST_CATEGORY_INTENT_FROM_SELF = 9990;

    private ImageView categoryIcon;
    private TextView categoryTitle;
    private RecyclerView categoryGrid;
    private int categoryId = 0;
    private BookShelfCategoryAdapter bookAdapter;
    private BaseArrayList<BookModel> mItemList = BaseArrayList.Builder(BookModel.class);
    private ImageView navLeftButton , navLogo , navButtonFirst , navButtonSecond , navButtonThird;
    private CategoryModel selectCategory;

    public static BookShelfViewCategory newInstance() {
        return new BookShelfViewCategory();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NLog.w(TAG,"onCreate");
        this.setContentView(R.layout.new_book_shelf_view_category);
        this.initialView();
        this.initialValue();
        this.setupView();
        this.setNavigationButtonOnClick();
    }


    private void initialView() {

        this.categoryIcon = (ImageView) findViewById(R.id.new_book_shelf_view_category__icon);
        this.categoryTitle = (TextView) findViewById(R.id.new_book_shelf_view_category__title);
        this.categoryGrid = (RecyclerView) findViewById(R.id.new_book_shelf_view_category__grid);
        this.navButtonFirst = (ImageView) findViewById(R.id.new_book_shelf_view_category__nav_bar_button_right_first);
        this.navButtonSecond = (ImageView) findViewById(R.id.new_book_shelf_view_category__nav_bar_button_right_second);
        this.navButtonThird = (ImageView) findViewById(R.id.new_book_shelf_view_category__nav_bar_button_right_third);
        this.navLeftButton = (ImageView) findViewById(R.id.new_book_shelf_view_category__nav_bar_button_left);
        this.navLogo = (ImageView) findViewById(R.id.new_book_shelf_view_category__nav_bar_logo);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this , 2);
        this.categoryGrid.setLayoutManager(gridLayoutManager);

        this.navButtonFirst.setVisibility(ThemeResourceManager.isShowHelpIcon(this) ? View.VISIBLE : View.GONE);
        this.navButtonSecond.setVisibility(ThemeResourceManager.isShowCategoryIcon(this) ? View.VISIBLE : View.GONE);
    }

    private void setupView() {
        Drawable searchIcon = ThemeResourceManager.getSearchIcon();
        Drawable myBookIcon = ThemeResourceManager.getMyBookIcon();
        Drawable categoryIcon = ThemeResourceManager.getCategoryIcon();
        Drawable backIcon = ThemeResourceManager.getBackIcon();
        Drawable logoIcon = ThemeResourceManager.getLogoIcon();

        Drawable bg = ThemeResourceManager.getSmallIconCategoryBackground();
        if(bg != null){
            CardView bgView = (CardView)findViewById(R.id.new_book_shelf_view_category__frame);
            bgView.setBackground(bg);
        }
        bg = null;

        if(searchIcon != null)
            this.navButtonFirst.setImageDrawable(searchIcon);

        if(categoryIcon != null)
            this.navButtonSecond.setImageDrawable(categoryIcon);

        if(myBookIcon != null)
            this.navButtonThird.setImageDrawable(myBookIcon);

        if(logoIcon != null)
            this.navLogo.setImageDrawable(logoIcon);

        if(backIcon != null)
            this.navLeftButton.setImageDrawable(backIcon);

        Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
        if(headerBackground != null){
            ImageView headerView = (ImageView)findViewById(R.id.headerBackground);
            headerView.setImageDrawable(headerBackground);
        }
    }

    private void setNavigationButtonOnClick() {
        this.navLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        this.navButtonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent categorySelectIntent = new Intent(BookShelfViewCategory.this , BkShelfTutorial.class);
//                startActivity(categorySelectIntent);
                Intent categorySelectIntent = new Intent(BookShelfViewCategory.this , SearchActivity.class);
                startActivity(categorySelectIntent);

            }
        });

        this.navButtonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int centerOfButton = (navButtonFirst.getWidth() + navButtonSecond.getWidth() + navButtonThird.getWidth()) / 2;
                int paddingFromLeft = ScreenSizeHelper.getScreenWidth() - centerOfButton;

                Intent categorySelectIntent = new Intent(BookShelfViewCategory.this , CategorySelectActivity.class);
                categorySelectIntent.putExtra("POINT" , paddingFromLeft);

                startActivityForResult(categorySelectIntent , REQUEST_CATEGORY_INTENT_FROM_SELF);
            }
        });

        this.navButtonThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent librarySelectIntent = new Intent(BookShelfViewCategory.this , LoadedBookMainFragment.class);
                startActivity(librarySelectIntent);
            }
        });
    }

    private void initialValue() {
        Bundle argumentBundle = getIntent().getExtras();
        if(argumentBundle != null) {
            this.selectCategory = (CategoryModel) argumentBundle.getSerializable("RESULT");
            this.categoryId = (int) this.selectCategory.getId();
            this.categoryTitle.setText(Utils.decodeUnicode(this.selectCategory.getName()));

            Glide.with(this).load(this.selectCategory.getImage().replace("\\" , ""))
                    .dontAnimate().placeholder(DrawableAnimation.createFromResource(this , R.drawable.custom_progress_loading))
                    .into(this.categoryIcon);
        }

       // this.categoryTitle.setTextColor(ThemeResourceManager.getFontColor(this));
        this.requestItemCategory(this.categoryId);
    }

    private void requestItemCategory(int categoryId) {

        SharedPreferences preferences = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE);

        String username = preferences.getString(SnPreferenceVariable.USER_NAME, "");
        String group = preferences.getString(SnPreferenceVariable.GROUP, "");

        RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                .addParameters("t" , 1)
                .addParameters("test" , 1)
                .addParameters("type" , 5)
                .addParameters("cat_id" , categoryId)
                .addParameters("username" , username)
                .addParameters("pgroup" , group)
                .addParameters("canvas" , 1)
                .addParameters("library" , UtConfig.LIBRARY_CODE)
                .setRequestTag(REQUEST_CATEGORY_ITEM_TAG)
                .setRequestURLListener(this)
                .start();

        NLog.w(TAG,"t : " + 1);
        NLog.w(TAG,"test : " + 1);
        NLog.w(TAG,"type : " + 5);
        NLog.w(TAG,"cat_id : " + categoryId);
        NLog.w(TAG,"username : " + username);
        NLog.w(TAG,"pgroup : " + group);
        NLog.w(TAG,"canvas : " + 1);
        NLog.w(TAG,"library : " + UtConfig.LIBRARY_CODE);

    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {
        if(mItemList != null){
            mItemList.clear();
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    categoryGrid.setAdapter(null);
                }
            });

        }

        if(code == 200) {
            ConfigurationJSONModelParams.getInstance().setEntriesFieldName("data");

            if(responseString.contains("\"data\""))
                this.mItemList.updateFromJson(responseString.replace("\"\"" , "null").replace("null" , "\"null\""), UpdatePolicy.ForceUpdate);

            this.bookAdapter = new BookShelfCategoryAdapter(this , this.mItemList);

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    categoryGrid.setAdapter(bookAdapter);
                }
            });
        }
    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CATEGORY_INTENT_FROM_SELF && resultCode == Activity.RESULT_OK) {

            this.selectCategory = (CategoryModel) data.getExtras().getSerializable("RESULT");
            this.categoryId = (int) this.selectCategory.getId();
            this.categoryTitle.setText(Utils.decodeUnicode(this.selectCategory.getName()));

            Glide.with(this).load(this.selectCategory.getImage().replace("\\" , ""))
                    .dontAnimate().placeholder(DrawableAnimation.createFromResource(this , R.drawable.custom_progress_loading))
                    .into(this.categoryIcon);

            this.requestItemCategory(this.categoryId);
        }
    }
}
