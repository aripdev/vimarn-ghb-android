package com.ghbank.bookshelf.change.category;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.instance.ApplicationInstance;
import com.ghbank.bookshelf.change.model.CategoryModel;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.ConfigurationJSONModelParams;
import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.enumeration.UpdatePolicy;

/**
 * Created by narztiizzer on 12/14/2016 AD.
 */

public class CategorySelectActivity extends Activity implements RequestURLListener {

    private static final String TAG = CategorySelectActivity.class.getSimpleName();

    private int paddingFromLeft;
    private ImageView pointerIcon, cardBackground;
    private LinearLayout linearContainer;
    private RecyclerView categoryGrid;
    private FrameLayout cardContainer;

    private BaseArrayList list;

    private static final String REQUEST_CATEGORY_TAG = "REQUEST_CATEGORY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.new_book_shelf_category_select);

        this.initialValue();
        this.initialView();
        this.setAction();

        this.requestCategory();

//        if (ApplicationInstance.getInstance().getCategoryInstance() == null) {
//            this.requestCategory();
//        } else {
//            this.setCategory(list);
//        }

        // Create category 3 level
    }

    private void initialView() {
        this.linearContainer = (LinearLayout) findViewById(R.id.new_book_shelf_category_select__container);
        this.cardContainer = (FrameLayout) findViewById(R.id.cardContainer);
        this.categoryGrid = (RecyclerView) findViewById(R.id.new_book_shelf_category_select__grid);
        this.cardBackground = (ImageView) findViewById(R.id.cardBackground);
        this.pointerIcon = (ImageView) findViewById(R.id.new_book_shelf_category_select__pointer);
        this.pointerIcon.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                pointerIcon.setPadding(paddingFromLeft - (pointerIcon.getWidth() / 2), 0, 0, 0);
                pointerIcon.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        Drawable pointerDrawable = ThemeResourceManager.getPointerTriangleIcon();
        if (pointerDrawable != null)
            this.pointerIcon.setImageDrawable(pointerDrawable);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), UtConfig.DESIGN_PATH_THEME + UtDesign.SMALL_ICON_7);
        roundedBitmapDrawable.setCornerRadius(16f);
        this.cardContainer.setBackground(roundedBitmapDrawable);

        Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
        if (headerBackground != null) {
            ImageView headerView = (ImageView) findViewById(R.id.headerBackground);
            headerView.setImageDrawable(headerBackground);
        }
    }

    private void setAction() {
        linearContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initialValue() {
        Bundle extraBundle = getIntent().getExtras();
        if (extraBundle != null) {
            this.paddingFromLeft = extraBundle.getInt("POINT");
        }
        list = ApplicationInstance.getInstance().getCategoryInstance();
    }

    protected void sendObjectBack(CategoryModel model) {
        Intent intent = new Intent();
        intent.putExtra("RESULT", model);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void requestCategory() {
        SharedPreferences preferences = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE);

        String username = preferences.getString(SnPreferenceVariable.USER_NAME, "");
        String group = preferences.getString(SnPreferenceVariable.GROUP, "");

        String t = "0";
        String test = "1";
        String type = "1";
        String library = UtConfig.LIBRARY_CODE;

        NLog.w("CategorySelectedActivity", "t : " + t);
        NLog.w("CategorySelectedActivity", "test : " + test);
        NLog.w("CategorySelectedActivity", "type : " + type);
        NLog.w("CategorySelectedActivity", "library : " + library);
        NLog.w("CategorySelectedActivity", "group : " + group);
        NLog.w("CategorySelectedActivity", "username : " + username);

        RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                .addParameters("t", t)
                .addParameters("test", test)
                .addParameters("login", username)
                .addParameters("pgroup", group)
                .addParameters("type", type)
                .addParameters("library", library)
                .setRequestTag(REQUEST_CATEGORY_TAG)
                .setRequestURLListener(this)
                .start();

    }

    private void setCategory(BaseArrayList list) {
        int gridColumn = 2; //ApplicationInstance.getInstance().getCategoryInstance().size() > 4 ? 3 : 2;
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, gridColumn);
        final CategorySelectAdapter categorySelectAdapter = new CategorySelectAdapter(this, list);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                categoryGrid.setLayoutManager(gridLayoutManager);
                categoryGrid.setAdapter(categorySelectAdapter);
            }
        });
    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {

        NLog.w("CategorySelectedActivity", "response : " + responseString);

        BaseArrayList<CategoryModel> mCategoryList = BaseArrayList.Builder(CategoryModel.class);
        if (code == 200) {

            ConfigurationJSONModelParams.getInstance().setEntriesFieldName("data");
            //mCategoryList.updateFromJson(responseString.replace("\\", ""), UpdatePolicy.ForceUpdate);
            mCategoryList.updateFromJson(responseString, UpdatePolicy.ForceUpdate);

            ApplicationInstance.getInstance().setCategoryInstance(mCategoryList);
            if(list != null){
                list.clear();
            }
            list = ApplicationInstance.getInstance().getCategoryInstance();
            this.setCategory(list);

        }

    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {

    }


}
