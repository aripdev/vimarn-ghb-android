package com.ghbank.bookshelf.change.category;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import group.aim.framework.basemodel.BaseArrayList;
import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.BookModel;

/**
 * Created by narztiizzer on 12/14/2016 AD.
 */

public class BookShelfCategoryAdapter extends RecyclerView.Adapter<BookShelfCategoryAdapter.BookViewHolder> {

    private BaseArrayList mItemList = BaseArrayList.Builder(BookModel.class);
    private Context mContext;

    public BookShelfCategoryAdapter() {

    }

    public BookShelfCategoryAdapter(Context context , BaseArrayList mItemList) {
        this.mItemList = mItemList;
        this.mContext = context;
    }

    @Override
    public BookShelfCategoryAdapter.BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_book_shelf_category_child , parent , false);
        return new BookViewHolder(childView);
    }

    @Override
    public void onBindViewHolder(BookShelfCategoryAdapter.BookViewHolder holder, int position) {
        BookModel currentBook = (BookModel) this.mItemList.get(position);
        holder.container.setTag(currentBook);
        Glide.with(this.mContext).load(currentBook.getImage().replace("\\" , ""))
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(this.mContext , R.drawable.custom_progress_loading))
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return this.mItemList.size();
    }

    protected class BookViewHolder extends RecyclerView.ViewHolder  {

        public ImageView thumbnail;
        public FrameLayout container;

        public BookViewHolder(View view) {
            super(view);
            this.thumbnail = (ImageView) view.findViewById(R.id.new_book_shelf_category_child__thumbnail);
            this.container = (FrameLayout) view.findViewById(R.id.new_book_shelf_category_child__container);
            this.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BookModel currentBook = (BookModel) v.getTag();
                    Log.d("TAG" , "ID : " + currentBook.getId());
                    Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
                    intent.putExtra("id_category", currentBook.getId() + "");
                    intent.putExtra("details", currentBook.getDescription());
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
