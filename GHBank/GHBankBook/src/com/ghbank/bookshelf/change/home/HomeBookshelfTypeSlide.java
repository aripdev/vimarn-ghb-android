package com.ghbank.bookshelf.change.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghbank.bookshelf.BkShelfEditProfile;
import com.ghbank.bookshelf.BkShelfLogIn;
import com.ghbank.bookshelf.BuildConfig;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.slideshow.SlideShowFragment;
import com.ghbank.bookshelf.change.tabs.TabFragment;
import com.ghbank.bookshelf.session.SnPreferenceVariable;

/**
 * Created by narztiizzer on 11/18/2016 AD.
 */

public class HomeBookshelfTypeSlide extends Fragment {

    private TextView versionNumber;
    private int onClickCounter = 0;
    private boolean isCounting = false;

    public static HomeBookshelfTypeSlide newInstance() { return new HomeBookshelfTypeSlide(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_book_shelf_type_slide, container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);
        this.initialValue();
        this.initialAction();
        this.setSlideShowView();
        this.setTabView();
    }

    private void initialView(View view) {
        this.versionNumber = (TextView) view.findViewById(R.id.versionNumber);
    }

    private void initialAction() {
        this.versionNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isCounting) {
                    isCounting = true;
                    resetOnClickCounter();
                }

                onClickCounter++;

                if(onClickCounter == 2) {
                    final SharedPreferences sPref = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, Context.MODE_PRIVATE );
                    if(sPref.getBoolean(SnPreferenceVariable.IS_USER_LOGIN,false)){
                        Intent librarySelectIntent = new Intent(getActivity(), BkShelfEditProfile.class);
                        startActivity(librarySelectIntent);
                    }else {
                        Intent librarySelectIntent = new Intent(getActivity(), BkShelfLogIn.class);
                        startActivity(librarySelectIntent);
                    }
                }
            }
        });
    }

    private void resetOnClickCounter() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onClickCounter = 0;
                isCounting = false;
            }
        } , 1500);
    }

    private void initialValue() {
        String versionName = "V" + BuildConfig.VERSION_NAME;
        this.versionNumber.setText(versionName);
    }

    private void setSlideShowView() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.new_main_book_shelf__slideshow , SlideShowFragment.newInstance());
        transaction.commit();
    }

    private void setTabView() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.new_main_book_shelf__tabs , TabFragment.newInstance());
        transaction.commit();
    }
}
