package com.ghbank.bookshelf.change.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import group.aim.framework.basemodel.BaseArrayList;

/**
 * Created by narztiizzer on 11/30/2016 AD.
 */

public class HomeBookShelfTypeShelfViewPagerAdapter extends FragmentPagerAdapter {

    private BaseArrayList<Fragment> pageList = BaseArrayList.Builder(Fragment.class);

    public HomeBookShelfTypeShelfViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public HomeBookShelfTypeShelfViewPagerAdapter(FragmentManager fm , BaseArrayList pageList) {
        super(fm);
        this.pageList = pageList;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) this.pageList.get(position);
    }

    @Override
    public int getCount() {
        return this.pageList.size();
    }
}
