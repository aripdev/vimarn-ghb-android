package com.ghbank.bookshelf.change.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.helper.ScreenSizeHelper;
import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.BookModel;

/**
 * Created by narztiizzer on 11/30/2016 AD.
 */

public class HomeBookShelfTypeShelfChild extends Fragment {


    private LinearLayout mShelfBookFirstRow , mShelfBookSecondRow , mShelfBookThirdRow;
    private LinearLayout.LayoutParams mDefaultParams;
    private int paddingLeftRight;
    private int paddingTopBottom;

    public static HomeBookShelfTypeShelfChild newInstance() { return new HomeBookShelfTypeShelfChild(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_book_shelf_type_shelf_child_page , container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);
        this.initialValue();
        this.setContent();
    }

    private void initialView(View view) {
        this.mShelfBookFirstRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_1);
        this.mShelfBookSecondRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_2);
        this.mShelfBookThirdRow = (LinearLayout) view.findViewById(R.id.home_book_shelf_type_shelf__shelf_row_3);

        this.mDefaultParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT , 1f);
        this.mDefaultParams.gravity = Gravity.BOTTOM;
    }

    private void initialValue() {
        this.paddingLeftRight = (int) ScreenSizeHelper.metrics.density * 4;
        this.paddingTopBottom = (int) ScreenSizeHelper.metrics.density * (ScreenSizeHelper.isTabletSize(getActivity()) ? 56 : 32);
    }

    private void setContent() {
        Bundle argumentBundle = getArguments();

        if(argumentBundle == null || argumentBundle.isEmpty())
            return;

        BaseArrayList itemList = (BaseArrayList) argumentBundle.getSerializable("ITEM");
        for(int itemIndex = 0; itemIndex < itemList.size() ; itemIndex++) {
            int row = itemIndex < 3 ? 1 : (itemIndex < 6 ? 2 : 3);
            final BookModel currentBook = (BookModel) itemList.get(itemIndex);
            ImageView imageBook = createImageView();
            Glide.with(getActivity()).load(currentBook.getImage().replace("\\" , "")).dontAnimate()
                    .placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                    .into(imageBook);
            imageBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
                    intent.putExtra("id_category", currentBook.getId() + "");
                    intent.putExtra("details", currentBook.getDescription());
                    startActivity(intent);
                }
            });

            switch (row) {
                case 1 : this.mShelfBookFirstRow.addView(imageBook);
                    break;
                case 2 : this.mShelfBookSecondRow.addView(imageBook);
                    break;
                case 3 : this.mShelfBookThirdRow.addView(imageBook);
                    break;

                default : Log.d("ROW ITEM" , "ROW FOR ITEM NOT MATCH");
            }

            if((itemIndex + 1) == itemList.size()) {
                LinearLayout selectRow = null;
                switch (row) {
                    case 1 : selectRow = this.mShelfBookFirstRow;
                        break;
                    case 2 : selectRow = mShelfBookSecondRow;
                        break;
                    case 3 : selectRow = mShelfBookThirdRow;
                        break;

                    default : Log.d("ROW ITEM" , "ROW FOR ITEM NOT MATCH");
                }

                if(selectRow != null)
                    checkChildOfRow(selectRow);
            }
        }
    }

    private void checkChildOfRow(LinearLayout row)  {
        int lostChild = 3 - row.getChildCount();
        if(lostChild > 0) {
            for(int iLost = 0 ; iLost < lostChild ; iLost++) {
                ImageView imageBook = createImageView();
                row.addView(imageBook);
            }
        }
    }

    private ImageView createImageView() {
        ImageView imageView = new ImageView(getActivity());
        imageView.setLayoutParams(this.mDefaultParams);
        imageView.setAdjustViewBounds(true);
        imageView.setPadding(paddingLeftRight , paddingLeftRight * 2 , paddingLeftRight , paddingTopBottom);
        return imageView;
    }
}
