package com.ghbank.bookshelf.change.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghbank.bookshelf.BkShelfEditProfile;
import com.ghbank.bookshelf.BkShelfLogIn;
import com.ghbank.bookshelf.BuildConfig;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.DrawableAnimation;
import com.ghbank.bookshelf.change.model.BookModel;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;

import group.aim.framework.basemodel.BaseArrayList;
import group.aim.framework.basemodel.ConfigurationJSONModelParams;
import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.enumeration.UpdatePolicy;
import group.aim.framework.helper.ScreenSizeHelper;

/**
 * Created by narztiizzer on 11/29/2016 AD.
 */

public class HomeBookshelfTypeShelf extends Fragment implements RequestURLListener {

    private ViewPager mBookPager;
    private String REQUEST_BOOK_LIST_TAG = "ALL BOOK";
    private BaseArrayList<BookModel> mBookList = BaseArrayList.Builder(BookModel.class);
    private BaseArrayList<Fragment> pagerPageList = BaseArrayList.Builder(Fragment.class);
    private ImageView mBackgroundFirstRow , mBackgroundSecondRow , mBackgroundThirdRow;
    private int paddingLeftRight , paddingTopBottom;
    private TextView versionNumber;
    private int onClickCounter = 0;
    private boolean isCounting = false;

    public static HomeBookshelfTypeShelf newInstance() { return  new HomeBookshelfTypeShelf(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_book_shelf_type_shelf, container , false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initialView(view);
        this.initialValue();
        this.initialAction();
        this.requestBookList();
    }

    private void initialView(View view) {
        this.mBookPager = (ViewPager) view.findViewById(R.id.home_book_shelf_type_shelf__pager);
        this.mBackgroundFirstRow = (ImageView) view.findViewById(R.id.home_book_shelf_type_shelf__bg_row_1);
        this.mBackgroundSecondRow = (ImageView) view.findViewById(R.id.home_book_shelf_type_shelf__bg_row_2);
        this.mBackgroundThirdRow = (ImageView) view.findViewById(R.id.home_book_shelf_type_shelf__bg_row_3);
        this.versionNumber = (TextView) view.findViewById(R.id.versionNumber);
    }

    private void requestBookList() {
        String username = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, getActivity().MODE_PRIVATE ).getString(SnPreferenceVariable.USER_NAME , "");
        String group = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, getActivity().MODE_PRIVATE ).getString(SnPreferenceVariable.GROUP , "");

        if(username == null)
            username = "";

        if(group == null)
            group = "";

        RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                .addParameters("t" , 1)
                .addParameters("test" , 1)
                .addParameters("type" , 5)
                .addParameters("cat_id" , 0)
                .addParameters("username" , username)
                .addParameters("pgroup" , group)
                .addParameters("canvas" , 1)
                .addParameters("library" , UtConfig.LIBRARY_CODE)
                .setRequestTag(REQUEST_BOOK_LIST_TAG)
                .setRequestURLListener(this)
                .start();
    }

    private void initialValue() {
        Glide.with(getActivity()).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                .into(this.mBackgroundFirstRow);
        Glide.with(getActivity()).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                .into(this.mBackgroundSecondRow);
        Glide.with(getActivity()).load(R.drawable.shelf_1_3)
                .dontAnimate().placeholder(DrawableAnimation.createFromResource(getActivity() , R.drawable.custom_progress_loading))
                .into(this.mBackgroundThirdRow);

        if(ScreenSizeHelper.isTabletSize(getActivity())) {
            this.paddingLeftRight = (int) ScreenSizeHelper.metrics.density * 16;
            this.paddingTopBottom = (int) ScreenSizeHelper.metrics.density * 8;

            this.mBookPager.setPadding(this.paddingLeftRight , 0 , this.paddingLeftRight , 0);
        }

        String versionName = "V" + BuildConfig.VERSION_NAME;
        this.versionNumber.setText(versionName);

        Log.d("SET SHELF" , "SET SHELF BAR");
    }

    private void initialAction() {
        this.versionNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isCounting) {
                    isCounting = true;
                    resetOnClickCounter();
                }

                onClickCounter++;

                if(onClickCounter == 2) {
                    final SharedPreferences sPref = getActivity().getSharedPreferences(SnPreferenceVariable.PF_ARIP, Context.MODE_PRIVATE );
                    if(sPref.getBoolean(SnPreferenceVariable.IS_USER_LOGIN,false)){
                        Intent librarySelectIntent = new Intent(getActivity(), BkShelfEditProfile.class);
                        startActivity(librarySelectIntent);
                    }else {
                        Intent librarySelectIntent = new Intent(getActivity(), BkShelfLogIn.class);
                        startActivity(librarySelectIntent);
                    }
                }
            }
        });
    }

    private void resetOnClickCounter() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onClickCounter = 0;
                isCounting = false;
            }
        } , 1500);
    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {
        ConfigurationJSONModelParams.getInstance().setEntriesFieldName("data");

        if(responseString.contains("\"data\""))
            this.mBookList.updateFromJson(responseString.replace("\"\"" , "null") , UpdatePolicy.ForceUpdate);

         int totalPage = this.mBookList.size() / 9;

        if(totalPage == 0 || this.mBookList.size() % (totalPage * 9) > 0)
            totalPage++;

        for(int pageIndex = 0 ; pageIndex < totalPage ; pageIndex++) {
            Bundle argumentBundle = new Bundle();
            BaseArrayList<BookModel> itemForPage = BaseArrayList.Builder(BookModel.class);
            int itemPerPage = this.mBookList.size() < 9 ? this.mBookList.size() : 9;

            for(int itemIndex = 0 ; itemIndex < itemPerPage ; itemIndex++) {
                BookModel currentBook = (BookModel) this.mBookList.get(itemIndex);
                if(currentBook.isShow())
                    itemForPage.add(currentBook);
            }
            this.mBookList.removeAll(itemForPage);

            argumentBundle.putSerializable("ITEM" , itemForPage);
            HomeBookShelfTypeShelfChild page = HomeBookShelfTypeShelfChild.newInstance();
            page.setArguments(argumentBundle);
            this.pagerPageList.add(page);
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBookPager.setAdapter(new HomeBookShelfTypeShelfViewPagerAdapter(getFragmentManager() , pagerPageList));
            }
        });


        Log.d("REQUEST BOOK" , "REQUEST ALL BOOK SUCCESS");
    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {
        Log.d("REQUEST BOOK" , "REQUEST ALL BOOK FAIL");
    }

}
