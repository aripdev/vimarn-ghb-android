package com.ghbank.bookshelf;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.ArrayList;

import com.ghbank.bookshelf.loader.LdBkShelfDesignLoader;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;


public class BkShelfSplashScreen extends Activity {
//	PackageInfo info;
//    Handler handler;
//    private long timeDelay = 2000; //2 seconds
	private static final String TAG = BkShelfSplashScreen.class.getSimpleName();
	private static final int REQUEST_MULTIPLE_PERMISSION_CODE = 9955;

	private EditText edtUsername;
    private EditText edtPassword;
    
	private Handler handler;
	private long timeDelay = 1000; //1 seconds
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_splash);
        final ActionBar actionBar = getActionBar();
        actionBar.hide();
        
        edtUsername = (EditText) findViewById(R.id.edt_username);
		edtPassword = (EditText) findViewById(R.id.edt_password);
        
		edtUsername.clearFocus();
		edtPassword.clearFocus();
		
		edtUsername.postDelayed(new Runnable() {

	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            InputMethodManager keyboard = (InputMethodManager)
	            getSystemService(Context.INPUT_METHOD_SERVICE);
	            keyboard.hideSoftInputFromWindow( edtUsername.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
	        }
	    },0);
		edtPassword.postDelayed(new Runnable() {
	
		        @Override
		        public void run() {
		            // TODO Auto-generated method stub
		            InputMethodManager keyboard = (InputMethodManager)
		            getSystemService(Context.INPUT_METHOD_SERVICE);
		            keyboard.hideSoftInputFromWindow( edtPassword.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
		        }
		    },0);
	
		UtConfig.SERVER_CONFIG = UtSharePreferences.getPrefServerConfig(getApplicationContext());

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			checkPermission();
		else
			new LdBkShelfDesignLoader(this).SetDesignLoader();

		NLog.d(TAG,UtSharePreferences.getPrefServerConfig(this));

    }
    
    private void startBookshelf(){
		handler = new Handler(); 
        handler.postDelayed(new Runnable() { 
             public void run() { 
            	 	Intent  i = new Intent(BkShelfSplashScreen.this, BookshelfStartActivity.class);
            	 	startActivity(i); 
            		finish();
            		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
             } 
        }, timeDelay); 
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		boolean isAllowAll = true;

		if(requestCode == REQUEST_MULTIPLE_PERMISSION_CODE) {
			int permissionIndex = 0;
			while(permissionIndex < grantResults.length && isAllowAll) {
				if(grantResults[permissionIndex] != PackageManager.PERMISSION_GRANTED) {
					isAllowAll = false;
				}
				permissionIndex++;
			}

			if(isAllowAll)
				new LdBkShelfDesignLoader(this).SetDesignLoader();
			else
				finish();
		}
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	private void checkPermission() {
		ArrayList<String> permission = new ArrayList<>();

		if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
			permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

		if(ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED);
			permission.add(Manifest.permission.INTERNET);

		if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED);
			permission.add(Manifest.permission.ACCESS_FINE_LOCATION);


		if(permission.size() > 0)
			ActivityCompat.requestPermissions(this, permission.toArray(new String[permission.size()]) , REQUEST_MULTIPLE_PERMISSION_CODE);
		else
			new LdBkShelfDesignLoader(this).SetDesignLoader();
	}
}
