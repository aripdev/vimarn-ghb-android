package com.ghbank.bookshelf.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.ghbank.bookshelf.utils.UtConfig;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

public class DbBookHandler {
	private Context mContext;
	private AQuery aq;
	private SQLiteDatabase database;
	private DbSQLiteHandler sqliteHandler;
	private String[] allColumns = { 
			  DbSQLiteHandler.COLUMN_ID,
		      DbSQLiteHandler.COLUMN_TITLE ,
		      DbSQLiteHandler.COLUMN_DETAIL ,
		      DbSQLiteHandler.COLUMN_DATE ,
		      DbSQLiteHandler.COLUMN_CATEGORY ,
		      DbSQLiteHandler.COLUMN_IMAGE ,
		      DbSQLiteHandler.COLUMN_REVISION};

	
	public DbBookHandler(Context context){
		sqliteHandler = new DbSQLiteHandler(context);
		aq = new AQuery(context);
		mContext = context;
	}
	
	public void open(){
	
		database = sqliteHandler.getWritableDatabase();
	}
	
	public boolean isOpen(){
		boolean status =false;
		status = database.isOpen();
		return status;
	}
	public void close() {
		sqliteHandler.close();
		database.close();
	}
	
	public List<DbBook> getAllFavorite() {
	    List<DbBook> favorites = new ArrayList<DbBook>();

	    Cursor cursor = database.query(DbSQLiteHandler.TABLE_BOOK,
	        allColumns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	DbBook favorite = cursorToFavorite(cursor);
	    	favorites.add(favorite);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    close();
	    return favorites;
	  }
	
	public void updateBook(String id,String revision){
		ContentValues args = new ContentValues();
		args.put(DbSQLiteHandler.COLUMN_REVISION, revision);
		database.update(DbSQLiteHandler.TABLE_BOOK, args, DbSQLiteHandler.COLUMN_ID + "=" + id, null);
	}
	
	public List<DbBook> sort(int type)
	{
		List<DbBook> favorites = new ArrayList<DbBook>();
		Cursor cursor = null;
		switch (type) {
			case 1:	cursor = database.query(DbSQLiteHandler.TABLE_BOOK, allColumns, null, null, null, null, DbSQLiteHandler.COLUMN_TITLE);
				break;
	
			default: cursor = database.query(DbSQLiteHandler.TABLE_BOOK, allColumns, null, null, null, null, null);
				break;
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
	    	DbBook favorite = cursorToFavorite(cursor);
	    	favorites.add(favorite);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    close();
	    return favorites;
	}
	
	public List<DbBook> search(String text,int type)
	{
		String selection = DbSQLiteHandler.COLUMN_TITLE+ " LIKE '"+text+"%'";
		
		List<DbBook> searchBook = new ArrayList<DbBook>();
		Cursor cursor = null;
		switch (type) {
			case 1:	cursor = database.query(DbSQLiteHandler.TABLE_BOOK, allColumns, selection, null, null, null, DbSQLiteHandler.COLUMN_TITLE);
				break;
	
			default: cursor = database.query(DbSQLiteHandler.TABLE_BOOK, allColumns, selection, null, null, null, null);
				break;
		}
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
	    	DbBook searchDB = cursorToFavorite(cursor);
	    	searchBook.add(searchDB);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    close();
	    return searchBook;
	}
	
	public Boolean check(String id){
	    List<DbBook> favorites = new ArrayList<DbBook>();
		String[] column = {DbSQLiteHandler.COLUMN_ID};
	    Cursor cursor = database.query(DbSQLiteHandler.TABLE_BOOK,
	    		column, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	DbBook favorite = cursorToFavoriteCheck(cursor);
	    	favorites.add(favorite);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
	    for (int i = 0; i < favorites.size(); i++) {
			if (favorites.get(i).getId().equals(id)) {
				return false;
			}
		}
	    return true;
	}
	
	public String getPDF(String id){
	    List<DbBook> favorites = new ArrayList<DbBook>();
	    Cursor cursor = database.query(DbSQLiteHandler.TABLE_BOOK,
	    		allColumns, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	DbBook favorite = cursorToFavorite(cursor);
	    	favorites.add(favorite);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
	    for (int i = 0; i < favorites.size(); i++) {
			if (favorites.get(i).getId().equals(id)) {
				return favorites.get(i).getCategory();			
			}
		}
		return null;
	}
	
	public void addFavorite(final String id, final String title, String detail, String date, String category, String image, String revision){
		final ContentValues values = new ContentValues();
		
		Log.e("DbBookHandler", "ID:"+id+" & Title:"+title);
		
		values.put(DbSQLiteHandler.COLUMN_ID, id);
		values.put(DbSQLiteHandler.COLUMN_TITLE, title);
		values.put(DbSQLiteHandler.COLUMN_DETAIL, detail);
		values.put(DbSQLiteHandler.COLUMN_DATE, date);
		values.put(DbSQLiteHandler.COLUMN_CATEGORY, category);
		values.put(DbSQLiteHandler.COLUMN_REVISION, revision);
		if( id == null || id.isEmpty())					Log.e(DbSQLiteHandler.COLUMN_ID, id);
		if( title == null || title.isEmpty())			Log.e(DbSQLiteHandler.COLUMN_TITLE, title);
		if( detail == null || detail.isEmpty())			Log.e(DbSQLiteHandler.COLUMN_DETAIL, detail);
		if( date == null || date.isEmpty())				Log.e(DbSQLiteHandler.COLUMN_DATE, date);
		if( category == null || category.isEmpty())		Log.e(DbSQLiteHandler.COLUMN_CATEGORY, category);
		if( revision == null || revision.isEmpty())		Log.e(DbSQLiteHandler.COLUMN_REVISION, revision);
		
		final ProgressDialog pd = ProgressDialog.show(mContext, null, "Saving");
		File ext = Environment.getExternalStorageDirectory();
		File target = new File(ext, "Android/data/" + mContext.getPackageName() + "/cache/" + id + "/" + "cover.jpg");
		Log.d("DOWNLOAD COVER", "DOWNLOAD ON ADD FAVOURITE METHOD : " + image);

		aq.download(image, target, new AjaxCallback<File>(){
		        public void callback(String url, File file, AjaxStatus status) {
		                pd.dismiss();
		                if(file != null){
		            			values.put(DbSQLiteHandler.COLUMN_IMAGE, file.getAbsolutePath());
		            				database.insert(DbSQLiteHandler.TABLE_BOOK, null, values);
			                        Toast.makeText(mContext, "Download " + title + " success.", Toast.LENGTH_LONG).show();
		                }
		        }
		        
		});
	}
	
	
	public void addFav(final String id,String title, String detail, String date, String category, String image, String revision){
	//public void addFav(final String id,String title, String revision){
		final ContentValues values = new ContentValues();
		
		Log.e("DbBookHandler", "ID:"+id+" & Title:"+title);
		
		values.put(DbSQLiteHandler.COLUMN_ID, id);
		values.put(DbSQLiteHandler.COLUMN_TITLE, title);
		values.put(DbSQLiteHandler.COLUMN_DETAIL, detail);
		values.put(DbSQLiteHandler.COLUMN_DATE, date);
		values.put(DbSQLiteHandler.COLUMN_CATEGORY, category);
		values.put(DbSQLiteHandler.COLUMN_REVISION, revision);
		
		if( id == null || id.isEmpty())					Log.e(DbSQLiteHandler.COLUMN_ID, id);
		if( title == null || title.isEmpty())			Log.e(DbSQLiteHandler.COLUMN_TITLE, title);
		if( detail == null || detail.isEmpty())			Log.e(DbSQLiteHandler.COLUMN_DETAIL, detail);
		if( date == null || date.isEmpty())				Log.e(DbSQLiteHandler.COLUMN_DATE, date);
		if( category == null || category.isEmpty())		Log.e(DbSQLiteHandler.COLUMN_CATEGORY, category);
		if( revision == null || revision.isEmpty())		Log.e(DbSQLiteHandler.COLUMN_REVISION, revision);
		
		//final ProgressDialog pd = ProgressDialog.show(mContext, null, "Saving");
		@SuppressWarnings("unused")
		File ext = Environment.getExternalStorageDirectory();
		File cover = new File( UtConfig.DESIGN_PATH+id+"/cover.jpg");
		//File target = new File(ext, "Android/data/"+mContext.getPackageName()+"/cache/"+String.valueOf(cover.hashCode()));     
		
	
		values.put(DbSQLiteHandler.COLUMN_IMAGE, cover.getAbsolutePath());
		database.insert(DbSQLiteHandler.TABLE_BOOK, null, values);
		
		/*
		aq.download(image, target, new AjaxCallback<File>(){
		        
		        public void callback(String url, File file, AjaxStatus status) {
		                pd.dismiss();
		                if(file != null){
		            			values.put(DbSQLiteHandler.COLUMN_IMAGE, file.getAbsolutePath());
		            				database.insert(DbSQLiteHandler.TABLE_BOOK, null, values);
			                        Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();         			
		                }
		        }
		        
		});*/
	}
	
	public void deleteComment(String id) {
	    //System.out.println("Comment deleted with id: " + id);
	    database.delete(DbSQLiteHandler.TABLE_BOOK, DbSQLiteHandler.COLUMN_ID
	        + " = " + id, null);
	  }
	
	private DbBook cursorToFavoriteCheck(Cursor cursor){
	    DbBook favorite = new DbBook();
	    favorite.setId(cursor.getString(0));
	    return favorite;
	}
	
	
	 private DbBook cursorToFavorite(Cursor cursor) {
		    DbBook favorite = new DbBook();
		    favorite.setId(cursor.getString(0));
		    favorite.setTitle(cursor.getString(1));
		    favorite.setDetail(cursor.getString(2));
		    favorite.setDate(cursor.getString(3));
		    favorite.setCategory(cursor.getString(4));
		    favorite.setImage(cursor.getString(5));
		    favorite.setRevision(cursor.getString(6));
		    return favorite;
		  }
}
