package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.service.SvForgotPassword;
import com.ghbank.bookshelf.utils.UtSharePreferences;

public class BkShelfForgotPassword extends FragmentActivity {

	 private AQuery aq;
	 private LinearLayout layForgotPassword;
	 private TextView txtForgotPassword;
	 private TextView txtForgotSubmit;
	 private EditText edtEmail;
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_forgot_password);
		
		final ActionBar actionBar = getActionBar();
	    actionBar.hide();
	        
	    aq = new AQuery(this);
	    layForgotPassword = (LinearLayout) findViewById(R.id.lay_forgot_password);
	    txtForgotPassword = (TextView) findViewById(R.id.txt_forgot_password);
	    txtForgotSubmit = (TextView) findViewById(R.id.txt_forgot_newpassword);
		edtEmail = (EditText) findViewById(R.id.edt_forgot_email);

		Drawable bg = ThemeResourceManager.getBackgroundFromType(3);
		if(bg != null){
			layForgotPassword.setBackground(bg);
		}
//		 File fBg = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1);
//         if (fBg.exists()) {
//
//        	 if (Build.VERSION.SDK_INT >= 16) {
//
//        		 layForgotPassword.setBackground(new BitmapDrawable(getResources(),
//              			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1)));
//
//         	} else {
//         		 layForgotPassword.setBackgroundDrawable(new BitmapDrawable(getResources(),
//              			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1)));
//         	}
//         }
         
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		txtForgotSubmit.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_newpassword)+"</u></body></html>"));
		
		txtForgotSubmit.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		txtForgotPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtEmail.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		
		txtForgotSubmit.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		txtForgotPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtEmail.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		
		txtForgotSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(edtEmail!=null && edtEmail.length()>0){
					new SvForgotPassword().reset(BkShelfForgotPassword.this, aq, edtEmail);
				}else{
					Toast.makeText(BkShelfForgotPassword.this, "Please completed all fields", Toast.LENGTH_LONG).show();
				}
			}
		});
    }
}
