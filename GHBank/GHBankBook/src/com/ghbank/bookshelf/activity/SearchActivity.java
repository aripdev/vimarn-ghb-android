package com.ghbank.bookshelf.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import group.aim.framework.connection.RequestHttpURL;
import group.aim.framework.connection.interfaces.RequestURLListener;
import group.aim.framework.helper.ScreenSizeHelper;
import com.ghbank.bookshelf.BkShelfTutorial;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.adapter.NoItemAdapter;
import com.ghbank.bookshelf.adapter.SearchAdapter;
import com.ghbank.bookshelf.change.category.CategorySelectActivity;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.loaded.LoadedBookMainFragment;
import com.ghbank.bookshelf.models.Book;
import com.ghbank.bookshelf.models.CreateModel;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.ItemDecoration;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.Utils;

/**
 * Created by androiddev on 11/01/2017.
 */

public class SearchActivity extends AppCompatActivity implements RequestURLListener,
        View.OnClickListener {

    private ProgressWheel progressWheel;
    private RecyclerView recyclerView;

    ImageView btnMenuIconHelp;
    ImageView btnMenuIconCategory;
    ImageView btnMenuIconMyBook;
    EditText textInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        viewMatching();

        Utils.hideKeyboard(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearch:
                String keyWord = textInput.getText().toString();
                startSearch(keyWord);
                break;
            case R.id.btnMenuIconBack:
                    finish();
                break;
            case R.id.btnMenuIconFirst:
                Intent categorySelectIntent = new Intent(SearchActivity.this , BkShelfTutorial.class);
                startActivity(categorySelectIntent);
                break;
            case R.id.btnMenuIconSecond:
                int centerOfButton = (btnMenuIconHelp.getWidth() + btnMenuIconCategory.getWidth() + btnMenuIconMyBook.getWidth()) / 2;
                int paddingFromLeft = ScreenSizeHelper.getScreenWidth() - centerOfButton;
                Intent intent = new Intent(SearchActivity.this , CategorySelectActivity.class);
                intent.putExtra("POINT" , paddingFromLeft);
                startActivity(intent);

                break;
            case R.id.btnMenuIconThird:
                Intent librarySelectIntent = new Intent(SearchActivity.this , LoadedBookMainFragment.class);
                startActivity(librarySelectIntent);
                break;
            default:
                break;
        }
    }

    private void setAdapter(List<Book> list) {
        Utils.hideKeyboard(SearchActivity.this);
        if (list == null || list.size() == 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new NoItemAdapter(SearchActivity.this,getResources().getString(R.string.not_found_search_result)));
            return;
        }
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(new SearchAdapter(SearchActivity.this, list));
    }

    private void viewMatching() {
        int space = (int)getResources().getDimension(R.dimen.spacing_x_small);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new ItemDecoration(space));
        recyclerView.setAdapter(new NoItemAdapter(SearchActivity.this,getResources().getString(R.string.search)));

        textInput = (EditText) findViewById(R.id.textSearchInput);

        progressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        progressWheel.stopSpinning();

        Drawable bg = ThemeResourceManager.getBackgroundFromType(3);
        if(bg != null){
            LinearLayout vbg = (LinearLayout)findViewById(R.id.searchBackground);
            vbg.setBackground(bg);
        }

        findViewById(R.id.btnSearch).setOnClickListener(this);

        Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
        if(headerBackground != null){
            ImageView headerView = (ImageView)findViewById(R.id.imgTabMenuBackground);
            headerView.setImageDrawable(headerBackground);
        }

        Drawable backIcon = ThemeResourceManager.getBackIcon();
        ImageView btnBack = (ImageView)findViewById(R.id.btnMenuIconBack);
        if(backIcon != null){
            btnBack.setImageDrawable(backIcon);
        }
        btnBack.setOnClickListener(this);

        Drawable logoIcon = ThemeResourceManager.getLogoIcon();
        if(logoIcon != null){
            ImageView logo = (ImageView)findViewById(R.id.img_nav_bar_logo);
            logo.setImageDrawable(logoIcon);
        }

        Drawable searchIcon = ThemeResourceManager.getSearchIcon();
        if(searchIcon != null){
            ImageView btnSearch = (ImageView)findViewById(R.id.btnSearch);
            btnSearch.setImageDrawable(searchIcon);
        }

        Drawable helpIcon = ThemeResourceManager.getHelpIcon();
        Drawable myBookIcon = ThemeResourceManager.getMyBookIcon();
        Drawable categoryIcon = ThemeResourceManager.getCategoryIcon();

         btnMenuIconHelp = (ImageView)findViewById(R.id.btnMenuIconFirst);
         btnMenuIconCategory = (ImageView)findViewById(R.id.btnMenuIconSecond);
         btnMenuIconMyBook = (ImageView)findViewById(R.id.btnMenuIconThird);

        btnMenuIconHelp.setOnClickListener(this);
        btnMenuIconCategory.setOnClickListener(this);
        btnMenuIconMyBook.setOnClickListener(this);

        if(helpIcon != null){
            btnMenuIconHelp.setImageDrawable(helpIcon);
        }
        if(myBookIcon != null){
            btnMenuIconMyBook.setImageDrawable(myBookIcon);
        }
        if(categoryIcon != null){
            btnMenuIconCategory.setImageDrawable(categoryIcon);
        }

        textInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    startSearch(v.getText().toString());
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onRequestURLSuccess(int code, String tag, String responseString) {

        final List<Book> list = CreateModel.books(responseString);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressWheel.stopSpinning();
                setAdapter(list);
            }
        });

    }

    @Override
    public void onRequestURLFailed(int code, String tag, String exception) {
        progressWheel.stopSpinning();
    }

    private void startSearch(String keyWord) {
        if (keyWord.length() == 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new NoItemAdapter(SearchActivity.this,getResources().getString(R.string.search)));
            Toast.makeText(this, getResources().getString(R.string.please_enter_search_keyword), Toast.LENGTH_SHORT).show();
            return;
        }

        progressWheel.spin();
        SharedPreferences preferences = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE);

        String username = preferences.getString(SnPreferenceVariable.USER_NAME, "");
        String group = preferences.getString(SnPreferenceVariable.GROUP, "");

        String t = "1";
        String test = "1";
        String type = "6";
        String library = UtConfig.LIBRARY_CODE;

        NLog.w("SearchActivity", "t : " + t);
        NLog.w("SearchActivity", "test : " + test);
        NLog.w("SearchActivity", "type : " + type);
        NLog.w("SearchActivity", "library : " + library);
        NLog.w("SearchActivity", "group : " + group);
        NLog.w("SearchActivity", "username : " + username);
        NLog.w("SearchActivity", "q : " + keyWord);

        RequestHttpURL.to(UtConfig.SERVER_DEFAULT)
                .addParameters("t", t)
                .addParameters("test", test)
                .addParameters("username", username)
                .addParameters("q", keyWord)
                .addParameters("pgroup", group)
                .addParameters("type", type)
                .addParameters("library", library)
                .setRequestTag("Search")
                .setRequestURLListener(this)
                .start();

    }

}
