package com.ghbank.bookshelf.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by androiddev on 11/01/2017.
 */

public class Book implements Parcelable {

    public String id;
    public String name;
    public String imageUrl;
    public ArrayList<String> images;
    public String description;
    public String status;

    public Book(String id,String name,String imageUrl, String description,
                String status, ArrayList<String> images){
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.description = description;
        this.status = status;
        this.images = images;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.imageUrl);
        dest.writeStringList(this.images);
        dest.writeString(this.description);
        dest.writeString(this.status);
    }

    protected Book(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.imageUrl = in.readString();
        this.images = in.createStringArrayList();
        this.description = in.readString();
        this.status = in.readString();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
}
