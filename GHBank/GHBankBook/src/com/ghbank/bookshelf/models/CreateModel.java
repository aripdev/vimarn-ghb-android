package com.ghbank.bookshelf.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.ghbank.bookshelf.utils.NLog;

/**
 * Created by androiddev on 11/01/2017.
 */

public class CreateModel {

    private static final String TAG = CreateModel.class.getSimpleName();

    public static List<Book> books(String jsonString){
        if (jsonString == null) {
            return null;
        }
        NLog.w(TAG,jsonString);
        List<Book> list = null;
        try{
            JSONObject object = new JSONObject(jsonString);
            if (object.getString("status").equals("1")){
                list = new ArrayList<>();
                JSONArray array = object.getJSONArray("data");

                for (int i=0;i<array.length();i++){
                    JSONObject info = array.getJSONObject(i);
                    String id = info.getString("id");
                    String name = info.getString("name");
                    String des = info.getString("description");
                    String image = info.getString("image");
                    String status = info.getString("status");

                    ArrayList<String> imageList = new ArrayList<>();
                    JSONArray images = info.getJSONArray("image_new");
                    for (int j=0;j<imageList.size();j++){
                        JSONObject imgObj = images.getJSONObject(j);
                        String img = imgObj.getString("url");
                        imageList.add(img);
                    }

                    list.add(new Book(id,name,image,des,status,imageList));
                }

            }
            NLog.d(TAG,"Create book list completed.");
        }catch (JSONException e){
            NLog.e(TAG,"Create book list error, " + e.getMessage());
        }
        return list;
    }
}
