package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ghbank.bookshelf.loader.LdBkShelfCategoryDetailPageLoader;


public class BkShelfByCategory extends FragmentActivity {

	private Intent i;
	@SuppressWarnings("unused")
	private LinearLayout layScreen;
	@SuppressWarnings("unused")
	private LinearLayout layBgCat;
	@SuppressWarnings("unused")
	private LinearLayout layBookCatDetail;
	@SuppressWarnings("unused")
	private ImageView imgTri;
	@SuppressWarnings("unused")
	private ImageView imgCategory;
	@SuppressWarnings("unused")
	private ImageView bBack;
	@SuppressWarnings("unused")
	private ImageView bCategory;
	@SuppressWarnings("unused")
	private ImageView bLibrary;	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_detail_category);
		
		i = getIntent();
		
		final ActionBar actionBar = getActionBar();
		actionBar.hide();
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
//		actionBar.setCustomView(R.layout.view_acb_logo_center);
		
		Initial();
		new LdBkShelfCategoryDetailPageLoader(this,i.getStringExtra("name_thumbnail_path"),
				i.getStringExtra("name_category")).SetCategoryDetailLoader(i.getStringExtra("id_category"));
	
//		bLibrary.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent i = new Intent(BkShelfByCategory.this,BkShelfLibrary.class);
//				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				startActivity(i);
//			}
//		});
//		
//		bCategory.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				BkShelf.categoryView = v;
//				new LdBkShelfPageLoader(BkShelfByCategory.this).QuickActionOnClick(v);
//			}
//		});
//
//		bBack.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				BackPressed();
//			}
//		});
	}
	
	private void Initial(){
		layScreen = (LinearLayout) findViewById(R.id.lay_detail);
		layBgCat = (LinearLayout) findViewById(R.id.lay_bg_category);
		layBookCatDetail = (LinearLayout) findViewById(R.id.lay_book_category_detail);
		imgTri = (ImageView) findViewById(R.id.img_category_detail);
		imgCategory = (ImageView) findViewById(R.id.img_category_thumbnail);
//		bBack  = (ImageView) findViewById(R.id.img_acb_back);
//		bCategory = (ImageView)findViewById(R.id.acb_category);
//		bLibrary = (ImageView) findViewById(R.id.acb_library);  
	}
	
	@SuppressWarnings("unused")
	private void BackPressed(){
		finish();
	}
	
}
