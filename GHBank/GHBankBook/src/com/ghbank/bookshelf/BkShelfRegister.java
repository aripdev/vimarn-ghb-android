package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.service.SvRegister;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;
import com.ghbank.bookshelf.utils.UtSharePreferences;

import java.io.File;

public class BkShelfRegister extends FragmentActivity {

	private String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	private LinearLayout layRegister;
	private ImageView imgProfile;
	private TextView txtHeaderRegister;
	private TextView btnSubmit;
    private TextView btnCanel;
    private EditText edtEmail;
    private EditText edtName;
    private EditText edtPassword;
    private EditText edtConfirmPassword;
    
    private AQuery aq;
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_register);
		
		final ActionBar actionBar = getActionBar();
	    actionBar.hide(); 
	    
	    aq = new AQuery(this);
	    layRegister = (LinearLayout) findViewById(R.id.lay_register);
	    imgProfile = (ImageView) findViewById(R.id.img_header_category);
	    txtHeaderRegister = (TextView) findViewById(R.id.txt_header_register);
	    btnSubmit = (TextView) findViewById(R.id.txt_submit_register);
	    btnCanel = (TextView) findViewById(R.id.txt_submit_cancel);
		edtEmail = (EditText) findViewById(R.id.edt_email);
		edtName = (EditText) findViewById(R.id.edt_name);
		edtPassword = (EditText) findViewById(R.id.edt_password);
		edtConfirmPassword = (EditText) findViewById(R.id.edit_confirm_password);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		Drawable bg = ThemeResourceManager.getBackgroundFromType(3);
		if(bg != null){
			layRegister.setBackground(bg);
		}
//
//		 File fBg = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1);
//         if (fBg.exists()) {
//        	if (Build.VERSION.SDK_INT >= 16) {
//
//        		 layRegister.setBackground(new BitmapDrawable(getResources(),
//             			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1)));
//
//         	} else {
//         		 layRegister.setBackgroundDrawable(new BitmapDrawable(getResources(),
//             			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1)));
//         	}
//
//         }
         
         File fIcon1 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1);
         if (fIcon1.exists()) {
        	 imgProfile.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1));
         }
		
		
		btnSubmit.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_register)+"</u></body></html>"));
		btnCanel.setText(Html.fromHtml("<html><body><u>"+getString(R.string.txt_edit_cancel)+"</u></body></html>"));
		
		txtHeaderRegister.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		btnSubmit.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	    btnCanel.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtEmail.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtName.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtConfirmPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		
		txtHeaderRegister.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		btnSubmit.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
	    btnCanel.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtEmail.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtName.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		edtConfirmPassword.setTextColor(Color.parseColor(UtSharePreferences.getPrefStringColor(this)));
		
		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if((edtEmail!=null && edtEmail.length()>0) && 
						(edtName!=null && edtName.length()>0) && 
						(edtPassword!=null  && edtPassword.length()>0) &&
						(edtConfirmPassword!=null  && edtConfirmPassword.length()>0)){
				
					boolean isComplete = true;
					String msgError = "";
					
					if(!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())){
						msgError = "Your password doesn't match with comfirm password";
						isComplete = false;
					}
	                if (edtEmail.getText().toString().matches(emailreg) == false) {
	                	msgError = "Address is Invalid";
	                	isComplete = false;
	                }
	                 
	                if(isComplete){
	                	new SvRegister().register(BkShelfRegister.this, aq, edtEmail, edtPassword, edtName);
	                }else{
	                	Toast.makeText(BkShelfRegister.this, msgError, Toast.LENGTH_LONG).show();
	                }
				}else{
					Toast.makeText(BkShelfRegister.this, "Please completed all fields", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		btnCanel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
    }
}
