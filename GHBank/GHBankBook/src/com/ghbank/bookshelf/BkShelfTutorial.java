package com.ghbank.bookshelf;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import com.ghbank.bookshelf.adapter.TutorialAdapter;
import com.ghbank.bookshelf.cache.Cache;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;


public class BkShelfTutorial extends FragmentActivity {

    private static final String TAG = BkShelfTutorial.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_base_tutorial);
        NLog.w(TAG, "onCreate");

        getImageList();

    }

    private void getImageList() {
        String strTutorial = new Cache(this).getStringCache(Cache.STR_HELP);
        ArrayList<String> urlList = new ArrayList<>();
        try {
            final JSONArray array = new JSONArray(strTutorial);
            for (int i = 0; i < array.length(); i++) {
                urlList.add(UtConfig.HELP_PATH + "help" + (i + 1) + ".png");
            }
        } catch (JSONException e) {
            NLog.e(TAG, "Get tutorial image url error.");
        }
        setAdapter(urlList);
    }

    private void setAdapter(ArrayList<String> list) {

        if (list != null && list.size() > 0) {
            ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(new TutorialAdapter(this, list));
            mViewPager.setCurrentItem(0);
        } else {
            NLog.w(TAG, "List is no item.");
        }
    }


}
