package com.ghbank.bookshelf.cache;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by androiddev on 19/01/2017.
 */

public class Cache {

    private static final String CACHE_NAME = "com.arip.it.cache";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public static final String STR_HELP = "str_help";

    public Cache (Context context){
        preferences = context.getSharedPreferences(CACHE_NAME,Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setStringCache(String result){
        editor.putString(STR_HELP,result).commit();
    }

    public String getStringCache(String cacheName){
        return preferences.getString(cacheName,"");
    }

    public void deleteCache(String cacheName){
        editor.remove(cacheName).commit();
    }
    public void deleteAllCache(String cacheName){
        editor.clear().commit();
    }

}
