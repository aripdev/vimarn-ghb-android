package com.ghbank.bookshelf.thread;

import android.os.Handler;
import android.support.v4.view.ViewPager;

import com.ghbank.bookshelf.adapter.AdtBannerAdapter;
import com.ghbank.bookshelf.utils.UtConfig;


public class BannerThread extends Thread {
	private ViewPager viewPager;
	private Handler handler;
	private AdtBannerAdapter bannerAdapter;
	
	public BannerThread(ViewPager banner,AdtBannerAdapter adapter) {
		viewPager = banner;
		handler = new Handler();
		bannerAdapter = adapter;
	}
	
    @Override
    public void run() {
    	while(true) {
            try {
				sleep(UtConfig.AD_DURATION);
				if (viewPager.getChildCount()>0) {
					if (viewPager.getCurrentItem()==bannerAdapter.getCount()-1) {
						handler.post(new Runnable() {
							
							@Override
							public void run() {
								viewPager.setCurrentItem(0);
								
							}
						});
					}
					else{
						handler.post(new Runnable() {
							
							@Override
							public void run() {		 
								viewPager.setCurrentItem(viewPager.getCurrentItem()+1);				    

							}
						});
					}
				}
				
			} catch (InterruptedException e) {
				
			}
          }
    }
}
