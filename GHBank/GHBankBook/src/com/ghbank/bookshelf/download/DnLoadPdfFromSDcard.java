package com.ghbank.bookshelf.download;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.arip.it.library.core.model.ContentItem;
import com.arip.it.library.core.model.GalleryItem;
import com.arip.it.library.core.model.MediaItem;
import com.arip.it.library.core.model.PdfRawItem;
import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ghbank.bookshelf.BkShelfLibrary;
import com.ghbank.bookshelf.db.DbBook;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.loader.LdBkShelfPreviewPageLoader;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtConvertRegion;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.utils.UtUtilities;

public class DnLoadPdfFromSDcard extends LdBkShelfPreviewPageLoader {

	private static final int DOWNLOAD_FILE = 1;
	private static final int DOWNLOAD_CARTOON = 2;
	private static final int DOWNLOAD_MEDIA = 3;
	private static final int DOWNLOAD_SYNC = 4;
	private static final int DOWNLOAD_GALLERY = 5;

	private static final int TYPE_VIDEO = 1;
	private static final int TYPE_AUDIO = 2;
	private static final int TYPE_IMAGE = 3;
	private static final int TYPE_LINK = 4;
	private static final int TYPE_PAGE = 5;
	private static final int TYPE_GALLERY = 6;
	private static final int TYPE_TOOLTIP = 7;
	private static final int TYPE_EXAM = 8;

	private int statusDownloadCurrent = 1;

	private Activity ac;
	private ProgressDialog pb;
	private AQuery aq;
	private ArrayList<PdfRawItem> pdfRaw;
	private ArrayList<MediaItem> mediaRaw;
	private ArrayList<GalleryItem> galleryRaw;
	private DbBookHandler bh;



	private String DB_PATH;
	private String DB_PATH_ZIP;
	private int processCount = 0;
	private int processTotal = 0;
	private String cover,rootPath,revision;
	private boolean pdfCheck,mediaCheck,galleryCheck,imageCheck,analyzeCheck = false;
	public static DownloadFile asynDownload;
	public static DownloadMediaFile asynDownloadMedia;
	public static DownloadGalleryFile asynDownloadGallery;
	public static AnalyzeFile asynSynce;
	public static String idRef,name,summary,datetime,totalpage;
	public String typeTooltip="",TooltipContent="";
	public int TotalPage;

	//Add on 06 Dec. 2016
	private static final String PDF_CANVAS_PARAMETER = "canvas";

	public DnLoadPdfFromSDcard (Activity activity ,String id,String zipName){
		super(activity);

		Log.e("DnLoadPdfFromSDcard","ID:"+id+"  ZIP:"+zipName);
		ac = activity;
//		pb = (ProgressBar)ac.findViewById(R.id.pgb_download);
		aq = new AQuery(ac);
		DB_PATH = UtConfig.DESIGN_PATH+id+"/";
		DB_PATH_ZIP = UtConfig.DESIGN_PATH_ZIP+zipName+"/"+id+"/";
		idRef = id;
		Log.e("DnLoad ", "BookPath:"+DB_PATH+"  &&  ZipPath:"+DB_PATH_ZIP);
		bh = new DbBookHandler(ac);
	}

	private String getHtmlMediaPath(String path){
		String resultPaht="";
		try{
			resultPaht = path.split("/")[0]+"/"+ UtConvertRegion.ConvertToHTML(path.split("/")[1]);
		}catch(Exception e){
			e.printStackTrace();
		}
		return resultPaht;
	}

	public String GetRev(){
		return revision;
	}

	public String GetName(){
		return name;
	}


	public void downloadLinkById(String id){

		Log.e("DnLoadPdf","downloadLinkById");
		try{
			File yourFile = new File(DB_PATH_ZIP, "book_json.json");
			FileInputStream stream = new FileInputStream(yourFile);
			String jsonStr = null;
			try {
				FileChannel fc = stream.getChannel();
				MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

				jsonStr = Charset.defaultCharset().decode(bb).toString();
			}
			finally {
				stream.close();
			}

			JSONObject json = new JSONObject(jsonStr);

			if (json != null) {


				if (json.getString("status").equals("1")) {
					name = json.getString("title");
							/*
							if(UtDevice.getHeight()>=1920){
           						cover = json.getJSONArray("coverpage_new").getJSONObject(2).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
           					}else if(UtDevice.getHeight()>=1280){
           						cover = json.getJSONArray("coverpage_new").getJSONObject(1).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
           					}else if(UtDevice.getHeight()<1280){
           						cover = json.getJSONArray("coverpage_new").getJSONObject(0).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
           					}*/


					summary = "zip_book";
					datetime = json.getString("created_date");
					totalpage = json.getString("pages");
					TotalPage = Integer.parseInt(totalpage);

					revision = json.getString("re_vision");
					//Set process count default = 0
					processTotal = 0;
					//if( json.getString("format_type").equalsIgnoreCase("Book") ){
					pdfRaw = new ArrayList<PdfRawItem>();
					mediaRaw = new ArrayList<MediaItem>();
					galleryRaw = new ArrayList<GalleryItem>();

					ArrayList<MediaItem> mediaArray = null;
					JSONArray canvasArray = json.getJSONArray(PDF_CANVAS_PARAMETER);
					int pdfPageTotal = Integer.parseInt(json.getString("pages"));

					for(int pdfCount = 0; pdfCount < pdfPageTotal ; pdfCount++){
						rootPath = DB_PATH_ZIP;
						pdfCheck = true;

						if (!json.getJSONArray("page").getJSONObject(pdfCount).isNull(PDF_CANVAS_PARAMETER)) {

							int totalMedia = json.getJSONArray("page").getJSONObject(pdfCount)
									.getJSONArray("media").length();
							mediaArray = new ArrayList<MediaItem>();
							for (int mediaCount = 0; mediaCount < totalMedia; mediaCount++) {

								// Set JSON type gallery
								if (json.getJSONArray("page").getJSONObject(pdfCount).getJSONArray("media")
										.getJSONObject(mediaCount).optJSONObject("path") != null) {

									int totalSide = Integer
											.parseInt(json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getJSONObject("path").getString("total_slide"));

									for (int side = 0; side < totalSide; side++) {

										mediaArray.add(new MediaItem(
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getString("page_id"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getInt("x"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getInt("y"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getInt("width"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getInt("height"),
												"",
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getJSONObject("path").getJSONArray("slide")
														.getJSONObject(side).getString("name"),
												!json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.isNull("imgplay")
														? json.getJSONArray("page")
														.getJSONObject(pdfCount)
														.getJSONArray("media")
														.getJSONObject(mediaCount)
														.getString("imgplay")
														: "",
												!json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.isNull("imgpause")
														? json.getJSONArray("page")
														.getJSONObject(pdfCount)
														.getJSONArray("media")
														.getJSONObject(mediaCount)
														.getString("imgpause")
														: "",
												// "","",
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getString("type"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getString("effect"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getString("option"),
												String.valueOf(json.getJSONArray("page")
														.getJSONObject(pdfCount).getJSONArray("media")
														.getJSONObject(mediaCount).getJSONObject("path")
														.getJSONArray("slide").length()),
														null // Media Property
												 ));

										galleryRaw.add(new GalleryItem(
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getString("page_id"),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getJSONObject("path").getJSONArray("slide")
														.getJSONObject(side).getString("title"),
												getHtmlMediaPath(json.getJSONArray("page")
														.getJSONObject(pdfCount).getJSONArray("media")
														.getJSONObject(mediaCount).getJSONObject("path")
														.getJSONArray("slide").getJSONObject(side)
														.getString("path")),
												json.getJSONArray("page").getJSONObject(pdfCount)
														.getJSONArray("media").getJSONObject(mediaCount)
														.getJSONObject("path").getJSONArray("slide")
														.getJSONObject(side).getString("name")));
									}

									galleryCheck = true;// end
									// gallery
								} else {
									mediaArray.add(new MediaItem(
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("page_id"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("x"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("y"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("width"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("height"),
											getHtmlMediaPath(json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("path")),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("name"),
											// "","",
											!json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.isNull("imgplay") ? json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("imgplay")
													: "",
											!json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.isNull("imgpause") ? json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("imgpause")
													: "",
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("type"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("effect"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("option"),
											"1",
											null // Media Property
											));

									mediaRaw.add(new MediaItem(
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("page_id"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("x"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("y"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("width"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getInt("height"),
											getHtmlMediaPath(json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("path")),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("name"),
											!json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.isNull("imgplay") ? json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("imgplay")
													: "",
											!json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.isNull("imgpause") ? json.getJSONArray("page")
													.getJSONObject(pdfCount).getJSONArray("media")
													.getJSONObject(mediaCount).getString("imgpause")
													: "",
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("type"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("effect"),
											json.getJSONArray("page").getJSONObject(pdfCount)
													.getJSONArray("media").getJSONObject(mediaCount)
													.getString("option"),
											"1",
											null // Media Property
											));
								}
							}
							mediaCheck = true;
						}else{
							mediaArray = null;
						}

						PdfRawItem pdfr = new PdfRawItem(
								json.getJSONArray("page").getJSONObject(pdfCount).getString("page_ID"),
								json.getJSONArray("page").getJSONObject(pdfCount).getString("page_name"),
								rootPath,
								json.getString("root_link"),
								json.getJSONArray("page").getJSONObject(pdfCount).getString("multiplier"),
								mediaArray);
						pdfRaw.add(pdfr);
					}

					analyzeCheck = true;

					if(pdfCheck){
						processTotal++;
					}
					if(mediaCheck){
						processTotal++;
					}
					if(imageCheck){
						processTotal++;
					}
					if(galleryCheck){
						processTotal++;
					}
					if(analyzeCheck){
						processTotal++;
					}

					if(!json.isNull("content")){
						int totalContent = json.getJSONArray("content").length();
						ArrayList<ContentItem> dContent = new ArrayList<ContentItem>();
						for(int contentCount = 0; contentCount<totalContent; contentCount++){

							dContent.add(new ContentItem(json.getJSONArray("content").getJSONObject(contentCount).getString("name"),
									json.getJSONArray("content").getJSONObject(contentCount).optInt("jumpto")));
						}


						if(dContent!=null && dContent.size()>0){
							writeContentFile(dContent);

						}
					}

					//////////////////////////////////////////////////

					try{
						File f = new File(DB_PATH);
						if (!f.exists()) {
							f.mkdirs();
						}

						//////////////////
					                /*
					                String BookTemp =DB_PATH+"temp.pdf";
					                String BookPath = DB_PATH+"book.pdf";
					                String pagePDFPath = DB_PATH_ZIP+"pdf/";
					                
					                
					                File BK = new File(BookTemp);
					                if(!BK.exists()){
					                	
					                	InputStream in = new FileInputStream(pagePDFPath+"page001.pdf");
					                    OutputStream out = new FileOutputStream(BookTemp);

					                    // Transfer bytes from in to out
					                    byte[] buf = new byte[1024];
					                    int len;
					                    while ((len = in.read(buf)) > 0) {
					                        out.write(buf, 0, len);
					                    }
					                    in.close();
					                    out.close();
					                	
					                }
					                
					                PdfReader reader = new PdfReader(BookTemp);
					                PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(BookPath));
					                
					                int count=1;
					                int ten=TotalPage/10;
					                int hun=TotalPage/100;
					                Log.e("Chompoo.DnLoadPdfFromSD", "total:"+TotalPage+" && hun:"+hun+" ten:"+ten);
					                
					                for(int a=0;a<=hun;a++){
					                	for(int b=0;b<=ten;b++){
					                		for(int c=0;c<=9;c++)
					                		{
					                			count++;
					                			if(a==0 && b==0 && c==0)c=2;
					                			Log.e("Chompoo.DnLoadPdfFromSD", "a:"+a+" b:"+b+" c:"+c+"  count:"+count+"/total:"+TotalPage);
					                			
					                			
					                			String aString = Integer.toString(a);
				                				String bString = Integer.toString(b);
				                				String cString = Integer.toString(c);
				                				
				                				String BookElement = pagePDFPath+"page"+aString+bString+cString+".pdf";
				                				Log.d("Chompoo", "BookElement "+BookElement);
				                				PdfReader nextPage = new PdfReader(BookElement);
				                				
				                				PdfImportedPage page = stamper.getImportedPage(nextPage, 1);
				                				stamper.insertPage(count, reader.getPageSize(1));
				                				stamper.getUnderContent(count).addTemplate(page,0,0);
					                			
					                			
					                			if(count==TotalPage)break;
					                		}
					                	}
					                }
					                stamper.setFullCompression();
					                stamper.close();
					                
					               
					                if(BK.exists()){
					                	
					                	BK.delete();
					                
					                }
					                */
						String coverString = DB_PATH_ZIP+"thumbnail"+"/normal/page001.jpg";
						File cov = new File(coverString);
						if(cov.exists()){

							InputStream in = new FileInputStream(coverString);
							OutputStream out = new FileOutputStream(DB_PATH+"cover.jpg");

							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = in.read(buf)) > 0) {
								out.write(buf, 0, len);
							}
							in.close();
							out.close();
						}

						Log.e("DnLoadPdf", "id:"+id+" name:"+name+" revision:"+revision);
						String coverPath = DB_PATH+"cover.jpg";
						String bookPath = DB_PATH+"book.pdf";
						bh.open();
						//bh.addFavorite(id, name, summary, datetime, DB_PATH+"book.pdf", cover, revision);
						bh.addFav(id, name,
								summary, datetime,
								bookPath, coverPath,
								revision);
						//	bh.addFav(id, name, revision);
						bh.updateBook(id, revision);




					}
					catch(Exception e){
						Log.e("Ch.DnLoadPdfFromSDcard", "in downloadlinkbyid", e);
					}
					/////////////////////////////////////////////////

					DnFile(pdfRaw);
							/*
								 if(mediaRaw!=null && mediaRaw.size()>0){
										DnMedia(mediaRaw);
									}
						    	 else if(galleryRaw!=null && galleryRaw.size()>0){
										DnGallery(galleryRaw);
									}
						    	else FileAnalyze(pdfRaw);
								*/
					///////////////////////////////////////////////
								
								/*
								if(mediaRaw!=null && mediaRaw.size()>0){
					        		asynDownloadMedia = new DownloadMediaFile();
					        		asynDownloadMedia.execute(mediaRaw);
					        		statusDownloadCurrent = DOWNLOAD_MEDIA;
//					        		new DownloadMediaFile().execute(mediaRaw);
					        	}else if(galleryRaw!=null && galleryRaw.size()>0){
					        		asynDownloadGallery= new DownloadGalleryFile();
					           		asynDownloadGallery.execute(galleryRaw);
					           		statusDownloadCurrent = DOWNLOAD_GALLERY;
					        	}else{
					        		asynSynce = new AnalyzeFile();
					        		asynSynce.execute(pdfRaw);
					        		statusDownloadCurrent = DOWNLOAD_SYNC;
//					        		new AnalyzeFile().execute(pdfRaw);
					        	}
								*/


					//////////////////////////////////////////////////
								/*
								asynDownload = new DownloadFile();
								asynDownload.execute(pdfRaw);
								statusDownloadCurrent = DOWNLOAD_FILE;*/

//								new DownloadFile().execute(pdfRaw);
					Log.d("Download", "Book");
					//}
				}
				else{
//							Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(aq.getContext(), "JSON :"+e.getMessage() , Toast.LENGTH_LONG).show();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			Toast.makeText(aq.getContext(), "FILE :"+e.getMessage() , Toast.LENGTH_LONG).show();
		}catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(aq.getContext(), "IO :"+e.getMessage() , Toast.LENGTH_LONG).show();
		}

	}




	public void clearStat(){
		processCount = 0;
		processTotal = 0;
		switch (statusDownloadCurrent) {
			case DOWNLOAD_FILE:
				if(asynDownload!=null)
					asynDownload.cancel(true);
				break;
			case DOWNLOAD_MEDIA:
				if(asynDownloadMedia!=null)
					asynDownloadMedia.cancel(true);
				break;
			case DOWNLOAD_SYNC:
				if(asynSynce!=null)
					asynSynce.cancel(true);
				break;
			case DOWNLOAD_GALLERY:
				if(asynDownloadGallery!=null)
					asynDownloadGallery.cancel(true);
				break;
			default:
				break;
		}
	}

	public void resetStatForDownload(String cat_id){

		clearStat();


		File f = new File(UtConfig.DESIGN_PATH+cat_id);
		if (f.isDirectory()) {
			String[] children = f.list();
			for (int i = 0; i < children.length; i++) {
				new File(f, children[i]).delete();
			}
		}

		Log.e("DnLoadPdf","resetStatForDownload");
		downloadLinkById(cat_id);
	}

	public class SetLoadSavedFavorite extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>{
		private DbBookHandler bookHandler;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			bookHandler = new DbBookHandler(aq.getContext());
			bookHandler.open();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			int type = 0;
			if( params[0].equals(BkShelfLibrary.TITLE))	type = 1;
			List<DbBook> data = bookHandler.sort(type);
			ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
			for (int i = 0; i < data.size(); i++) {
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("id", data.get(i).getId());
				hash.put("name", data.get(i).getTitle());
				hash.put("datetime", data.get(i).getDate());
				hash.put("description",data.get(i).getDetail());
				hash.put("category",data.get(i).getCategory());
				hash.put("image",data.get(i).getImage());
				hash.put("revision",data.get(i).getRevision());
				arrayList.add(hash);
			}

			return arrayList;
		}


		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
//				GridView grid =  (GridView)findViewById(R.id.grd_book_category_detail);      
//	   			grid.setExpanded(true);   
				/*
		        if (isNetworkConnected()) {
		        	ShowUpdatedLib(result);
				}else{
		    
			        adapter = new AdtLibraryAdapter(BkShelfLibrary.this, result, null);
			        if(adapter!=null && adapter.getCount()>0){
			        	grid.setAdapter(adapter);	
			        	adapter.notifyDataSetChanged();
			        }else{
			        	grid.setAdapter(null);
			        }
			        
			        fileGarbage();
				}*/
				
				/*
				BkShelfLibrary.adapter = new AdtLibraryAdapter(ac, result, null);
				BkShelfLibrary.grid.setAdapter(BkShelfLibrary.adapter);	
				BkShelfLibrary.adapter.notifyDataSetChanged();*/
		}
	}


	public void DnFile(ArrayList<PdfRawItem>... pdfRawItem){

		int current=0;
		int totalRow = 0;

		for( ArrayList<PdfRawItem> pdf : pdfRawItem){
			int rows = pdf.size();
			totalRow = pdf.size();
			while(current < rows)
			{

				try{
//			           
					String pdfPath = DB_PATH_ZIP+"pdf/"+pdf.get(current).getPdfName()+".pdf";
					Log.e("DnLoadPdfSD","pdfPath:"+pdfPath);
					InputStream input = new FileInputStream(pdfPath);

					// download the file
					File f = new File(DB_PATH);
					if (!f.exists()) {
						f.mkdirs();
					}
					// InputStream input = new BufferedInputStream(url.openStream(), 8192);
					//                OutputStream output = new FileOutputStream(DB_PATH+"book.pdf");
					OutputStream output = new FileOutputStream(DB_PATH+pdf.get(current).getPdfName()+".pdf");

					// Transfer bytes from in to out
					byte[] buf = new byte[1024];
					int len;
					while ((len = input.read(buf)) > 0) {
						output.write(buf, 0, len);
					}
					output.flush();
					output.close();
					input.close();

				}catch(Exception ex){
					//ex.printStackTrace();
				}
				current++;


			}


		}

		if(mediaRaw!=null && mediaRaw.size()>0){
			DnMedia(mediaRaw);
		}
		else if(galleryRaw!=null && galleryRaw.size()>0){
			DnGallery(galleryRaw);
		}
		else FileAnalyze(pdfRaw);


	}

	public void DnMedia(ArrayList<MediaItem>... mediaRawItem){
		int current=0;
		int totalRow = 0;

		//ArrayList<MediaItem> media;
		String error = null;
		for( ArrayList<MediaItem> media : mediaRawItem){
			int rows = media.size();
			totalRow = media.size();



			String pageIdCheck1 = null, nameResult = "";
			int nameRunning = 1;

			while(current < rows)
			{

				try{

					String mediaPath = DB_PATH_ZIP+"media/"+media.get(current).getNameMedia();
					File m = new File(mediaPath);
					if(m.exists()){

						Log.e("DnLoadPdfSD","pathMedia:"+mediaPath);
						InputStream input = new FileInputStream(mediaPath);

						// download the file
						File f = new File(DB_PATH+media.get(current).getPageId());
						if (!f.exists()) {
							f.mkdirs();
						}

						//InputStream input = new BufferedInputStream(url.openStream(), 8192);

						String convertName;
						int numberRunning = 1;


						boolean tooltip=false;
						boolean video=false;


						//////////////////////////////
						if(	media.get(current).getNameMedia().replace("_", "-").endsWith("jpg") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("png") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("bmp") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("JPG") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("PNG") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("BMP")) {

							convertName = media.get(current).getNameMedia().replace("_", "-")
									.replace(".jpg", "_"+numberRunning+".jpg")
									.replace(".png", "_"+numberRunning+".png")
									.replace(".bmp", "_"+numberRunning+".bmp")
									.replace(".JPG", "_"+numberRunning+".jpg")
									.replace(".PNG", "_"+numberRunning+".png")
									.replace(".BMP", "_"+numberRunning+".bmp").toLowerCase();


						}

						else if(media.get(current).getPathMedia().replace("_", "-").endsWith("jpg") ||
								media.get(current).getPathMedia().replace("_", "-").endsWith("png") ||
								media.get(current).getPathMedia().replace("_", "-").endsWith("bmp") ||
								media.get(current).getPathMedia().replace("_", "-").endsWith("JPG") ||
								media.get(current).getPathMedia().replace("_", "-").endsWith("PNG") ||
								media.get(current).getPathMedia().replace("_", "-").endsWith("BMP")  ) {

							convertName = media.get(current).getPathMedia().replace("_", "-")
									.replace(".jpg", "_"+numberRunning+".jpg")
									.replace(".png", "_"+numberRunning+".png")
									.replace(".bmp", "_"+numberRunning+".bmp")
									.replace(".JPG", "_"+numberRunning+".jpg")
									.replace(".PNG", "_"+numberRunning+".png")
									.replace(".BMP", "_"+numberRunning+".bmp").toLowerCase();
							tooltip=true;

							if(convertName.endsWith("jpg"))typeTooltip=".jpg";
							else if(convertName.endsWith("png"))typeTooltip=".png";
							else if(convertName.endsWith("bmp"))typeTooltip=".bmp";
							else typeTooltip=".xxx";
						}
						else if(media.get(current).getNameMedia().replace("_", "-").endsWith("mp4") ||
								media.get(current).getNameMedia().replace("_", "-").endsWith("MP4"))
						{
							convertName = media.get(current).getPathMedia().replace("_", "-")
									.replace(".mp4", "_"+numberRunning+".mp4")
									.replace(".MP4", "_"+numberRunning+".mp4")
									.toLowerCase();
							video=true;
						}

						else{
							convertName = media.get(current).getNameMedia().replace("_", "-").toLowerCase();

						}
						////////////////////////////////
       			           /*     if(media.get(current).getPathMedia().replace("_", "-").endsWith("jpg") ||
       			                		media.get(current).getPathMedia().replace("_", "-").endsWith("png") ||
       			                		media.get(current).getPathMedia().replace("_", "-").endsWith("bmp")) {
       			    			
       			                	convertName = media.get(current).getPathMedia().replace("_", "-")
       			                			.replace(".jpg", "_"+numberRunning+".jpg")
       			                			.replace(".png", "_"+numberRunning+".png")
       			                			.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();
       			    			}else{
       			    				convertName = media.get(current).getPathMedia().replace("_", "-").toLowerCase();
       			    			}*/
						////////////////////////////////
						String pageIdCheck2 = media.get(current).getPageId();

						if(pageIdCheck1!=null && pageIdCheck1.equals(pageIdCheck2)){
							nameResult = String.valueOf(nameRunning);
							nameRunning++;
						}else{
							nameResult = String.valueOf(0);
							pageIdCheck1 = pageIdCheck2;
							nameRunning = 1;
						}





						if(tooltip)
						{


							//InputStream input2 = new BufferedInputStream(url.openStream(), 8192);
							InputStream input2 = new FileInputStream(mediaPath);
							OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
									+"/"+(Integer.valueOf(nameResult)+1)+"_"+(Integer.valueOf(1))+typeTooltip);

							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input2.read(buf)) > 0) {

								output.write(buf, 0, len);
							}

							output.flush();
							output.close();
							input2.close();


						}

						//Implement code for display gallery.
						if(tooltip)
						{

							//download image
							OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
									+"/"+"tooltip"+(Integer.valueOf(nameResult)+1)+typeTooltip);

							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								output.write(buf, 0, len);
							}
							output.flush();
							output.close();
							input.close();


							tooltip=false;
						}
						else if(video)
						{

							String p =DB_PATH+media.get(current).getPageId()
									+"/"+"video"+(Integer.valueOf(nameResult)+1)+".mp4";
							Log.e("video", "write file : " +p );
							OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
									+"/video"+(Integer.valueOf(nameResult)+1)+".mp4");


							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								output.write(buf, 0, len);
							}
							output.flush();
							output.close();
							input.close();


							video=false;

						}
						else
						{

							OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
									+"/"+(Integer.valueOf(nameResult)+1)+"_"+(Integer.valueOf(1))+UtUtilities.getSourceFile(convertName));



							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								output.write(buf, 0, len);
							}
							output.flush();
							output.close();
							input.close();

						}
					}


				}catch (Exception e) {
//       		            	
//       		            	if(mProgressDialog.isShowing());{
//       		            		mProgressDialog.dismiss();
//       		            		processCount = 0;
//       		            		processTotal = 0;
//       		            		e.printStackTrace();
////       		            		Toast.makeText(aq.getContext(), "Media file have a problem", Toast.LENGTH_LONG).show();
//       			            	break;	
//       		            	}

				}



				current++;

			}




		}
	 
          	
      /*    if(mediaRaw!=null && mediaRaw.size()>0){
				DnMedia(mediaRaw);
			}
    	 else */
		if(galleryRaw!=null && galleryRaw.size()>0){
			DnGallery(galleryRaw);
		}
		else FileAnalyze(pdfRaw);





	}

	public void DnGallery(ArrayList<GalleryItem>... galleryRawItem){
		int current=0;
		int totalRow = 0;


		for( ArrayList<GalleryItem> media : galleryRawItem){
			int rows = media.size();
			totalRow = media.size();

			String pageIdCheck1 = null, nameResult = "",patternCount = "";
			int nameRunning = 0;
			int patternRunning= 1;



			while(current < rows)
			{

				try{
					String mediaPath = DB_PATH_ZIP+"media/"+media.get(current).getPath();
					File m = new File(mediaPath);
					if(m.exists()){

						Log.e("DnLoadPdfSD","pathMedia:"+mediaPath);
						InputStream input = new FileInputStream(mediaPath);

						// download the file
						File f = new File(DB_PATH+media.get(current).getPageid());
						if (!f.exists()) {
							f.mkdirs();
						}
						int numberRunning = 1;

						String convertName;
						String name = media.get(current).getName();

						convertName = name.replace("_", "-")
								.replace(".jpg", "_"+numberRunning+".jpg")
								.replace(".png", "_"+numberRunning+".png")
								.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();


						String pageIdCheck2 = media.get(current).getPageid();

						if(pageIdCheck1!=null && pageIdCheck1.equals(pageIdCheck2)){
							nameResult = String.valueOf(nameRunning);
							patternCount = String.valueOf(patternRunning);
							patternRunning++;
						}else{
							nameRunning++;
							nameResult = String.valueOf(nameRunning);
							patternCount = String.valueOf(0);
							pageIdCheck1 = pageIdCheck2;
							patternRunning = 1;

						}

						//Implement code for display gallery.
						OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageid()
								+"/"+"p"+(Integer.valueOf(nameResult))+"_"+(Integer.valueOf(patternCount)+1)+UtUtilities.getSourceFile(convertName));
						byte data[] = new byte[1024];
						long total = 0;
						int count;


						// Transfer bytes from in to out
						byte[] buf = new byte[1024];
						int len;
						while ((len = input.read(buf)) > 0) {
							output.write(buf, 0, len);
						}


						output.flush();
						output.close();
						input.close();
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}

				current++;
			}
		}
          	
          	
          /*if(mediaRaw!=null && mediaRaw.size()>0){
				DnMedia(mediaRaw);
			}
    	 else 
          	if(galleryRaw!=null && galleryRaw.size()>0){
				DnGallery(galleryRaw);
			}
    	 else FileAnalyze(pdfRaw);*/

		FileAnalyze(pdfRaw);

	}


	public void FileAnalyze(ArrayList<PdfRawItem>... pdfRawItem){

		int current=0;
		int totalRow = 0;
		String pdfResultPath = DB_PATH + "book.pdf";

		for( ArrayList<PdfRawItem> pdf : pdfRawItem){
			int rows = pdf.size();
			totalRow = pdf.size();

			if(processTotal!=2){
				AddLinkContentOnPDF(pdf);
			}
			Document document = new Document();

			PdfCopy cp = null;
			PdfReader reader = null;
			try {

				cp = new PdfCopy(document,  new FileOutputStream( DB_PATH+"book.pdf"));
				document.open();

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


			while(current < rows)
			{
				try {
					if(processTotal!=2){
						reader = new PdfReader(DB_PATH+pdf.get(current).getPdfName()+"_"+current+".pdf");
					}else{
						reader = new PdfReader(DB_PATH+pdf.get(current).getPdfName()+".pdf");
					}

					PdfImportedPage page;
					page = cp.getImportedPage(reader, 1);
					cp.addPage(page);
					cp.freeReader(reader);

					File f = null;

					if(processTotal!=2){
						f = new File(DB_PATH+pdf.get(current).getPdfName()+"_"+current+".pdf");
					}else{
						f = new File(DB_PATH+pdf.get(current).getPdfName()+".pdf");
					}

					if(f.exists()){
						f.delete();
					}
//				                publishProgress((int) (pb.getProgress()+(current * 90/ totalRow )));
					current++;
					if(current<rows){
						document.newPage();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			cp.close();
			document.close();
		}



	}










	public class DownloadFile extends AsyncTask<ArrayList<PdfRawItem>, Integer, String> {
		int current=0;
		int totalRow = 0;



		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// instantiate it within the onCreate method
            /*
            processCount++;
            pb = new ProgressDialog(new ContextThemeWrapper(ac,android.R.style.Theme_Holo));
            pb.setMessage("Process file [DownloadFile class] please wait...");
            pb.setIndeterminate(false);
            pb.setCancelable(false);
            pb.setMax(processTotal*100);
            pb.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        
            pb.show();*/

		}

		/*
    	   @Override
    	    protected void onProgressUpdate(Integer... progress) {
    	        super.onProgressUpdate(progress);
    	        pb.setProgress(progress[0]);
    	    }*/

		@Override
		protected String doInBackground(ArrayList<PdfRawItem>... sUrl) {

			Configuration config = ac.getResources().getConfiguration();
			if (config.orientation == Configuration.ORIENTATION_PORTRAIT){
				ac.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				ac.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}


			for( ArrayList<PdfRawItem> pdf : sUrl){
				int rows = pdf.size();
				totalRow = pdf.size();
				while(current < rows)
				{
					try {
						if (isCancelled()){
							break;
						}

						try{
//			           
							String pdfPath = DB_PATH_ZIP+"pdf/";
							Log.e("DnLoadPdfSD","pdfPath:"+pdfPath);
							InputStream input = new FileInputStream(pdfPath+pdf.get(current).getPdfName()+".pdf");

							// download the file
							File f = new File(DB_PATH);
							if (!f.exists()) {
								f.mkdirs();
							}
							// InputStream input = new BufferedInputStream(url.openStream(), 8192);
							//                OutputStream output = new FileOutputStream(DB_PATH+"book.pdf");
							OutputStream output = new FileOutputStream(DB_PATH+pdf.get(current).getPdfName()+".pdf");

							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								output.write(buf, 0, len);
							}
							output.flush();
							output.close();
							input.close();

						}catch(Exception ex){
							//ex.printStackTrace();
						}
						current++;

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
//        	mProgressDialog.dismiss();
      /*  	if(pb!=null && pb.isShowing()){
        		pb.dismiss();
        	}*/

			if(mediaRaw!=null && mediaRaw.size()>0){
				asynDownloadMedia = new DownloadMediaFile();
				asynDownloadMedia.execute(mediaRaw);
				statusDownloadCurrent = DOWNLOAD_MEDIA;
//        		new DownloadMediaFile().execute(mediaRaw);
			}else if(galleryRaw!=null && galleryRaw.size()>0){
				asynDownloadGallery= new DownloadGalleryFile();
				asynDownloadGallery.execute(galleryRaw);
				statusDownloadCurrent = DOWNLOAD_GALLERY;
			}else{
				asynSynce = new AnalyzeFile();
				asynSynce.execute(pdfRaw);
				statusDownloadCurrent = DOWNLOAD_SYNC;
//        		new AnalyzeFile().execute(pdfRaw);
			}


		}
	}

	private class DownloadMediaFile extends AsyncTask<ArrayList<MediaItem>, Integer, String> {
		int current=0;
		int totalRow = 0;

		@Override
		protected String doInBackground(ArrayList<MediaItem>... sUrl) {

//          	 
			String error = null;
			for( ArrayList<MediaItem> media : sUrl){
				int rows = media.size();
				totalRow = media.size();

				String pageIdCheck1 = null, nameResult = "";
				int nameRunning = 1;

				while(current < rows)
				{
					try {
						if (isCancelled()){
							break;
						}
						try{
   		            		
   		            		/*URL url = new URL(rootPath+media.get(current).getPathMedia().replace("$", UtSharePreferences.getPrefServerName(ac)));   
   			                URLConnection connection = url.openConnection();
   			                connection.connect();
   			                // this will be useful so that you can show a typical 0-100% progress bar
   			                int fileLength = connection.getContentLength();*/

							String mediaPath = DB_PATH_ZIP+"media/"+media.get(current).getNameMedia();
							Log.e("DnLoadPdfSD","pathMedia:"+mediaPath);
							InputStream input = new FileInputStream(mediaPath);

							// download the file
							File f = new File(DB_PATH+media.get(current).getPageId());
							if (!f.exists()) {
								f.mkdirs();
							}

							//InputStream input = new BufferedInputStream(url.openStream(), 8192);

							String convertName;
							int numberRunning = 1;


							boolean tooltip=false;
							boolean video=false;


							//////////////////////////////
							if(	media.get(current).getNameMedia().replace("_", "-").endsWith("jpg") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("png") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("bmp") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("JPG") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("PNG") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("BMP")) {

								convertName = media.get(current).getNameMedia().replace("_", "-")
										.replace(".jpg", "_"+numberRunning+".jpg")
										.replace(".png", "_"+numberRunning+".png")
										.replace(".bmp", "_"+numberRunning+".bmp")
										.replace(".JPG", "_"+numberRunning+".jpg")
										.replace(".PNG", "_"+numberRunning+".png")
										.replace(".BMP", "_"+numberRunning+".bmp").toLowerCase();


							}

							else if(media.get(current).getPathMedia().replace("_", "-").endsWith("jpg") ||
									media.get(current).getPathMedia().replace("_", "-").endsWith("png") ||
									media.get(current).getPathMedia().replace("_", "-").endsWith("bmp") ||
									media.get(current).getPathMedia().replace("_", "-").endsWith("JPG") ||
									media.get(current).getPathMedia().replace("_", "-").endsWith("PNG") ||
									media.get(current).getPathMedia().replace("_", "-").endsWith("BMP")  ) {

								convertName = media.get(current).getPathMedia().replace("_", "-")
										.replace(".jpg", "_"+numberRunning+".jpg")
										.replace(".png", "_"+numberRunning+".png")
										.replace(".bmp", "_"+numberRunning+".bmp")
										.replace(".JPG", "_"+numberRunning+".jpg")
										.replace(".PNG", "_"+numberRunning+".png")
										.replace(".BMP", "_"+numberRunning+".bmp").toLowerCase();
								tooltip=true;

								if(convertName.endsWith("jpg"))typeTooltip=".jpg";
								else if(convertName.endsWith("png"))typeTooltip=".png";
								else if(convertName.endsWith("bmp"))typeTooltip=".bmp";
								else typeTooltip=".xxx";
							}
							else if(media.get(current).getNameMedia().replace("_", "-").endsWith("mp4") ||
									media.get(current).getNameMedia().replace("_", "-").endsWith("MP4"))
							{
								convertName = media.get(current).getPathMedia().replace("_", "-")
										.replace(".mp4", "_"+numberRunning+".mp4")
										.replace(".MP4", "_"+numberRunning+".mp4")
										.toLowerCase();
								video=true;
							}

							else{
								convertName = media.get(current).getNameMedia().replace("_", "-").toLowerCase();

							}
							////////////////////////////////
   			           /*     if(media.get(current).getPathMedia().replace("_", "-").endsWith("jpg") ||
   			                		media.get(current).getPathMedia().replace("_", "-").endsWith("png") ||
   			                		media.get(current).getPathMedia().replace("_", "-").endsWith("bmp")) {
   			    			
   			                	convertName = media.get(current).getPathMedia().replace("_", "-")
   			                			.replace(".jpg", "_"+numberRunning+".jpg")
   			                			.replace(".png", "_"+numberRunning+".png")
   			                			.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();
   			    			}else{
   			    				convertName = media.get(current).getPathMedia().replace("_", "-").toLowerCase();
   			    			}*/
							////////////////////////////////
							String pageIdCheck2 = media.get(current).getPageId();

							if(pageIdCheck1!=null && pageIdCheck1.equals(pageIdCheck2)){
								nameResult = String.valueOf(nameRunning);
								nameRunning++;
							}else{
								nameResult = String.valueOf(0);
								pageIdCheck1 = pageIdCheck2;
								nameRunning = 1;
							}





							if(tooltip)
							{


								//InputStream input2 = new BufferedInputStream(url.openStream(), 8192);
								InputStream input2 = new FileInputStream(mediaPath);
								OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
										+"/"+(Integer.valueOf(nameResult)+1)+"_"+(Integer.valueOf(1))+typeTooltip);

								// Transfer bytes from in to out
								byte[] buf = new byte[1024];
								int len;
								while ((len = input2.read(buf)) > 0) {

									output.write(buf, 0, len);
								}

								output.flush();
								output.close();
								input2.close();
   			                	
   			                	/*
   			                	
   			                	  byte data[] = new byte[1024];
   					                long total = 0;
   					                int count;
   					     
   					                 
   					                while ((count = input2.read(data)) != -1) {
   					                    total += count;
//   					                    publishProgress((int) (total * 100 / fileLength));
   					                    output.write(data, 0, count);
   					                }
   					
   					                output.flush();
   					                output.close();
   					                input2.close();*/
							}

							//Implement code for display gallery.
							if(tooltip)
							{

								//download image
								OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
										+"/"+"tooltip"+(Integer.valueOf(nameResult)+1)+typeTooltip);

								// Transfer bytes from in to out
								byte[] buf = new byte[1024];
								int len;
								while ((len = input.read(buf)) > 0) {
									output.write(buf, 0, len);
								}
								output.flush();
								output.close();
								input.close();
			                    
   			                	/*
   			                	 byte data[] = new byte[1024];
   					                long total = 0;
   					                int count;
   					                 
   					                while ((count = input.read(data)) != -1) {
   					                    total += count;
//   					                    publishProgress((int) (total * 100 / fileLength));
   					                    output.write(data, 0, count);
   					                }
   					
   					                output.flush();
   					                output.close();
   					                input.close();*/

								tooltip=false;
							}
							else if(video)
							{

								//       	String p =DB_PATH+media.get(current).getPageId()
								//         		+"/"+"video"+(Integer.valueOf(nameResult)+1)+UtUtilities.getSourceFile(convertName);
								//     	Log.e("video", "write file : " +p );
								//     	OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
								//         		+"/video"+(Integer.valueOf(nameResult)+1)+UtUtilities.getSourceFile(convertName));

								String p =DB_PATH+media.get(current).getPageId()
										+"/"+"video"+(Integer.valueOf(nameResult)+1)+".mp4";
								Log.e("video", "write file : " +p );
								OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
										+"/video"+(Integer.valueOf(nameResult)+1)+".mp4");


								// Transfer bytes from in to out
								byte[] buf = new byte[1024];
								int len;
								while ((len = input.read(buf)) > 0) {
									output.write(buf, 0, len);
								}
								output.flush();
								output.close();
								input.close();
   			                	
   			                	/*
   			                	 byte data[] = new byte[1024];
   					                long total = 0;
   					                int count;
   					                 
   					                while ((count = input.read(data)) != -1) {
   					                    total += count;
//   					                    publishProgress((int) (total * 100 / fileLength));
   					                    output.write(data, 0, count);
   					                }
   					
   					                output.flush();
   					                output.close();
   					                input.close();*/

								video=false;

							}
							else
							{

								OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
										+"/"+(Integer.valueOf(nameResult)+1)+"_"+(Integer.valueOf(1))+UtUtilities.getSourceFile(convertName));
   			                	
   			                	
   			             /*   	OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
   				                		+"/"+"jjjjj"+UtUtilities.getSourceFile(convertName));*/



								// Transfer bytes from in to out
								byte[] buf = new byte[1024];
								int len;
								while ((len = input.read(buf)) > 0) {
									output.write(buf, 0, len);
								}
								output.flush();
								output.close();
								input.close();
   			                	
   			                	/*byte data[] = new byte[1024];
   					                long total = 0;
   					                int count;
   					     
   					                 
   					                while ((count = input.read(data)) != -1) {
   					                    total += count;
//   					                    publishProgress((int) (total * 100 / fileLength));
   					                    output.write(data, 0, count);
   					                }
   					
   					                output.flush();
   					                output.close();
   					                input.close();*/

							}
							//  publishProgress(((processCount-1)*100)+((int) ((current * (100)/ totalRow ))));
						}catch(Exception ex){
							ex.printStackTrace();
						}

						current++;
					} catch (Exception e) {
//   		            	
//   		            	if(mProgressDialog.isShowing());{
//   		            		mProgressDialog.dismiss();
//   		            		processCount = 0;
//   		            		processTotal = 0;
//   		            		e.printStackTrace();
////   		            		Toast.makeText(aq.getContext(), "Media file have a problem", Toast.LENGTH_LONG).show();
//   			            	break;	
//   		            	}

					}
				}
			}
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
//          	if(mProgressDialog.isShowing()){
//          		mProgressDialog.dismiss();
//          		new AnalyzeFile().execute(pdfRaD);


			asynDownloadGallery= new DownloadGalleryFile();
			asynDownloadGallery.execute(galleryRaw);
			statusDownloadCurrent = DOWNLOAD_GALLERY;


//          	}
//          	if(result != null && result.equals("true") ){
//          		Toast.makeText(aq.getContext(), "Media file have a problem", Toast.LENGTH_LONG).show();
//          	}





		}
	}
    
    /*
    private class DownloadMediaFile extends AsyncTask<ArrayList<MediaItem>, Integer, String> {
   	 int current=0;
   	 int totalRow = 0;
   	 

	@Override
       protected void onPreExecute() {
           super.onPreExecute();
           processCount++;
           pb = new ProgressDialog(new ContextThemeWrapper(ac,android.R.style.Theme_Holo));
           pb.setMessage("Process file please wait...");
           pb.setIndeterminate(false);
           pb.setCancelable(false);
           pb.setMax(processTotal*100);
           pb.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
       
           pb.show();
           // instantiate it within the onCreate method
//           mProgressDialog = new ProgressDialog(new ContextThemeWrapper(DetailActivity.this,android.R.style.Theme_Holo));
//           mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
//           mProgressDialog.setMessage("Downloading media file...");
//           mProgressDialog.setIndeterminate(false);
//           mProgressDialog.setCancelable(false);
//           mProgressDialog.setMax(100);
//           mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//           mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//
//					ResetStatDownload();
//					DownloadMediaFile.this.cancel(true);
//				}
//			});
//           mProgressDialog.show();
       }
   	
   	   @Override
   	    protected void onProgressUpdate(Integer... progress) {
   	        super.onProgressUpdate(progress);
   	        pb.setProgress(progress[0]);
//   	        mProgressDialog.setProgress(progress[0]);
//   	        mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
//   	        mProgressDialog.setMessage("Downloading media file... "+current+"/"+totalRow);
   	    }
   	
       @Override
       protected String doInBackground(ArrayList<MediaItem>... sUrl) {
       
//       	 
    String error = null;
       	for( ArrayList<MediaItem> media : sUrl){
       		int rows = media.size();
       		totalRow = media.size();
       		
       		String pageIdCheck1 = null, nameResult = "";
       		int nameRunning = 1;
       		
	             while(current < rows)
	             {
		            try {
		            	if (isCancelled()){
		            		break;
		            	}
		            	try{
			            	URL url = new URL(rootPath+media.get(current).getPathMedia());   
			                URLConnection connection = url.openConnection();
			                connection.connect();
			                // this will be useful so that you can show a typical 0-100% progress bar
			                int fileLength = connection.getContentLength();
			                
			                // download the file
			                File f = new File(DB_PATH+media.get(current).getPageId());
			                if (!f.exists()) {
								f.mkdirs();
							}
			                InputStream input = new BufferedInputStream(url.openStream(), 8192);
			                String convertName;
			                int numberRunning = 1;
			                if(media.get(current).getNameMedia().replace("_", "-").endsWith("jpg") ||
			                		media.get(current).getNameMedia().replace("_", "-").endsWith("png") ||
			                		media.get(current).getNameMedia().replace("_", "-").endsWith("bmp")) {
			    			
			                	convertName = media.get(current).getNameMedia().replace("_", "-")
			                			.replace(".jpg", "_"+numberRunning+".jpg")
			                			.replace(".png", "_"+numberRunning+".png")
			                			.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();
			    			}else{
			    				convertName = media.get(current).getNameMedia().replace("_", "-").toLowerCase();
			    			}
			                String pageIdCheck2 = media.get(current).getPageId();
			                
			                if(pageIdCheck1!=null && pageIdCheck1.equals(pageIdCheck2)){
			                	nameResult = String.valueOf(nameRunning);
			                	nameRunning++;
			                }else{
			                	nameResult = String.valueOf(0);
			                	pageIdCheck1 = pageIdCheck2;
			                	nameRunning = 1;
			                }
			                
			                //Implement code for display gallery.
			                OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageId()
			                		+"/"+(Integer.valueOf(nameResult)+1)+"_"+(Integer.valueOf(1))+UtUtilities.getSourceFile(convertName));
			                byte data[] = new byte[1024];
			                long total = 0;
			                int count;
			     
			                 
			                while ((count = input.read(data)) != -1) {
			                    total += count;
//			                    publishProgress((int) (total * 100 / fileLength));
			                    output.write(data, 0, count);
			                }
			
			                output.flush();
			                output.close();
			                input.close();
			                publishProgress(((processCount-1)*100)+((int) ((current * (100)/ totalRow ))));
		            	}catch(Exception ex){
		            		ex.printStackTrace();
		            	}
		            	
		                current++;
		            } catch (Exception e) {
//		            	
//		            	if(mProgressDialog.isShowing());{
//		            		mProgressDialog.dismiss();
//		            		processCount = 0;
//		            		processTotal = 0;
//		            		e.printStackTrace();
////		            		Toast.makeText(aq.getContext(), "Media file have a problem", Toast.LENGTH_LONG).show();
//			            	break;	
//		            	}
		            	
		            }
	             }
            }
           return null;
       }
       
       @SuppressWarnings("unchecked")
       @Override
       protected void onPostExecute(String result) {
       	super.onPostExecute(result);
       	
       	if(pb!=null && pb.isShowing()){
    		pb.dismiss();
    	}
       		asynDownloadGallery= new DownloadGalleryFile();
       		asynDownloadGallery.execute(galleryRaw);
       		statusDownloadCurrent = DOWNLOAD_GALLERY;
//       	}
//       	if(result != null && result.equals("true") ){
//       		Toast.makeText(aq.getContext(), "Media file have a problem", Toast.LENGTH_LONG).show();
//       	}
       
       }
   }*/

	private class DownloadGalleryFile extends AsyncTask<ArrayList<GalleryItem>, Integer, String> {
		int current=0;
		int totalRow = 0;


		@Override
		protected String doInBackground(ArrayList<GalleryItem>... sUrl) {

//          	 
			for( ArrayList<GalleryItem> media : sUrl){
				int rows = media.size();
				totalRow = media.size();

				String pageIdCheck1 = null, nameResult = "",patternCount = "";
				int nameRunning = 0;
				int patternRunning= 1;

				while(current < rows)
				{
					try {
						if (isCancelled()){
							break;
						}
						try{
   		            		
   			            	/*URL url = new URL(rootPath+media.get(current).getPath());   
   			                URLConnection connection = url.openConnection();
   			                connection.connect();
   			                // this will be useful so that you can show a typical 0-100% progress bar
   			                int fileLength = connection.getContentLength();
   			                InputStream input = new BufferedInputStream(url.openStream(), 8192);*/

							String mediaPath = DB_PATH_ZIP+"media/"+media.get(current).getPath();
							Log.e("DnLoadPdfSD","pathMedia:"+mediaPath);
							InputStream input = new FileInputStream(mediaPath);

							// download the file
							File f = new File(DB_PATH+media.get(current).getPageid());
							if (!f.exists()) {
								f.mkdirs();
							}
							int numberRunning = 1;

							String convertName;
							String name = media.get(current).getName();

							convertName = name.replace("_", "-")
									.replace(".jpg", "_"+numberRunning+".jpg")
									.replace(".png", "_"+numberRunning+".png")
									.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();


							String pageIdCheck2 = media.get(current).getPageid();

							if(pageIdCheck1!=null && pageIdCheck1.equals(pageIdCheck2)){
								nameResult = String.valueOf(nameRunning);
								patternCount = String.valueOf(patternRunning);
								patternRunning++;
							}else{
								nameRunning++;
								nameResult = String.valueOf(nameRunning);
								patternCount = String.valueOf(0);
								pageIdCheck1 = pageIdCheck2;
								patternRunning = 1;

							}

							//Implement code for display gallery.
							OutputStream output = new FileOutputStream(DB_PATH+media.get(current).getPageid()
									+"/"+"p"+(Integer.valueOf(nameResult))+"_"+(Integer.valueOf(patternCount)+1)+UtUtilities.getSourceFile(convertName));
							byte data[] = new byte[1024];
							long total = 0;
							int count;


							// Transfer bytes from in to out
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								output.write(buf, 0, len);
							}


							output.flush();
							output.close();
							input.close();
   			     
   			                 /*
   			                while ((count = input.read(data)) != -1) {
   			                    total += count;
//   			                    publishProgress((int) (total * 100 / fileLength));
   			                    output.write(data, 0, count);
   			                }
   			
   			                output.flush();
   			                output.close();
   			                input.close();*/


							// publishProgress(((processCount-1)*100)+((int) ((current * (100)/ totalRow ))));
						}catch(Exception ex){
							ex.printStackTrace();
						}

						current++;
					} catch (Exception e) {


					}
				}
			}
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
        	
          	/*
           	if(pb!=null && pb.isShowing()){
        		pb.dismiss();
        	}*/

			asynSynce = new AnalyzeFile();
			asynSynce.execute(pdfRaw);
			statusDownloadCurrent = DOWNLOAD_SYNC;

		}
	}




	private class AnalyzeFile extends AsyncTask<ArrayList<PdfRawItem>, Integer, String> {
		int current=0;
		int totalRow = 0;
		String pdfResultPath = DB_PATH + "book.pdf";

		@Override
		protected String doInBackground(ArrayList<PdfRawItem>... sUrl) {

			for( ArrayList<PdfRawItem> pdf : sUrl){
				int rows = pdf.size();
				totalRow = pdf.size();

				if(processTotal!=2){
					AddLinkContentOnPDF(pdf);
				}
				Document document = new Document();

				PdfCopy cp = null;
				PdfReader reader = null;
				try {

					cp = new PdfCopy(document,  new FileOutputStream( DB_PATH+"book.pdf"));
					document.open();

				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


				while(current < rows)
				{
					try {
						if(processTotal!=2){
							reader = new PdfReader(DB_PATH+pdf.get(current).getPdfName()+"_"+current+".pdf");
						}else{
							reader = new PdfReader(DB_PATH+pdf.get(current).getPdfName()+".pdf");
						}

						PdfImportedPage page;
						page = cp.getImportedPage(reader, 1);
						cp.addPage(page);
						cp.freeReader(reader);

						File f = null;

						if(processTotal!=2){
							f = new File(DB_PATH+pdf.get(current).getPdfName()+"_"+current+".pdf");
						}else{
							f = new File(DB_PATH+pdf.get(current).getPdfName()+".pdf");
						}

						if(f.exists()){
							f.delete();
						}
//		                publishProgress((int) (pb.getProgress()+(current * 90/ totalRow )));
						current++;
						if(current<rows){
							document.newPage();
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				cp.close();
				document.close();
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			// 	SetBookHandler(revision,idRef,name,summary,datetime,DB_PATH+"book.pdf",cover);

		}
	}

	private void AddLinkContentOnPDF(ArrayList<PdfRawItem> pdf){
		int rows = pdf.size();
		int current=0;
		int breakNum=0;

		while(current < rows)
		{
			try {
				Document document = new Document();
				PdfReader reader = new PdfReader(DB_PATH+pdf.get(current).getPdfName()+".pdf");
				PdfWriter  writer = PdfWriter.getInstance(document, new FileOutputStream(DB_PATH+pdf.get(current).getPdfName()+"_"+current+".pdf"));
				document.setPageSize(reader.getPageSize(1));
				document.open();



				if(pdf.get(current).getMedia()!=null && pdf.get(current).getMedia().size()>0){
					int mediaTotal = pdf.get(current).getMedia().size();

					breakNum=0;
					Image img;
					Image imgThumbnail = null;


					for(int mediaCount = 0; mediaCount<mediaTotal; mediaCount++){

						try{
							File f;
							String cName;
							int nRunning = 1;

							Rectangle r = new Rectangle(pdf.get(current).getMedia().get(mediaCount).getCodinateX()*Float.parseFloat(pdf.get(current).getMultiplier()),
									document.getPageSize().getTop()-(pdf.get(current).getMedia().get(mediaCount).getCodinateY()*Float.parseFloat(pdf.get(current).getMultiplier())),
									pdf.get(current).getMedia().get(mediaCount).getCodinateWidth(),
									pdf.get(current).getMedia().get(mediaCount).getCodinateHight());



							switch (mathPdfTypeToInteger(pdf.get(current).getMedia().get(mediaCount).getPdfType())) {

								case TYPE_VIDEO:


									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/video"+(mediaCount+1)+UtUtilities.getSourceFile(cName));
									String video=DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/video"+(mediaCount+1)+UtUtilities.getSourceFile(cName);

									Log.e("video check","video : "+video);

									if (f.exists()) {

										Log.e("video","exist path : "+video);

										String convertName;
										Annotation annotation;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();

										//imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.video_btn);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										imgThumbnail = Image.getInstance(stream.toByteArray());


										imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
										float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
										float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
										imgThumbnail.setAbsolutePosition((float)r.getMinX()+(imgW),(float)r.getMinY()-((imgH)+((imgH)/2)-5));

										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/video"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)+"?warect=full");


										imgThumbnail.setAnnotation(annotation);
										document.add(imgThumbnail);

									}


									else if(mediaCount != 0)
									{
										breakNum = breakNum++;

										f=new File(pdf.get(current).getMedia().get(mediaCount).getPageId()+"/video"+breakNum+".mp4");
										if(f.exists())
										{
											String convertName;
											Annotation annotation;

											convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();

											//imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.video_btn);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											imgThumbnail = Image.getInstance(stream.toByteArray());

											imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
											float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
											float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
											imgThumbnail.setAbsolutePosition((float)r.getMinX()+(imgW),(float)r.getMinY()-((imgH)+((imgH)/2)-5));

											annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/video"+breakNum+UtUtilities.getSourceFile(convertName)+"?warect=full");


											imgThumbnail.setAnnotation(annotation);
											document.add(imgThumbnail);
										}


									}


									break;

								case TYPE_AUDIO:

									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();
								/*f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()
										+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(cName));*/
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()
											+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(cName));

									String sound = DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(cName);
									//boolean sound=true;
									Log.e("check","sound : "+sound);


									if (f.exists()) {
										//if(sound){

										Log.e("audio","exist path : "+sound);

										String convertName;
										Annotation annotation;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();

										//imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
										//imgThumbnail = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										imgThumbnail = Image.getInstance(stream.toByteArray());

										imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
										float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
										float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
										imgThumbnail.setAbsolutePosition((float)r.getMinX()+(imgW),(float)r.getMinY()-((imgH)+((imgH)/2)-5));

										// annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)+"?warect=no");
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()
												+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(convertName)+"?warect=no");

										imgThumbnail.setAnnotation(annotation);
										document.add(imgThumbnail);
									}

									else if(mediaCount != 0)
									{
										breakNum++;

										Log.e("audio","exist path : "+sound);

										String convertName;
										Annotation annotation;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();

										//imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
										//imgThumbnail = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										imgThumbnail = Image.getInstance(stream.toByteArray());

										imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
										float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
										float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
										imgThumbnail.setAbsolutePosition((float)r.getMinX()+(imgW),(float)r.getMinY()-((imgH)+((imgH)/2)-5));

										// annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)+"?warect=no");
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()
												+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName)+"?warect=no");

										imgThumbnail.setAnnotation(annotation);
										document.add(imgThumbnail);
									}
								 /*
								 if(mediaCount != 0)
								 {
									 breakNum = breakNum++;
									 
									  
									 
									 String convertName;
								        Annotation annotation;
						              
					    				convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-").toLowerCase();
					    				
					    				
					    				
					    				imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
					    				imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);					    
					    				 float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
					    				 float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
					    				 imgThumbnail.setAbsolutePosition((float)r.getMinX()+(imgW),(float)r.getMinY()-((imgH)+((imgH)/2)-5)); 
					    				 
									    // annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)+"?warect=no");
					    				 annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()
					    						 +"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(convertName)+"?warect=no");
					    				 
									     imgThumbnail.setAnnotation(annotation);
						                 document.add(imgThumbnail);
									 
								 }*/

									break;

								case TYPE_IMAGE:

									Log.e("check","image");

									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
											.replace(".jpg", "_"+nRunning+".jpg")
											.replace(".png", "_"+nRunning+".png")
											.replace(".bmp", "_"+nRunning+".bmp").toLowerCase();
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(cName));
									String image =DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(cName);

									if (f.exists()) {

										Log.e("image","exist path : "+image);

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();


//					                	img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)); 


										if(pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){

											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());
					                		
					                	/*	Drawable d = ac.getResources().getDrawable (R.drawable.transparent_bg);
					                				Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
					                				ByteArrayOutputStream stream = new ByteArrayOutputStream();
					                				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					                				byte[] bitmapData = stream.toByteArray();
//					                	
					                		img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png"); */
//					                		Log.e("ITEXT",fileUri.getPath());

										}else{
											//background on bottom thumbnail.
											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(convertName));

										}

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(convertName)+"?warect=full");

										img.setAnnotation(annotation);
										document.add(img);

										//annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(convertName)+"?warect=full");


										if(!pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){

											//thumbnail on tap image.
											//imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.gallery_btn);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());
											imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
											float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
											float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
											imgThumbnail.setAbsolutePosition((float)(r.getMinX()),(float)(r.getMinY()-(img.getScaledHeight()-3)));

											imgThumbnail.setAnnotation(annotation);
											document.add(imgThumbnail);
										}
									}
									else if(mediaCount != 0)
									{
										breakNum++;

										Log.e("image","exist path : "+image);

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();


//					                	img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)); 


										if(pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){
//					                	
											//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
//					                		Log.e("ITEXT",fileUri.getPath());

											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());

										}else{
											//background on bottom thumbnail.
											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_"+(1)+UtUtilities.getSourceFile(convertName));

										}

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_"+(1)+UtUtilities.getSourceFile(convertName)+"?warect=full");

										img.setAnnotation(annotation);
										document.add(img);

										//annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(1)+UtUtilities.getSourceFile(convertName)+"?warect=full");


										if(!pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){

											//thumbnail on tap image.
											// imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.gallery_btn);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());

											imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
											float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
											float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
											imgThumbnail.setAbsolutePosition((float)(r.getMinX()),(float)(r.getMinY()-(img.getScaledHeight()-3)));

											imgThumbnail.setAnnotation(annotation);
											document.add(imgThumbnail);
										}
									}


									break;

								case TYPE_GALLERY:

									Log.e("check","gallery");

									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
											.replace(".jpg", "_"+nRunning+".jpg")
											.replace(".png", "_"+nRunning+".png")
											.replace(".bmp", "_"+nRunning+".bmp").toLowerCase();
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"p"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(cName));
									String gallery=DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"p"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(cName);
									Log.e("gallery","path : "+gallery);

									if (f.exists()) {

										Log.e("gallery","exist path : "+gallery);

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();


//					                	img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName)); 


										if(pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){
//					                	
											//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");

//					                		Log.e("ITEXT",fileUri.getPath());

											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());

										}else{
											//background on bottom thumbnail.
											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"p"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));



										}
										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"p"+(mediaCount+1)+"_"+pdf.get(current).getMedia().get(mediaCount).getPdfTypeGalleryCount()+UtUtilities.getSourceFile(convertName)+"?warect=full");

										img.setAnnotation(annotation);
										document.add(img);

										if(!pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){

											//thumbnail on tap image.
											// imgThumbnail = Image.getInstance(new URL(pdf.get(current).getMedia().get(mediaCount).getPathMediaImagePlay()));
											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.gallery_btn);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());

											imgThumbnail.scaleToFit(((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()))/2,((float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()))/2);
											float imgH = imgThumbnail.getScaledHeight()*Float.parseFloat(pdf.get(current).getMultiplier());
											float imgW = imgThumbnail.getScaledWidth()*Float.parseFloat(pdf.get(current).getMultiplier());
											imgThumbnail.setAbsolutePosition((float)(r.getMinX()),(float)(r.getMinY()-(img.getScaledHeight()-3)));

											imgThumbnail.setAnnotation(annotation);
											document.add(imgThumbnail);
										}
									}

									break;



								case TYPE_PAGE:

									Log.e("check","page");

									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
											.replace(".jpg", "_"+nRunning+".jpg")
											.replace(".png", "_"+nRunning+".png")
											.replace(".bmp", "_"+nRunning+".bmp").toLowerCase();
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(cName));


									if (f.exists()) {


										//Log.e("page","exist path : "+gallery);

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning1 = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning1+".jpg")
												.replace(".png", "_"+numberRunning1+".png")
												.replace(".bmp", "_"+numberRunning1+".bmp").toLowerCase();


										img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+
												UtUtilities.getSourceFile(convertName)+"?warect=page&page_jump="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);
									}
									else if(mediaCount != 0 &&
											(pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".jpg")
													||pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".png")
													||pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".bmp"))
											)
									{
										breakNum++;

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning1 = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning1+".jpg")
												.replace(".png", "_"+numberRunning1+".png")
												.replace(".bmp", "_"+numberRunning1+".bmp").toLowerCase();


										img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName));

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+
												UtUtilities.getSourceFile(convertName)+"?warect=page&page_jump="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);

									}

									else
									{
										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning1 = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning1+".jpg")
												.replace(".png", "_"+numberRunning1+".png")
												.replace(".bmp", "_"+numberRunning1+".bmp").toLowerCase();


										//img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));
										//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										img = Image.getInstance(stream.toByteArray());

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+
												UtUtilities.getSourceFile(convertName)+"?warect=page&page_jump="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);
									}

									break;

								case TYPE_LINK:

									Log.e("check","link :" +pdf.get(current).getMedia().get(mediaCount).getPdfOption());

									cName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
											.replace(".jpg", "_"+nRunning+".jpg")
											.replace(".png", "_"+nRunning+".png")
											.replace(".bmp", "_"+nRunning+".bmp").toLowerCase();
									f = new File(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(cName));


									if(f.exists())
									{
										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();

										Log.e("link", "img path : "+DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(convertName));
										img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_1"+UtUtilities.getSourceFile(convertName));
			                	
									/*
									if(pdf.get(current).getMedia().get(mediaCount).getPdfEffect().equalsIgnoreCase("popup")){
//					                	
					                		img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png"); 
//					                		Log.e("ITEXT",fileUri.getPath());
					                }
									*/

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
			                	 /*annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+
			                			 UtUtilities.getSourceFile(convertName)+"?warect=link&link_url="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());
			    				*/
										annotation = new Annotation( 0f, 0f, 0f, 0f,pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);

									}

									else if(mediaCount != 0 &&
											(pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".jpg")
													||pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".png")
													||pdf.get(current).getMedia().get(mediaCount).getPathMedia().endsWith(".bmp"))
											)
									{
										breakNum++;

										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();

										Log.e("link", "img path : "+DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName));
										img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName));

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));  
				                	 /*annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+
				                			 UtUtilities.getSourceFile(convertName)+"?warect=link&link_url="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());
				    				*/
										annotation = new Annotation( 0f, 0f, 0f, 0f,pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);

									}
									else
									{
										String convertName;
										Annotation annotation;
										PdfContentByte canvas = writer.getDirectContent();
										int numberRunning = 1;

										convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
												.replace(".jpg", "_"+numberRunning+".jpg")
												.replace(".png", "_"+numberRunning+".png")
												.replace(".bmp", "_"+numberRunning+".bmp").toLowerCase();

										Log.e("link", "img path : "+DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName));
										//img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+breakNum+"_1"+UtUtilities.getSourceFile(convertName));
										//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										img = Image.getInstance(stream.toByteArray());

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
			                	 /*annotation = new Annotation( 0f, 0f, 0f, 0f,"http://localhost/"+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+
			                			 UtUtilities.getSourceFile(convertName)+"?warect=link&link_url="+pdf.get(current).getMedia().get(mediaCount).getPdfOption());
			    				*/
										annotation = new Annotation( 0f, 0f, 0f, 0f,pdf.get(current).getMedia().get(mediaCount).getPdfOption());

										img.setAnnotation(annotation);
										document.add(img);
									}


									break;

								case TYPE_TOOLTIP:


									String convertName,path;
									Annotation annotation,annotationn;
									PdfContentByte canvas = writer.getDirectContent();
									int numberRunning = 1;

									convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia()+typeTooltip.replace("_", "-")
											.replace(".jpg", ".jpg")
											.replace(".png", ".png")
											.replace(".bmp", ".bmp").toLowerCase();



									path=DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"tooltip"+(mediaCount+1);

									Log.e("Tooltip", "Path : "+path);
									File f_txt = new File(path+".txt");
									File f1,f2,f3;

									if(f_txt.exists()){
										Log.e("ToolTip","txt exist");

										f1 = new File(path+".jpg");
										if(f1.exists()){
											Log.e("ToolTip","jpg exist");

											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"tooltip"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));
											img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
											img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
											annotation = new Annotation( 0f, 0f, 0f, 0f,path);
											img.setAnnotation(annotation);
											document.add(img);
										}

										f2 = new File(path+".png");
										if(f2.exists()){
											Log.e("ToolTip","png exist");

											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"tooltip"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));
											img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
											img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
											annotation = new Annotation( 0f, 0f, 0f, 0f,path);
											img.setAnnotation(annotation);
											document.add(img);
										}

										f3 = new File(path+".bmp");
										if(f3.exists()){
											Log.e("ToolTip","bmp exist");

											img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+"tooltip"+(mediaCount+1)+UtUtilities.getSourceFile(convertName));
											img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
											img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
											annotation = new Annotation( 0f, 0f, 0f, 0f,path);
											img.setAnnotation(annotation);
											document.add(img);
										}

										if(!f1.exists() && !f2.exists() && !f3.exists()){


											//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
											Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
											ByteArrayOutputStream stream = new ByteArrayOutputStream();
											bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
											img = Image.getInstance(stream.toByteArray());

											img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
											img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
											annotation = new Annotation( 0f, 0f, 0f, 0f,path);
											img.setAnnotation(annotation);
											document.add(img);
										}


									}
									else
									{
										//img = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png");
										Bitmap bitmap = BitmapFactory.decodeResource(ac.getResources(),R.drawable.transparent);
										ByteArrayOutputStream stream = new ByteArrayOutputStream();
										bitmap.compress(Bitmap.CompressFormat.PNG, 100 , stream);
										img = Image.getInstance(stream.toByteArray());

										img.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
										img.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(img.getScaledHeight()-3));
										annotation = new Annotation( 0f, 0f, 0f, 0f,path);
										img.setAnnotation(annotation);
										document.add(img);
									}








									//	img = Image.getInstance(DB_PATH+pdf.get(current).getMedia().get(mediaCount).getPageId()+"/"+(mediaCount+1)+"_"+(mediaCount+1)+UtUtilities.getSourceFile(convertName2));
									//img = Image.getInstance("http://203.148.174.2/contents/images/web/link-button-1.png");
				                
			                	 /*
			                	 imgThumbnail = Image.getInstance("http://epub.arip.co.th/contents/images/app/transparent_bg.png"); 
			                	 imgThumbnail.scaleToFit((float)r.getWidth()*Float.parseFloat(pdf.get(current).getMultiplier()),(float)r.getHeight()*Float.parseFloat(pdf.get(current).getMultiplier()));
			                	 imgThumbnail.setAbsolutePosition((float)r.getMinX(),(float)r.getMinY()-(imgThumbnail.getScaledHeight()-3));  
			                	 annotationn = new Annotation( 0f, 0f, 0f, 0f,path);
			                	 imgThumbnail.setAnnotation(annotationn);
			                	 document.add(imgThumbnail);
				                 */


									break;
								case TYPE_EXAM:



									break;
								default:
									break;
							}

						}catch(Exception e){
							e.printStackTrace();
						}

					}

				}
				PdfImportedPage page;
				page = writer.getImportedPage(reader,1);
				writer.getDirectContentUnder().addTemplate(page, 0, 0);
				writer.flush();
				document.close();

				if(reader!=null){
					reader.close();
				}

				File f = new File(DB_PATH+pdf.get(current).getPdfName()+".pdf");
				if(f.exists()){
					f.delete();
				}


			} catch (Exception e) {
				e.printStackTrace();
			}
			current++;
		}

	}

	private boolean isImagefile(String mediaPaht){
		return mediaPaht.replace("_", "-").endsWith("jpg") || mediaPaht.replace("_", "-").endsWith("png") || mediaPaht.replace("_", "-").endsWith("bmp")?true:false;
	}

	private int mathPdfTypeToInteger(String type){
		int result = 1;
		if(type.equalsIgnoreCase("video")){
			result = TYPE_VIDEO;
		}else if(type.equalsIgnoreCase("audio")){
			result = TYPE_AUDIO;
		}else if(type.equalsIgnoreCase("image")){
			result = TYPE_IMAGE;
		}else if(type.equalsIgnoreCase("link")){
			result = TYPE_LINK;
		}else if(type.equalsIgnoreCase("page")){
			result = TYPE_PAGE;
		}else if(type.equalsIgnoreCase("gallery")){
			result = TYPE_GALLERY;
		}else if(type.equalsIgnoreCase("tooltip")){
			result = TYPE_TOOLTIP;
		}else if(type.equalsIgnoreCase("exam")){
			result = TYPE_EXAM;
		}

		return result;
	}


//    private void SetBookHandler(){
//    	bh.open();
//        if (bh.check(id)) {
//        	   bh.addFavorite(id, name, summary, datetime, DB_PATH+"book.pdf", cover, revision);
//		}
//        if (ac.getIntent().getExtras().containsKey("tag")) {
//        	bh.updateBook(id, revision);
//        	aq.id(R.id.img_download).clicked(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					openPdfIntent(bh.getPDF(id));
//					
//				}
//			});
//		}
//        aq.id(R.id.img_download).getImageView().setImageResource(R.drawable.op_ic_download_finish);
//// 		aq.id(R.id.btn_download_detail).text("Read");
//		final Handler handler = new Handler();
//		final Runnable r = new Runnable()
//		{
//		    public void run() 
//		    {
//		    	//Set Rotation ON
//				ac.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
//		    }
//		};
//
//		handler.postDelayed(r, 1000);
//		
//    }

//    public void openPdfIntent(String path) {
//    	
//    	TextView txtTitle = (TextView)ac.findViewById(R.id.txt_description);
//    	
//    	SharedPreferences prefs = ac.getSharedPreferences("arip", Context.MODE_PRIVATE);
//	    SharedPreferences.Editor edit = prefs.edit();
//        edit.putInt("arip_session", 1);
//        edit.commit();
//        
//		Uri uri = Uri.parse(path);
//		Intent intent = new Intent(ac, MuPDFActivity.class);
//		intent.setAction(Intent.ACTION_VIEW);
//		intent.setData(uri);
//		intent.putExtra("title", txtTitle.getText().toString());
//		ac.startActivity(intent);
//    }

	private void writeContentFile(ArrayList<ContentItem> contentItem){

		try {

			File f = new File(DB_PATH);
			if (!f.exists()) {
				f.mkdirs();
			}


			FileWriter file = new FileWriter(DB_PATH+"content.txt");
			BufferedWriter bf = new BufferedWriter(file);

			for (ContentItem cItm: contentItem){
				bf.write(cItm.getDescription());
				bf.write(",");
				bf.write(String.valueOf(cItm.getJumpto()));
				bf.newLine();
			}

			bf.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

