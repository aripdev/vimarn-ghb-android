package com.ghbank.bookshelf.download;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.arip.it.library.core.model.ContentItem;
import com.arip.it.library.core.model.GalleryItem;
import com.arip.it.library.core.model.MediaItem;
import com.arip.it.library.core.model.MediaProperty;
import com.arip.it.library.core.model.PdfRawItem;
import com.arip.it.library.core.model.SlideItem;
import com.arip.it.library.database.DatabaseManager;
import com.arip.it.library.model.ShareLink;
import com.ghbank.bookshelf.BkShelfLibrary;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.async.UnzipAsyncTask;
import com.ghbank.bookshelf.change.helper.ImageHelper;
import com.ghbank.bookshelf.db.DbBook;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.loader.LdBkShelfPreviewPageLoader;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.ImageManager;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtConvertRegion;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.utils.UtUtilities;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import group.aim.framework.basemodel.BaseArrayList;

public class DnLoadPdfFromUrl extends LdBkShelfPreviewPageLoader {

    private static final String TAG = DnLoadPdfFromUrl.class.getSimpleName();

    private static final int DOWNLOAD_FILE = 1;
    private static final int DOWNLOAD_CARTOON = 2;
    private static final int DOWNLOAD_MEDIA = 3;
    private static final int DOWNLOAD_SYNC = 4;
    private static final int DOWNLOAD_GALLERY = 5;

    private static final int TYPE_VIDEO = 1;
    private static final int TYPE_AUDIO = 2;
    private static final int TYPE_IMAGE = 3;
    private static final int TYPE_LINK = 4;
    private static final int TYPE_PAGE = 5;
    private static final int TYPE_GALLERY = 6;
    private static final int TYPE_TOOLTIP = 7;
    private static final int TYPE_EXAM = 8;
    private static final int TYPE_YOUTUBE = 9;
    private static final int TYPE_HTML5 = 10;

    private int statusDownloadCurrent = 1;

    private Activity ac;
    private ProgressBar pb;
    private AQuery aq;
    private ArrayList<PdfRawItem> pdfRaw;
    private ArrayList<MediaItem> mediaRaw;
    private ArrayList<GalleryItem> galleryRaw;
    private ArrayList<MediaItem> examRaw;
    @SuppressWarnings("unused")
    private DbBookHandler bh;

    private String BOOK_ID = "";
    private String DB_PATH;
    private int processCount = 0;
    private int processTotal = 0;
    private String cover, rootPath, revision;
    private boolean pdfCheck, mediaCheck, galleryCheck, imageCheck, analyzeCheck = false;
    public static DownloadFile asynDownload;
    public static DownloadMediaFile asynDownloadMedia;
    public static DownloadCartoon asynDownloadCartoon;
    public static DownloadGalleryFile asynDownloadGallery;
    public static AnalyzeFile asynSynce;
    public static String idRef;
    public String typeTooltip = "", TooltipContent = "", coverpage = "", description = "";


    private String exam_type, question_id;
    private int totalExam = 0;
    private String ExamMediaLink;

    private String imageTransparent = "http://img.thaibuzz.com/it/transparent.png";
    private String image_test = "http://img.thaibuzz.com/it/transparent.png";

    //Add on 06 Dec. 2016
    private static final String EPUB_ID_PARAMETER = "epub_ID";
    private static final String PDF_CANVAS_PARAMETER = "canvas";
    private static final String INSERT_ID_PARAMETER = "insert_id";
    private static final String STATUS_PARAMETER = "status";
    private static final String REVISION_PARAMETER = "re_vision";
    private static final String FORMAT_TYPE_PARAMETER = "format_type";
    private static final String PAGES_COUNT_PARAMETER = "pages";
    private static final String ROOT_LINK_PARAMETER = "root_link";
    private static final String CANVAS_OBJECT_PARAMETER = "object";
    private static final String PROPERTY_PARAMETER = "property";
    private static final String TYPE_PARAMETER = "type";
    private static final String PATH_PARAMETER = "path";
    private static final String SLIDE_PARAMETER = "slide";
    private static final String CANVAS_ID_PARAMETER = "canvas_id";
    private static final String X_PARAMETER = "x";
    private static final String Y_PARAMETER = "y";
    private static final String WIDTH_PARAMETER = "width";
    private static final String HEIGHT_PARAMETER = "height";
    private static final String SOURCE_PARAMETER = "source";
    private static final String EFFECT_PARAMETER = "effect";
    private static final String LINK_PARAMETER = "link";
    private static final String HYPER_TEXT_PARAMETER = "href";
    private static final String LINK_TYPE_PARAMETER = "linkType";
    private static final String OPTION_PARAMETER = "option";
    private static final String NAME_PARAMETER = "name";
    private static final String PAGE_NAME_PARAMETER = "page_name";
    private static final String PDF_LINK_PARAMETER = "pdf_link";
    private static final String IMG_PARAMETER = "img";

    private String html5Thumbnail = "";

    public DnLoadPdfFromUrl(Activity activity, String id) {
        super(activity);
        NLog.w(TAG, TAG + " onCreate object.");
        ac = activity;
        pb = (ProgressBar) ac.findViewById(R.id.view_detail__pgb_download);
        aq = new AQuery(ac);
        DB_PATH = UtConfig.DESIGN_PATH + id + "/";
        idRef = id;
        bh = new DbBookHandler(ac);

        @SuppressWarnings({"unused", "static-access"})
        final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE);
        String ttt = UtSharePreferences.getPrefServerExamConfig(ac);
        ExamMediaLink = replaceAll(ttt, "content.php?", "");
        Log.e("Ch.DN_URL", ExamMediaLink);


    }

    private String replaceAll(String source, String pattern, String replacement) {
        if (source == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        int index;
        int patIndex = 0;
        while ((index = source.indexOf(pattern, patIndex)) != -1) {
            sb.append(source.substring(patIndex, index));
            sb.append(replacement);
            patIndex = index + pattern.length();
        }
        sb.append(source.substring(patIndex));
        return sb.toString();
    }

    private String getHtmlMediaPath(String path) {
        String resultPaht = "";
        try {
            resultPaht = path.split("/")[0] + "/" + UtConvertRegion.ConvertToHTML(path.split("/")[1]);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultPaht;
    }

    public void downloadLinkById(final String id) {
        // final ProgressDialog pd = ProgressDialog.show(this, null,
        // "Preparing...");
        NLog.w(TAG, TAG + " downloadLinkById " + id);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            System.out.println("*** My thread is now configured to allow connection");
        }
        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @SuppressWarnings("unchecked")
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                super.callback(url, json, status);
                NLog.w(TAG, "Download URL " + url);

                // pd.dismiss();


//				if(id.equalsIgnoreCase("2142"))
//					try {
//						json = new JSONObject(BookMockData.getBookDataFromID(BOOK.ID_2142));
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}

                if (json != null) {
                    try {
                        if (json.getString(STATUS_PARAMETER).equals("1")) {

                            BOOK_ID = json.getString(EPUB_ID_PARAMETER);
                            revision = json.getString(REVISION_PARAMETER);
                            // Set process count default = 0
                            processTotal = 0;

                            if (UtConfig.LIBRARY_CODE.equalsIgnoreCase("2030"))
                                json.put(FORMAT_TYPE_PARAMETER, "Book");

                            if (json.getString(FORMAT_TYPE_PARAMETER).equalsIgnoreCase("Canvas")) {
                                pdfRaw = new ArrayList<>();
                                mediaRaw = new ArrayList<>();
                                galleryRaw = new ArrayList<>();

                                ArrayList<MediaItem> mediaArray = null;

                                int pdfPageTotal = Integer.parseInt(json.getString(PAGES_COUNT_PARAMETER));
                                for (int pdfCount = 0; pdfCount < pdfPageTotal; pdfCount++) {
                                    rootPath = json.getString("root_link").split("package/")[0] + "package/"
                                            + UtConvertRegion
                                            .ConvertToHTML(
                                                    json.getString("root_link").split("package/")[1]
                                                            .substring(0,
                                                                    json.getString("root_link")
                                                                            .split("package/")[1].length() - 1))
                                            + "/";
                                    pdfCheck = true;
                                    Log.w(TAG, "RootPath : " + rootPath);
                                    // getMedia
                                    if (!json.getJSONArray("page").getJSONObject(pdfCount).isNull("media")) {
                                        int totalMedia = json.getJSONArray("page").getJSONObject(pdfCount)
                                                .getJSONArray("media").length();
                                        mediaArray = new ArrayList<MediaItem>();
                                        for (int mediaCount = 0; mediaCount < totalMedia; mediaCount++) {

                                            // Set JSON type gallery
                                            if (json.getJSONArray("page").getJSONObject(pdfCount).getJSONArray("media")
                                                    .getJSONObject(mediaCount).optJSONObject("path") != null) {
                                                NLog.w(TAG, "In if case Set JSON type gallery");
                                                int totalSide = Integer
                                                        .parseInt(json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getJSONObject("path").getString("total_slide"));

                                                for (int side = 0; side < totalSide; side++) {

                                                    mediaArray.add(new MediaItem(
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getString("page_id"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getInt("x"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getInt("y"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getInt("width"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getInt("height"),
                                                            "",
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getJSONObject("path").getJSONArray("slide")
                                                                    .getJSONObject(side).getString("name"),
                                                            !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .isNull("imgplay")
                                                                    ? json.getJSONArray("page")
                                                                    .getJSONObject(pdfCount)
                                                                    .getJSONArray("media")
                                                                    .getJSONObject(mediaCount)
                                                                    .getString("imgplay")
                                                                    : "",
                                                            !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .isNull("imgpause")
                                                                    ? json.getJSONArray("page")
                                                                    .getJSONObject(pdfCount)
                                                                    .getJSONArray("media")
                                                                    .getJSONObject(mediaCount)
                                                                    .getString("imgpause")
                                                                    : "",
                                                            // "","",
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getString("type"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getString("effect"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getString("option"),
                                                            String.valueOf(json.getJSONArray("page")
                                                                    .getJSONObject(pdfCount).getJSONArray("media")
                                                                    .getJSONObject(mediaCount).getJSONObject("path")
                                                                    .getJSONArray("slide").length()),
                                                            null  // Media Property
                                                    ));

                                                    galleryRaw.add(new GalleryItem(
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getString("page_id"),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getJSONObject("path").getJSONArray("slide")
                                                                    .getJSONObject(side).getString("title"),
                                                            getHtmlMediaPath(json.getJSONArray("page")
                                                                    .getJSONObject(pdfCount).getJSONArray("media")
                                                                    .getJSONObject(mediaCount).getJSONObject("path")
                                                                    .getJSONArray("slide").getJSONObject(side)
                                                                    .getString("path")),
                                                            json.getJSONArray("page").getJSONObject(pdfCount)
                                                                    .getJSONArray("media").getJSONObject(mediaCount)
                                                                    .getJSONObject("path").getJSONArray("slide")
                                                                    .getJSONObject(side).getString("name")));
                                                }

                                                galleryCheck = true;// end
                                                // gallery
                                            } else {
                                                NLog.w(TAG, "Out if case Set JSON type gallery");

                                                mediaArray.add(new MediaItem(
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("page_id"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("x"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("y"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("width"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("height"),
                                                        getHtmlMediaPath(json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("path")),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("name"),
                                                        // "","",
                                                        !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .isNull("imgplay") ? json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("imgplay")
                                                                : "",
                                                        !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .isNull("imgpause") ? json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("imgpause")
                                                                : "",
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("type"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("effect"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("option"),
                                                        "1",
                                                        null // Media Property
                                                ));

                                                mediaRaw.add(new MediaItem(
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("page_id"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("x"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("y"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("width"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getInt("height"),
                                                        getHtmlMediaPath(json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("path")),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("name"),
                                                        !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .isNull("imgplay") ? json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("imgplay")
                                                                : "",
                                                        !json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .isNull("imgpause") ? json.getJSONArray("page")
                                                                .getJSONObject(pdfCount).getJSONArray("media")
                                                                .getJSONObject(mediaCount).getString("imgpause")
                                                                : "",
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("type"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("effect"),
                                                        json.getJSONArray("page").getJSONObject(pdfCount)
                                                                .getJSONArray("media").getJSONObject(mediaCount)
                                                                .getString("option"),
                                                        "1",
                                                        null // Media Property
                                                ));
                                            }
                                        }
                                        mediaCheck = true;
                                    } else {
                                        mediaArray = null;
                                    }
                                    // oom
                                    PdfRawItem pdfr = new PdfRawItem(
                                            json.getJSONArray("page").getJSONObject(pdfCount).getString("page_ID"),
                                            json.getJSONArray("page").getJSONObject(pdfCount).getString("page_name"),
                                            rootPath + json.getJSONArray("page").getJSONObject(pdfCount)
                                                    .getString("pdf_link").split("/")[0]
                                                    + "/"
                                                    + UtConvertRegion.ConvertToHTML(
                                                    json.getJSONArray("page").getJSONObject(pdfCount)
                                                            .getString("pdf_link").split("/")[1]),
                                            json.getString("root_link"),
                                            1.0592170818505 + "", //json.getJSONArray("page").getJSONObject(pdfCount).getString("multiplier"),
                                            mediaArray);
                                    pdfRaw.add(pdfr);
                                }

                                analyzeCheck = true;

                                if (pdfCheck) {
                                    processTotal++;
                                }
                                if (mediaCheck) {
                                    processTotal++;
                                }
                                if (imageCheck) {
                                    processTotal++;
                                }
                                if (galleryCheck) {
                                    processTotal++;
                                }
                                if (analyzeCheck) {
                                    processTotal++;
                                }

                                if (!json.isNull("content")) {
                                    int totalContent = json.getJSONArray("content").length();
                                    ArrayList<ContentItem> dContent = new ArrayList<ContentItem>();
                                    for (int contentCount = 0; contentCount < totalContent; contentCount++) {

                                        dContent.add(new ContentItem(
                                                json.getJSONArray("content").getJSONObject(contentCount)
                                                        .getString("name"),
                                                json.getJSONArray("content").getJSONObject(contentCount)
                                                        .optInt("jumpto")));
                                    }

                                    if (dContent != null && dContent.size() > 0) {
                                        writeContentFile(dContent);

                                    }
                                }
                                asynDownload = new DownloadFile();
                                asynDownload.execute(pdfRaw);
                                statusDownloadCurrent = DOWNLOAD_FILE;
                                // new DownloadFile().execute(pdfRaw);
                                Log.d("Download", "Book");
                            } else if (json.getString(FORMAT_TYPE_PARAMETER).equalsIgnoreCase("Cartoon")) {
                                asynDownloadCartoon = new DownloadCartoon();
                                asynDownloadCartoon.execute(json);
                                statusDownloadCurrent = DOWNLOAD_CARTOON;
                                // new DownloadCartoon().execute(json);
                                Log.d("Download", json.toString());
                                Log.d("Download", "Cartoon");

                                // Canvas OOM
                            } else if (json.getString(FORMAT_TYPE_PARAMETER).equalsIgnoreCase("Book")) {
                                pdfRaw = new ArrayList<>();
                                mediaRaw = new ArrayList<>();
                                galleryRaw = new ArrayList<>();
                                ArrayList<MediaItem> mediaArray = null;
                                JSONArray canvasArray = json.getJSONArray(PDF_CANVAS_PARAMETER);

                                String rootLink = "";
                                if(!json.isNull(ROOT_LINK_PARAMETER)){
                                    rootLink = json.getString(ROOT_LINK_PARAMETER);
                                }
                                DatabaseManager dataManager = new DatabaseManager(ac);

                                int pdfPageTotal = Integer.parseInt(json.getString(PAGES_COUNT_PARAMETER));
                                for (int pdfCount = 0; pdfCount < pdfPageTotal; pdfCount++) {
                                    rootPath = json.getString(ROOT_LINK_PARAMETER);
                                    pdfCheck = true;
                                    JSONObject canvasObject = canvasArray.getJSONObject(pdfCount);

                                    if(!canvasObject.isNull(IMG_PARAMETER)){
                                        String imageUrl = rootLink + canvasObject.getString(IMG_PARAMETER);
                                        dataManager.addShareLink(new ShareLink(BOOK_ID,imageUrl,pdfCount));
                                    }

                                    if (!canvasObject.isNull(CANVAS_OBJECT_PARAMETER)) {

                                        mediaArray = new ArrayList<>();
                                        JSONArray arrayObjectInCanvas = canvasObject.getJSONArray(CANVAS_OBJECT_PARAMETER);
                                        int totalMedia = arrayObjectInCanvas.length();

                                        int totalGallery = 0;

                                        for (int mediaCount = 0; mediaCount < totalMedia; mediaCount++) {
                                            JSONObject objectItem = arrayObjectInCanvas.getJSONObject(mediaCount);
                                            // Set JSON type gallery


                                            if (!objectItem.isNull("type")) {
                                                if (objectItem.getString("type").equalsIgnoreCase("html5")) {
                                                    html5Thumbnail = "";
                                                    if (!objectItem.isNull("property")) {
                                                        String str = getHtml5Thumbnail(objectItem.getJSONObject("property"));
                                                        if (str.length() > 0) {
                                                            html5Thumbnail = json.getString("root_link") + str;
                                                        }
                                                    }
                                                }
                                            }

                                            // Get Media Property Object
                                            // Add by Chain 20/01/2017
                                            MediaProperty mediaPropertyObject = null;
                                            if (!objectItem.isNull(PROPERTY_PARAMETER)) {
                                                mediaPropertyObject = getMediaPropertyObject(objectItem.getJSONObject(PROPERTY_PARAMETER));
                                            }

                                            if (getTypeCanvasObject(objectItem.getString(TYPE_PARAMETER)).equalsIgnoreCase("gallery")) {
                                                totalGallery++;

                                                // Set JSON Slide
                                                if (!objectItem.optJSONObject(PATH_PARAMETER).isNull(SLIDE_PARAMETER)) {
                                                    JSONArray slideArray = objectItem.optJSONObject(PATH_PARAMETER).getJSONArray(SLIDE_PARAMETER);
                                                    int totalSide = slideArray.length();
                                                    ArrayList<SlideItem> slideItems = new ArrayList<>();
                                                    for (int side = 0; side < totalSide; side++) {
                                                        JSONObject slideObject = slideArray.getJSONObject(side);

                                                        SlideItem item = new SlideItem(
                                                                objectItem.optString(CANVAS_ID_PARAMETER)
                                                                , objectItem.optString(INSERT_ID_PARAMETER)
                                                                , rootPath + "img/" + slideObject.getString(SOURCE_PARAMETER)
                                                                , slideObject.getString(SOURCE_PARAMETER)
                                                        );

                                                        slideItems.add(item);

                                                        GalleryItem galleryItem = new GalleryItem();
                                                        galleryItem.setName(slideObject.getString(SOURCE_PARAMETER));
                                                        galleryItem.setPageid(objectItem.getString(CANVAS_ID_PARAMETER));
                                                        galleryItem.setPath("img/" + galleryItem.getName());
                                                        galleryItem.setGroup(objectItem.optInt(INSERT_ID_PARAMETER));
                                                        galleryItem.setNumber(side);
                                                        galleryItem.setTitle("");

                                                        galleryRaw.add(galleryItem);
                                                    }

                                                    mediaArray.add(new MediaItem(
                                                            objectItem.getString(CANVAS_ID_PARAMETER)
                                                            , objectItem.getInt(X_PARAMETER)
                                                            , objectItem.getInt(Y_PARAMETER)
                                                            , objectItem.getInt(WIDTH_PARAMETER)
                                                            , objectItem.getInt(HEIGHT_PARAMETER)
                                                            , rootPath + "img/" + slideArray.getJSONObject(0).getString(SOURCE_PARAMETER)
                                                            , slideArray.getJSONObject(0).getString(SOURCE_PARAMETER)
                                                            , ""
                                                            , ""
                                                            , objectItem.getString(TYPE_PARAMETER)
                                                            , objectItem.getString(EFFECT_PARAMETER)
                                                            , ""
                                                            , slideItems.size() + ""
                                                            , slideItems
                                                            , objectItem.optInt(INSERT_ID_PARAMETER),
                                                            mediaPropertyObject));
                                                }
                                                galleryCheck = true;
                                            } else {
                                                String option = "";
                                                String type_media = "";

                                                if (objectItem.getString(LINK_PARAMETER).equalsIgnoreCase("true")) {

                                                    option = objectItem.getString(HYPER_TEXT_PARAMETER);
                                                    type_media = objectItem.getString(LINK_TYPE_PARAMETER);


                                                } else {
                                                    if (objectItem.getString(TYPE_PARAMETER).equalsIgnoreCase("youtube")) {
                                                        option = objectItem.getString(OPTION_PARAMETER);
                                                    } else if (objectItem.getString(TYPE_PARAMETER).equalsIgnoreCase("html5")) {
                                                        option = objectItem.getString(HYPER_TEXT_PARAMETER);

                                                    } else {
                                                        option = "";
                                                    }
                                                    type_media = objectItem.getString(TYPE_PARAMETER);
                                                }
                                                if (type_media.equalsIgnoreCase("youtube")) {

                                                    mediaArray.add(
                                                            new MediaItem(
                                                                    objectItem.getString(CANVAS_ID_PARAMETER)
                                                                    , objectItem.getInt(X_PARAMETER)
                                                                    , objectItem.getInt(Y_PARAMETER)
                                                                    , objectItem.getInt(WIDTH_PARAMETER)
                                                                    , objectItem.getInt(HEIGHT_PARAMETER)
                                                                    , objectItem.getString(PATH_PARAMETER)
                                                                    , objectItem.getString(NAME_PARAMETER)
                                                                    , imageTransparent
                                                                    , imageTransparent
                                                                    , type_media
                                                                    , objectItem.getString(EFFECT_PARAMETER)
                                                                    , option
                                                                    , "1",
                                                                    mediaPropertyObject
                                                            ));
                                                } else {
                                                    NLog.w(TAG, "HTML5 Media patch : " + objectItem.getString(PATH_PARAMETER));
                                                    mediaArray.add(
                                                            new MediaItem(
                                                                    objectItem.getString(CANVAS_ID_PARAMETER)
                                                                    , objectItem.getInt(X_PARAMETER)
                                                                    , objectItem.getInt(Y_PARAMETER)
                                                                    , objectItem.getInt(WIDTH_PARAMETER)
                                                                    , objectItem.getInt(HEIGHT_PARAMETER)
                                                                    //, objectItem.getString(PATH_PARAMETER) //getHtmlMediaPath(objectItem.getString(PATH_PARAMETER))
                                                                    , objectItem.getString(PATH_PARAMETER) //getHtmlMediaPath(objectItem.getString(PATH_PARAMETER))
                                                                    , objectItem.getString(NAME_PARAMETER)
                                                                    , imageTransparent
                                                                    , imageTransparent
                                                                    , type_media
                                                                    , objectItem.getString(EFFECT_PARAMETER)
                                                                    , option
                                                                    , objectItem.optString("imgplay_show") != null && objectItem.optString("imgplay_show").equalsIgnoreCase("1"),
                                                                    mediaPropertyObject
                                                            ));
                                                }

                                                mediaRaw.add(
                                                        new MediaItem(
                                                                objectItem.getString(CANVAS_ID_PARAMETER)
                                                                , objectItem.getInt(X_PARAMETER)
                                                                , objectItem.getInt(Y_PARAMETER)
                                                                , objectItem.getInt(WIDTH_PARAMETER)
                                                                , objectItem.getInt(HEIGHT_PARAMETER)
                                                                , objectItem.getString(PATH_PARAMETER) //getHtmlMediaPath(objectItem.getString(PATH_PARAMETER))
                                                                , objectItem.getString(NAME_PARAMETER)
                                                                , imageTransparent
                                                                , imageTransparent
                                                                , type_media
                                                                , objectItem.getString(EFFECT_PARAMETER)
                                                                , option
                                                                , objectItem.optString("imgplay_show").equalsIgnoreCase("1") ? "1" : "0",
                                                                mediaPropertyObject));
                                            }
                                        }
                                        mediaCheck = true;
                                    } else {
                                        mediaArray = null;
                                    }

                                    PdfRawItem pdfr = new PdfRawItem(
                                            canvasObject.getString(CANVAS_ID_PARAMETER)
                                            , canvasObject.getString(PAGE_NAME_PARAMETER)
                                            , rootPath + canvasObject.getString(PDF_LINK_PARAMETER).split("/")[0]
                                            + "/" + canvasObject.getString(PDF_LINK_PARAMETER).split("/")[1]
                                            , json.getString(ROOT_LINK_PARAMETER)
                                            , 1.0592170818505 + ""
                                            //, 1024.0 / Float.parseFloat(canvasObject.getString(HEIGHT_PARAMETER)) - 0.0052 + "", //// "1.538",
                                            , mediaArray
                                            , json.getString("width")
                                            , json.getString("height")
                                    );

                                    try {
                                        pdfRaw.add(pdfr);
                                    } catch (Exception e) {
                                        e.getStackTrace();
                                    }
                                }
                                analyzeCheck = true;

                                if (pdfCheck) {
                                    processTotal++;
                                }

                                if (mediaCheck) {
                                    processTotal++;
                                }
                                if (imageCheck) {
                                    processTotal++;
                                }
                                if (galleryCheck) {
                                    processTotal++;
                                }

                                if (analyzeCheck) {
                                    processTotal++;
                                }

                                if (!json.isNull("content")) {
                                    int totalContent = json.getJSONArray("content").length();
                                    ArrayList<ContentItem> dContent = new ArrayList<ContentItem>();
                                    for (int contentCount = 0; contentCount < totalContent; contentCount++) {

                                        dContent.add(new ContentItem(
                                                json.getJSONArray("content").getJSONObject(contentCount)
                                                        .getString("name"),
                                                json.getJSONArray("content").getJSONObject(contentCount)
                                                        .optInt("jumpto")));
                                    }

                                    if (dContent != null && dContent.size() > 0) {
                                        writeContentFile(dContent);
                                    }
                                }

                                asynDownload = new DownloadFile();
                                asynDownload.execute(pdfRaw);
                                statusDownloadCurrent = DOWNLOAD_FILE;
                                // new DownloadFile().execute(pdfRaw);
                                Log.d("Download", "Canvas");

                            }
                        } else {
                            // Toast.makeText(aq.getContext(), "Please check
                            // your internet connection",
                            // Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(aq.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        };
        cb.header("User-Agent", "android");
        final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE);

        String userName = sPref.getString(SnPreferenceVariable.USER_NAME, "");
        String pGroup = sPref.getString(SnPreferenceVariable.GROUP, "");

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("t", "1"));
        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
        pairs.add(new BasicNameValuePair("id", id));

        pairs.add(new BasicNameValuePair("login", userName));
        pairs.add(new BasicNameValuePair("pgroup", pGroup));
        // for canvas
        pairs.add(new BasicNameValuePair("canvas", "1"));
        pairs.add(new BasicNameValuePair("type", "5"));


        NLog.w(TAG, TAG + " t " + 1);
        NLog.w(TAG, TAG + " library " + UtConfig.LIBRARY_CODE);
        NLog.w(TAG, TAG + " id " + id);
        NLog.w(TAG, TAG + " login " + userName);
        NLog.w(TAG, TAG + " pgroup " + pGroup);
        NLog.w(TAG, TAG + " canvas " + 1);
        NLog.w(TAG, TAG + " type " + 5);


        // pairs.add(new BasicNameValuePair("library", "0010"));
        // pairs.add(new BasicNameValuePair("id", "2406"));
        // pairs.add(new BasicNameValuePair("username",
        // sPref.getString(SnPreferenceVariable.USER_NAME, "")));
        // pairs.add(new BasicNameValuePair("pgroup",
        // sPref.getString(SnPreferenceVariable.GROUP, "0")));

        HttpEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(pairs, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);

        aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
    }

    public void clearStat() {
        processCount = 0;
        processTotal = 0;
        switch (statusDownloadCurrent) {
            case DOWNLOAD_FILE:
                if (asynDownload != null)
                    asynDownload.cancel(true);
                break;
            case DOWNLOAD_CARTOON:
                if (asynDownloadCartoon != null)
                    asynDownloadCartoon.cancel(true);
                break;
            case DOWNLOAD_MEDIA:
                if (asynDownloadMedia != null)
                    asynDownloadMedia.cancel(true);
                break;
            case DOWNLOAD_SYNC:
                if (asynSynce != null)
                    asynSynce.cancel(true);
                break;
            case DOWNLOAD_GALLERY:
                if (asynDownloadGallery != null)
                    asynDownloadGallery.cancel(true);
                break;
            default:
                break;
        }
    }

    public void IndexExam(String exam) {


    }

    public void DnMediaExam(String type, String ExamID, String examid, String media) {

        String Link = "", Path = "";

        if (type.equalsIgnoreCase("choice")) {
            Link = ExamMediaLink + "objective/images/" + examid + "/" + media;
            Path = DB_PATH + "exam/" + ExamID + "/choice/" + examid;
            File f = new File(Path);
            if (!f.exists()) {
                f.mkdirs();
            }
        } else if (type.equalsIgnoreCase("match")) {
            Link = ExamMediaLink + "match/images/" + examid + "/" + media;
            Path = DB_PATH + "exam/" + ExamID + "/match/" + examid;
            File f = new File(Path);
            if (!f.exists()) {
                f.mkdirs();
            }
        } else if (type.equalsIgnoreCase("sound")) {
            Link = ExamMediaLink + "soundquiz/images/" + examid + "/" + media;
            Path = DB_PATH + "exam/" + ExamID + "/sound/" + examid;
            File f = new File(Path);
            if (!f.exists()) {
                f.mkdirs();
            }
        }

        Log.i("Ch.DnURL", Link);
        Log.i("Ch.DnURL", Path);

        doDownload(Link, Path, media);

		
		/*
            try {
            	
            	URL url = new URL(Link);
                URLConnection connection = url.openConnection();
                connection.connect();  
                
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(Path+"/"+media);
                
            	  byte data[] = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		*/
    }


    protected void doDownload(final String urlLink, final String path, final String filename) {
        Thread dx = new Thread() {

            public void run() {

                String Path = path + "/" + filename;
                Log.e("Ch.Dnnn", "exMedia : " + urlLink);
                Log.e("Ch.Dnnn", "exMedia : " + Path);


                try {
                    URL url = new URL(urlLink);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    // this will be useful so that you can show a typical 0-100% progress bar
                    @SuppressWarnings("unused")
                    int fileLength = connection.getContentLength();

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream());
                    OutputStream output = new FileOutputStream(Path);

                    byte data[] = new byte[1024];
                    @SuppressWarnings("unused")
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;

                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("DOWNLOADING FILES", "ERROR IS" + e);
                }
            }
        };
        dx.start();
    }


    public void CreateExam(final String exam_id) {

        final String exam = exam_id;


        Log.e("DnURL", "CreateExam ID : " + exam);

        //String examPath =DB_PATH+"exam/"+exam_id+"/";

        File f = new File(DB_PATH + "exam/" + exam);
        if (!f.exists()) {
            f.mkdirs();
        }


        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json,
                                 AjaxStatus status) {
                super.callback(url, json, status);

                if (json != null) {
                    try {

                        Log.d("DnURL", json.toString());
                        if (json.getString("status").equals("1")) {

                            totalExam = json.getJSONArray("data").length();
                            Log.d("DnURL", "TotalExam : " + totalExam);

                            try {

                                FileWriter exW = new FileWriter(DB_PATH + "exam/" + exam + "/exam.txt");
                                BufferedWriter ee = new BufferedWriter(exW);

                                ee.write(totalExam + "\n");

                                for (int section = 0; section < totalExam; section++) { //section=ข้อที่

                                    ee.write("section" + section + "\n");

                                    //question_id = json.getJSONArray("data").getJSONObject(section).getString("id");
                                    exam_type = json.getJSONArray("data").getJSONObject(section).getString("exam_type");

                                    Log.e("DnURL", section + " Question : " + question_id);


                                    //////////////////////choice/////////////////////////
                                    if (exam_type.equalsIgnoreCase("choice")) {
                                        Log.d("DnURL", "choice");


                                        File f = new File(DB_PATH + "exam/" + exam + "/choice");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                        }

                                        String ch_id = json.getJSONArray("data").getJSONObject(section).getString("id");
                                        String question = json.getJSONArray("data").getJSONObject(section).getString("question");
                                        String picture = json.getJSONArray("data").getJSONObject(section).getString("picture");
                                        String answer = json.getJSONArray("data").getJSONObject(section).getString("answer");
                                        Log.d("DnURL", "Question : " + question + " Answer : " + answer);

                                        if (!json.getJSONArray("data").getJSONObject(section).isNull("picture")) {
                                            DnMediaExam("choice", exam_id, ch_id, picture);
                                        }


                                        ee.write("choice\n");
                                        ee.write(ch_id + "\n");


                                        try {

                                            FileWriter choiceW = new FileWriter(DB_PATH + "exam/" + exam + "/choice/" + ch_id + ".txt");
                                            BufferedWriter bf = new BufferedWriter(choiceW);


                                            bf.write(question + "\n");
                                            bf.write(picture + "\n");
                                            bf.write(answer + "\n");


                                            int totalChoice = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").length();

                                            bf.write(totalChoice + "\n");

                                            Log.d("DnURL", "TotalChoice : " + totalChoice);
                                            for (int choice = 0; choice < totalChoice; choice++) {

                                                bf.write("choice" + choice + "\n");

                                                String choice_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("id");
                                                String choice_text = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("title");
                                                String choice_picture = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("picture");

                                                if (!json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).isNull("picture")) {

                                                    DnMediaExam("choice", exam_id, ch_id, choice_picture);
                                                }

                                                Log.d("DnURL", "Choice : " + choice_id + " Text : " + choice_text);

                                                bf.write(choice_id + "\n");
                                                bf.write(choice_text + "\n");
                                                bf.write(choice_picture + "\n");

                                            }

									 /*
                                            Log.d("Chompoo", "content : "+Content);
											bf.write(Content);*/
                                            bf.close();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }


                                    //////////////////////sound/////////////////////////
                                    if (exam_type.equalsIgnoreCase("sound")) {
                                        Log.d("DnURL", "sound");

                                        File f = new File(DB_PATH + "exam/" + exam + "/sound");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                        }

                                        String sound_id = json.getJSONArray("data").getJSONObject(section).getString("id");

                                        ee.write("sound\n");
                                        ee.write(sound_id + "\n");

                                        try {

                                            FileWriter soundW = new FileWriter(DB_PATH + "exam/" + exam + "/sound/" + sound_id + ".txt");
                                            BufferedWriter bf = new BufferedWriter(soundW);


                                            String quest_txt = json.getJSONArray("data").getJSONObject(section).getString("text");
                                            String quest_sound = json.getJSONArray("data").getJSONObject(section).getString("sound");
                                            String quest_img = json.getJSONArray("data").getJSONObject(section).getString("picture");

                                            bf.write(quest_txt + "\n");
                                            bf.write(quest_sound + "\n");
                                            bf.write(quest_img + "\n");

                                            if (!json.getJSONArray("data").getJSONObject(section).isNull("sound")) {
                                                DnMediaExam("sound", exam_id, sound_id, quest_sound);
                                            }

                                            if (!json.getJSONArray("data").getJSONObject(section).isNull("picture")) {
                                                DnMediaExam("sound", exam_id, sound_id, quest_img);
                                            }


                                            int totalChoice = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").length();

                                            bf.write(totalChoice + "\n");

                                            Log.d("DnURL", "TotalChoice : " + totalChoice);
                                            for (int choice = 0; choice < totalChoice; choice++) {

                                                bf.write("choice" + choice + "\n");

                                                String choice_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("id");
                                                String choice_text = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("text");
                                                String choice_sound = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("sound");
                                                String choice_picture = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("picture");
                                                String answer = json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).getString("isAnswer");

                                                if (!json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).isNull("sound")) {
                                                    DnMediaExam("sound", exam_id, sound_id, choice_sound);
                                                }

                                                if (!json.getJSONArray("data").getJSONObject(section).getJSONArray("choice").getJSONObject(choice).isNull("picture")) {
                                                    DnMediaExam("sound", exam_id, sound_id, choice_picture);
                                                }


                                                bf.write(choice_id + "\n");
                                                bf.write(choice_text + "\n");
                                                bf.write(choice_sound + "\n");
                                                bf.write(choice_picture + "\n");
                                                bf.write(answer + "\n");
                                            }

									/*Log.d("Chompoo", "content : "+Content);
                                    bf.write(Content);*/
                                            bf.close();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }


                                    }


                                    //////////////////////match/////////////////////////
                                    if (exam_type.equalsIgnoreCase("match")) {
                                        Log.d("DnURL", "match");


                                        File f = new File(DB_PATH + "exam/" + exam + "/match");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                        }

                                        String IDmatch = json.getJSONArray("data").getJSONObject(section).getString("id");

                                        ee.write("match\n");
                                        ee.write(IDmatch + "\n");


                                        try {

                                            FileWriter matchW = new FileWriter(DB_PATH + "exam/" + exam + "/match/" + IDmatch + ".txt");
                                            BufferedWriter bf = new BufferedWriter(matchW);

                                            int totalMatch = json.getJSONArray("data").getJSONObject(section).getJSONArray("quest").length();

                                            //bf.write(totalMatch+"\n+++question+++\n---\n");
                                            bf.write(totalMatch + "\n");

                                            Log.d("DnURL", "TotalMatch : " + totalMatch);

                                            for (int match = 0; match < totalMatch; match++) {

                                                bf.write("question" + match + "\n");

                                                String match_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("quest").getJSONObject(match).getString("id");
                                                String match_question = json.getJSONArray("data").getJSONObject(section).getJSONArray("quest").getJSONObject(match).getString("question");
                                                String match_type = json.getJSONArray("data").getJSONObject(section).getJSONArray("quest").getJSONObject(match).getString("type");
                                                String match_pair = json.getJSONArray("data").getJSONObject(section).getJSONArray("quest").getJSONObject(match).getString("pair");
                                                Log.d("DnURL", "Match Quest : " + match_question + " type : " + match_type + " pair : " + match_pair);

                                                if (match_type.equalsIgnoreCase("1")) {
                                                    DnMediaExam("match", exam_id, IDmatch, match_question);
                                                }

                                                bf.write(match_id + "\n");
                                                bf.write(match_question + "\n");
                                                bf.write(match_type + "\n");
                                                bf.write(match_pair + "\n");


                                            }

                                            //bf.write("+++pair+++\n---\n");

                                            for (int match = 0; match < totalMatch; match++) {

                                                bf.write("pair" + match + "\n");

                                                String pair_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("pair").getJSONObject(match).getString("id");
                                                String pair_position = json.getJSONArray("data").getJSONObject(section).getJSONArray("pair").getJSONObject(match).getString("position");
                                                String pair_text = json.getJSONArray("data").getJSONObject(section).getJSONArray("pair").getJSONObject(match).getString("text");
                                                Log.d("DnURL", "Match Pair : " + pair_text + " position : " + pair_position);

                                                bf.write(pair_id + "\n");
                                                bf.write(pair_position + "\n");
                                                bf.write(pair_text + "\n");

                                            }

                                            bf.close();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }


                                    //////////////////////sort/////////////////////////
                                    if (exam_type.equalsIgnoreCase("sort")) {
                                        Log.d("DnURL", "sort");


                                        File f = new File(DB_PATH + "exam/" + exam + "/sort");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                        }

                                        String sort_id = json.getJSONArray("data").getJSONObject(section).getString("id");

                                        ee.write("sort\n");
                                        ee.write(sort_id + "\n");

                                        try {

                                            FileWriter sortW = new FileWriter(DB_PATH + "exam/" + exam + "/sort/" + sort_id + ".txt");
                                            BufferedWriter bf = new BufferedWriter(sortW);

                                            int totalSort = json.getJSONArray("data").getJSONObject(section).getJSONArray("group").length();

                                            bf.write(totalSort + "\n");

                                            Log.d("DnURL", "TotalSort : " + totalSort);
                                            for (int sort = 0; sort < totalSort; sort++) {

                                                bf.write("group" + sort + "\n");

                                                String group_word = json.getJSONArray("data").getJSONObject(section).getJSONArray("group").getJSONObject(sort).getString("word");
                                                String group_position = json.getJSONArray("data").getJSONObject(section).getJSONArray("group").getJSONObject(sort).getString("position");
                                                Log.d("DnURL", "Sort word : " + group_word + " position : " + group_position);

                                                bf.write(group_word + "\n");
                                                bf.write(group_position + "\n");

                                            }


                                            for (int sort = 0; sort < totalSort; sort++) {

                                                bf.write("answer" + sort + "\n");

                                                String answer_word = json.getJSONArray("data").getJSONObject(section).getJSONArray("answer").getJSONObject(sort).getString("word");
                                                String answer_position = json.getJSONArray("data").getJSONObject(section).getJSONArray("answer").getJSONObject(sort).getString("position");
                                                Log.d("DnURL", "Sort ans word : " + answer_word + " position : " + answer_position);

                                                bf.write(answer_word + "\n");
                                                bf.write(answer_position + "\n");

                                            }


                                            bf.close();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }


                                    //////////////////////fill/////////////////////////
                                    if (exam_type.equalsIgnoreCase("fill")) {
                                        Log.d("DnURL", "fill");


                                        File f = new File(DB_PATH + "exam/" + exam + "/fill");
                                        if (!f.exists()) {
                                            f.mkdirs();
                                        }

                                        String IDfill = json.getJSONArray("data").getJSONObject(section).getString("id");

                                        ee.write("fill\n");
                                        ee.write(IDfill + "\n");


                                        try {

                                            FileWriter fillW = new FileWriter(DB_PATH + "exam/" + exam + "/fill/" + IDfill + ".txt");
                                            BufferedWriter bf = new BufferedWriter(fillW);

                                            int totalFill = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").length();

                                            bf.write(totalFill + "\n");

                                            Log.d("DnURL", "TotalFill : " + totalFill);
                                            for (int fill = 0; fill < totalFill; fill++) {

                                                bf.write("content" + fill + "\n");

                                                String fill_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getString("id");
                                                String fill_type = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getString("type");
                                                Log.d("DnURL", "Fill ID : " + fill_id + " TYPE : " + fill_type);

                                                bf.write(fill_id + "\n");
                                                bf.write(fill_type + "\n");

                                                if (fill_type.equalsIgnoreCase("1")) {
                                                    String fill_text = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getString("text");
                                                    Log.d("DnURL", "Fill_1 TEXT : " + fill_text);

                                                    bf.write(fill_text + "\n");
                                                } else {

                                                    int totalFillChoice = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getJSONArray("choice").length();
                                                    Log.d("DnURL", "TotalFillChoice : " + totalFillChoice);

                                                    bf.write(totalFillChoice + "\n");
                                                    //bf.write("+++\n");

                                                    for (int fillChoice = 0; fillChoice < totalFillChoice; fillChoice++) {

                                                        bf.write("choice" + fillChoice + "\n");

                                                        String choice_id = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getJSONArray("choice").getJSONObject(fillChoice).getString("id");
                                                        String choice_text = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getJSONArray("choice").getJSONObject(fillChoice).getString("text");
                                                        String choice_isAnswer = json.getJSONArray("data").getJSONObject(section).getJSONArray("content").getJSONObject(fill).getJSONArray("choice").getJSONObject(fillChoice).getString("isAnswer");
                                                        Log.d("DnURL", "Fill_2 ID : " + choice_id + " Text : " + choice_text + " isAnswer : " + choice_isAnswer);

                                                        bf.write(choice_id + "\n");
                                                        bf.write(choice_text + "\n");
                                                        bf.write(choice_isAnswer + "\n");

                                                    }

                                                }

                                                //bf.write("---\n");

                                            }

                                            bf.close();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }

                                }

                                ee.close();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(aq.getContext(), e.getMessage() , Toast.LENGTH_LONG).show();
                    }
                }


            }
        };
        cb.header("User-Agent", "android");
        @SuppressWarnings({"unused", "static-access"})
        final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE);

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        //pairs.add(new BasicNameValuePair("t", "1"));
        //pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
        //pairs.add(new BasicNameValuePair("test", "1"));
        //pairs.add(new BasicNameValuePair("offline", "2"));
        pairs.add(new BasicNameValuePair("id", exam_id));
        //pairs.add(new BasicNameValuePair("username", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
        //pairs.add(new BasicNameValuePair("pgroup", sPref.getString(SnPreferenceVariable.GROUP, "0")));

        HttpEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(pairs, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);

        aq.ajax(UtSharePreferences.getPrefServerExamConfig(ac), params, JSONObject.class, cb);


    }


    public void DnExam(ArrayList<MediaItem>... examRawItem) {


    }


    public void resetStatForDownload(String cat_id) {

        clearStat();

        File f = new File(UtConfig.DESIGN_PATH + cat_id);
        if (f.isDirectory()) {
            String[] children = f.list();
            for (int i = 0; i < children.length; i++) {
                new File(f, children[i]).delete();
            }
        }

        downloadLinkById(cat_id);
    }

    public class SetLoadSavedFavorite extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {
        private DbBookHandler bookHandler;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bookHandler = new DbBookHandler(aq.getContext());
            bookHandler.open();
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
            int type = 0;
            if (params[0].equals(BkShelfLibrary.TITLE)) type = 1;
            List<DbBook> data = bookHandler.sort(type);
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < data.size(); i++) {
                HashMap<String, String> hash = new HashMap<String, String>();
                hash.put("id", data.get(i).getId());
                hash.put("name", data.get(i).getTitle());
                hash.put("datetime", data.get(i).getDate());
                hash.put("description", data.get(i).getDetail());
                hash.put("category", data.get(i).getCategory());
                hash.put("image", data.get(i).getImage());
                hash.put("revision", data.get(i).getRevision());
                arrayList.add(hash);
            }

            return arrayList;
        }
    }

    public class DownloadFile extends AsyncTask<ArrayList<PdfRawItem>, Integer, String> {
        int current = 0;
        int totalRow = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate it within the onCreate method
            processCount++;
            pb.setMax(processTotal * 100);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(ArrayList<PdfRawItem>... sUrl) {

            Configuration config = ac.getResources().getConfiguration();
            if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
                ac.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                ac.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }


            for (ArrayList<PdfRawItem> pdf : sUrl) {
                int rows = pdf.size();
                totalRow = pdf.size();
                while (current < rows) {

                    try {
                        if (isCancelled()) {
                            break;
                        }

                        try {

                            URL url = new URL(pdf.get(current).getPdfPath().replace("$", UtSharePreferences.getPrefServerName(ac)));
                            URLConnection connection = url.openConnection();
                            connection.connect();
                            // this will be useful so that you can show a typical 0-100% progress bar
                            @SuppressWarnings("unused")
                            int fileLength = connection.getContentLength();
                            // download the file
                            File f = new File(DB_PATH);
                            if (!f.exists()) {
                                f.mkdirs();
                            }
                            InputStream input = new BufferedInputStream(url.openStream(), 8192);
                            // OutputStream output = new FileOutputStream(DB_PATH+"book.pdf");
                            OutputStream output = new FileOutputStream(DB_PATH + pdf.get(current).getPdfName() + ".pdf");

                            Log.e("Ch.Dnload", "DnFileOutPath : " + DB_PATH + pdf.get(current).getPdfName() + ".pdf");
                            byte data[] = new byte[1024];
                            @SuppressWarnings("unused")
                            long total = 0;
                            int count;

                            while ((count = input.read(data)) != -1) {
                                total += count;
//			                    publishProgress((int) (total * 100 / fileLength));
                                output.write(data, 0, count);
                            }


                            output.flush();
                            output.close();
                            input.close();
                            publishProgress((int) (current * (100) / totalRow));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Log.e("DOWNLOAD PDF", "DOWNLOAD ERROR CAUSE : " + ex.getMessage());
                            current = totalRow;
                        }
                        current++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        //@SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//        	mProgressDialog.dismiss();

            if (mediaRaw != null && mediaRaw.size() > 0) {
                asynDownloadMedia = new DownloadMediaFile();
                asynDownloadMedia.execute(mediaRaw);
                statusDownloadCurrent = DOWNLOAD_MEDIA;
//        		new DownloadMediaFile().execute(mediaRaw);
            } else if (galleryRaw != null && galleryRaw.size() > 0) {
                asynDownloadGallery = new DownloadGalleryFile();
                asynDownloadGallery.execute(galleryRaw);
                statusDownloadCurrent = DOWNLOAD_GALLERY;
            } else {
                asynSynce = new AnalyzeFile();
                asynSynce.execute(pdfRaw);
                statusDownloadCurrent = DOWNLOAD_SYNC;
//        		new AnalyzeFile().execute(pdfRaw);
            }


        }
    }


    private class DownloadMediaFile extends AsyncTask<ArrayList<MediaItem>, Integer, String> {
        int current = 0;
        int totalRow = 0;
        private BaseArrayList html5List = BaseArrayList.Builder(String.class);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processCount++;
            pb.setMax(processTotal * 100);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(ArrayList<MediaItem>... sUrl) {
            @SuppressWarnings("unused")
            String error = null;
            for (ArrayList<MediaItem> media : sUrl) {
                int rows = media.size();
                totalRow = media.size();

                String tempPageId = null, nameResult = "";
                int nameRunning = 1;

                while (current < rows) {
                    try {
                        if (isCancelled()) {
                            break;
                        }
                        try {
                            MediaItem currentMedia = media.get(current);
                            String aurl = (currentMedia.getPathMedia().contains("http") ||
                                    currentMedia.getPathMedia().contains("https") ? "" : rootPath) + currentMedia.getPathMedia();
                            aurl = aurl.replace("$", UtSharePreferences.getPrefServerName(ac));
                            URL url = new URL(aurl);

                            URLConnection connection = url.openConnection();
                            connection.connect();

                            // download the file
                            File f = new File(DB_PATH + currentMedia.getPageId());
                            if (!f.exists()) {
                                f.mkdirs();
                            }
                            InputStream input = new BufferedInputStream(url.openStream(), 8192);
                            String convertName = null;
                            String mediaName = currentMedia.getNameMedia().replace("_", "-");
                            String pathMedia = currentMedia.getPathMedia().replace("_", "-");
                            int numberRunning = 1;


                            boolean isTooltip = false;
                            boolean isVideo = false;
                            boolean isHtml5 = false;

                            if (mediaName.endsWith("jpg") || mediaName.endsWith("JPG")
                                    || mediaName.endsWith("png") || mediaName.endsWith("PNG")
                                    || mediaName.endsWith("bmp") || mediaName.endsWith("BMP")) {

                                convertName = pathMedia.replace(".jpg", "_" + numberRunning + ".jpg")
                                        .replace(".png", "_" + numberRunning + ".png")
                                        .replace(".bmp", "_" + numberRunning + ".bmp")
                                        .replace(".JPG", "_" + numberRunning + ".jpg")
                                        .replace(".PNG", "_" + numberRunning + ".png")
                                        .replace(".BMP", "_" + numberRunning + ".bmp").toLowerCase();

                            } else if (currentMedia.getPdfType().equalsIgnoreCase("tooltip")) {
                                convertName = pathMedia.replace(".jpg", "_" + numberRunning + ".jpg")
                                        .replace(".png", "_" + numberRunning + ".png")
                                        .replace(".bmp", "_" + numberRunning + ".bmp")
                                        .replace(".JPG", "_" + numberRunning + ".jpg")
                                        .replace(".PNG", "_" + numberRunning + ".png")
                                        .replace(".BMP", "_" + numberRunning + ".bmp").toLowerCase();

                                isTooltip = true;

                                if (convertName.endsWith("jpg")) typeTooltip = ".jpg";
                                else if (convertName.endsWith("png")) typeTooltip = ".png";
                                else if (convertName.endsWith("bmp")) typeTooltip = ".bmp";
                                else typeTooltip = ".xxx";
                            } else if (currentMedia.getPdfType().equalsIgnoreCase("vdo") || currentMedia.getPdfType().equalsIgnoreCase("video")) {
                                convertName = pathMedia.replace(".mp4", "_" + numberRunning + ".mp4")
                                        .replace(".MP4", "_" + numberRunning + ".mp4")
                                        .toLowerCase();
                                isVideo = true;
                            } else if (currentMedia.getPdfType().equalsIgnoreCase("html5")) {
                                isHtml5 = true;
                            } else {
                                convertName = mediaName.toLowerCase();
                            }

                            String currentPageId = currentMedia.getPageId();
                            if (tempPageId != null && tempPageId.equals(currentPageId)) {
                                nameResult = String.valueOf(nameRunning);
                                nameRunning++;
                            } else {
                                nameResult = String.valueOf(0);
                                tempPageId = currentPageId;
                                nameRunning = 1;
                            }


                            String filePath;
                            if (isTooltip) {
                                filePath = DB_PATH + currentMedia.getPageId() + "/" + (Integer.valueOf(nameResult) + 1)
                                        + "_" + (Integer.valueOf(1)) + typeTooltip;
                            } else if (isVideo) {
                                filePath = DB_PATH + currentMedia.getPageId() + "/video"
                                        + (Integer.valueOf(nameResult) + 1) + ".mp4";
                                Log.e("video", "write file : " + filePath);
                            } else if (isHtml5) {
                                File html5Path = new File(DB_PATH + currentMedia.getPageId() + "/html5");
                                if (!html5Path.exists()) {
                                    html5Path.mkdirs();
                                }

                                filePath = DB_PATH + currentMedia.getPageId() + "/html5/" + currentMedia.getNameMedia();
                                html5List.add(filePath);
                            } else {
                                filePath = DB_PATH + currentMedia.getPageId() + "/" + (Integer.valueOf(nameResult) + 1) + "_"
                                        + (Integer.valueOf(1)) + UtUtilities.getSourceFile(convertName);
                                Log.e("DnLoad", "image");
                            }

                            OutputStream output = new FileOutputStream(filePath);
                            byte data[] = new byte[1024];
                            @SuppressWarnings("unused")
                            long total = 0;
                            int count;

                            while ((count = input.read(data)) != -1) {
                                total += count;
//					                    publishProgress((int) (total * 100 / fileLength));
                                output.write(data, 0, count);
                            }

                            output.flush();
                            output.close();
                            input.close();

                            isVideo = isTooltip = isHtml5 = false;

                            publishProgress(((processCount - 1) * 100) + (current * (100) / totalRow));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        current++;
                    } catch (Exception e) {
                        Log.e("Error", "Error : " + e);
                    }
                }
            }
            return null;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            asynDownloadGallery = new DownloadGalleryFile();
            asynDownloadGallery.execute(galleryRaw);
            statusDownloadCurrent = DOWNLOAD_GALLERY;

            for (int iUnzip = 0; iUnzip < this.html5List.size(); iUnzip++) {
                String path = (String) this.html5List.get(iUnzip);
                UnzipAsyncTask.unzip(path);
            }
        }
    }

    private class DownloadGalleryFile extends AsyncTask<ArrayList<GalleryItem>, Integer, String> {
        int current = 0;
        int totalRow = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processCount++;
            pb.setMax(processTotal * 100);

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(ArrayList<GalleryItem>... sUrl) {

            //
            for (ArrayList<GalleryItem> media : sUrl) {
                int rows = media.size();
                totalRow = media.size();

                String pageIdCheck1 = null, nameResult = "", patternCount = "";
                int nameRunning = 0;
                int patternRunning = 1;

                while (current < rows) {
                    try {
                        if (isCancelled()) {
                            break;
                        }
                        try {
                            URL url = new URL(rootPath + media.get(current).getPath());
                            URLConnection connection = url.openConnection();
                            connection.connect();
                            // this will be useful so that you can show a
                            // typical 0-100% progress bar
                            int fileLength = connection.getContentLength();

                            // download the file
                            File f = new File(DB_PATH + media.get(current).getPageid());
                            if (!f.exists()) {
                                f.mkdirs();
                            }
                            int numberRunning = 1;
                            InputStream input = new BufferedInputStream(url.openStream(), 8192);
                            String convertName;
                            String name = media.get(current).getName();

                            convertName = name.replace("_", "-").replace(".jpg", "_" + numberRunning + ".jpg")
                                    .replace(".png", "_" + numberRunning + ".png")
                                    .replace(".bmp", "_" + numberRunning + ".bmp").toLowerCase();

                            String pageIdCheck2 = media.get(current).getPageid();

                            if (pageIdCheck1 != null && pageIdCheck1.equals(pageIdCheck2)) {
                                nameResult = String.valueOf(nameRunning);
                                patternCount = String.valueOf(patternRunning);
                                patternRunning++;
                            } else {
                                nameRunning++;
                                nameResult = String.valueOf(nameRunning);
                                patternCount = String.valueOf(0);
                                pageIdCheck1 = pageIdCheck2;
                                patternRunning = 1;

                            }

                            // Implement code for display gallery.
                            OutputStream output = new FileOutputStream(DB_PATH + media.get(current).getPageid() + "/"
                                    + "p" + media.get(current).getGroup() + "_" + (media.get(current).getNumber() + 1)
                                    + UtUtilities.getSourceFile(convertName));
                            byte data[] = new byte[1024];
                            long total = 0;
                            int count;

                            while ((count = input.read(data)) != -1) {
                                total += count;
                                // publishProgress((int) (total * 100 /
                                // fileLength));
                                output.write(data, 0, count);
                            }

                            output.flush();
                            output.close();
                            input.close();
                            publishProgress(((processCount - 1) * 100) + ((int) ((current * (100) / totalRow))));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        current++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            asynSynce = new AnalyzeFile();
            asynSynce.execute(pdfRaw);
            statusDownloadCurrent = DOWNLOAD_SYNC;

        }
    }

    private class DownloadCartoon extends AsyncTask<JSONObject, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pb.setMax(processTotal * 100);
            // instantiate it within the onCreate method
//            mProgressDialog = new ProgressDialog(DetailActivity.this);
//            mProgressDialog.setMessage("Downloading cartoon...");
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.setMax(100);
//            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {		
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					ResetStatDownload();
//					DownloadCartoon.this.cancel(true);
//				}
//			});
//            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
//    	    mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected String doInBackground(JSONObject... json) {
            try {

                cover = json[0].getString("coverpage");
                URL urlCover = new URL(cover);
                URLConnection connectionCover = urlCover.openConnection();
                connectionCover.connect();

                // download the file
                File f = new File(DB_PATH);
                if (!f.exists()) {
                    f.mkdirs();
                }

                FileWriter cartoon = new FileWriter(DB_PATH + "/" + "cartoon.txt");
                BufferedWriter bf = new BufferedWriter(cartoon);
                bf.write(json[0].getString("pages"));
                bf.close();

                InputStream inputCover = new BufferedInputStream(urlCover.openStream(), 8192);
                OutputStream outputCover = new FileOutputStream(DB_PATH + "cartoon.jpg");

                byte dataCover[] = new byte[1024];
                long total = 0;
                int size = (int) (Float.valueOf(json[0].getString("size")) * 1024 * 1024);
                int countCover;
                while ((countCover = inputCover.read(dataCover)) != -1) {
                    if (isCancelled()) {
                        break;
                    }
                    total += countCover;
                    publishProgress((int) (total * 100 / size));
                    outputCover.write(dataCover, 0, countCover);
                }

                outputCover.flush();
                outputCover.close();
                inputCover.close();

                int pages = Integer.valueOf(json[0].getString("pages"));
                String root = json[0].getString("root_link");
                JSONArray pageArray = json[0].getJSONArray("page");
                for (int i = 0; i < pages; i++) {
                    File pageDir = new File(DB_PATH + i);
                    if (!pageDir.exists()) {
                        pageDir.mkdirs();
                    }

                    JSONArray frameArray = pageArray.getJSONObject(i).getJSONArray("frame");
                    int frame = Integer.valueOf(frameArray.length());

                    for (int j = 0; j < frame; j++) {
                        File frameDir = new File(DB_PATH + i + "/" + j);
                        if (!frameDir.exists()) {
                            frameDir.mkdirs();
                        }

                        FileWriter frameSize = new FileWriter(frameDir + "/" + "frame.txt");
                        BufferedWriter frameBf = new BufferedWriter(frameSize);
                        frameBf.write(frameArray.getJSONObject(j).getString("top_frame") + "\n");
                        frameBf.write(frameArray.getJSONObject(j).getString("left_frame") + "\n");
                        frameBf.write(frameArray.getJSONObject(j).getString("width_frame") + "\n");
                        frameBf.write(frameArray.getJSONObject(j).getString("height_frame") + "\n");
                        frameBf.write(frameArray.getJSONObject(j).getString("has_sound") + "\n");
                        frameBf.write(frameArray.getJSONObject(j).getString("sound_frame"));
                        frameBf.close();

                        if (!frameArray.getJSONObject(j).getString("has_sound").equals("1"))
                            continue;

                        String sound = frameArray.getJSONObject(j).getString("sound_frame");
                        URL url = new URL(root + sound);
                        URLConnection connection = url.openConnection();
                        connection.connect();

                        InputStream input = new BufferedInputStream(url.openStream(), 8192);
                        OutputStream output = new FileOutputStream(frameDir + "/" + sound);

                        byte data[] = new byte[1024];
                        int count;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            publishProgress((int) (total * 100 / size));
                            output.write(data, 0, count);
                        }

                        output.flush();
                        output.close();
                        input.close();
                    }

                    FileWriter pageSize = new FileWriter(pageDir + "/" + "page.txt");
                    BufferedWriter pageBf = new BufferedWriter(pageSize);
                    pageBf.write(pageArray.getJSONObject(i).getString("id") + "\n");
                    pageBf.write(pageArray.getJSONObject(i).getString("file") + "\n");
                    pageBf.write(pageArray.getJSONObject(i).getString("total_frame") + "\n");
                    pageBf.write(pageArray.getJSONObject(i).getString("width") + "\n");
                    pageBf.write(pageArray.getJSONObject(i).getString("height"));
                    pageBf.close();

                    String file = pageArray.getJSONObject(i).getString("file");
                    URL url = new URL(root + file);
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    OutputStream output = new FileOutputStream(pageDir + "/" + file);

                    byte data[] = new byte[1024];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
//                        publishProgress((int) (total * 100 / size));
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
                    publishProgress(((processCount - 1) * 100) + ((int) ((total * 100 / size))));
                }
                return "Successful";
            } catch (Exception e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//        	mProgressDialog.dismiss();
//        	if( result != null )	Log.d("onPostExecute", result);
//        	if( result == null )	Log.d("onPostExecute", "NULL");
//        	String DB_PATH = Environment.getExternalStorageDirectory()+
//            		"/Android/data/com.ghbank.bookshelf/"+id+"/";
//            bh.open();
//            if (bh.check(id)) {
//            	   bh.addFavorite(id, name, summary, datetime, DB_PATH+"cartoon.jpg", cover, revision);
//			}
//            if (ac.getIntent().getExtras().containsKey("tag")) {
//            	bh.updateBook(id, revision);
//            	aq.id(R.id.btn_download_detail).clicked(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						Intent cartoon = new Intent(ac, CartoonActivity.class);
//						cartoon.putExtra("id", Integer.valueOf(id));
//						ac.startActivity(cartoon);
//					}
//				});
//			}
//			aq.id(R.id.btn_download_detail).text("Read");
//        	
        }
    }

    private class AnalyzeFile extends AsyncTask<ArrayList<PdfRawItem>, Integer, String> {
        int current = 0;
        @SuppressWarnings("unused")
        int totalRow = 0;
        @SuppressWarnings("unused")
        String pdfResultPath = DB_PATH + "book.pdf";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate it within the onCreate method
            processCount++;
//           mProgressDialog = new ProgressDialog(new ContextThemeWrapper(DetailActivity.this,android.R.style.Theme_Holo));
//           mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
//           mProgressDialog.setMessage("Analyze File...");
//           mProgressDialog.setIndeterminate(true);
//           mProgressDialog.setCancelable(false);
//           mProgressDialog.setProgressNumberFormat(null);
//           mProgressDialog.setProgressPercentFormat(null);
//           mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//           mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
//   	     mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
//   	        mProgressDialog.setMessage("Analyze File... "+current+"/"+totalRow);
        }


        @Override
        protected String doInBackground(ArrayList<PdfRawItem>... sUrl) {

            for (ArrayList<PdfRawItem> pdf : sUrl) {
                int rows = pdf.size();
                totalRow = pdf.size();

                if (processTotal != 2) {
                    addMediaContentOnPDF(pdf);
                }
                Document document = new Document();

                PdfCopy cp = null;
                PdfReader reader = null;
                try {

                    cp = new PdfCopy(document, new FileOutputStream(DB_PATH + "book.pdf"));
                    document.open();

                } catch (FileNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (DocumentException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }


                while (current < rows) {
                    try {
                        if (processTotal != 2) {
                            reader = new PdfReader(DB_PATH + pdf.get(current).getPdfName() + "_" + current + ".pdf");
                        } else {
                            reader = new PdfReader(DB_PATH + pdf.get(current).getPdfName() + ".pdf");
                        }

                        PdfImportedPage page;
                        page = cp.getImportedPage(reader, 1);
                        cp.addPage(page);
                        cp.freeReader(reader);

                        File f = null;

                        if (processTotal != 2) {
                            f = new File(DB_PATH + pdf.get(current).getPdfName() + "_" + current + ".pdf");
                        } else {
                            f = new File(DB_PATH + pdf.get(current).getPdfName() + ".pdf");
                        }

                        if (f.exists()) {
                            f.delete();
                        }
//		                publishProgress((int) (pb.getProgress()+(current * 90/ totalRow )));
                        current++;
                        if (current < rows) {
                            document.newPage();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (cp != null)
                    cp.close();

                if (document != null)
                    document.close();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//       	mProgressDialog.dismiss();
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    publishProgress(processTotal * 100);
                }
            });

            SetBookHandler(revision);

        }
    }

    private void addMediaContentOnPDF(ArrayList<PdfRawItem> pdf) {
        int rows = pdf.size();
        int current = 0;
        int breakNum = 0;
        int youtubeThumbnailCount = 1;

        while (current < rows) {
            try {

                Document document = new Document();
                PdfReader reader = new PdfReader(DB_PATH + pdf.get(current).getPdfName() + ".pdf");
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DB_PATH + pdf.get(current).getPdfName() + "_" + current + ".pdf"));
                document.setPageSize(reader.getPageSize(1));
                document.open();

                PdfRawItem currentRaw = pdf.get(current);

                if (pdf.get(current).getMedia() != null && pdf.get(current).getMedia().size() > 0) {
                    breakNum = 0;
                    Image imgThumbnail = null;
                    int mediaTotal = currentRaw.getMedia().size();

                    int mediaCountType = 0;
                    int tempMediaType = 999999;

                    for (int mediaCount = 0; mediaCount < mediaTotal; mediaCount++) {
                        MediaItem currentMedia = currentRaw.getMedia().get(mediaCount);



                        try {
                            File f;
                            String cName;
                            int nRunning = 1;
                            int pdfType = matchPdfTypeToInteger(currentMedia.getPdfType());
                            float pageWidth = reader.getPageSize(1).getWidth();
                            float pageHeight = reader.getPageSize(1).getHeight();
                            float multiplier = pageHeight / Float.parseFloat(currentRaw.getHeight());
                            float rectangleWidth = currentMedia.getCodinateWidth() * multiplier;
                            float rectangleHeight = currentMedia.getCodinateHight() * multiplier;
                            float currentX = currentMedia.getCodinateX() * multiplier;
                            float currentY = currentMedia.getCodinateY() * multiplier;
                            float finalY = pageHeight - (currentY + rectangleHeight);

                            MediaProperty property = currentMedia.getProperty();

                            //float scaleIcon = (rectangleWidth < rectangleHeight ? rectangleHeight : rectangleWidth) / 2;
                            // float imagePointX = currentX + ((rectangleWidth / 2) - (scaleIcon / 2));
                            //float imagePointY = finalY + ((rectangleHeight / 2) - (scaleIcon / 2));

//                            NLog.w(TAG, "Type : " + currentMedia.getPdfType());
//                            NLog.w(TAG, "pageWidth : " + pageWidth);
//                            NLog.w(TAG, "pageHeight : " + pageHeight);
//                            NLog.w(TAG, "Rectangle Width : " + rectangleWidth);
//                            NLog.w(TAG, "Rectangle Height : " + rectangleHeight);
//                            NLog.w(TAG, "Current X : " + currentX);
//                            NLog.w(TAG, "Current Y : " + currentY);

                            mediaCountType += 1;
                            if (pdfType != tempMediaType){
                                mediaCountType = 1;
                                tempMediaType = pdfType;
                            }

                            switch (pdfType) {

                                case TYPE_VIDEO:
                                    String mediaUrl = null;
                                    Annotation annotation;
                                    ByteArrayOutputStream streamPlayIcon = ImageHelper.createBitmapByteArrayOutputStreamFromResource(ac, R.drawable.play);
                                    String convertName = currentMedia.getNameMedia().replace("_", "-").toLowerCase();

                                    String video = DB_PATH + currentMedia.getPageId() + "/video" + mediaCountType + UtUtilities.getSourceFile(convertName);
                                    f = new File(video);

                                    if (property != null) {
                                        if (property.thumbnailVisible) {
                                            imgThumbnail = Image.getInstance(streamPlayIcon.toByteArray());
                                        } else {
                                            imgThumbnail = Image.getInstance(this.imageTransparent);
                                        }
                                    } else {
                                        imgThumbnail = Image.getInstance(this.imageTransparent);
                                    }


                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);

                                    float videoThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                    float videoThumbnailPositionY = finalY + ((rectangleHeight / 2) - (imgThumbnail.getScaledHeight() / 2));
                                    imgThumbnail.setAbsolutePosition(videoThumbnailPositionX, videoThumbnailPositionY);

                                    Log.d("VDO CHECKER", "FIND : " + video);

                                    if (f.exists()) {
                                        Log.d("VDO CHECKER", "FOUND : " + video);
                                        mediaUrl = "http://localhost/" + currentMedia.getPageId() +
                                                "/video" + mediaCountType + UtUtilities.getSourceFile(convertName) + "?warect=full";
                                    } else if (mediaCount != 0) {
                                        breakNum = breakNum++;
                                        f = new File(currentMedia.getPageId() + "/video" + breakNum + ".mp4");

                                        if (f.exists()) {
                                            mediaUrl = "http://localhost/" + currentMedia.getPageId() +
                                                    "/video" + breakNum + UtUtilities.getSourceFile(convertName) + "?warect=full";
                                        }
                                    }

                                    annotation = new Annotation(0f, 0f, 0f, 0f, mediaUrl);
                                    imgThumbnail.setAnnotation(annotation);
                                    document.add(imgThumbnail);

                                    break;

                                case TYPE_AUDIO:

                                    convertName = currentMedia.getNameMedia().replace("_", "-").toLowerCase();
                                    String soundPath = currentMedia.getPageId() + "/" + mediaCountType + "_1" + UtUtilities.getSourceFile(convertName);

                                    f = new File(DB_PATH + soundPath);
                                    String soundPathLocal = DB_PATH + soundPath;
                                    String annotationSoundPath = "http://localhost/" + currentMedia.getPageId()
                                            + "/" + mediaCountType + "_1" + UtUtilities.getSourceFile(convertName)
                                            + "?warect=no&icon_visible=" + currentMedia.isShowIcon();

                                    Log.d("SOUND CHECKER", "CHECK SOUND : " + soundPathLocal);

                                    if (f.exists()) {
                                        Log.d("SOUND CHECKER", "SOUND EXITS : " + soundPathLocal);
                                    } else if (mediaCount != 0) {
                                        breakNum++;
                                        annotationSoundPath = "http://localhost/" + currentMedia.getPageId() + "/" + breakNum + "_1" + UtUtilities.getSourceFile(convertName) + "?warect=no";
                                        Log.d("SOUND CHECKER", "MEDIA MORE THAN 0 SOUND NOT EXITS : " + soundPathLocal);
                                    }

                                    if (property != null) {
                                        if (property.iconVisible) {
                                            ByteArrayOutputStream playAudioIcon = ImageHelper.createBitmapByteArrayOutputStreamFromResource(ac, R.drawable.ic_play_no_padding);
                                            imgThumbnail = Image.getInstance(playAudioIcon.toByteArray());
                                        } else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }
                                    } else {
                                        imgThumbnail = Image.getInstance(imageTransparent);
                                    }

                                    annotation = new Annotation(0f, 0f, 0f, 0f, annotationSoundPath);
                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                    float soundThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                    imgThumbnail.setAbsolutePosition(soundThumbnailPositionX, finalY);
                                    imgThumbnail.setAnnotation(annotation);

                                    document.add(imgThumbnail);

                                    break;

                                case TYPE_IMAGE:

                                    if(currentMedia.getPdfEffect().equals("exam")){
                                        NLog.d(TAG,"Image type exam");
                                        final String EXAM_URL = "http://epub.arip.co.th/authoring/exam/view/" + currentMedia.getNameMedia() + ".html";
                                        //final String EXAM_URL = "http://epub.arip.co.th/authoring/exam/content.php?id=" + currentMedia.getNameMedia();
                                        NLog.d(TAG,"Exam Url : " + EXAM_URL);

                                        convertName = currentMedia.getNameMedia().replace("_", "-")
                                                .replace(".jpg", "_" + nRunning + ".jpg")
                                                .replace(".png", "_" + nRunning + ".png")
                                                .replace(".bmp", "_" + nRunning + ".bmp").toLowerCase();

                                        String filePath = DB_PATH + currentMedia.getPageId() +
                                                "/" + mediaCountType + "_" + (1) + UtUtilities.getSourceFile(convertName);

                                        // imgThumbnail = Image.getInstance(filePath);
                                        if (property != null) {
                                            if (property.thumbnailVisible) {
                                                imgThumbnail = Image.getInstance(filePath);
                                            } else {
                                                imgThumbnail = Image.getInstance(imageTransparent);
                                            }
                                        } else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }

                                        imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);

                                        float imageThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                        imgThumbnail.setAbsolutePosition(imageThumbnailPositionX, finalY);
                                        annotation = new Annotation(0f, 0f, 0f, 0f, EXAM_URL);
                                        imgThumbnail.setAnnotation(annotation);

                                        document.add(imgThumbnail);
                                    }else {

                                        NLog.w(TAG, "Type image : " + currentMedia.getNameMedia());

                                        convertName = currentMedia.getNameMedia().replace("_", "-")
                                                .replace(".jpg", "_" + nRunning + ".jpg")
                                                .replace(".png", "_" + nRunning + ".png")
                                                .replace(".bmp", "_" + nRunning + ".bmp").toLowerCase();

                                        String filePath = DB_PATH + currentMedia.getPageId() +
                                                "/" + mediaCountType + "_" + (1) + UtUtilities.getSourceFile(convertName);

                                        String mediaPath = "http://localhost/" + currentMedia.getPageId() +
                                                "/" + mediaCountType + "_" + (1) + UtUtilities.getSourceFile(convertName) + "?warect=full";

                                        f = new File(filePath);

                                        // imgThumbnail = Image.getInstance(filePath);
                                        if (property != null) {
                                            if (property.thumbnailVisible) {
                                                imgThumbnail = Image.getInstance(filePath);
                                            } else {
                                                imgThumbnail = Image.getInstance(imageTransparent);
                                            }
                                        } else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }
//
                                        imgThumbnail.scaleAbsolute(rectangleWidth,rectangleHeight);

                                        float imageThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                        imgThumbnail.setAbsolutePosition(imageThumbnailPositionX, finalY);
                                        annotation = new Annotation(0f, 0f, 0f, 0f, mediaPath);
                                        imgThumbnail.setAnnotation(annotation);

                                        document.add(imgThumbnail);
                                    }


                                    break;

                                case TYPE_GALLERY:

                                    NLog.w(TAG,"Gallery page : " + currentMedia.getPageId());

                                    convertName = currentMedia.getNameMedia().replace("_", "-")
                                            .replace(".jpg", "_" + nRunning + ".jpg")
                                            .replace(".png", "_" + nRunning + ".png")
                                            .replace(".bmp", "_" + nRunning + ".bmp")
                                            .replace(".JPG", "_" + nRunning + ".jpg")
                                            .replace(".PNG", "_" + nRunning + ".png")
                                            .replace(".BMP", "_" + nRunning + ".bmp").toLowerCase();

                                    String galleryFilePath = DB_PATH + currentMedia.getPageId() + "/" + "p" + currentMedia.getInsert_id() + "_1" + UtUtilities.getSourceFile(convertName);
                                    f = new File(galleryFilePath);
                                    String annotationPath = "http://localhost/" + currentMedia.getPageId() +
                                            "/p" + currentMedia.getInsert_id() + "_" + currentMedia.getPdfTypeGalleryCount() +
                                            UtUtilities.getSourceFile(convertName) + "?warect=full";


                                    Log.d("GALLERY CHECKER", "START CHECK GALLERY " + galleryFilePath);
                                    if (f.exists()) {
                                        Log.d("GALLERY CHECKER", "FOUND GALLERY : " + galleryFilePath);
                                    }

                                    NLog.e(TAG,"Gallery file path : " + galleryFilePath);
                                    if(property != null){
                                        if(property.thumbnailVisible){
                                            imgThumbnail = Image.getInstance(galleryFilePath);
                                        }else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }
                                    }else {
                                        imgThumbnail = Image.getInstance(imageTransparent);
                                    }


                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                    float galleryThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                    imgThumbnail.setAbsolutePosition(galleryThumbnailPositionX, finalY);

                                    annotation = new Annotation(0f, 0f, 0f, 0f, annotationPath);
                                    imgThumbnail.setAnnotation(annotation);
                                    document.add(imgThumbnail);

                                    break;

                                case TYPE_PAGE:

                                    convertName = pdf.get(current).getMedia().get(mediaCount).getNameMedia().replace("_", "-")
                                            .replace(".jpg", "_" + nRunning + ".jpg")
                                            .replace(".png", "_" + nRunning + ".png")
                                            .replace(".bmp", "_" + nRunning + ".bmp").toLowerCase();

                                    String fileIconPath = DB_PATH + currentMedia.getPageId() + "/" + (mediaCount + 1) +
                                            "_" + (mediaCount + 1) + UtUtilities.getSourceFile(convertName);
                                    String annotationPathPage = "http://localhost/" + currentMedia.getPageId() +
                                            "/" + (mediaCount + 1) + "_" + (mediaCount + 1) + UtUtilities.getSourceFile(convertName) +
                                            "?warect=page&page_jump=" + currentMedia.getPdfOption();

                                    f = new File(fileIconPath);

                                    if (f.exists()) {
                                        if(property != null){
                                            if(property.thumbnailVisible) {
                                                imgThumbnail = Image.getInstance(fileIconPath);
                                            }else {
                                                imgThumbnail = Image.getInstance(imageTransparent);
                                            }
                                        }else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }

                                    } else {
                                        imgThumbnail = Image.getInstance(imageTransparent);
                                    }

                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                    imgThumbnail.setAbsolutePosition(currentX, finalY);

                                    annotation = new Annotation(0f, 0f, 0f, 0f, annotationPathPage);
                                    imgThumbnail.setAnnotation(annotation);

                                    PdfContentByte canvasPage = writer.getDirectContent();
                                    canvasPage.rectangle(currentX, finalY, rectangleWidth, rectangleHeight);
                                    canvasPage.addImage(imgThumbnail);
                                    break;

                                case TYPE_LINK:
                                    Log.e("LINK CHECKER", "CHECK LINK :" + currentMedia.getPdfOption());

//                                    convertName = currentMedia.getNameMedia().replace("_", "-")
//                                            .replace(".jpg", "_" + nRunning + ".jpg")
//                                            .replace(".png", "_" + nRunning + ".png")
//                                            .replace(".bmp", "_" + nRunning + ".bmp").toLowerCase();
//
//                                    String filePathTypeLink = currentMedia.getPageId() + "/" + (mediaCount + 1) + "_1" + UtUtilities.getSourceFile(convertName);
//                                    f = new File(filePathTypeLink);

                                    // imgThumbnail = Image.getInstance(rootPath + currentMedia.getPathMedia());

                                    //ByteArrayOutputStream icon = ImageHelper.createBitmapByteArrayOutputStreamFromResource(ac, R.drawable.ip_bg_gray);
                                    //imgThumbnail = Image.getInstance(icon.toByteArray());
                                    //imgThumbnail = Image.getInstance((int) rectangleWidth, (int) rectangleHeight, icon.toByteArray(), new byte[1024]);

                                    imgThumbnail = Image.getInstance(imageTransparent);
                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                    float linkThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                    imgThumbnail.setAbsolutePosition(linkThumbnailPositionX, finalY);
                                    annotation = new Annotation(0f, 0f, 0f, 0f, currentMedia.getPdfOption());
                                    imgThumbnail.setAnnotation(annotation);
                                    document.add(imgThumbnail);

                                    break;
                                case TYPE_HTML5:
                                    NLog.w(TAG,"HTML5 Thumbnail : " + html5Thumbnail);
                                    // Check Thumbnail visible
                                    if (property != null) {
                                        if (property.thumbnailVisible) {
                                            if(html5Thumbnail.length() > 0) {
                                                imgThumbnail = Image.getInstance(html5Thumbnail);
                                            }else {
                                                imgThumbnail = Image.getInstance(imageTransparent);
                                            }
                                        } else {
                                            imgThumbnail = Image.getInstance(imageTransparent);
                                        }
                                    } else {
                                        imgThumbnail = Image.getInstance(imageTransparent);
                                    }

                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);

                                    float lThumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));

                                    imgThumbnail.setAbsolutePosition(lThumbnailPositionX, finalY);
                                    annotation = new Annotation(0f, 0f, 0f, 0f, currentMedia.getPdfOption());

                                    imgThumbnail.setAnnotation(annotation);
                                    document.add(imgThumbnail);


                                    break;
                                case TYPE_YOUTUBE:

                                    NLog.e("Youtube CHECKER", "Youtube LINK :" + currentMedia.getPathMedia());
                                    NLog.e("Youtube CHECKER", "Youtube Thumbnail :" + currentMedia.getPdfOption());

                                    final String path = UtConfig.DESIGN_PATH + BOOK_ID + "/" + currentMedia.getPageId() + "/";
                                    final String fileName = "youtube_thumbnail_" + youtubeThumbnailCount + UtConfig.FILE_TYPE_PNG;

                                    NLog.w(TAG, "Youtube Thumbnail path : " + path);
                                    NLog.w(TAG, "Youtube Thumbnail filename : " + fileName);

                                    if(property != null && property.thumbnailVisible) {
                                        Bitmap bitmap = null;
                                        try {
                                            URL url = new URL(currentMedia.getPdfOption());
                                            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                        } catch (IOException e) {
                                            NLog.e(TAG, "Get youtube thumbnail from url error.");
                                            e.printStackTrace();
                                        }

                                        if (bitmap != null) {
                                            String fileP = ImageManager.saveImageFile(path, fileName, bitmap);
                                            imgThumbnail = Image.getInstance(fileP);
                                        } else {
                                            NLog.i(TAG, "Get youtube thumbnail from url is null.");
                                            ByteArrayOutputStream youtubeIcon = ImageHelper.createBitmapByteArrayOutputStreamFromResource(ac, R.drawable.fhd_default);
                                            imgThumbnail = Image.getInstance(youtubeIcon.toByteArray());
                                        }
                                        bitmap = null;
                                    }else {
                                        imgThumbnail = Image.getInstance(imageTransparent);
                                    }

                                    imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                    float thumbnailPositionX = currentX + ((rectangleWidth / 2) - (imgThumbnail.getScaledWidth() / 2));
                                    imgThumbnail.setAbsolutePosition(thumbnailPositionX, finalY);
                                    annotation = new Annotation(0f, 0f, 0f, 0f, currentMedia.getPathMedia());

                                    imgThumbnail.setAnnotation(annotation);
                                    document.add(imgThumbnail);

                                    youtubeThumbnailCount += 1;
                                    break;
                                case TYPE_TOOLTIP:

                                    Log.d("TOOLTIP CHECKER", "FIND TOOLTIP FILE");
                                    convertName = currentMedia.getNameMedia() + typeTooltip.replace("_", "-")
                                            .replace(".jpg", ".jpg")
                                            .replace(".png", ".png")
                                            .replace(".bmp", ".bmp").toLowerCase();
//
                                    String fileTooltipPath = DB_PATH + currentMedia.getPageId() + "/" + "tooltip" + (mediaCount + 1);
                                    PdfContentByte canvasTooltip = writer.getDirectContent();

                                    File fileTooltip = new File(fileTooltipPath + ".txt");
                                    File fileTooltip_1, fileTooltip_2, fileTooltip_3;
//
                                    if (fileTooltip.exists()) {
                                        Log.d("TOOLTIP CHECKER", "TOOLTIP FILE EXITS");
                                        fileTooltip_1 = new File(fileTooltipPath + ".jpg");
                                        fileTooltip_2 = new File(fileTooltipPath + ".png");
                                        fileTooltip_3 = new File(fileTooltipPath + ".bmp");

                                        imgThumbnail = Image.getInstance(imageTransparent);

                                        if (fileTooltip_1.exists()) {

                                            Log.d("TOOLTIP CHECKER", "TOOLTIP JPG FILE EXITS");

                                            imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                            imgThumbnail.setAbsolutePosition(currentX, finalY);

                                            annotation = new Annotation(0f, 0f, 0f, 0f, fileTooltipPath);
                                            imgThumbnail.setAnnotation(annotation);

                                            canvasTooltip.rectangle(currentX, finalY, rectangleWidth, rectangleHeight);
                                            canvasTooltip.addImage(imgThumbnail);
                                        }

                                        if (fileTooltip_2.exists()) {

                                            Log.d("TOOLTIP CHECKER", "TOOLTIP PNG FILE EXITS");
//
                                            imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                            imgThumbnail.setAbsolutePosition(currentX, finalY);

                                            annotation = new Annotation(0f, 0f, 0f, 0f, fileTooltipPath);
                                            imgThumbnail.setAnnotation(annotation);

                                            canvasTooltip.rectangle(currentX, finalY, rectangleWidth, rectangleHeight);
                                            canvasTooltip.addImage(imgThumbnail);
                                        }

                                        if (fileTooltip_3.exists()) {

                                            Log.d("TOOLTIP CHECKER", "TOOLTIP BMP FILE EXITS");

                                            imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                            imgThumbnail.setAbsolutePosition(currentX, finalY);

                                            annotation = new Annotation(0f, 0f, 0f, 0f, fileTooltipPath);
                                            imgThumbnail.setAnnotation(annotation);

                                            canvasTooltip.rectangle(currentX, finalY, rectangleWidth, rectangleHeight);
                                            canvasTooltip.addImage(imgThumbnail);

                                        }
                                    } else {
                                        imgThumbnail.scaleAbsolute(rectangleWidth, rectangleHeight);
                                        imgThumbnail.setAbsolutePosition(currentX, finalY);

                                        annotation = new Annotation(0f, 0f, 0f, 0f, fileTooltipPath);
                                        imgThumbnail.setAnnotation(annotation);

                                        canvasTooltip.rectangle(currentX, finalY, rectangleWidth, rectangleHeight);
                                        canvasTooltip.addImage(imgThumbnail);
                                    }
                                    break;
                                case TYPE_EXAM:

                                    Log.e("check", "exam");
                                    break;

                                default:
                                    break;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                PdfImportedPage page = writer.getImportedPage(reader, 1);
                writer.getDirectContentUnder().addTemplate(page, 0, 0);
                writer.flush();
                document.close();

                if (reader != null) {
                    reader.close();
                }

                File f = new File(DB_PATH + currentRaw.getPdfName() + ".pdf");
                if (f.exists()) {
                    f.delete();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            current++;
        }

    }

    private int matchPdfTypeToInteger(String type) {
        switch (type) {
            case "youtube":
                return TYPE_YOUTUBE;

            case "vdo":
            case "video":
                return TYPE_VIDEO;

            case "img":
            case "image":
                return TYPE_IMAGE;

            case "gallery":
                return TYPE_GALLERY;

            case "url":
            case "link":
                return TYPE_LINK;

            case "page":
                return TYPE_PAGE;

            case "tooltip":
                return TYPE_TOOLTIP;

            case "audio":
                return TYPE_AUDIO;

            case "html5":
                return TYPE_HTML5;

            default:
                return -99;

        }
    }

    private void writeContentFile(ArrayList<ContentItem> contentItem) {

        try {

            File f = new File(DB_PATH);
            if (!f.exists()) {
                f.mkdirs();
            }

            FileWriter file = new FileWriter(DB_PATH + "content.txt");
            BufferedWriter bf = new BufferedWriter(file);

            for (ContentItem cItm : contentItem) {
                bf.write(cItm.getDescription());
                bf.write(",");
                bf.write(String.valueOf(cItm.getJumpto()));
                bf.newLine();
            }

            bf.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String getTypeCanvasObject(String params) {
        switch (params) {
            case "gallery":
            case "gallery1":
            case "gallery2":
            case "gallery3":
            case "gallery4":
            case "gallery5":
            case "gallery6":
                return "gallery";
            default:
                return "---";
        }
    }

    private String getHtml5Thumbnail(JSONObject object) {
        String result = "";
        if (object != null) {
            if (!object.isNull("thumbnail")) {
                try {
                    result = object.getString("thumbnail");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private MediaProperty getMediaPropertyObject(JSONObject object) {
        if (object == null) {
            return null;
        }

        boolean areaVisible = false;
        boolean thumbnailVisible = false;
        boolean iconVisible = false;
        boolean autoPlay = false;
        String playIconUrl = "";
        String thumbnailUrl = "";

        try {
            if (!object.isNull("area_visible")) {
                areaVisible = object.getString("area_visible").equalsIgnoreCase("1");
            }
            if (!object.isNull("thumbnail_visible")) {
                thumbnailVisible = object.getString("thumbnail_visible").equalsIgnoreCase("1");
            }
            if (!object.isNull("icon_visible")) {
                iconVisible = object.getString("icon_visible").equalsIgnoreCase("1");
            }
            if (!object.isNull("auto_play")) {
                autoPlay = object.getString("auto_play").equalsIgnoreCase("1");
            }
            if (!object.isNull("playicon")) {
                playIconUrl = object.getString("playicon");
            }
            if (!object.isNull("thumbnail")) {
                thumbnailUrl = object.getString("thumbnail");
            }

            return new MediaProperty(areaVisible, thumbnailVisible, iconVisible, autoPlay, playIconUrl, thumbnailUrl);
        } catch (JSONException e) {
            NLog.e(TAG, "getMediaPropertyObject : error.");
        }
        return null;
    }


}
