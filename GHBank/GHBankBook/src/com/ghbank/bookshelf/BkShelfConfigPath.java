package com.ghbank.bookshelf;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;

public class BkShelfConfigPath extends Activity{
	
	private EditText edtFixPath;
	private EditText edtDynamicPath;
	private TextView txtStoreage;
	private TextView txtAvailble;
	private RadioGroup rdoPath;
	private RadioButton rdoFixPath;
	private RadioButton rdoDynamicPath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_path);
		
		edtFixPath = (EditText) findViewById(R.id.edit_path_default);
		edtDynamicPath = (EditText) findViewById(R.id.edit_path_dynamic);
		
		txtStoreage = (TextView) findViewById(R.id.txt_storage); 
		txtAvailble = (TextView) findViewById(R.id.txt_availble); 
		
		rdoPath = (RadioGroup) findViewById(R.id.rdo_path);
		rdoFixPath = (RadioButton) findViewById(R.id.rd_btn_default);
		rdoDynamicPath = (RadioButton) findViewById(R.id.rd_btn_dynamic);
		
		edtFixPath.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		edtDynamicPath.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		txtStoreage.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		txtAvailble.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		rdoFixPath.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		rdoDynamicPath.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
	
		try{
			
			txtAvailble.setText(GetAvailbleInternalStorageMomory());
			
		if(!UtSharePreferences.getPrefStatusServer(this)){
			((RadioButton)rdoPath.getChildAt(0)).setChecked(true); 
			edtFixPath.setSelected(true);
		}else{
			((RadioButton)rdoPath.getChildAt(1)).setChecked(true); 
			rdoDynamicPath.setSelected(true);

			edtDynamicPath.setText(UtSharePreferences.getPrefServerConfig(BkShelfConfigPath.this).split("/").length>1?
					UtSharePreferences.getPrefServerConfig(BkShelfConfigPath.this).split("/")[2]:UtSharePreferences.getPrefServerConfig(BkShelfConfigPath.this));
			
			}
		
		}catch (Exception e){
			
		}
		
		rdoPath.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					if(checkedId == R.id.rd_btn_default){
						edtFixPath.requestFocus();
						UtSharePreferences.setServerConfig(BkShelfConfigPath.this, UtConfig.SERVER_DEFAULT);
						UtSharePreferences.setStatusServer(BkShelfConfigPath.this, false);
						UtSharePreferences.setPrefServerName(BkShelfConfigPath.this, edtFixPath.getText().toString());
					}else{
						edtDynamicPath.requestFocus();
						UtSharePreferences.setServerConfig(BkShelfConfigPath.this, "http://"+edtDynamicPath.getText().toString()+"/contents/index.php?");
						UtSharePreferences.setStatusServer(BkShelfConfigPath.this, true);
						UtSharePreferences.setPrefServerName(BkShelfConfigPath.this, edtDynamicPath.getText().toString());
					}
					
					UtSharePreferences.setStatusUpdateServer(BkShelfConfigPath.this, true);
				}
			});
		
		edtDynamicPath.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				((RadioButton)rdoPath.getChildAt(1)).setChecked(true); 
				return false;
			}
		});
		edtDynamicPath.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				UtSharePreferences.setServerConfig(BkShelfConfigPath.this, "http://"+s.toString()+"/contents/index.php?");
				UtSharePreferences.setStatusServer(BkShelfConfigPath.this, true);
				UtSharePreferences.setPrefServerName(BkShelfConfigPath.this, s.toString());
			}
		});
	}
	
	private String GetAvailbleInternalStorageMomory(){
		String result = "0mb";
		StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
		@SuppressWarnings("deprecation")
		long bytesAvailable = (long)stat.getFreeBlocks() * (long)stat.getBlockSize();
		long megAvailable = bytesAvailable / 1048576;
		result = "Available: " +megAvailable+"MB";
		return result;
	}
	
}
