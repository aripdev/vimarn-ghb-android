package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ghbank.bookshelf.connection.CnInternet;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.loader.LdBkShelfPageLoader;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.utils.UtUtilities;

import java.io.File;


public class BkShelf extends FragmentActivity implements OnClickListener{

	private SharedPreferences sPref;
	public static View categoryView; 
	private RelativeLayout lnScreen;
	private RelativeLayout layBanner;
	private RelativeLayout layBodyFeature;
	private ViewPager vpBanner;
	private ImageView btnFeature,btnPopular,btnNewArrival; 
	private ImageView[] navigateLine;
	private int currentFeature;
	
	private ImageView bCategory;
	private ImageView bLibrary;	
	private ImageView bEditProfile;	
	private ImageView bTutorial;
	private TextView txtUsername;
	private AnimationDrawable animProgress;
	
	private Handler handler;
	private long timeDelay = 2000; //1 seconds
	private boolean isExit = false;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_main);
		
		final ActionBar actionBar = getActionBar();
		actionBar.setIcon(R.drawable.ip_logo_large);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
		actionBar.setCustomView(R.layout.view_acb_username);
		
		Initial();
		
		
		  
        File fBg = new File(UtConfig.DESIGN_PATH_THEME+"bg1.png");
        if (fBg.exists()) {
//       	 lnScreen.setBackground(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+"bg1_1.png"));
       	 lnScreen.setBackground(new BitmapDrawable(getResources(),
       			 UtUtilities.decodeSampledBitmapFromResource(UtConfig.DESIGN_PATH_THEME+UtDesign.BG1)));
        	 
        }
        
        File fFeature= new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON7);
        if (fFeature.exists()) {
        	btnFeature.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON7));
        }
        
        File fPopular = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON8);
        if (fPopular.exists()) {
        	btnPopular.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON8));
        }
        
        File fNewArrival = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON9);
        if (fNewArrival.exists()) {
        	btnNewArrival.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON9));
        }
        
        File fBody = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_BACKGROUND1);
        if (fBody.exists()) {
        	if (Build.VERSION.SDK_INT >= 16) {

        		layBodyFeature.setBackground(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_BACKGROUND1));

        	} else {
        		layBodyFeature.setBackgroundDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_BACKGROUND1));
        	}
        	
        }
        
        File fNav1= new File(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1);
        if (fNav1.exists()) {
        	navigateLine[0].setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1));
        }
        
        File fNav2 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1);
        if (fNav2.exists()) {
        	navigateLine[1].setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1));
        }
        
        File fNav3 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1);
        if (fNav3.exists()) {
        	navigateLine[2].setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.THEME_TRI1_1));
        }
        
        File fAcb1 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1);
        if (fAcb1.exists()) {
        	bEditProfile.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON1));
        }
        
        File fAcb2 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON2);
        if (fAcb2.exists()) {
        	bCategory.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON2));
        }
        
        File fAcb3 = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON3);
        if (fAcb3.exists()) {
        	bLibrary.setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON3));
        }
  

		
		bLibrary.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelf.this,BkShelfLibrary.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});

		bCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//NLog.w("BkShelf","on button category clicked.");
				categoryView = v;
				new LdBkShelfPageLoader(BkShelf.this).QuickActionOnClick(v);
			}
		});
		
		bEditProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent  i = null;
            	 final SharedPreferences sPref = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE );
            	
            		 if(sPref!=null && sPref.getString(SnPreferenceVariable.USER_KEY, "").length()>0){
            			 i = new Intent(BkShelf.this, BkShelfEditProfile.class);
            		 }else{
            			 i = new Intent(BkShelf.this, BkShelfLogIn.class);
            		 }
                 startActivity(i); 
              
			}
		});
		
		bTutorial.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelf.this, BkShelfTutorial.class);
			    startActivity(i);
			}
		});
		
		//Check display tutorial first start
		if(UtSharePreferences.getPrefTutorialFirstInstall(this)){
//			Toast.makeText(this, "true", Toast.LENGTH_LONG).show();
			SharedPreferences sPref = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE );
			SharedPreferences.Editor sPrefEditor = sPref.edit();
			sPrefEditor.putString( SnPreferenceVariable.DISPLAY_TUTORIAL,"false" );
			sPrefEditor.commit();
//			
			Intent i = new Intent(BkShelf.this, BkShelfTutorial.class);
		    startActivity(i);
		
		}
	}
	
	private void Initial(){
//		aq = new AQuery(this);
		sPref = getSharedPreferences(SnPreferenceVariable.PF_ARIP, MODE_PRIVATE );
		lnScreen = (RelativeLayout) findViewById(R.id.lay_main);
		layBanner  = (RelativeLayout)findViewById(R.id.lay_banner);
		layBodyFeature = (RelativeLayout)findViewById(R.id.view_preview__features_body);
		vpBanner = (ViewPager)findViewById(R.id.vp_banner);
		
		layBanner.getLayoutParams().width = (int) (UtUtilities.getScreenWidth(this));
		layBanner.getLayoutParams().height = (int) (layBanner.getLayoutParams().width*(1.0f/2.0f)); 
       	
        btnFeature = (ImageView)findViewById(R.id.btn_features_home);
        btnPopular = (ImageView)findViewById(R.id.btn_popular_home);
        btnNewArrival = (ImageView)findViewById(R.id.btn_newarrival_home);
       
       	navigateLine = new ImageView[3];
       	navigateLine[0] = (ImageView)findViewById(R.id.navigate_features_home);
        navigateLine[1] = (ImageView)findViewById(R.id.navigate_popular_home);
        navigateLine[2] = (ImageView)findViewById(R.id.navigate_newarrival_home);
       	
        bCategory = (ImageView)findViewById(R.id.acb_category);
        bLibrary = (ImageView) findViewById(R.id.acb_library);
        bEditProfile = (ImageView) findViewById(R.id.acb_profile);
        bTutorial = (ImageView)findViewById(R.id.acb_tutorial);
        
        txtUsername = (TextView) findViewById(R.id.acb_username);
        txtUsername.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(BkShelf.this));
        txtUsername.setText(sPref.getString(SnPreferenceVariable.NICK_NAME, "login").length()>0?sPref.getString(SnPreferenceVariable.NICK_NAME, "login"):"login");
     
        
    	 btnFeature.setOnClickListener(this);
         btnPopular.setOnClickListener(this);
         btnNewArrival.setOnClickListener(this);
         
    	 for(int i=0;i<navigateLine.length;i++)
         {
         	navigateLine[i].setOnClickListener(this);
         }
    	 
    	 currentFeature = 0; 
    	 
    	 if( !CnInternet.AlertInternetConnection(this) ){
 			new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetBannerLoader();
// 			new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetFeatureLoader();
// 			new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetPopularLoader();
// 			new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetNewArrivalLoader();
 			new LdBkShelfPageLoader(this).SetAllCategoryLoader();
 			
 	      	switch (currentFeature) {
 	 			case 0:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetFeatureLoader();		break;
 	 			case 1:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetPopularLoader();		break;
 	 			case 2:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetNewArrivalLoader();	break;
// 	      	case 0:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetFeatureLoader();		break;
//			case 1:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetFeatureLoader();		break;
//			case 2:	new LdBkShelfPageLoader(this, vpBanner, navigateLine).SetFeatureLoader();	break;
 	      	}
 		}

	}
	
	private void stopAnimationProgress(){
		if(animProgress!=null){
			animProgress.stop();
		}	
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);

	    ImageView pb = (ImageView) findViewById(R.id.progressBar_horizontal);
	    ImageView pbBanner = (ImageView) findViewById(R.id.pb_banner);
	    if(pb!=null && pbBanner!=null){
		    animProgress = (AnimationDrawable) pb.getDrawable();
		    animProgress.start();
		    animProgress = (AnimationDrawable) pbBanner.getDrawable();
		    animProgress.start();
		   
	    }
	}
	 @Override
	   	public void onClick(View v) {
	
		
	   		if (v==btnFeature || v==navigateLine[0]) {
	   			navigateLine[currentFeature].startAnimation(getNavigateAnimation(currentFeature, 0));
	   			currentFeature = 0;
	   		}
	   		if (v==btnPopular || v==navigateLine[1]) {
	   			navigateLine[currentFeature].startAnimation(getNavigateAnimation(currentFeature, 1));
	   			currentFeature = 1;
	   		}
	   		if (v==btnNewArrival || v==navigateLine[2]) {
	   			navigateLine[currentFeature].startAnimation(getNavigateAnimation(currentFeature, 2));
	   			currentFeature = 2;
	   		}
	 }
	   	
	 @Override
		protected void onPause() {
			stopAnimationProgress();
			super.onPause();
		}
	 
	 @Override
		public void onBackPressed() {
		 handler = new Handler(); 
	        handler.postDelayed(new Runnable() { 
	             public void run() { 
	            	 isExit = false;
	             } 
	        }, timeDelay); 
			
			
			 if(isExit){
        		 finish();
        	 }
			 Toast.makeText(this, "Press back once more to exit.", Toast.LENGTH_SHORT).show();
			 isExit = true;
		}
		
	 @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		 if(txtUsername!=null){
			 txtUsername.setText(sPref.getString(SnPreferenceVariable.NICK_NAME, "login").length()>0?sPref.getString(SnPreferenceVariable.NICK_NAME, "login"):"login");
		 }
		super.onResume();
	}

	@Override
		public void finish() {
		
			super.finish();
			System.gc();
			System.exit(0);
		}
	  
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.menu_bookshelf_setting, menu);
//		return true;
//	}
//	
//	   @Override
//	    public boolean onOptionsItemSelected(MenuItem item) {
//	        // Take appropriate action for each action item click
//	        switch (item.getItemId()) {
//	        case R.id.txt_opt_tutorial:
//	        	Toast.makeText(this, "Display tutorial", Toast.LENGTH_LONG).show();
//	       
//	        default:
//	            return super.onOptionsItemSelected(item);
//	        }
//	    }
	
		
	 private TranslateAnimation getNavigateAnimation(int indexFrom, int indexTo)
	 	{

	 		TranslateAnimation animation = new TranslateAnimation(0, navigateLine[indexTo].getX()-navigateLine[indexFrom].getX(), 0, 0);
	 		animation.setDuration(200);
	 		animation.setAnimationListener(new LdBkShelfPageLoader(indexFrom, indexTo, navigateLine, this));
	 		return animation;
	 	}
	
	 

}
