package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.arip.it.library.db.BookHandler;
import com.ghbank.bookshelf.adapter.AdtLibraryAdapter;
import com.ghbank.bookshelf.db.DbBook;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.download.DnLoadPdfFromSDcard;
import com.ghbank.bookshelf.spinner.SpLibrarySort;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ipaulpro.afilechooser.utils.FileUtils;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class BkShelfLibrary extends FragmentActivity {

	private static final String TAG = BkShelfLibrary.class.getSimpleName();
	private static final int REQUEST_CODE = 6384; // onActivityResult request
	// code
	public static String DATE = "date";
	public static String TITLE = "title";
	public static String CURRENT_SORT = "date";

	private AQuery aq;
	public static GridView grid;
	public static  AdtLibraryAdapter adapter;
	private boolean isOpenBucket = false;
	private static DbBookHandler bookHandler;

	private ImageButton btnStoreSDcard;
	private ImageButton btnConfigPaht;
	private ImageButton btnFindCategory;
	private ImageView btnSearchFilter;
	private EditText edtSearchFilter;
	private ImageView imgLogo;

	private Handler handler;
	private long timeDelay = 2000; //1 seconds
	private boolean isExit = false;
	private boolean isRefresh = false;

	private Spinner spSort;
	String defaultTextForSpinner = "Title";
	String[] arrayForSpinner = {"Title", "Date"};
	private int currentSort = 0;

	private ProgressDialog mProgressDialog;

	private int totalSize = 0;
	public Activity ac = BkShelfLibrary.this;

	ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hash = new HashMap<String, String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.lay_library);


		final ActionBar actionBar = getActionBar();
		actionBar.hide();
//		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
//		actionBar.setCustomView(R.layout.view_acb_logo_center);

		Initial();

		spSort.setAdapter(new SpLibrarySort(this,R.layout.row_spin_sort_list, arrayForSpinner, defaultTextForSpinner));
//	
		DisplayLibraryBySort(TITLE);

//		btnStoreSDcard.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				showChooser();
//			}
//		});

		btnConfigPaht.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelfLibrary.this, com.ghbank.bookshelf.BkShelfConfigPath.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});

		btnFindCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelfLibrary.this, com.ghbank.bookshelf.BkShelfCategory.class);
//				Intent i = new Intent(BkShelfLibrary.this,BkShelf.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});

		btnSearchFilter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(edtSearchFilter.getText().length()==0){
					if(edtSearchFilter.isShown()){
						DisplayLibraryBySearch(edtSearchFilter.getText().toString());
					}
					showSearchFilter(false);

				}else{
					DisplayLibraryBySearch(edtSearchFilter.getText().toString());
				}

			}
		});

		spSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub
				if(position==0){
					currentSort = 0;
					DisplayLibraryBySort(TITLE);
				}else{
					currentSort = 1;
					DisplayLibraryBySort(DATE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

	}

	private void showSearchFilter(boolean refresh){
		if(!refresh){
			if(!edtSearchFilter.isShown()){
				edtSearchFilter.setVisibility(View.VISIBLE);
				imgLogo.setVisibility(View.GONE);
			}else{
				edtSearchFilter.setVisibility(View.GONE);
				imgLogo.setVisibility(View.VISIBLE);
			}
			isRefresh = false;
		}else{
			edtSearchFilter.setVisibility(View.GONE);
			imgLogo.setVisibility(View.VISIBLE);
			isRefresh = true;
		}

	}
	private void showChooser() {

		File f = new File(UtConfig.DESIGN_PATH_ZIP);
		if (!f.exists()) {
			f.mkdirs();
		}



		// Use the GET_CONTENT intent from the utility class
		Intent target = FileUtils.createGetContentIntent();


		// Create the chooser Intent
		Intent intent = Intent.createChooser(target, "");

		try {
			startActivityForResult(intent, REQUEST_CODE);



		} catch (ActivityNotFoundException e) {
			// The reason for the existence of aFileChooser
		}






	}

	private void fileGarbage(){
//		ImageView btnCut = (ImageView)grid.findViewById(R.id.img_cut);
//		if(grid !=null && grid.getCount()>0){
//			if(isOpenBucket){
//				
//				if(adapter!=null)	
//					adapter.setShowItemDeleteBook(true);
//				isOpenBucket = false;
//			}else{
//				
//				if(adapter!=null)
//					adapter.setShowItemDeleteBook(false);
//				isOpenBucket = true;
//			}
//			adapter.notifyDataSetChanged();
//		}
	}

	private void Initial(){
		grid =  (GridView)findViewById(R.id.grd_book_category_detail);
		aq = new AQuery(BkShelfLibrary.this);
		spSort = (Spinner)findViewById(R.id.sp_sort);
		imgLogo = (ImageView)findViewById(R.id.img_logo);
		btnStoreSDcard = (ImageButton)findViewById(R.id.btn_store_sdcard);
		btnConfigPaht = (ImageButton)findViewById(R.id.btn_config_path);
		btnFindCategory = (ImageButton)findViewById(R.id.btn_find_category);
		btnSearchFilter = (ImageView)findViewById(R.id.img_search_filter);
		edtSearchFilter = (EditText)findViewById(R.id.edt_search_filter);


	}
	
	/*
	private boolean unpackZip(String path, String zipname) {
	
		
		Log.d(TAG, "UnPackZIP Path : "+path);
		
		String FilePath=UtConfig.DESIGN_PATH_ZIP;
		  InputStream is;
		  ZipInputStream zis;
		  try {
		   String filename;
		  
		   is = new FileInputStream(path+"/" + zipname);
		  
		   zis = new ZipInputStream(new BufferedInputStream(is));
		   ZipEntry mZipEntry;
		   byte[] buffer = new byte[1024];
		   int count;

		   while ((mZipEntry = zis.getNextEntry()) != null) {
		    // zapis do souboru
			   filename = mZipEntry.getName();

		    // Need to create directories if not exists, or
		    // it will generate an Exception...
			   if (mZipEntry.isDirectory()) {
		    	
				   	File fmd = new File(FilePath + filename);
		    // File fmd = new File(FilePath);
				   	fmd.mkdirs();
				   	continue;
			   }

		    //
			   FileOutputStream fout = new FileOutputStream(FilePath + filename);

		    // cteni zipu a zapis
			   while ((count = zis.read(buffer)) != -1) {
				   fout.write(buffer, 0, count);
			   }

		    fout.close();
		    zis.closeEntry();
		    Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
		   }

		   zis.close();
		  } catch (IOException e) {
		   e.printStackTrace();
		   return false;
		  }

		  return true;
		 }
	*/

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		/*
        switch (requestCode) {
            case REQUEST_CODE:
                // If the file selection was successful
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        Log.i(TAG, "Uri = " + uri.toString());
                        
                        
                        try {
                            // Get the file path from the URI
                        	
//                            final String filename = FileUtils.getPath(this, uri);
                            final File location = FileUtils.getPathWithoutFilename(FileUtils.getFile(this, uri));
                           
                          
                           
//                            totalSize = FileUtils.getFile(this, uri).l

//                            Toast.makeText(BkShelfLibrary.this,
//                                    "File Selected: " + filename, Toast.LENGTH_LONG).show();
                            
                            ZipFile zip = new ZipFile(FileUtils.getFile(this, uri));
                            totalSize = zip.size();
                            
//                            
                            String zipFile = FileUtils.getFile(BkShelfLibrary.this, uri).getName();
                            
                            File[] files = new File(UtConfig.DESIGN_PATH_ZIP+zipFile.replace(".zip", "")).listFiles();
                            String idTest="";
                            for (File f : files)
                            {
                                if (f.isDirectory())
                                {
                                	idTest = f.getName();
                                	Log.d("ID FOLDER", f.getName());
                                    // process file.
                                }
                            }
                            
                         	DnLoadPdfFromSDcard download = new DnLoadPdfFromSDcard(BkShelfLibrary.this,idTest,zipFile.replace(".zip", ""));
                        	download.resetStatForDownload(idTest);
                        	
                            new UnzipProcess().execute(data);
                        } catch (Exception e) {
                            Log.e("FileSelectorTestActivity", "File select error", e);
                        }
                    }
                }
                break;
        }*/
        
        /*
        final Uri uri = data.getData();
        Log.i(TAG, "Uri = " + uri.toString());
        
        final File location = FileUtils.getPathWithoutFilename(FileUtils.getFile(this, uri));
        String zipFile = FileUtils.getFile(BkShelfLibrary.this, uri).getName();
        Log.d(TAG, "Location : "+location.toString()+" &&& Name : "+zipFile.toString());
        unpackZip(location.toString(),zipFile.toString());
        */
		try{

			final Uri uri = data.getData();
			Log.i(TAG, "Uri = " + uri.toString());


			final File location = FileUtils.getPathWithoutFilename(FileUtils.getFile(this, uri));
			String zipFile = FileUtils.getFile(BkShelfLibrary.this, uri).getName();
			ZipFile zip = new ZipFile(FileUtils.getFile(this, uri));
			totalSize = zip.size();
			Log.d(TAG, "Location : "+location.toString()+" &&& Name : "+zipFile.toString());
			//unpackZip(location.toString(),zipFile.toString());
			new UnzipProcess().execute(data);



			File[] files = new File(UtConfig.DESIGN_PATH_ZIP+zipFile.replace(".zip", "")).listFiles();
			String idTest="";
			for (File f : files)
			{
				if (f.isDirectory())
				{
					idTest = f.getName();
					Log.d("ID FOLDER", f.getName());
					// process file.
                	/*
                	DnLoadPdfFromSDcard download = new DnLoadPdfFromSDcard(BkShelfLibrary.this,idTest,zipFile.replace(".zip", ""));
                	download.resetStatForDownload(idTest);
                	*/
				}
			}

		}
		catch (Exception e) {
			Log.e("Ch.BkShelfLibrary", "File select error", e);
		}


		super.onActivityResult(requestCode, resultCode, data);

	}



	private class LoadSavedFavorite extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			bookHandler = new DbBookHandler(aq.getContext());
			bookHandler.open();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {
			int type = 0;
			ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
			try{
				if( params[0].equals(TITLE) )	type = 1;
				List<DbBook> data = bookHandler.sort(type);

				for (int i = 0; i < data.size(); i++) {
					HashMap<String, String> hash = new HashMap<String, String>();
					hash.put("id", data.get(i).getId());
					hash.put("name", data.get(i).getTitle());
					hash.put("datetime", data.get(i).getDate());
					hash.put("description",data.get(i).getDetail());
					hash.put("category",data.get(i).getCategory());
					hash.put("image",data.get(i).getImage());
					hash.put("revision",data.get(i).getRevision());
					arrayList.add(hash);
				}
			}catch(Exception e){

			}
			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
//				GridView grid =  (GridView)findViewById(R.id.grd_book_category_detail);      
//	   			grid.setExpanded(true);      
			if (isNetworkConnected()) {
				ShowUpdatedLib(result);
			}else{

				adapter = new AdtLibraryAdapter(BkShelfLibrary.this, result, null);
				if(adapter!=null && adapter.getCount()>0){
					grid.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}else{
					grid.setAdapter(null);
				}

				fileGarbage();
			}
		}

	}

	private class LoadBookBySearch extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(bookHandler!=null && !bookHandler.isOpen()){
				bookHandler = new DbBookHandler(aq.getContext());
				bookHandler.open();
			}
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {

			List<DbBook> data = bookHandler.search(params[0],currentSort);
			ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
			for (int i = 0; i < data.size(); i++) {
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("id", data.get(i).getId());
				hash.put("name", data.get(i).getTitle());
				hash.put("datetime", data.get(i).getDate());
				hash.put("description",data.get(i).getDetail());
				hash.put("category",data.get(i).getCategory());
				hash.put("image",data.get(i).getImage());
				hash.put("revision",data.get(i).getRevision());
				arrayList.add(hash);
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
//				GridView grid =  (GridView)findViewById(R.id.grd_book_category_detail);      
//	   			grid.setExpanded(true);      
			if (isNetworkConnected()) {
				ShowUpdatedLib(result);
			}else{

				adapter = new AdtLibraryAdapter(BkShelfLibrary.this, result, null);
				if(adapter!=null && adapter.getCount()>0){
					grid.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}else{
					grid.setAdapter(null);
				}

				fileGarbage();
			}
		}

	}

	private class UnzipProcess extends AsyncTask<Intent, Integer, String> {

		int current=0;
		int totalRow = 0;
		Uri mUri;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// instantiate it within the onCreate method

			mProgressDialog = new ProgressDialog(new ContextThemeWrapper(BkShelfLibrary.this,android.R.style.Theme_Holo));
//            mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
			mProgressDialog.setMessage("Process file please wait...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setCancelable(false);
			mProgressDialog.setMax(totalSize);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

			mProgressDialog.show();
//            
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);
			mProgressDialog.setProgress(progress[0]);

		}

		@Override
		protected String doInBackground(Intent... data) {
			try {

				final Uri uri = data[0].getData();
				mUri = uri;

//                  UtZip.unzip(FileUtils.getFile(this, uri), location);
				String targetDirectory = UtConfig.DESIGN_PATH_ZIP;
//        	 File targetDirectory = FileUtils.getPathWithoutFilename(FileUtils.getFile(BkShelfLibrary.this, uri));
				File zipFile = FileUtils.getFile(BkShelfLibrary.this, uri);
				ZipInputStream zis = new ZipInputStream(
						new BufferedInputStream(new FileInputStream(zipFile)));
				try {
					ZipEntry ze;
					int count;
					long total = 0;
					byte[] buffer = new byte[8192];
					while ((ze = zis.getNextEntry()) != null) {
						total++;
						File file = new File(targetDirectory, ze.getName());
						File dir = ze.isDirectory() ? file : file.getParentFile();
						if (!dir.isDirectory() && !dir.mkdirs())
							throw new FileNotFoundException("Failed to ensure directory: " +
									dir.getAbsolutePath());
						if (ze.isDirectory())
							continue;
						FileOutputStream fout = new FileOutputStream(file);
						try {
							while ((count = zis.read(buffer)) != -1){
								fout.write(buffer, 0, count);
							}
							publishProgress((int) (total));
//	     	                	publishProgress((int) (total* 100)/(int)dir.getParentFile().length());

						} finally {
							zis.closeEntry();
							fout.close();
						}
					}
				} finally {
					zis.close();
				}
			} catch (Exception e) {
//                  Log.e("FileSelectorTestActivity", "File select error", e);
			}


			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			//	mProgressDialog.dismiss();



			//pd = ProgressDialog.show(ac, null, "Process...");
			//final ProgressDialog pd = ProgressDialog.show(ac, null, "Process...");
			//ProgressDialog pd = ProgressDialog.show(ac, null, "Process...");

			String zipFile = FileUtils.getFile(BkShelfLibrary.this, mUri).getName();
			File[] files = new File(UtConfig.DESIGN_PATH_ZIP + zipFile.replace(".zip", "")).listFiles();

			for(File f : files){

				String idTest = f.getName().toString();
				Log.e("delete ID FOLDER : ", f.getName());
				Log.e("delete idTest : ", idTest);                  // process file.

				String DB_PATH = UtConfig.DESIGN_PATH+idTest+"/";
				File book = new File (DB_PATH);
				if(book.exists()){
					BookHandler favoriteHandler = new BookHandler(ac);
					favoriteHandler.open();
					favoriteHandler.deleteComment(idTest);
					deleteFileById(idTest);
				}


			}

			for (File f : files)
			{
				if (f.isDirectory())
				{
					String idTest = f.getName().toString();
					Log.e("ID FOLDER", f.getName());
					Log.e("idTest", idTest);                  // process file.

					DisplayLibraryBySort(TITLE);

					DnLoadPdfFromSDcard download = new DnLoadPdfFromSDcard(BkShelfLibrary.this,idTest,zipFile.replace(".zip", ""));
					download.resetStatForDownload(idTest);

					DisplayLibraryBySort(TITLE);
                	
                	/*
                	 hash.put("id",idTest);
                     arrayList.add(hash);
                	adapter = new AdtLibraryAdapter(BkShelfLibrary.this, arrayList, null);
                	//adapter.getView(position, convertView, parent)
                	//adapter.getCount();
                	grid.setAdapter(adapter);	
		        	adapter.notifyDataSetChanged();
		        	*/

				}
			}
            
            /*
            try{
            for (File f : files)
            {
                if (f.isDirectory())
                {
                	String idTest = f.getName().toString();
                	Log.e("ID FOLDER", f.getName());
                	Log.e("idTest", idTest);                  // process file.
                	
                	
                	
                	DnLoadPdfFromSDcard download = new DnLoadPdfFromSDcard(BkShelfLibrary.this,idTest,zipFile.replace(".zip", ""));
                	download.resetStatForDownload(idTest);
                	
                	
                	 hash.put("id",idTest);
                     arrayList.add(hash);
                	adapter = new AdtLibraryAdapter(BkShelfLibrary.this, arrayList, null);
                	//adapter.getView(position, convertView, parent)
                	adapter.getCount();
                	grid.setAdapter(adapter);	
		        	adapter.notifyDataSetChanged();
		        	
		        	
                }
            }
            
            }catch(Exception e){
            	Log.e("Ch.BkShelfLibrary", "UnZip class error", e);
            }
            */
//        	DnLoadPdfFromSDcard download = new DnLoadPdfFromSDcard(ac,id);
//        	download.resetStatForDownload(id);



			DisplayLibraryBySort(TITLE);


			if(mProgressDialog.isShowing()){

				mProgressDialog.dismiss();
				//mProgressDialog=null;
			}


			File f = new File(UtConfig.DESIGN_PATH+"zip");
			deleteRecursive(f);



		}
	}


	public void DisplayLibraryBySearch(String nameSearch){
		new LoadBookBySearch().execute(nameSearch);
	}



	public void DisplayLibraryBySort(String sortType){
		if(edtSearchFilter.getText().length()>0){
			new LoadBookBySearch().execute(edtSearchFilter.getText().toString());
		}else{
			new LoadSavedFavorite().execute(sortType);
		}

	}

	private void ShowUpdatedLib(final ArrayList<HashMap<String, String>> compare){
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
			@Override
			public void callback(String url, JSONObject json,
								 AjaxStatus status) {
				super.callback(url, json, status);
				ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
				if(json != null){
					try {
						if (json.optString("status").equals("1")) {
							JSONArray c = json.optJSONArray("data");
							if(c != null)
								for(int i=0;i<c.length();i++){
									HashMap<String, String> hash = new HashMap<String, String>();
									JSONObject json2 = c.getJSONObject(i);
									String id = json2.getString("id");
									String re_vision = json2.getString("re_vision");
									hash.put("id",id);
									hash.put("revision",re_vision);
									arrayList.add(hash);
								}
						}

						else{
							Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
						}

//	           			grid.setExpanded(true);
						adapter = new AdtLibraryAdapter(BkShelfLibrary.this, compare, arrayList);

						if(adapter!=null && adapter.getCount()>0){
							grid.setAdapter(adapter);
							adapter.notifyDataSetChanged();
						}else{
							grid.setAdapter(null);
						}
						fileGarbage();

					} catch (JSONException e) {
						if(grid!=null){
							grid.setAdapter(null);
						}
					} catch (RuntimeException e) {
						Log.d("UPDATE LIBRARY" , "FAIL CAUSE : " + e.getMessage());
					} catch (Exception e) {
						Log.d("UPDATE LIBRARY" , "FAIL CAUSE : " + e.getMessage());
					}

				}else{

					adapter = new AdtLibraryAdapter(BkShelfLibrary.this, compare, null);
					if(adapter!=null && adapter.getCount()>0){
						grid.setAdapter(adapter);
						adapter.notifyDataSetChanged();
					}else{
						grid.setAdapter(null);
					}

					fileGarbage();
					Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
				}
			}
		};
		cb.header("User-Agent", "android");

		//Toast.makeText(this, gid.substring(0, gid.length()-1), Toast.LENGTH_SHORT).show();

		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("t", "1"));
		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
		pairs.add(new BasicNameValuePair("type", "4"));
		if (compare.size()>0) {
			String gid = "";
			for (int i = 0; i < compare.size(); i++) {
				gid += compare.get(i).get("id")+",";
			}
			pairs.add(new BasicNameValuePair("gid", gid.substring(0, gid.length()-1)));
		}


		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);

		aq.ajax(UtSharePreferences.getPrefServerConfig(this), params, JSONObject.class, cb);
	}


	private boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			return false;
		} else
			return true;
	}

	@Override
	public void onBackPressed() {
		if(!isRefresh){
			showSearchFilter(true);
		}else{
			handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					isExit = false;
				}
			}, timeDelay);


			if(isExit){
				finish();
			}
			Toast.makeText(this, "Press back once more to exit.", Toast.LENGTH_SHORT).show();
			isExit = true;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		if(UtSharePreferences.getPrefStatusUpdateServer(BkShelfLibrary.this)){
			DisplayLibraryBySort(TITLE);
			UtSharePreferences.setStatusUpdateServer(BkShelfLibrary.this, false);
		}else{
			DisplayLibraryBySort(TITLE);
		}

		super.onResume();
	}

	private void deleteFileById(String id){
		new DeleteFile().execute(id);
	}

	private void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteRecursive(child);

		fileOrDirectory.delete();
	}

	private class DeleteFile extends AsyncTask<String, Void, Void>{
		private ProgressDialog pd;

		@Override
		protected Void doInBackground(String... params) {
			String DB_PATH = UtConfig.DESIGN_PATH+params[0]+"/";
			File fileOrDirectory = new File(DB_PATH);
			deleteRecursive(fileOrDirectory);
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = ProgressDialog.show(ac, null, "Deleting");
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			pd.dismiss();
			BkShelfLibrary libraly= (BkShelfLibrary)ac;
			libraly.DisplayLibraryBySort(BkShelfLibrary.TITLE);
		}
	}

}
