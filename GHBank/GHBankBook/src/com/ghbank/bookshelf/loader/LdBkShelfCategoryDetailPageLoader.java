package com.ghbank.bookshelf.loader;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.adapter.AdtCategoryDetailAdapter;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.R;

public class LdBkShelfCategoryDetailPageLoader {
	
	private Activity ac;
	private AQuery aq;
	private GridView gv;
	private ImageView imgCategoryThumbnail;
	
//	protected ImageLoader imageLoader = ImageLoader.getInstance();
	
	public LdBkShelfCategoryDetailPageLoader(Activity ac, String imgPahtPosition, String categoryName){
		this.ac = ac;
		aq = new AQuery(ac);
		gv = (GridView) ac.findViewById(R.id.grd_book_category_detail);
	    imgCategoryThumbnail = (ImageView) ac.findViewById(R.id.img_category_thumbnail);

		
		 switch (Integer.parseInt(imgPahtPosition)) {
			case 0:
				imgCategoryThumbnail.setImageResource(R.drawable.ip_ic_library_book);
				break;
			case 1:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_2);	
				break;
			case 2:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_3);
				break;
			case 3:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_1);
				break;
			case 4:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_4);
				break;
			case 5:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_5);
				break;
			case 6:
				imgCategoryThumbnail.setImageResource(R.drawable.ic_subject_6);
				break;
				
			default:
				break;
			}
		 
//		imageLoader.displayImage(imgPaht, imgCategoryThumbnail,ImageDisplayOptions.getImageOptionCategory());
	}
	public void SetCategoryDetailLoader(String cat_id){
		
    	AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){     
    		@Override
        	public void callback(String url, JSONObject json,
        			AjaxStatus status) {
        		super.callback(url, json, status);
        		final ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();   
            	arrayList.clear();
            	Log.d("puy","get json");
        		 if(json != null){
            			try {
							if (json.getString("status").equals("1")) {
								JSONArray c = json.getJSONArray("data");
								Log.d("puy",c.toString());
		           				for(int i=0;i<c.length();i++){
		           					HashMap<String, String> hash = new HashMap<String, String>();
		           					JSONObject json2 = c.getJSONObject(i);
		           					String id = json2.getString("id");
		           					String name = json2.getString("name");
		           					String image = json2.getString("image");
		           					String st = json2.getString("status");
		           					Log.d("Ch.LdBkShelfCate","book ID : "+id+" Name : "+name);
		           					hash.put("id",id);
		           					hash.put("name",name.replace("$", UtSharePreferences.getPrefServerName(ac)));
		           					hash.put("image", image.replace("$", UtSharePreferences.getPrefServerName(ac)));	
		           					hash.put("status", st);
		           					arrayList.add(hash);   
		           				}          				     				
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
            			
            		
            			
            			AdtCategoryDetailAdapter adapter = new AdtCategoryDetailAdapter(aq.getContext(), arrayList);
            			
//            			gv.setExpanded(true);
            			gv.setAdapter(adapter);

        		 }
        	}
    	};
    	cb.header("User-Agent", "android");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        @SuppressWarnings({ "unused", "static-access" })
		final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
        pairs.add(new BasicNameValuePair("t", "1"));    
        pairs.add(new BasicNameValuePair("type", "5")); 
        //
       // pairs.add(new BasicNameValuePair("offline", "2")); 
        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
        pairs.add(new BasicNameValuePair("cat_id", cat_id));
       // pairs.add(new BasicNameValuePair("username", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
        //pairs.add(new BasicNameValuePair("pgroup", sPref.getString(SnPreferenceVariable.GROUP, "0")));
  
        HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);
        aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
       
        
    }
}
