package com.ghbank.bookshelf.loader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ghbank.bookshelf.BkShelfLogIn;
import com.ghbank.bookshelf.BookshelfStartActivity;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.cache.Cache;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.ImageManager;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDevice;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LdBkShelfDesignLoader {

    private AQuery aq;
    private Handler handler;
    private long timeDelay = 1000; //1 seconds
    private Activity activity;
    private String revision;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    private ProgressBar pb;
    private TextView txtProgress;
    private Button btnReload;
    private ArrayList<String> designList;
    private ArrayList<String> tutorialImageUrlList;

    @SuppressWarnings("unused")
    private int processCount = 0;
    @SuppressWarnings("unused")
    private int processTotal = 0;
    private String DB_PATH;
    private SharedPreferences sPref;
    private boolean isDownloadComplete;

    @SuppressWarnings("static-access")
    public LdBkShelfDesignLoader(Activity ac) {
        activity = ac;
        aq = new AQuery(ac);
        DB_PATH = UtConfig.DESIGN_PATH_THEME;
        pb = (ProgressBar) ac.findViewById(R.id.view_detail__pgb_download);
        txtProgress = (TextView) ac.findViewById(R.id.txt_progress);
        btnReload = (Button) ac.findViewById(R.id.btn_reload);
        sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE);

        btnReload.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!isDownloadComplete) {
                    SetDesignLoader();
                }
            }
        });
    }

    public boolean isCheckFileComplete() {
        boolean isComplete = true;
        File f = new File(DB_PATH);
        int count = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE)
                .getInt(SnPreferenceVariable.THEME_TOTAL_FILE, -1);
        if (f.isDirectory()) {
            if (f.exists()) {
                if (f.listFiles().length != count || count == -1) {
                    isComplete = false;
                }
            }
        } else {
            isComplete = false;
        }

        return isComplete;
    }

    public void clearThemeFile() {

        File f = new File(DB_PATH);
        if (f.isDirectory()) {
            String[] children = f.list();
            for (int i = 0; i < children.length; i++) {
                new File(f, children[i]).delete();
            }
        }

    }

    public void SetDesignLoader() {

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
            @SuppressWarnings("unchecked")
            @Override
            public void callback(String url, JSONObject json,
                                 AjaxStatus status) {
                super.callback(url, json, status);
                final ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
                arrayList.clear();

                NLog.w("Design","Design loader URL : " + url);
//            	Log.i("JOSN_DOWNLOAD",json.toString());
                if (json != null) {
                    try {
                        if (json.getString("status").equals("1")) {
                            designList = new ArrayList<String>();
                            revision = json.getString("re_vision");

                            designList.add(json.getJSONObject("image").getJSONObject("small_icon4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon8").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon7").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon6").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon5").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon11").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon10").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon13").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon12").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                            designList.add(json.getJSONObject("image").getJSONObject("small_icon9").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));


                            if (UtDevice.getHeight() >= 1920) {
                                designList.add(json.getJSONObject("image").getJSONObject("background").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));

                                designList.add(json.getJSONObject("image").getJSONObject("logo3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon9").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon11").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon8").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon10").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon7").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon13").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon6").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon12").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon5").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon42").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon43").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon44").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon15").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon40").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(0).getString("url"));

                            } else if (UtDevice.getHeight() >= 1280) {
                                designList.add(json.getJSONObject("image").getJSONObject("background").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));

                                designList.add(json.getJSONObject("image").getJSONObject("logo3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon9").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon11").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon8").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon10").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon7").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon13").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon6").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon12").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon5").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon42").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon43").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon44").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon15").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));

                                designList.add(json.getJSONObject("image").getJSONObject("icon40").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));

                            } else if (UtDevice.getHeight() < 1280) {
                                designList.add(json.getJSONObject("image").getJSONObject("background").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(3).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("background4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(1).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));

                                designList.add(json.getJSONObject("image").getJSONObject("logo3").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon9").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo1").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("logo2").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon11").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon8").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon10").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon7").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon13").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon6").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon12").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon5").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon4").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon42").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon43").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon44").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon15").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                                designList.add(json.getJSONObject("image").getJSONObject("icon40").getJSONArray(UtConfig.DESIGN_VERSION).getJSONObject(2).getString("url"));
                            }

                            SharedPreferences.Editor sPrefEditor = sPref.edit();
                            sPrefEditor.putString(SnPreferenceVariable.THEME_ID, !json.isNull("theme_id") ? json.getString("theme_id") : "");
                            sPrefEditor.putString(SnPreferenceVariable.THEME_PLACEHOLDER_COLOR, !json.isNull("placeholder_color") ? json.getString("placeholder_color") : "");
                            sPrefEditor.putString(SnPreferenceVariable.THEME_COLOR2, !json.isNull("theme_color2") ? json.getString("theme_color2") : "");
                            sPrefEditor.putString(SnPreferenceVariable.THEME_COLOR, !json.isNull("theme_color") ? json.getString("theme_color") : "");
                            sPrefEditor.putString(SnPreferenceVariable.THEME_FONT_COLOR, !json.isNull("font_color") ? json.getString("font_color") : "");
                            sPrefEditor.putString(SnPreferenceVariable.THEME_NAME, !json.isNull("theme_name") ? json.getString("theme_name") : "");
                            //Not initial on json service (tag name "background_library_left_color" and "background_library_right_color") //09-05-2014 Pakorn commment.
                            sPrefEditor.putString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_LEFT, !json.isNull("background_library_left_color") ? json.getString("background_library_left_color") : UtConfig.DEFAULT_BACKFROUND_CATEGORY_LEFT_COLOR);
                            sPrefEditor.putString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_RIGHT, !json.isNull("background_library_right_color") ? json.getString("background_library_right_color") : UtConfig.DEFAULT_BACKFROUND_CATEGORY_RIGHT_COLOR);

                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_01, json.optInt("button1") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_02, json.optInt("button2") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_03, json.optInt("button3") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_04, json.optInt("button4") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_05, json.optInt("button5") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_06, json.optInt("button6") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_07, json.optInt("button7") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_08, json.optInt("button8") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_09, json.optInt("button9") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_10, json.optInt("button10") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_11, json.optInt("button11") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_12, json.optInt("button12") != 0);
                            sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_BUTTON_13, json.optInt("button13") != 0);

                            if (!json.isNull("help")) {
                                new Cache(activity).setStringCache(json.getString("help"));
                            }

                            if (!json.isNull("login_show")) {
                                sPrefEditor.putBoolean(SnPreferenceVariable.THEME_SHOW_LOGIN, json.getString("login_show").equals("1"));
                            } else {
                                Log.e("SHOW LOGIN", "NOT FOUND SHOW LOGIN PARAMETER");
                            }

                            if (!json.isNull("home_show")) {
                                sPrefEditor.putInt(SnPreferenceVariable.THEME_HOME_TYPE, json.getString("home_show").equals("1") ? 1 : 2);
                            } else {
                                Log.e("HOME TYPE", "NOT FOUND HOME TYPE PARAMETER");
                            }
                            // image icon7..8..8  , name
                            if(!json.getJSONObject("image").getJSONObject("icon7").isNull("name")){
                                // Tab 1 title
                                String title = json.getJSONObject("image").getJSONObject("icon7").getString("name");
                                NLog.w("T","Title 1 " + title);
                                sPrefEditor.putString(SnPreferenceVariable.THEME_TAB_1_TITLE,title).apply();
                            }
                            if(!json.getJSONObject("image").getJSONObject("icon8").isNull("name")){
                                // Tab 1 title
                                String title = json.getJSONObject("image").getJSONObject("icon8").getString("name");
                                NLog.w("T","Title 2 " + title);
                                sPrefEditor.putString(SnPreferenceVariable.THEME_TAB_2_TITLE,title).apply();
                            }
                            if(!json.getJSONObject("image").getJSONObject("icon9").isNull("name")){
                                // Tab 1 title
                                String title = json.getJSONObject("image").getJSONObject("icon9").getString("name");
                                NLog.w("T","Title 3 " + title);
                                sPrefEditor.putString(SnPreferenceVariable.THEME_TAB_3_TITLE,title).apply();
                            }

                            sPrefEditor.commit();

//								final SharedPreferences sPref = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE );

                            if (sPref.getString(SnPreferenceVariable.DESIGN_REVISION, null) != null && sPref.getString(SnPreferenceVariable.DESIGN_REVISION, "").equalsIgnoreCase(revision)) {
                                if (!isCheckFileComplete()) {
                                    isDownloadComplete = true;
                                    pb.setVisibility(View.VISIBLE);
                                    txtProgress.setVisibility(View.VISIBLE);
                                    btnReload.setVisibility(View.GONE);
                                    new ListDesignLoading().execute(designList);
                                } else {
                                    startBookshelf();
                                }
                            } else {
                                isDownloadComplete = true;
                                pb.setVisibility(View.VISIBLE);
                                txtProgress.setVisibility(View.VISIBLE);
                                btnReload.setVisibility(View.GONE);
                                new ListDesignLoading().execute(designList);
                            }
                        }

                    } catch (JSONException e) {


                        if (sPref.getString(SnPreferenceVariable.DESIGN_REVISION, "") != null && sPref.getString(SnPreferenceVariable.DESIGN_REVISION, "").length() > 0) {

                            startBookshelf();

                        } else {
                            isDownloadComplete = false;
                            btnReload.setVisibility(View.VISIBLE);
                            Toast.makeText(activity, "Something went wrong, Please reload try again", Toast.LENGTH_LONG).show();
                        }
//							isDownloadComplete = false;
//							btnReload.setVisibility(View.VISIBLE);
//							Toast.makeText(activity, "Something went wrong, Please reload try again", Toast.LENGTH_LONG).show();
                    }


                } else {
                    if (sPref.getString(SnPreferenceVariable.DESIGN_REVISION, "") != null && sPref.getString(SnPreferenceVariable.DESIGN_REVISION, "").length() > 0) {

                        startBookshelf();

                    } else {
                        isDownloadComplete = false;
                        btnReload.setVisibility(View.VISIBLE);
                        Toast.makeText(activity, "Something went wrong, Please reload try again", Toast.LENGTH_LONG).show();
                    }
//        			 	isDownloadComplete = false;
//						btnReload.setVisibility(View.VISIBLE);
//						Toast.makeText(activity, "Something went wrong, Please reload try again", Toast.LENGTH_LONG).show();
                }
            }
        };
        cb.header("User-Agent", "android");
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("t", "0"));
        pairs.add(new BasicNameValuePair("type", "4"));
        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));

        NLog.w("Design","t : " + 0);
        NLog.w("Design","type : " + 4);
        NLog.w("Design","library : " + UtConfig.LIBRARY_CODE);

        HttpEntity entity = null;
        try {
            entity = new UrlEncodedFormEntity(pairs, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AQuery.POST_ENTITY, entity);
        aq.ajax(UtSharePreferences.getPrefServerConfig(activity), params, JSONObject.class, cb);
    }

    private void startBookshelf() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent i;
                if (ThemeResourceManager.isThemeShowLogin(activity)) {
                    boolean isLoggedIn = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE)
                            .getBoolean(SnPreferenceVariable.IS_USER_LOGIN, false);
                    if (isLoggedIn)
                        i = new Intent(activity, BookshelfStartActivity.class);
                    else
                        i = new Intent(activity, BkShelfLogIn.class);
                } else {
                    i = new Intent(activity, BookshelfStartActivity.class);
                }

                activity.startActivity(i);
                activity.finish();
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, timeDelay);
    }


    private class ListDesignLoading extends AsyncTask<ArrayList<String>, Integer, String> {
        int current = 0;
        int totalRow = 0;

        public ListDesignLoading(){
            ArrayList<String> help = getHelpImageUrl();
            for(int i=0;i<help.size();i++){
                final int index = i;
                Glide.with(activity)
                        .load(help.get(i))
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                if(resource != null) {
                                    ImageManager.saveImageFile(UtConfig.HELP_PATH,"help"+(index + 1)+".png",resource);
                                }
                            }
                        });
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // instantiate it within the onCreate method
            processCount++;
            pb.setMax(100);


        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            pb.setProgress(progress[0]);
            txtProgress.setText("File Downloading... " + current + "/" + totalRow);

//   	        pb.setSecondaryProgress(stat*pb.getProgress());
//   	        mProgressDialog.setProgress(progress[0]);
//   	        mProgressDialog.setTitle("Process "+processCount+"/"+processTotal);
//   	        mProgressDialog.setMessage("Downloading pdf file... "+current+"/"+totalRow);

        }

        @Override
        protected String doInBackground(ArrayList<String>... sUrl) {



            clearThemeFile();

            for (ArrayList<String> soureFile : sUrl) {
                totalRow = soureFile.size();
                //NLog.e("ROW","Row : " + totalRow);
                while (current < soureFile.size()) {
                    try {
//		            	if (isCancelled()){
//		            		break;
//		            	}

                        try {
                            URL url = new URL(soureFile.get(current).toString());
                            //NLog.e("URL",url.toString());
                            URLConnection connection = url.openConnection();
                            connection.connect();
                            int fileLength = connection.getContentLength();
                            // download the file
                            File f = new File(DB_PATH);
                            if (!f.exists()) {
                                f.mkdirs();
                            }
                            InputStream input = new BufferedInputStream(url.openStream(), 8192);
                            OutputStream output = null;
                            //NLog.w("ERROR_download Theme design ",soureFile.get(current).split("/")[soureFile.get(current).split("/").length-1].split("_")[1]);
                            if (checkIntegerType(soureFile.get(current).split("/")[soureFile.get(current).split("/").length - 1])) {
                                output = new FileOutputStream(DB_PATH + soureFile.get(current).split("/")[soureFile.get(current).split("/").length - 1].split("_")[0] + ".png");
                            } else {
                                output = new FileOutputStream(DB_PATH + soureFile.get(current).split("/")[soureFile.get(current).split("/").length - 1].toString());
                            }

                            byte data[] = new byte[1024];
                            long total = 0;
                            int count;

                            while ((count = input.read(data)) != -1) {
                                total += count;
                                publishProgress((int) (total * 100 / fileLength));
                                output.write(data, 0, count);
                            }


                            output.flush();
                            output.close();
                            input.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        current++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (current == totalRow) {
                    sPref = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sPref.edit();
                    editor.putInt(SnPreferenceVariable.THEME_TOTAL_FILE, totalRow);
                    editor.commit();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                @SuppressWarnings("static-access")
                final SharedPreferences sPref = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE);
                SharedPreferences.Editor sPrefEditor = sPref.edit();
                sPrefEditor.putString(SnPreferenceVariable.DESIGN_REVISION, revision);
                sPrefEditor.commit();

                startBookshelf();

            } catch (Exception e) {
                Log.i("ERROR_download Theme", e.toString());
            }

        }
    }

    public boolean checkIntegerType(String text) {
        try {
            Log.i("ERROR_SPLIT", text);
            Integer.parseInt(text.split("_")[1].split("\\.")[0]);
            Log.i("ERROR_SPLIT_OUT", "true");
            return true;
        } catch (Exception e) {
            Log.i("ERROR_SPLIT_OUT", "false");
            return false;
        }
    }

    private ArrayList<String> getHelpImageUrl() {

        String strTutorial = new Cache(activity).getStringCache(Cache.STR_HELP);
        ArrayList<String> urlList = new ArrayList<>();
        try {
            final JSONArray array = new JSONArray(strTutorial);
            for (int i = 0; i < array.length(); i++) {
                urlList.add(array.getString(i));
            }
        } catch (JSONException e) {
            NLog.e("LdBkShelfDesignLoader", "Get tutorial image url error.");
        }

        return urlList;
    }

}