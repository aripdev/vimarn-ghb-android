package com.ghbank.bookshelf.loader;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.BkShelf;
import com.ghbank.bookshelf.BkShelfByCategory;
import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.adapter.AdtBannerAdapter;
import com.ghbank.bookshelf.custom.ActionItem;
import com.ghbank.bookshelf.custom.BookSize;
import com.ghbank.bookshelf.custom.QuickAction;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.image.ImageDisplayOptions;
import com.ghbank.bookshelf.service.SvBanner;
import com.ghbank.bookshelf.thread.BannerThread;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDevice;
import com.ghbank.bookshelf.utils.UtSharePreferences;

public class LdBkShelfPageLoader implements AnimationListener {
	
	private AdtBannerAdapter adtBanner;
	private BannerThread bannerThread;
	private ViewPager vpBanner;
	public static QuickAction quickAction;
	private Activity ac;
	private AQuery aq;
	private ImageView[] navigateLine;
	
	private HorizontalScrollView scrViewFeature;
	private LinearLayout horz;
	
	private int indexFrom;
	private int indexTo;
	
	private boolean isDisplayAllCategory = false;
	
	private ArrayList<JSONArray> jCategory = new ArrayList<JSONArray>();
	private static ArrayList<JSONArray> jCategoryArrayList = new ArrayList<JSONArray>();
	private static ArrayList<HashMap<String, String>> categoryArrayList = null;
	private ImageLoader imageLoader = ImageLoader.getInstance();
	public LdBkShelfPageLoader(Activity ac, ViewPager vpBanner, ImageView[] navigateLine){
		this.ac = ac;
		this.vpBanner = vpBanner;
		this.navigateLine = navigateLine;
		Initial();
	}
	
	public LdBkShelfPageLoader(int indexFrom, int indexTo, ImageView[] navigateLine, Activity ac)
	{
		this.ac = ac;
		this.indexFrom = indexFrom;
		this.indexTo = indexTo;
		this.navigateLine = navigateLine;
		Initial();
	}
	
	public LdBkShelfPageLoader(Activity ac){
		this.ac = ac;
		Initial();
	}
	
	private void Initial(){
		
		aq = new AQuery(ac);
		
		scrViewFeature = (HorizontalScrollView)ac.findViewById(R.id.scroll_features);
		 horz = (LinearLayout)ac.findViewById(R.id.horz);
		new BookSize(ac).setNormalSize();
    
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		
        
		navigateLine[indexFrom].setVisibility(View.INVISIBLE);
		navigateLine[indexTo].setVisibility(View.VISIBLE);
		// TODO Auto-generated method stub
		switch (indexTo) {
			case 0:	SetFeatureLoader();		break;
			case 1:	SetPopularLoader();		break;
			case 2:	SetNewArrivalLoader();	break;
		}
		
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}


	public void SetBannerLoader(){      
	    	AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
	    		@Override
	    		public void callback(String url, JSONObject json,
	    				AjaxStatus status) {
	    			super.callback(url, json, status);
	    			SvBanner svBanner = new SvBanner();
	    	      	ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
	    	      	arrayList = svBanner.GetDataBannerFromJSONObject(ac, json);
	    	       if(arrayList!=null){
	    	    	   ImageView pb = (ImageView) ac.findViewById(R.id.pb_banner);
	    	    	   pb.setVisibility(View.GONE);
	    	       }
	            	adtBanner = new AdtBannerAdapter(ac, arrayList);
	                vpBanner.setAdapter(adtBanner);
	                	if (vpBanner.getChildCount()>0) {
	                		bannerThread = new BannerThread(vpBanner, adtBanner);
	                		bannerThread.start();
						}
	            		                         
	            }
			};
	    	cb.header("User-Agent", "android");
	    	List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	        pairs.add(new BasicNameValuePair("t", "2"));    
	        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE)); 
	  
	        HttpEntity entity = null;
			try {
				entity = new UrlEncodedFormEntity(pairs, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put(AQuery.POST_ENTITY, entity);
	        
	        aq.image(R.id.pb_banner).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);            
	    }
	
	public void SetFeatureLoader(){
		 AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
	    		@Override
	        	public void callback(String url, JSONObject json,
	        			AjaxStatus status) {
	        		super.callback(url, json, status);
	        		final ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();   
	            	arrayList.clear();
	        		 if(json != null){
	            			try {
								if (json.getString("status").equals("1")) {
									JSONArray c = json.getJSONArray("data");
			           				for(int i=0;i<c.length();i++){
			           					HashMap<String, String> hash = new HashMap<String, String>();
			           					JSONObject json2 = c.getJSONObject(i);
			           					String id = json2.getString("id");
			           					String name = json2.getString("name");
			           					String image = "";
			           					
			           					if(UtDevice.getHeight()>=1920){
			           						image = json2.getJSONArray("image_new").getJSONObject(2).getString("url");
			           					}else if(UtDevice.getHeight()>=1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(1).getString("url");
			           					}else if(UtDevice.getHeight()<1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(0).getString("url");
			           					}
			           						
			           					String st = json2.getString("status");
			           					hash.put("id",id);
			           					hash.put("name",name);
			           					hash.put("image", image);	
			           					hash.put("status", st);	
			           					arrayList.add(hash);   
//			           					dgUpdateVersion(ac);
			           				
			           				}          				     				
								}else if (json.getString("status").equals("2")) {
									dgUpdateVersion(ac);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
	            			
	            			
//	            			 horz = (LinearLayout)ac.findViewById(R.id.horz);
	            			horz.removeAllViews();
	            			

	            			TextView txtHeader = (TextView)ac.findViewById(R.id.view_detail__txt_header_book);
	            			txtHeader.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
	            			txtHeader.setText(ac.getString(R.string.txt_header_book_feature));
//	            			horz.removeAllViews();
	            	
	            			for (int a = 0; a < arrayList.size(); a++) {
	            				View v = ac.getLayoutInflater().inflate(R.layout.view_book, null);
	                			ImageView img = (ImageView) v.findViewById(R.id.img_book);
	                			final ImageView pb = (ImageView) v.findViewById(R.id.pg_loaging);
	                			final ImageView tag = (ImageView) v.findViewById(R.id.img_status_book);
	                			final int pos = a;
	                			final AnimationDrawable animProgress = (AnimationDrawable) pb.getDrawable();
	                		
	                			img.getLayoutParams().height = BookSize.book_nomal_heigth;
	                			img.getLayoutParams().width =   BookSize.book_nomal_width;
	                			   
	                			img.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
//										Intent intent = new Intent(v.getContext(), DetailActivity.class);
//										intent.putExtra("id", arrayList.get(pos).get("id"));
//										startActivity(intent);
										Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
										intent.putExtra("id_category", arrayList.get(pos).get("id"));
										ac.startActivity(intent);
										
									}
								});
	                			
	                			FrameLayout.LayoutParams param0 = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);     
	                			horz.setLayoutParams(param0);
	                			horz.addView(v);
	                			
//	                			Handler handler = new Handler();
//	                			handler.postDelayed(new Runnable() {
//	                			    public void run() {
//	                			        scrViewFeature.fullScroll(HorizontalScrollView.FOCUS_LEFT);
//	                			    }
//	                			}, 100L);
	                			imageLoader.displayImage(arrayList.get(a).get("image"), img, ImageDisplayOptions.getImageOption(), new SimpleImageLoadingListener(){
	                				
	                				@Override
	                				public void onLoadingStarted(String imageUri, View view) {
	                					pb.setVisibility(View.VISIBLE);
	                					animProgress.start();
	                				}

	                				@Override
	                				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
	                					@SuppressWarnings("unused")
										String message = null;
	                					switch (failReason.getType()) {
	                						case IO_ERROR:
	                							message = "Input/Output error";
	                							break;
	                						case DECODING_ERROR:
	                							message = "Image can't be decoded";
	                							break;
	                						case NETWORK_DENIED:
	                							message = "Downloads are denied";
	                							break;
	                						case OUT_OF_MEMORY:
	                							message = "Out Of Memory error";
	                							break;
	                						case UNKNOWN:
	                							message = "Unknown error";
	                							break;
	                					}

	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                				}

	                				@Override
	                				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                					if (arrayList.get(pos).get("status").equals("New")) {
	        								aq.id(tag).visible();
	        							}
	                				}
	                			});
		
	                		
							}
//	            			scrViewFeature.addView(horz);
	            			scrViewFeature.scrollTo(0, 0);

	        		 }
	        	}
	    	};
	    	cb.header("User-Agent", "android");
	        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	        pairs.add(new BasicNameValuePair("t", "1"));    
	        pairs.add(new BasicNameValuePair("type", "1"));  
	        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE)); 
	  
	        HttpEntity entity = null;
			try {
				entity = new UrlEncodedFormEntity(pairs, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put(AQuery.POST_ENTITY, entity);
	        
	        aq.image(R.id.progressBar_horizontal).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);     
	 }
	 
	 public void SetPopularLoader(){
		 AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
	    		@Override
	        	public void callback(String url, JSONObject json,
	        			AjaxStatus status) {
	        		super.callback(url, json, status);
	            	final ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();   
	            	arrayList.clear();
	        		 if(json != null){
	            			try {
								if (json.getString("status").equals("1")) {
									JSONArray c = json.getJSONArray("data");
			           				for(int i=0;i<c.length();i++){
			           					HashMap<String, String> hash = new HashMap<String, String>();
			           					JSONObject json2 = c.getJSONObject(i);
			           					String id = json2.getString("id");
			           					String name = json2.getString("name");
			           					String image = "";
			           					
			           					if(UtDevice.getHeight()>=1920){
			           						image = json2.getJSONArray("image_new").getJSONObject(2).getString("url");
			           					}else if(UtDevice.getHeight()>=1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(1).getString("url");
			           					}else if(UtDevice.getHeight()<1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(0).getString("url");
			           					}
			           					String st = json2.getString("status");
			           					hash.put("id",id);
			           					hash.put("name",name);
			           					hash.put("image", image);	
			           					hash.put("status", st);				
			           					arrayList.add(hash);   
			           				}          				     				
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
	            			
	            			
	            			TextView txtHeader = (TextView)ac.findViewById(R.id.view_detail__txt_header_book);
	            			txtHeader.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
	            			txtHeader.setText(ac.getString(R.string.txt_header_book_popular));
	            			horz.removeAllViews();
	            			
	            			for (int a = 0; a < arrayList.size(); a++) {
	            				View v = ac.getLayoutInflater().inflate(R.layout.view_book, null);
	                			ImageView img = (ImageView) v.findViewById(R.id.img_book);
	                			final ImageView pb = (ImageView) v.findViewById(R.id.pg_loaging);
	                			final ImageView tag = (ImageView) v.findViewById(R.id.img_status_book);
	                			final int pos = a;
	                			final AnimationDrawable animProgress = (AnimationDrawable) pb.getDrawable();
	                			
	                			img.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
										intent.putExtra("id_category", arrayList.get(pos).get("id"));
										ac.startActivity(intent);
										
									}
								});
	                			
	                			img.getLayoutParams().height = BookSize.book_nomal_heigth;
	                			img.getLayoutParams().width =   BookSize.book_nomal_width;
	                			
	                			FrameLayout.LayoutParams param0 = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);     
	                			horz.setLayoutParams(param0);                		
	                			horz.addView(v);
	                			
	                			imageLoader.displayImage(arrayList.get(a).get("image"), img, ImageDisplayOptions.getImageOption(), new SimpleImageLoadingListener(){
	                				
	                				@Override
	                				public void onLoadingStarted(String imageUri, View view) {
	                					pb.setVisibility(View.VISIBLE);
	                					animProgress.start();
	                				}

	                				@Override
	                				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
	                					@SuppressWarnings("unused")
										String message = null;
	                					switch (failReason.getType()) {
	                						case IO_ERROR:
	                							message = "Input/Output error";
	                							break;
	                						case DECODING_ERROR:
	                							message = "Image can't be decoded";
	                							break;
	                						case NETWORK_DENIED:
	                							message = "Downloads are denied";
	                							break;
	                						case OUT_OF_MEMORY:
	                							message = "Out Of Memory error";
	                							break;
	                						case UNKNOWN:
	                							message = "Unknown error";
	                							break;
	                					}
	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                				}

	                				@Override
	                				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                					if (arrayList.get(pos).get("status").equals("New")) {
	        								aq.id(tag).visible();
	        							}
	                				}
	                			});
							}
	            			scrViewFeature.scrollTo(0, 0);
	        		 }
	        	}
	    	};
	    	cb.header("User-Agent", "android");
	        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	        pairs.add(new BasicNameValuePair("t", "1"));    
	        pairs.add(new BasicNameValuePair("type", "2"));  
	        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE)); 
	  
	        HttpEntity entity = null;
			try {
				entity = new UrlEncodedFormEntity(pairs, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put(AQuery.POST_ENTITY, entity);
	        
	        aq.image(R.id.progressBar_horizontal).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	 }
	 
	 public void SetNewArrivalLoader(){
		 AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
	    		@Override
	        	public void callback(String url, JSONObject json,
	        			AjaxStatus status) {
	        		super.callback(url, json, status);
	            	final ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();   
	            	arrayList.clear();
	        		 if(json != null){
	            			try {
								if (json.getString("status").equals("1")) {
									JSONArray c = json.getJSONArray("data");
			           				for(int i=0;i<c.length();i++){
			           					HashMap<String, String> hash = new HashMap<String, String>();
			           					JSONObject json2 = c.getJSONObject(i);
			           					String id = json2.getString("id");
			           					String name = json2.getString("name");
			           					
			           					String image = "";
			           					
			           					if(UtDevice.getHeight()>=1920){
			           						image = json2.getJSONArray("image_new").getJSONObject(2).getString("url");
			           					}else if(UtDevice.getHeight()>=1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(1).getString("url");
			           					}else if(UtDevice.getHeight()<1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(0).getString("url");
			           					}
			           					String st = json2.getString("status");
			           					hash.put("id",id);
			           					hash.put("name",name);
			           					hash.put("image", image);	
			           					hash.put("status", st);				
			           					arrayList.add(hash);   
			           				}          				     				
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
	            			
	            			
	            			TextView txtHeader = (TextView)ac.findViewById(R.id.view_detail__txt_header_book);
	            			txtHeader.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
	            			txtHeader.setText(ac.getString(R.string.txt_header_book_newarrival));
	            			horz.removeAllViews();
	            			
	            			for (int a = 0; a < arrayList.size(); a++) {
	            				View v = ac.getLayoutInflater().inflate(R.layout.view_book, null);
	                			ImageView img = (ImageView) v.findViewById(R.id.img_book);
	                			final ImageView pb = (ImageView) v.findViewById(R.id.pg_loaging);
	                			final ImageView tag = (ImageView) v.findViewById(R.id.img_status_book);
	                			final int pos = a;
	                			final AnimationDrawable animProgress = (AnimationDrawable) pb.getDrawable();
	                			
	                			img.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										Intent intent = new Intent(v.getContext(), BkShelfPreview.class);
										intent.putExtra("id_category", arrayList.get(pos).get("id"));
										ac.startActivity(intent);
										
									}
								});
	                			
	                			img.getLayoutParams().height = BookSize.book_nomal_heigth;
	                			img.getLayoutParams().width =   BookSize.book_nomal_width;
	                			
	                			FrameLayout.LayoutParams param0 = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);     
	                			horz.setLayoutParams(param0);             		
	                			horz.addView(v);
	                			
	                			imageLoader.displayImage(arrayList.get(a).get("image"), img, ImageDisplayOptions.getImageOption(), new SimpleImageLoadingListener(){
	                				
	                				@Override
	                				public void onLoadingStarted(String imageUri, View view) {
	                					pb.setVisibility(View.VISIBLE);
	                					animProgress.start();
	                				}

	                				@Override
	                				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
	                					@SuppressWarnings("unused")
										String message = null;
	                					switch (failReason.getType()) {
	                						case IO_ERROR:
	                							message = "Input/Output error";
	                							break;
	                						case DECODING_ERROR:
	                							message = "Image can't be decoded";
	                							break;
	                						case NETWORK_DENIED:
	                							message = "Downloads are denied";
	                							break;
	                						case OUT_OF_MEMORY:
	                							message = "Out Of Memory error";
	                							break;
	                						case UNKNOWN:
	                							message = "Unknown error";
	                							break;
	                					}
	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                				}

	                				@Override
	                				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
	                					pb.setVisibility(View.GONE);
	                					animProgress.stop();
	                					if (arrayList.get(pos).get("status").equals("New")) {
	        								aq.id(tag).visible();
	        							}
	                				}
	                			});
							}
	            			scrViewFeature.scrollTo(0, 0);
	        		 }
	        	}
	    	};
	    	cb.header("User-Agent", "android");
	        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	        pairs.add(new BasicNameValuePair("t", "1"));    
	        pairs.add(new BasicNameValuePair("type", "3"));  
	        pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE)); 
	  
	        HttpEntity entity = null;
			try {
				entity = new UrlEncodedFormEntity(pairs, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	        Map<String, Object> params = new HashMap<String, Object>();
	        params.put(AQuery.POST_ENTITY, entity);
	        
	        aq.image(R.id.progressBar_horizontal).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	 }
	
	 public void SetAllCategoryLoader(){
	    	AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
	    		 @Override
	             public void callback(String url, JSONObject json, AjaxStatus status) {           	
	             
	                    if(json != null){                    	
	                		try {
	            			if (json.getString("status").equals("1")) { 
//	            				ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
	            				categoryArrayList = new ArrayList<HashMap<String,String>>();
	            				JSONArray c = json.getJSONArray("data");  
//	        					Log.e("c", c.toString());
	        					for(int i=0;i<c.length();i++){
	            					HashMap<String, String> hash = new HashMap<String, String>();
	            					JSONObject json2 = c.getJSONObject(i);
	            					String id = json2.getString("id");
	            					String name = json2.getString("name");
	            					
	            					String image = "";
		           					
	            					try{
	            						
			           					if(UtDevice.getHeight()>=1920){
			           						image = json2.getJSONArray("image_new").getJSONObject(2).getString("url");
			           					}else if(UtDevice.getHeight()>=1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(1).getString("url");
			           					}else if(UtDevice.getHeight()<1280){
			           						image = json2.getJSONArray("image_new").getJSONObject(0).getString("url");
			           					}
			           					
	            					}catch(Exception e){
	            						image = json2.getJSONArray("image_new").getJSONObject(0).getString("url");
	            					}
	            					
	            					//String parent = json2.getString("parent");
	            					hash.put("id",id);
	            					hash.put("name",name);
	            					hash.put("image", image);	
	            					
	            					
	            					if(!json2.isNull("subcategory") && json2.optJSONArray("subcategory")!=null){
	            						jCategoryArrayList.add(json2.getJSONArray("subcategory"));
		            					hash.put("subcategory", i+"");
										
	            					}else{
//	            						hash.put("subcategory", json2.getJSONObject("subcategory").toString());
	            						jCategoryArrayList.add(null);
	            					}
	            					
	            					categoryArrayList.add(hash); 
	 
	            				}         
	            				AddQuickactionbutton(categoryArrayList);
	            			}
	            			
	            			else{
	            				Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
	            			}
	            		} catch (JSONException e) {
	            				
	            		} catch (RuntimeException e) {
	            			e.printStackTrace();
	            	} catch (Exception e) {
	            		
	            		}
	                		
	                		
	                    }else{                      
	                        Toast.makeText(aq.getContext(), "Please check  your internet connection", Toast.LENGTH_LONG).show();
	                }                            
	             }
	    	};
	    	cb.header("User-Agent", "android");

		 String t = "0";
		 String type = "1";
		 String pNew = "1";
		 String library = UtConfig.LIBRARY_CODE;

		 NLog.w("Load","t : " + t);
		 NLog.w("Load","type : " + type);
		 NLog.w("Load","new : " + pNew);
		 NLog.w("Load","Library : " + library);

	    	 List<NameValuePair> pairs = new ArrayList<NameValuePair>();
	         pairs.add(new BasicNameValuePair("t", t));
	         pairs.add(new BasicNameValuePair("type", type));
	         pairs.add(new BasicNameValuePair("new", pNew));
	         pairs.add(new BasicNameValuePair("library", library));
	   
	         HttpEntity entity = null;
	 		try {
	 			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
	 		} catch (UnsupportedEncodingException e) {
	 			e.printStackTrace();
	 		}
	         Map<String, Object> params = new HashMap<String, Object>();
	         params.put(AQuery.POST_ENTITY, entity);
	         
	         aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	    }
	 
	 private void AddQuickactionbutton(final ArrayList<HashMap<String, String>> category){	
			quickAction = new QuickAction(ac, QuickAction.VERTICAL,category.get(0).get("name"));
			
			for (int a = 0; a < category.size(); a++) {
				File ext = Environment.getExternalStorageDirectory();
				File target = new File(ext, "Android/data/"+ac.getPackageName()+"/cache/"+String.valueOf(category.get(a).get("image").hashCode()));        
				aq.download(category.get(a).get("image"), target, new AjaxCallback<File>(){

			        public void callback(String url, File file, AjaxStatus status) {
			                if(file != null){

			                }
			        }     
			});		
				ActionItem act = null;
//				if (category.get(a).get("subcategory")!=null) {
//					if (category.get(a).get("subcategory").equals("2")) {
//						act = new ActionItem(a, ""+category.get(a).get("name"), null);
//					}
//					if (category.get(a).get("subcategory").equals("3")) {
//						act = new ActionItem(a, ""+category.get(a).get("name"), null);
//					}
//
//				}
//				else{
//					act = new ActionItem(Drawable.createFromPath(target.getAbsolutePath()));
//				}
				act = new ActionItem(a,category.get(a).get("name").toString(),Drawable.createFromPath(target.getAbsolutePath()),category.get(a).get("image").toString());
				quickAction.addActionItem(act, category.size()); 
				
			}
			
		    
			quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {			
				@Override
				public void onItemClick(QuickAction source, int pos, int actionId) {				
					ActionItem actionItem = quickAction.getActionItem(pos);
					if(category.get(actionItem.getActionId()).get("subcategory")!= null && category.get(actionItem.getActionId()).get("subcategory").length()>0){
						if(isDisplayAllCategory){
							
							AddQuickactionSubcategoryButton(jCategoryArrayList.get(Integer.parseInt(category.get(actionItem.getActionId()).get("subcategory"))));
						}else{
							AddQuickactionSubcategoryButton(jCategory.get(Integer.parseInt(category.get(actionItem.getActionId()).get("subcategory"))));
						}
						
						if (quickAction!=null) {
							quickAction.show(BkShelf.categoryView);
						}
						isDisplayAllCategory = false;
					}else{
						
            			
						if(ac.getClass().equals(BkShelfByCategory.class)){
							new LdBkShelfCategoryDetailPageLoader(ac, category.get(actionId).get("image"),
									category.get(actionId).get("name")).SetCategoryDetailLoader(category.get(actionId).get("id"));
						}else{
							Intent i = new Intent(ac, BkShelfByCategory.class);
							i.putExtra("id_category", category.get(actionId).get("id"));
							i.putExtra("name_category", category.get(actionId).get("name"));
	//						i.putExtra("img_header_path", category.get(actionId).get("img"));
							i.putExtra("name_thumbnail_path", category.get(actionId).get("image"));
							i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							ac.startActivity(i); 
						}
					}
//					showCategoryDetail(category.get(actionItem.getActionId()).get("id"), category.get(actionItem.getActionId()).get("name"));
				}
			});
	        aq.id(R.id.acb_category).clickable(true);
		}
		
	 private void AddQuickactionSubcategoryButton(final JSONArray categoryJson){	
		 
		 
		 try {
 			if (categoryJson!=null) { 
 				ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
 				JSONArray c = categoryJson;  
//					Log.e("c", c.toString());
					int countSubcatArray = 0;
					for(int i=0;i<c.length();i++){
 					HashMap<String, String> hash = new HashMap<String, String>();
 					JSONObject json2 = c.getJSONObject(i);
 					String id = json2.getString("id");
 					String name = json2.getString("name");
 					String image = json2.getString("image");
 					
 					//String parent = json2.getString("parent");
 					hash.put("id",id);
 					hash.put("name",name);
 					hash.put("image", image);	
 					if(!json2.isNull("subcategory")&& json2.optJSONArray("subcategory")!=null){
 						jCategory = new ArrayList<JSONArray>();
    					jCategory.add(json2.getJSONArray("subcategory"));
    					hash.put("subcategory", countSubcatArray+"");
    					countSubcatArray++;
					}else{
						jCategory.add(null);
					}
 					
 					arrayList.add(hash); 

 				}         
 				AddQuickactionbutton(arrayList);
 				
 			}
 			
 			else{
 				Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
 			}
 			} catch (JSONException e) {
 				
 			} catch (RuntimeException e) {
 			e.printStackTrace();
 			} catch (Exception e) {
 		
 			}
		 
		}
	 

	 private void dgUpdateVersion(final Activity ac) {
	
		          AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ac);
		          
		          alertDialogBuilder.setTitle("Application Update!");		
		          alertDialogBuilder.setMessage("Please update new version.");
		          alertDialogBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
		 
		                 public void onClick(DialogInterface dialog,int id) {
		
		                	StringBuilder localStringBuilder = new StringBuilder("market://details?id=");
		             	    String str = ac.getPackageName();
		             	    localStringBuilder.append(str);
		             	    Uri localUri = Uri.parse(localStringBuilder.toString());
		             	    Intent localIntent = new Intent("android.intent.action.VIEW", localUri);
		             	    ac.startActivity(localIntent);
		             	    dialog.dismiss();
		             	    System.gc();
		             	   	System.exit(0);
		                 }
		 
		               });
		 
		          AlertDialog alertDialog = alertDialogBuilder.create();
	
		          alertDialog.show();
		 
		     }

//		private void showCategoryDetail(){
//
//			  aq.id(R.id.acb_category).clicked(new OnClickListener() {
//					
//				@Override
//				public void onClick(View v) {
//					if (quickAction!=null) {
//						quickAction.show(v);
//					}
//				
//				}
//			});
//	    }
		
		public void QuickActionOnClick(View v){
			if(categoryArrayList!=null && categoryArrayList.size()>0){
				isDisplayAllCategory = true;
				AddQuickactionbutton(categoryArrayList);
			}else{
				SetAllCategoryLoader();
			}
			if (quickAction!=null) {
				quickAction.show(v);
			}
		}
}
