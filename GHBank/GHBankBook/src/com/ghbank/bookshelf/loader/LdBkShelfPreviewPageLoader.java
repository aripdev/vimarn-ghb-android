package com.ghbank.bookshelf.loader;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.arip.it.library.MuPDFActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ghbank.bookshelf.custom.BookSize;
import com.ghbank.bookshelf.custom.PopupReview;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.download.DnLoadPdfFromUrl;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.NLog;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtSharePreferences;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.custom.HorizontalScrollViewCustom;
import com.ghbank.bookshelf.custom.StarRating;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.image.ImageDisplayOptions;
import com.ghbank.bookshelf.utils.UtDesign;
import com.ghbank.bookshelf.utils.UtDevice;

public class LdBkShelfPreviewPageLoader extends PopupReview implements OnDismissListener {

	private static final String TAG = LdBkShelfPreviewPageLoader.class.getSimpleName();

	private static String DB_PATH;
	private Activity ac;
	private AQuery aq;
	private DbBookHandler bh;
	private Intent i;
	private String txtDescription;
	private PopupReview popupReview;

	private HorizontalScrollViewCustom scrViewFeature;

	public static String id,name,summary,datetime,cover;
	private boolean isDownload = false;
	private static boolean isDownloadFinish = false;

	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private Handler handler;
	private long timeDelay = 3000; //2 seconds
	private boolean isDoReload = true;
	private OnDismissListener mDismissListener;
	private SharedPreferences sPref;
	public LdBkShelfPreviewPageLoader(Activity ac){
		this.ac = ac;
		sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		Initial();

	}

	private void Initial(){

		aq = new AQuery(ac);
		bh = new DbBookHandler(ac);
		bh.open();
		i = ac.getIntent();
		scrViewFeature = (HorizontalScrollViewCustom)ac.findViewById(R.id.scroll_features);
		new BookSize(ac).setNormalSize();

	}


	public void SetCategoryPreviewLoader(final String cat_id){
		id = cat_id;

		DB_PATH = UtConfig.DESIGN_PATH+id+"/";


		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
			@Override
			public void callback(String url, JSONObject json,
								 AjaxStatus status) {
				super.callback(url, json, status);
				ArrayList<HashMap<String, String>> arrayImage = new ArrayList<HashMap<String,String>>();
				if(json != null){
					try {
						final JSONObject jsonReview = json;
						if (json.getString("status").equals("1")) {
							name = json.getString("name");
							txtDescription = name;

							int index = 0;
							if(UtDevice.getHeight() >= 1920){
								index = 2;
							}else if(UtDevice.getHeight()>=1280){
								index = 1;
							}else if(UtDevice.getHeight()<1280){
								index = 0;
							}

							cover = json.getJSONArray("cover_new").getJSONObject((json.getJSONArray("cover_new").length() - 1) >= index ? index : 0)
									.getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));

							summary = json.getString("summary");
							datetime = json.getString("datetime");
							String size = json.getString("size");
							String ratingCount = json.getString("rating_count");
							float ratingAverage = Float.parseFloat(json.getString("rating_average"));
							String messageError = json.getString("message_error");
							final String statusType = json.getString("status");

							//tag json review_all is type array
							if(!json.isNull("preview_pic_new")) {
								JSONArray jArr = json.getJSONArray("preview_pic_new");
								for (int i = 0; i < jArr.length(); i++) {
									HashMap<String, String> hash2 = new HashMap<String, String>();
									String img = "";

									if (UtDevice.getHeight() >= 1920) {
										img = jArr.getJSONArray(i).getJSONObject(1).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
									} else if (UtDevice.getHeight() >= 1280) {
										img = jArr.getJSONArray(i).getJSONObject(1).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
									} else if (UtDevice.getHeight() < 1280) {
										img = jArr.getJSONArray(i).getJSONObject(0).getString("url").replace("$", UtSharePreferences.getPrefServerName(ac));
									}

									hash2.put("image", img);
									arrayImage.add(hash2);
								}
							}

//	           					aq.id(R.id.image_cover_detail).image(cover, true, true, 0, 0, null, 0);    
//	           					aq.id(R.id.txt_name_detail).text(name);
//	           					aq.id(R.id.txt_summary_detail).text(summary);
//	           					aq.id(R.id.txt_date_detail).text(datetime);
//	           					aq.id(R.id.txt_size_detail).text(size+" MB");

//	           					ImageView img_cover = (ImageView) ac.findViewById(R.id.img_cover_detail);
//	           					TextView txt_description = (TextView) ac.findViewById(R.id.txt_description); 
//	           					TextView txt_facebook = (TextView) ac.findViewById(R.id.txt_facebook_friend);
//	           					TextView txt_rating = (TextView) ac.findViewById(R.id.txt_rating);
//	           					TextView txt_date = (TextView) ac.findViewById(R.id.txt_date);
//	           					ImageView img_star1 = (ImageView) ac.findViewById(R.id.img_star1);
//	           					ImageView img_star2 = (ImageView) ac.findViewById(R.id.img_star2);
//	           					ImageView img_star3 = (ImageView) ac.findViewById(R.id.img_star3);
//	           					ImageView img_star4 = (ImageView) ac.findViewById(R.id.img_star4);
//	           					ImageView img_star5 = (ImageView) ac.findViewById(R.id.img_star5);
//	           					ProgressBar pgb_download = (ProgressBar) ac.findViewById(R.id.pgb_download);
//	           					TextView txt_book_size = (TextView) ac.findViewById(R.id.txt_book_size);
//	           					ImageView img_cover_detail = (ImageView) ac.findViewById(R.id.img_cover_detail);
//	           					ImageView img_review = (ImageView) ac.findViewById(R.id.img_review);
							final ImageView pb = (ImageView) ac.findViewById(R.id.pb_cover_loading);

							aq.id(R.id.view_detail__txt_title).text(name).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
							aq.id(R.id.view_detail__txt_rating).text("("+ratingCount+" Ratings)").typeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
							aq.id(R.id.view_detail__txt_book_size).text(size+" MB.").typeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
							aq.id(R.id.txt_facebook_friend).text("0 Friends").typeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));

							aq.id(R.id.img_star1).getImageView().setImageResource(StarRating.setDisplay(ratingAverage, 1));
							aq.id(R.id.img_star2).getImageView().setImageResource(StarRating.setDisplay(ratingAverage, 2));
							aq.id(R.id.img_star3).getImageView().setImageResource(StarRating.setDisplay(ratingAverage, 3));
							aq.id(R.id.img_star4).getImageView().setImageResource(StarRating.setDisplay(ratingAverage, 4));
							aq.id(R.id.img_star5).getImageView().setImageResource(StarRating.setDisplay(ratingAverage, 5));

							if (!bh.check(cat_id)) {
//	           					  if(i.getStringExtra("tag") !=null && i.getStringExtra("tag").equals("update")){
								if (i.getStringExtra("status_book")!=null && i.getStringExtra("status_book").equalsIgnoreCase("update")) {
									isDownloadFinish = false;
								}else{

									File fDownload = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON13);

									if(fDownload.exists()){
										aq.id(R.id.view_detail_img_download).getImageView().setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON13));
									}else{
										txtDescription = aq.id(R.id.view_detail__txt_title).getText().toString();
										aq.id(R.id.view_detail_img_download).getImageView().setImageResource(R.drawable.op_ic_download_finish);
									}

									aq.id(R.id.view_detail__pgb_download).getProgressBar().setProgress(100);
									isDownloadFinish = true;
								}
							}else{
								isDownloadFinish = false;
							}



//	           				  if (getIntent().getExtras().containsKey("tag")) {
//	           					aq.id(R.id.btn_download_detail).text("Update");
//	           					}

//							if(size.equalsIgnoreCase("0")) {
//								aq.id(R.id.view_detail_img_download).invisible();
//							}

							aq.id(R.id.view_detail_img_download).clicked(new OnClickListener() {

								@Override
								public void onClick(View v) {
									NLog.w(TAG,"SetCategoryPreviewLoader " + "Btn download clicked.");
									handler = new Handler();
									handler.postDelayed(new Runnable() {
										public void run() {
											isDoReload = true;
//		           			            	aq.id(R.id.img_download).getImageView().setAlpha(100f);
											aq.id(R.id.view_detail_img_download).enabled(true);
										}
									}, timeDelay);


									if(isDoReload){
										downloadEventHandler();
									}
//		           					aq.id(R.id.img_download).getImageView().setAlpha(1f);
									aq.id(R.id.view_detail_img_download).enabled(false);
									isDoReload = false;

//		           						getDownloadLink(id);

//		           						if (!bh.check(id)&&!getIntent().getExtras().containsKey("tag")) {	
//		           							String path = bh.getPDF(id);
//		           							if( path.indexOf("book.pdf") != -1 )OpenPdfIntent(bh.getPDF(id));
//		           							else if( path.indexOf("cartoon.jpg") != -1 ){
////		           								Intent cartoon = new Intent(ac, CartoonActivity.class);
////		           								cartoon.putExtra("id", Integer.valueOf(id));
////		           								ac.startActivity(cartoon);
//		           							}
//		           						}
//		           						else if(!bh.check(id)&&getIntent().getExtras().containsKey("tag")){
//		           							getDownloadLink(id);
//		           						}
//		           						else{
//		           							getDownloadLink(id);
//		           						}
//
								}
							});

							aq.id(R.id.view_detail_img_vote).clicked(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// popupReview = new PopupReview(ac,id);
									popupReview = new PopupReview();
									popupReview.initial(ac, id);
									popupReview.show(jsonReview);
								}

							});


							Date date;
							try {
								date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime);
								String formattedDate = new SimpleDateFormat("dd MMM yyyy").format(date);
//									txt_date.setText(formattedDate);
								aq.id(R.id.view_detail__txt_date).text(formattedDate).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// format the java.util.Date object to the desired format

//	           					img_cover.getLayoutParams().height = book_cover_heigth-50;
//	           					img_cover.getLayoutParams().width = book_cover_width;


							imageLoader.displayImage(cover, aq.id(R.id.img_cover_detail).getImageView(), ImageDisplayOptions.getImageOptionCategory(), new SimpleImageLoadingListener(){

								@Override
								public void onLoadingStarted(String imageUri, View view) {
									pb.setVisibility(View.VISIBLE);
								}

								@Override
								public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
									String message = null;
									switch (failReason.getType()) {
										case IO_ERROR:
											message = "Input/Output error";
											break;
										case DECODING_ERROR:
											message = "Image can't be decoded";
											break;
										case NETWORK_DENIED:
											message = "Downloads are denied";
											break;
										case OUT_OF_MEMORY:
											message = "Out Of Memory error";
											break;
										case UNKNOWN:
											message = "Unknown error";
											break;
									}
									Toast.makeText(ac, message, Toast.LENGTH_SHORT).show();

									pb.setVisibility(View.GONE);
								}

								@Override
								public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
									pb.setVisibility(View.INVISIBLE);
									aq.id(R.id.lay_detail).visibility(View.VISIBLE);
//	           						if (i.getStringExtra("status_book")!=null && i.getStringExtra("status_book").equalsIgnoreCase("new")) {
//	           							aq.id(R.id.img_status_book).image(R.drawable.ip_ic_newtag);
//	           							aq.id(R.id.img_status_book).visibility(View.VISIBLE);
//	           						   }
//	           						   
//	           						if (i.getStringExtra("status_book")!=null && i.getStringExtra("status_book").equalsIgnoreCase("update")) {
//	           							aq.id(R.id.img_status_book).image(R.drawable.ip_ic_update);
//	           							aq.id(R.id.img_status_book).visibility(View.VISIBLE);
//	           						   }


								}
							});

						}
						else{
							Toast.makeText(aq.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

					LinearLayout horz = (LinearLayout)ac.findViewById(R.id.horz);
					TextView txtHeader = (TextView)ac.findViewById(R.id.view_detail__txt_header_book);
					txtHeader.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(ac));
					txtHeader.setText(ac.getString(R.string.txt_preview));

					if(arrayImage.size() != 0)
						horz.removeAllViews();
					else
						horz.getChildAt(0).setVisibility(View.INVISIBLE);

					for (int a = 0; a < arrayImage.size(); a++) {
						View v = ac.getLayoutInflater().inflate(R.layout.view_book, null);
						ImageView img = (ImageView) v.findViewById(R.id.img_book);
						final ImageView pb = (ImageView) v.findViewById(R.id.pg_loaging);
						final AnimationDrawable animProgress = (AnimationDrawable) pb.getDrawable();

						img.getLayoutParams().height = BookSize.book_nomal_heigth;
						img.getLayoutParams().width =   BookSize.book_nomal_width;

						FrameLayout.LayoutParams param0 = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
						horz.setLayoutParams(param0);
						horz.addView(v);

						imageLoader.displayImage(arrayImage.get(a).get("image"), img, ImageDisplayOptions.getImageOption(), new SimpleImageLoadingListener(){

							@Override
							public void onLoadingStarted(String imageUri, View view) {
								pb.setVisibility(View.VISIBLE);

								animProgress.start();
							}

							@Override
							public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
								String message = null;
								switch (failReason.getType()) {
									case IO_ERROR:
										message = "Input/Output error";
										break;
									case DECODING_ERROR:
										message = "Image can't be decoded";
										break;
									case NETWORK_DENIED:
										message = "Downloads are denied";
										break;
									case OUT_OF_MEMORY:
										message = "Out Of Memory error";
										break;
									case UNKNOWN:
										message = "Unknown error";
										break;
								}

								pb.setVisibility(View.GONE);
								animProgress.stop();
							}

							@Override
							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
								animProgress.stop();
								pb.setVisibility(View.GONE);

							}
						});


					}

					scrViewFeature.scrollTo(0, 0);
				}
			}
		};
		cb.header("User-Agent", "android");
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("t", "1"));
		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));
		//pairs.add(new BasicNameValuePair("test", "1"));
		pairs.add(new BasicNameValuePair("preview", "1"));

		pairs.add(new BasicNameValuePair("offline", "2"));
		pairs.add(new BasicNameValuePair("id", id));
		pairs.add(new BasicNameValuePair("username", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
		pairs.add(new BasicNameValuePair("pgroup", sPref.getString(SnPreferenceVariable.GROUP, "0")));

		NLog.w("Param","t : 1");
		NLog.w("Param","library : " + UtConfig.LIBRARY_CODE);
		NLog.w("Param","preview : 1");
		NLog.w("Param","offline : 2");
		NLog.w("Param","id : " + id);
		NLog.w("Param","Username : " + sPref.getString(SnPreferenceVariable.USER_NAME, ""));
		NLog.w("Param","pgroup : " + sPref.getString(SnPreferenceVariable.GROUP, "0"));

		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);

		aq.image(R.id.progressBar_horizontal).ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);
	}


	private void downloadEventHandler(){
		DnLoadPdfFromUrl download = new DnLoadPdfFromUrl(ac,id);

		if(isDownloadFinish){

			OpenPdfIntent(bh.getPDF(id));

		}else{


//				if(!isDownload){
//					File f = new File(DB_PATH);
//			    	if (f.isDirectory()) {
//			            String[] children = f.list();
//			            for (int i = 0; i < children.length; i++) {
//			                new File(f, children[i]).delete();
//			            }
//			        }
//					download.downloadLinkById(id);
//					isDownload = true;
//				}else{
			download.resetStatForDownload(id);
			isDownload = false;
//				}

			File fReDownload = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON10);

			if(fReDownload.exists()){
				setDownloadBookLog(id);
				aq.id(R.id.view_detail_img_download).getImageView().setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON10));
				aq.id(R.id.view_detail_img_download).getImageView().setAlpha(0.4f);
			}else{
				aq.id(R.id.view_detail_img_download).getImageView().setImageResource(R.drawable.op_ic_re_download);
			}
		}
	}
	public void SetBookHandler(String revision){
		bh.open();

		if (bh.check(id)) {
			bh.addFavorite(id, name, summary, datetime, DB_PATH+"book.pdf", cover, revision);
		}
		if (ac.getIntent().getExtras().containsKey("tag")) {
			bh.updateBook(id, revision);
			aq.id(R.id.view_detail_img_download).clicked(new OnClickListener() {

				@Override
				public void onClick(View v) {
					OpenPdfIntent(bh.getPDF(id));

				}
			});
		}

//	 		aq.id(R.id.btn_download_detail).text("Read");
		final Handler handler = new Handler();
		final Runnable r = new Runnable()
		{
			public void run()
			{
				//Set Rotation ON
				ac.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

				File fDownload = new File(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON13);

				if(fDownload.exists()){
					aq.id(R.id.view_detail_img_download).getImageView().setImageDrawable(Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.ICON13));
					aq.id(R.id.view_detail_img_download).getImageView().setAlpha(1.0f);
				}else{
					aq.id(R.id.view_detail_img_download).getImageView().setImageResource(R.drawable.op_ic_download_finish);
				}

				isDownloadFinish = true;
			}
		};

		handler.postDelayed(r, 1000);

	}

	public void SetBookHandler(String revision,final String idRef,String name, String summary, String datetime, String path, String cover){
		bh.open();

		if (bh.check(idRef)) {
			bh.addFavorite(idRef, name, summary, datetime, path, cover, revision);
		}
		if (ac.getIntent().getExtras().containsKey("tag")) {
			bh.updateBook(idRef, revision);
			aq.id(R.id.view_detail_img_download).clicked(new OnClickListener() {

				@Override
				public void onClick(View v) {
					OpenPdfIntent(bh.getPDF(idRef));

				}
			});
		}

//	 		aq.id(R.id.btn_download_detail).text("Read");
		final Handler handler = new Handler();
		final Runnable r = new Runnable()
		{
			public void run()
			{
				//Set Rotation ON
				ac.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

				isDownloadFinish = true;
			}
		};

		handler.postDelayed(r, 1000);

	}

	public void setDownloadBookLog(String bookId){
		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>(){
			@Override
			public void callback(String url, JSONObject json, AjaxStatus status) {

				if(json != null){
					try {
						if (json.getString("status").equals("1")) {


						}

					} catch (JSONException e) {

					} catch (RuntimeException e) {
						e.printStackTrace();
					} catch (Exception e) {

					}


				}else{
					Toast.makeText(aq.getContext(), "Please check  your internet connection", Toast.LENGTH_LONG).show();
				}
			}
		};
		final SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		cb.header("User-Agent", "android");
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("t", "4"));
		pairs.add(new BasicNameValuePair("offline", "2"));
		pairs.add(new BasicNameValuePair("username", sPref.getString(SnPreferenceVariable.USER_NAME, "")));
		pairs.add(new BasicNameValuePair("id", bookId));
		pairs.add(new BasicNameValuePair("library", UtConfig.LIBRARY_CODE));

		HttpEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(pairs, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(AQuery.POST_ENTITY, entity);

		aq.ajax(UtSharePreferences.getPrefServerConfig(ac), params, JSONObject.class, cb);

	}
	private void OpenPdfIntent(String path) {

		SharedPreferences prefs = ac.getSharedPreferences("arip", Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = prefs.edit();
		edit.putInt("arip_session", 1);
		edit.commit();

		if(path == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(ac);
			builder.setTitle("PDF Error")
					.setMessage("Cannot open PDF, please try again.")
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							ac.finish();
						}
					})
					.create().show();
			return;
		}


		Uri uri = Uri.parse(path);
		Intent intent = new Intent(ac,MuPDFActivity.class);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setData(uri);
		intent.putExtra("bookid", id);
		intent.putExtra("title", txtDescription);
		ac.startActivity(intent);
	}

	public interface OnDismissListener {
		public abstract void onDismiss();
	}


	@Override
	public void onDismiss() {
		if (mDismissListener != null) {
			mDismissListener.onDismiss();
		}

	}
}
