package com.ghbank.bookshelf.fonts;

import java.util.Hashtable;

import android.content.Context;
import android.graphics.Typeface;

public class FcustomFonts {
	
	public static final String TYPEFACE_FOLDER = "fonts";
	public static final String TYPEFACE_EXTENSION_TTC = ".ttc";
	public static final String TYPEFACE_EXTENSION_TTF = ".ttf";
	public static final String TYPEFACE_EXTENSION_OTF = ".otf";


	private static Hashtable<String, Typeface> sTypeFaces = new Hashtable<String, Typeface>(4);

	public static Typeface getTypeFace(Context context) {
	Typeface tempTypeface = sTypeFaces.get("futura");

	if (tempTypeface == null) {
	    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("futura").append(TYPEFACE_EXTENSION_TTC).toString();
	    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
	    sTypeFaces.put("futura", tempTypeface);
	}

	return tempTypeface;
	}
	
	public static Typeface getTypeFaceFuturaLight(Context context) {
		Typeface tempTypeface = sTypeFaces.get("futuralight");

		if (tempTypeface == null) {
		    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("futuralight").append(TYPEFACE_EXTENSION_TTF).toString();
		    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
		    sTypeFaces.put("futuralight", tempTypeface);
		}

		return tempTypeface;
	}
	
	public static Typeface getTypeFaceHelvetica(Context context) {
		Typeface tempTypeface = sTypeFaces.get("helveticaneueltcomlt");

		if (tempTypeface == null) {
		    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("helveticaneueltcomlt").append(TYPEFACE_EXTENSION_TTF).toString();
		    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
		    sTypeFaces.put("helveticaneueltcomlt", tempTypeface);
		}

		return tempTypeface;
	}
	
	public static Typeface getTypeFaceHelveticaXBlk(Context context) {
		Typeface tempTypeface = sTypeFaces.get("DB Helvethaica X Blk Cond");

		if (tempTypeface == null) {
		    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("DB Helvethaica X Blk Cond").append(TYPEFACE_EXTENSION_TTF).toString();
		    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
		    sTypeFaces.put("DB Helvethaica X Blk Cond", tempTypeface);
		}

		return tempTypeface;
	}
	
	public static Typeface getTypeFaceHelveticaUltraLight(Context context) {
		Typeface tempTypeface = sTypeFaces.get("helveticaneueltstdultlt");

		if (tempTypeface == null) {
		    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("helveticaneueltstdultlt").append(TYPEFACE_EXTENSION_OTF).toString();
		    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
		    sTypeFaces.put("helveticaneueltstdultlt", tempTypeface);
		}

		return tempTypeface;
	}
	
	public static Typeface getTypeFaceHelveticaLight(Context context) {
		Typeface tempTypeface = sTypeFaces.get("helveticaneueltstdlt");

		if (tempTypeface == null) {
		    String fontPath = new StringBuilder(TYPEFACE_FOLDER).append('/').append("helveticaneueltstdlt").append(TYPEFACE_EXTENSION_OTF).toString();
		    tempTypeface = Typeface.createFromAsset(context.getAssets(), fontPath);
		    sTypeFaces.put("helveticaneueltstdlt", tempTypeface);
		}

		return tempTypeface;
	}
}
