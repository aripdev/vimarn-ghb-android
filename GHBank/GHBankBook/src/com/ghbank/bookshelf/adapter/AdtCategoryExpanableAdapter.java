package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import com.ghbank.bookshelf.R;

public class AdtCategoryExpanableAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
 
    public AdtCategoryExpanableAdapter(Context context, List<String> listDataHeader,
            HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
 
        final String childText = (String) getChild(groupPosition, childPosition);
 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_category_list, null);
        }
 
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.txt_subject);
        
        ImageView imgListChild = (ImageView) convertView.findViewById(R.id.img_subject);
        
        switch (childPosition) {
		case 0:
			imgListChild.setImageResource(R.drawable.ip_ic_library_book);
			break;
		case 1:
			imgListChild.setImageResource(R.drawable.ic_subject_2);	
			break;
		case 2:
			imgListChild.setImageResource(R.drawable.ic_subject_3);
			break;
		case 3:
			imgListChild.setImageResource(R.drawable.ic_subject_1);
			break;
		case 4:
			imgListChild.setImageResource(R.drawable.ic_subject_4);
			break;
		case 5:
			imgListChild.setImageResource(R.drawable.ic_subject_5);
			break;
		case 6:
			imgListChild.setImageResource(R.drawable.ic_subject_6);
			break;
	
		default:
			break;
		}
 
        txtListChild.setText(childText.split(":")[0]);
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_category_grop, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.txt_header_subject);
        ImageView imgDropList = (ImageView) convertView.findViewById(R.id.img_drop_list);
        if(isExpanded){
        	imgDropList.setImageResource(R.drawable.ic_drop_list);
        }else{
        	imgDropList.setImageResource(R.drawable.ic_drop_plus);
        }
        
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
