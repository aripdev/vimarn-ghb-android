package com.ghbank.bookshelf.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.arip.it.library.MuPDFActivity;
import com.arip.it.library.db.BookHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.ghbank.bookshelf.BkShelfLibrary;
import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDevice;
import com.ghbank.bookshelf.R;


public class AdtLibraryAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<HashMap<String, String>> d;
	private ArrayList<HashMap<String, String>> d2;
	private AQuery aq;
	
	private int book_width;
	private int book_heigth;
	private int book_width_vertical = 4;
	private int book_width_horizontal = 5;
	private int book_heigth_vertical = 3;
	private int book_heigth_horizontal = 4;
	private boolean isShowItemDelete = false;
	
	public AdtLibraryAdapter(Context context, ArrayList<HashMap<String, String>> data,
			ArrayList<HashMap<String, String>> compare) {
		mContext = context;
		d = data;
		
		book_width_vertical = (int) mContext.getResources().getInteger(R.integer.book_width_vertical);
       	book_width_horizontal = (int) mContext.getResources().getInteger(R.integer.book_width_horizontal);
       	book_heigth_vertical = (int) mContext.getResources().getInteger(R.integer.book_heigth_vertical);
       	book_heigth_horizontal = (int) mContext.getResources().getInteger(R.integer.book_heigth_horizontal);
       	
    	Configuration config = mContext.getResources().getConfiguration();
    	if (config.orientation == Configuration.ORIENTATION_PORTRAIT){
//    		 setContentView(R.layout.lay_main);
    		 book_width = UtDevice.getWidth()/book_width_vertical;
    		 book_heigth= UtDevice.getWidth()/book_heigth_vertical;
    	}else{
//    		 setContentView(R.layout.main_horizontal);
    		 book_width = UtDevice.getWidth()/book_width_horizontal;
    		 book_heigth= UtDevice.getWidth()/book_heigth_horizontal;
    	}	
    	
		if (compare!=null) {
			d2 = compare;
		}
	}
	
	public void setShowItemDeleteBook(boolean isShow){
		isShowItemDelete = isShow;
	}
	
	@Override
	  public int getCount() {
	   return d.size();
	  }

	  @Override
	  public Object getItem(int position) {
	   return d.get(position);
	  }

	  @Override
	  public long getItemId(int position) {
	   return position;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		LayoutInflater inflater;
		  if (convertView==null) {
				inflater =LayoutInflater.from(mContext);
				convertView = inflater.inflate(R.layout.row_book, null);
	        	holder = new ViewHolder();
	        	holder.type = (ImageView)convertView.findViewById(R.id.img_more);
	        	holder.img = (ImageView)convertView.findViewById(R.id.img_grid);
	        	holder.txt = (TextView)convertView.findViewById(R.id.txt_grid);
	        	holder.tag = (ImageView)convertView.findViewById(R.id.img_status_grid);
	           	holder.pg_grid = (ImageView)convertView.findViewById(R.id.pg_grid);
	        	holder.btnCut = (ImageView)convertView.findViewById(R.id.img_cut);
	        	convertView.setTag(holder);
		}	  
		  else{
	        	holder =(ViewHolder)convertView.getTag();
		  }
		  
		
	   aq = new AQuery(convertView);
	   holder.pg_grid.setVisibility(View.GONE);
	   aq.id(holder.img).image(d.get(position).get("image"),true, true, 0, 0, null, AQuery.FADE_IN_NETWORK);
	   aq.id(holder.txt).text(d.get(position).get("name"));
	   aq.id(holder.txt).textColor(Color.GRAY);
	   aq.id(holder.txt).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(mContext));
	   
	   aq.id(holder.img).getImageView().getLayoutParams().height = book_heigth;
	   aq.id(holder.img).getImageView().getLayoutParams().width = book_width;
	   
	   if(isShowItemDelete){
		   holder.btnCut.setVisibility(View.VISIBLE);
	   }else{
		   holder.btnCut.setVisibility(View.INVISIBLE);
	   }
	   
	   
	   if(d.get(position).get("description").toString().equalsIgnoreCase("zip_book")){
		   holder.type.setImageResource(R.drawable.ic_drop_list);
	   }
	   else holder.type.setImageResource(R.drawable.ip_ic_more);
	  
	   /*
		   Log.e("Ch.AdtLib", "Id : "+d.get(position).get("id").toString());
		   Log.e("Ch.AdtLib", "Name : "+d.get(position).get("name").toString());
		   Log.e("Ch.AdtLib", "description : "+d.get(position).get("description").toString());
		   Log.e("Ch.AdtLib", "Revision : "+d.get(position).get("revision").toString());
		   */
		
		   
	
	   aq.id(holder.img).clicked(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			SharedPreferences prefs = mContext.getSharedPreferences("arip", Context.MODE_PRIVATE);
		    SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("arip_session", 1);
            edit.commit();
            
			String pdfResultPath = d.get(position).get("category");
			

			if(holder.btnCut.isShown()){
				confirmDelete(position);
			}else{
				if( pdfResultPath.indexOf("book.pdf") != -1 ){
					Uri uri = Uri.parse(pdfResultPath);
					Intent intent = new Intent(mContext,MuPDFActivity.class);
					intent.setAction(Intent.ACTION_VIEW);
					intent.setData(uri);
					intent.putExtra("bookid", d.get(position).get("id"));
					intent.putExtra("title", d.get(position).get("name"));
					mContext.startActivity(intent);
				}
	//			else if( pdfResultPath.indexOf("cartoon.jpg") != -1 ){
	//				Intent cartoon = new Intent(mContext, CartoonActivity.class);
	//				cartoon.putExtra("id", Integer.valueOf(d.get(position).get("id")));
	//				mContext.startActivity(cartoon);
	//			}
			}
		}
	});
	   
	   holder.img.setLongClickable(true);
	   holder.img.setOnLongClickListener(new OnLongClickListener() {
		
		@Override
		public boolean onLongClick(View v) {
			confirmDelete(position);
			return true;
		}
	});
	   
	   
	   try{
		   
		   
		   if ( !(d.get(position).get("description").toString().equalsIgnoreCase("zip_book")) && d2!=null) {
			   double re_new = d2.size() > 0 ? Double.parseDouble(d2.get(position).get("revision")) : 0;
			   double re_old = d.size() > 0 ? Double.parseDouble(d.get(position).get("revision")) : 0;
			   //aq.id(holder.txt).text(d.get(position).get("revision"));
			   if (re_new > re_old) {
	
				   holder.tag.setVisibility(View.VISIBLE);
				   holder.tag.setImageResource(R.drawable.ip_ic_update);
				   aq.id(holder.img).clicked(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(mContext, BkShelfPreview.class);
							intent.putExtra("id_category", d.get(position).get("id"));
							intent.putExtra("status_book", "update");
							intent.putExtra("tag", "update");
							mContext.startActivity(intent);
						}
					});
			   }
			   else{
				   holder.tag.setVisibility(View.INVISIBLE);
			   }
		   }
	   }catch(NumberFormatException e){
		   Log.e("LibraryAdapter", "NumberFormatException");
	   }

	   
	   return convertView;
	  }
	  
		private void confirmDelete(final int position){
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
	        builder.setCancelable(true);
	        builder.setTitle("Are you sure you want to delete this book?");
	        builder.setInverseBackgroundForced(true);
	        builder.setPositiveButton("Yes",
	                new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog,
	                            int which) {
	                    	
	                    	Log.e("AdtLibraryAdapter", "delete : "+d.get(position).get("id"));
	                    	 
	    					BookHandler favoriteHandler = new BookHandler(mContext);
	    					favoriteHandler.open();
	    					favoriteHandler.deleteComment(d.get(position).get("id"));
	    					deleteFileById(d.get(position).get("id"));
	    					
	    					
	    					

	                    }
	                });
	        builder.setNegativeButton("No",
	                new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog,
	                            int which) {
	                        dialog.dismiss();
	                    }
	                });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
	  
	   static class ViewHolder{
//		   LinearLayout layout;
	        ImageView img;
	        TextView txt;
	        ImageView tag;
	        ImageView pg_grid;
	        ImageView btnCut;
	        ImageView type;
	    }
	   
	   private void deleteFileById(String id){
			 new DeleteFile().execute(id);
		 }
		 
		 private void deleteRecursive(File fileOrDirectory) {
			    if (fileOrDirectory.isDirectory())
			        for (File child : fileOrDirectory.listFiles())
			            deleteRecursive(child);

			    fileOrDirectory.delete();
			}
		   
		 private class DeleteFile extends AsyncTask<String, Void, Void>{
			   private ProgressDialog pd;
			   
			   @Override
			   protected Void doInBackground(String... params) {
		        	String DB_PATH = UtConfig.DESIGN_PATH+params[0]+"/";
		        	File fileOrDirectory = new File(DB_PATH);
		        	deleteRecursive(fileOrDirectory);
				return null;
			   }
			
			   @Override
			   protected void onPreExecute() {
				super.onPreExecute();
				pd = ProgressDialog.show(mContext, null, "Deleting");
			   }
			   
			   @Override
			   protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				pd.dismiss();
				BkShelfLibrary libraly= (BkShelfLibrary)mContext;
				libraly.DisplayLibraryBySort(BkShelfLibrary.TITLE);
			   }
		   }
		   
}