package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;

/**
 * Created by androiddev on 12/01/2017.
 */

public class NoItemAdapter extends RecyclerView.Adapter<NoItemAdapter.NoItemViewHolder>{

    private Context context;
    private String message;

    public NoItemAdapter(Context context,String message){
        this.context = context;
        this.message = message;
    }

    @Override
    public NoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.view_no_item,parent,false);
        return new NoItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoItemViewHolder holder, int position) {
        holder.textMessage.setText(message);
        holder.textMessage.setTextColor(ThemeResourceManager.getFontColor(context));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    static class NoItemViewHolder extends RecyclerView.ViewHolder{
        TextView textMessage;
        NoItemViewHolder(View itemView) {
            super(itemView);
            textMessage = (TextView)itemView.findViewById(R.id.textMessage);
        }
    }
}
