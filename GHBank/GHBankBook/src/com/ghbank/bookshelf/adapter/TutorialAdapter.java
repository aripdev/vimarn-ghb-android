package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.utils.NLog;

/**
 * Created by androiddev on 19/01/2017.
 */

public class TutorialAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> list;

    public TutorialAdapter(Context context, ArrayList<String> list){
        this.context = context;
        this.list = list;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.tutorial_item_view,container,false);
        final ImageView image = (ImageView)view.findViewById(R.id.image);
        NLog.w("P",list.get(position));
        final ProgressWheel progressWheel = (ProgressWheel)view.findViewById(R.id.progress_wheel);
        progressWheel.spin();
        Glide.with(context)
                .load(list.get(position))
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        progressWheel.stopSpinning();
                        if(resource != null) {
                            image.setImageBitmap(resource);
                        }
                    }
                });

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

}
