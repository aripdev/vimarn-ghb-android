package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;

import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.db.DbBookHandler;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.image.ImageDisplayOptions;
import com.ghbank.bookshelf.utils.UtDevice;

public class AdtCategoryDetailAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<HashMap<String, String>> d;
	private AQuery aq;
	private DbBookHandler bh;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	
	private int book_width;
	private int book_heigth;
	private int book_width_vertical = 4;
	private int book_width_horizontal = 5;
	private int book_heigth_vertical = 3;
	private int book_heigth_horizontal = 4;
	
	public AdtCategoryDetailAdapter(Context context, ArrayList<HashMap<String, String>> data) {
		mContext = context;
		d = data;
		bh = new DbBookHandler(mContext);
		book_width_vertical = (int) mContext.getResources().getInteger(R.integer.book_width_vertical);
       	book_width_horizontal = (int) mContext.getResources().getInteger(R.integer.book_width_horizontal);
       	book_heigth_vertical = (int) mContext.getResources().getInteger(R.integer.book_heigth_vertical);
       	book_heigth_horizontal = (int) mContext.getResources().getInteger(R.integer.book_heigth_horizontal);
       	
     	Configuration config = mContext.getResources().getConfiguration();
    	if (config.orientation == Configuration.ORIENTATION_PORTRAIT){
//    		 setContentView(R.layout.lay_main);
    		 book_width = UtDevice.getWidth()/book_width_vertical;
    		 book_heigth= UtDevice.getWidth()/book_heigth_vertical;
    	}else{
//    		 setContentView(R.layout.main_horizontal);
    		 book_width = UtDevice.getWidth()/book_width_horizontal;
    		 book_heigth= UtDevice.getWidth()/book_heigth_horizontal;
    	}	
	}
	
	@Override
	  public int getCount() {
	   return d.size();
	  }

	  @Override
	  public Object getItem(int position) {
	   return d.get(position);
	  }

	  @Override
	  public long getItemId(int position) {
	   return position;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		LayoutInflater inflater;
		  if (convertView==null) {
				inflater =LayoutInflater.from(mContext);
				convertView = inflater.inflate(R.layout.row_category, null);
	        	holder = new ViewHolder();
	        	holder.img = (ImageView)convertView.findViewById(R.id.img_grid);
	        	holder.txt = (TextView)convertView.findViewById(R.id.txt_grid);
	        	holder.tag = (ImageView)convertView.findViewById(R.id.img_status_grid);
	        	holder.pb = (ImageView)convertView.findViewById(R.id.pg_grid);
	        	convertView.setTag(holder);
		}	  
		  else{
	        	holder =(ViewHolder)convertView.getTag();
		  }
	   aq = new AQuery(convertView);

	   aq.id(holder.txt).text(d.get(position).get("name"));
	   aq.id(holder.txt).textColor(Color.GRAY);
	   aq.id(holder.txt).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(mContext));
	   holder.img.getLayoutParams().height = book_heigth;
	   holder.img.getLayoutParams().width = book_width;
	   final AnimationDrawable animProgress = (AnimationDrawable) holder.pb.getDrawable();
	   imageLoader.displayImage(d.get(position).get("image"), holder.img, ImageDisplayOptions.getImageOptionCategory(), new SimpleImageLoadingListener(){
			
			@Override
			public void onLoadingStarted(String imageUri, View view) {
				holder.pb.setVisibility(View.VISIBLE);
				animProgress.start();
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				String message = null;
				switch (failReason.getType()) {
					case IO_ERROR:
						message = "Input/Output error";
						break;
					case DECODING_ERROR:
						message = "Image can't be decoded";
						break;
					case NETWORK_DENIED:
						message = "Downloads are denied";
						break;
					case OUT_OF_MEMORY:
						message = "Out Of Memory error";
						break;
					case UNKNOWN:
						message = "Unknown error";
						break;
				}
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

				holder.pb.setVisibility(View.GONE);
				animProgress.stop();
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				holder.pb.setVisibility(View.GONE);
				animProgress.stop();
				if (d.get(position).get("status").equals("New")) {
					holder.tag.setVisibility(View.VISIBLE);
				   }
				   else{
					   holder.tag.setVisibility(View.INVISIBLE);
				   }
			}
		});

	   aq.id(holder.img).clicked(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(mContext, BkShelfPreview.class);
			intent.putExtra("id_category", d.get(position).get("id"));
			bh.open();
			 if (!bh.check(d.get(position).get("id"))) {
				 intent.putExtra("tag", "update");
			 }
			 if (d.get(position).get("status").length()>0) {
				 intent.putExtra("status_book", d.get(position).get("status").toString());
			 }
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			mContext.startActivity(intent);
		}
	});
	   
//	   if (d.get(position).get("status").equals("New")) {
//		holder.tag.setVisibility(View.VISIBLE);
//	   }
//	   else{
//		   holder.tag.setVisibility(View.INVISIBLE);
//	   }
	   return convertView;
	  }
	  
	   static class ViewHolder{
	        ImageView img;
	        TextView txt;
	        ImageView tag;
	        ImageView pb;
	    }
}
	   