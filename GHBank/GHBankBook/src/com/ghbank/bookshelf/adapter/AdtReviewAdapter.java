package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.HashMap;

import com.ghbank.bookshelf.custom.StarRating;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.R;

public class AdtReviewAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<HashMap<String, String>> d;
	private AQuery aq;

	public AdtReviewAdapter(Context context, ArrayList<HashMap<String, String>> data) {
		mContext = context;
		d = data;}
	
	@Override
	  public int getCount() {
	   return d.size();
	  }

	  @Override
	  public Object getItem(int position) {
	   return d.get(position);
	  }

	  @Override
	  public long getItemId(int position) {
	   return position;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		LayoutInflater inflater;
		  if (convertView==null) {
				inflater =LayoutInflater.from(mContext);
				convertView = inflater.inflate(R.layout.row_review, null);
	        	holder = new ViewHolder();
	        	
	        	holder.img1 = (ImageView)convertView.findViewById(R.id.img_star1);
	        	holder.img2 = (ImageView)convertView.findViewById(R.id.img_star2);
	        	holder.img3 = (ImageView)convertView.findViewById(R.id.img_star3);
	        	holder.img4 = (ImageView)convertView.findViewById(R.id.img_star4);
	        	holder.img5 = (ImageView)convertView.findViewById(R.id.img_star5);
	        	holder.txt_desc = (TextView)convertView.findViewById(R.id.txt_desc);
	        	holder.txt_title = (TextView)convertView.findViewById(R.id.view_detail__txt_title);
	        	holder.txt_date = (TextView)convertView.findViewById(R.id.txt_review_by_date);
	        	convertView.setTag(holder);
		}	  
		  else{
	        	holder =(ViewHolder)convertView.getTag();
		  }
		  
		  try{
			   aq = new AQuery(convertView);
			
			   aq.id(holder.img1).getImageView().setImageResource(StarRating.setDisplay(Float.parseFloat(d.get(position).get("average")), 1));
			   aq.id(holder.img2).getImageView().setImageResource(StarRating.setDisplay(Float.parseFloat(d.get(position).get("average")), 2));
			   aq.id(holder.img3).getImageView().setImageResource(StarRating.setDisplay(Float.parseFloat(d.get(position).get("average")), 3));
			   aq.id(holder.img4).getImageView().setImageResource(StarRating.setDisplay(Float.parseFloat(d.get(position).get("average")), 4));
			   aq.id(holder.img5).getImageView().setImageResource(StarRating.setDisplay(Float.parseFloat(d.get(position).get("average")), 5));
			   aq.id(holder.txt_desc).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(mContext));
			   aq.id(holder.txt_title).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(mContext));
			   aq.id(holder.txt_date).typeface(FcustomFonts.getTypeFaceHelveticaXBlk(mContext));
			   
			   aq.id(holder.txt_desc).text(d.get(position).get("desc"));
			   aq.id(holder.txt_title).text(d.get(position).get("title"));
			   aq.id(holder.txt_date).text(d.get(position).get("date"));
		  }catch (Exception e){
			  
		  }
	   return convertView;
	  }
	  
	   static class ViewHolder{
	
	        ImageView img1;
	        ImageView img2;
	        ImageView img3;
	        ImageView img4;
	        ImageView img5;
	        TextView txt_desc;
	        TextView txt_title;
	        TextView txt_date; 
	    }
}