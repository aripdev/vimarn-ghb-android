package com.ghbank.bookshelf.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import com.ghbank.bookshelf.BkShelfPreview;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.models.Book;
import com.ghbank.bookshelf.view.SearchViewHolder;

/**
 * Created by androiddev on 11/01/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder>{

    private Context context;
    private List<Book> list;

    public SearchAdapter(Context context, List<Book> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.view_book_card,parent,false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, final int position) {

        final Book item = list.get(position);
        holder.textName.setText(item.name);
        holder.textName.setTextColor(ThemeResourceManager.getFontColor(context));
        Glide.with(context).load(item.imageUrl)
                .placeholder(R.drawable.place_holder_image)
                .error(R.drawable.default_book_cover)
                .into(holder.imgBook);

        holder.btnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BkShelfPreview.class);
                intent.putExtra("id_category", item.id);
                intent.putExtra("details", item.description);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
