package com.ghbank.bookshelf;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ghbank.bookshelf.activity.SearchActivity;
import com.ghbank.bookshelf.change.category.BookShelfViewCategory;
import com.ghbank.bookshelf.change.category.CategorySelectActivity;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.change.loaded.LoadedBookMainFragment;
import com.ghbank.bookshelf.custom.BookSize;
import com.ghbank.bookshelf.download.DnLoadPdfFromUrl;
import com.ghbank.bookshelf.loader.LdBkShelfPreviewPageLoader;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.commons.lang3.StringEscapeUtils;

import group.aim.framework.helper.ScreenSizeHelper;

public class BkShelfPreview extends FragmentActivity {

	private Intent i;
	private ImageView img_cover;
	private RelativeLayout layCover;
	@SuppressWarnings("unused")
	private ImageView imgDownload;

	protected ImageLoader imageLoader = ImageLoader.getInstance();
	private AnimationDrawable animProgress;
	private LdBkShelfPreviewPageLoader ldkPreviewLoader;

	//Add on 1 Dec 2016
	private RelativeLayout layoutFeature;
	private ProgressBar progressBarDownload;
	private ImageView pointerIcon , downloadIcon , ratingIcon;
	private LinearLayout layoutContainer , layoutDetails;
	private ImageView navLeftButton , navLogo , navButtonFirst , navButtonSecond , navButtonThird;
	private static final int REQUEST_CATEGORY_INTENT_FROM_PREVIEW = 10010;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_preview);

		this.initialView();
		this.setupView();
		this.setNavigationButtonOnClick();
		this.initialValue();
		this.loadBookDetails();
	}

	private void initialView(){
//		final ActionBar actionBar = getActionBar();
//		actionBar.hide();

		this.img_cover = (ImageView) findViewById(R.id.img_cover_detail);
		this.imgDownload = (ImageView) findViewById(R.id.view_detail_img_download);
		this.layCover = (RelativeLayout) findViewById(R.id.lay_cover_loading);
		this.ldkPreviewLoader = new LdBkShelfPreviewPageLoader(this);

		this.progressBarDownload = (ProgressBar) findViewById(R.id.view_detail__pgb_download);
		this.pointerIcon = (ImageView) findViewById(R.id.view_detail__arrow_book);
		this.layoutContainer = (LinearLayout) findViewById(R.id.view_preview__container);
		this.layoutDetails = (LinearLayout) findViewById(R.id.view_preview__detail_book);
		this.layoutFeature = (RelativeLayout) findViewById(R.id.view_preview__features_body);
		this.navButtonFirst = (ImageView) findViewById(R.id.view_preview__nav_bar_button_right_first);
		this.navButtonSecond = (ImageView) findViewById(R.id.view_preview__nav_bar_button_right_second);
		this.navButtonThird = (ImageView) findViewById(R.id.view_preview__nav_bar_button_right_third);
		this.navLeftButton = (ImageView) findViewById(R.id.view_preview__nav_bar_button_left);
		this.navLogo = (ImageView) findViewById(R.id.view_preview__nav_bar_logo);
		this.downloadIcon = (ImageView) findViewById(R.id.view_detail_img_download);
		this.ratingIcon = (ImageView) findViewById(R.id.view_detail_img_vote);

		Drawable headerBackground = ThemeResourceManager.getBackgroundFromType(4);
		if(headerBackground != null){
			ImageView headerView = (ImageView)findViewById(R.id.headerBackground);
			headerView.setImageDrawable(headerBackground);
		}

	}

	private void setupView() {

		Drawable backgroundDrawable = ThemeResourceManager.getBackground1(this);
		Drawable searchIcon = ThemeResourceManager.getSearchIcon();
		Drawable myBookIcon = ThemeResourceManager.getMyBookIcon();
		Drawable categoryIcon = ThemeResourceManager.getCategoryIcon();
		Drawable backIcon = ThemeResourceManager.getBackIcon();
		Drawable logoIcon = ThemeResourceManager.getLogoIcon();
		Drawable downloadIcon = ThemeResourceManager.getDownloadIcon();
		Drawable ratingIcon = ThemeResourceManager.getRatingIcon();


		if(downloadIcon != null)
			this.downloadIcon.setImageDrawable(downloadIcon);

		if(backgroundDrawable != null) {
			this.layoutContainer.setBackground(backgroundDrawable);
		}else{
			this.layoutContainer.setBackgroundResource(R.drawable.bg1_1);
		}

		if(searchIcon != null)
			this.navButtonFirst.setImageDrawable(searchIcon);

		if(categoryIcon != null)
			this.navButtonSecond.setImageDrawable(categoryIcon);

		if(myBookIcon != null)
			this.navButtonThird.setImageDrawable(myBookIcon);

		if(logoIcon != null)
			this.navLogo.setImageDrawable(logoIcon);

		if(backIcon != null)
			this.navLeftButton.setImageDrawable(backIcon);

		View ratingView = findViewById(R.id.ratingView);
		View lineView = findViewById(R.id.view_detail__line_break);

		if(ThemeResourceManager.isShowRatingIcon(this)) {
			this.ratingIcon.setVisibility(View.VISIBLE);
			ratingView.setVisibility(View.VISIBLE);
			lineView.setVisibility(View.VISIBLE);
			if (ratingIcon != null) {
				this.ratingIcon.setImageDrawable(ratingIcon);
				lineView.setBackgroundColor(ThemeResourceManager.getSecondaryThemeColor(this));
			}
		}else{
			this.ratingIcon.setVisibility(View.GONE);
			ratingView.setVisibility(View.GONE);
			lineView.setVisibility(View.GONE);
		}

		// this.layoutDetails.setBackgroundColor(ThemeResourceManager.getPlaceholderColor(this));
		// this.layoutFeature.setBackgroundColor(ThemeResourceManager.getPlaceholderColor(this));
		this.pointerIcon.setImageDrawable(ThemeResourceManager.getPointerTriangleIcon());
		this.progressBarDownload.getProgressDrawable().setColorFilter(
				ThemeResourceManager.getSecondaryThemeColor(this) , android.graphics.PorterDuff.Mode.SRC_IN);

		findViewById(R.id.view_detail__txt_details).setAlpha(0.6f);
		findViewById(R.id.view_detail__body_line_features).setBackgroundColor(ThemeResourceManager.getPrimaryThemeColor(this));

		((TextView) findViewById(R.id.view_detail__txt_header_book)).setTextColor(ThemeResourceManager.getPrimaryThemeColor(this));
		((TextView) findViewById(R.id.view_detail__txt_book_size)).setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));
		((TextView) findViewById(R.id.view_detail__txt_rating)).setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));
		((TextView) findViewById(R.id.view_detail__txt_title)).setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));
		((TextView) findViewById(R.id.view_detail__txt_details)).setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));
		((TextView) findViewById(R.id.view_detail__txt_date)).setTextColor(ThemeResourceManager.getSecondaryThemeColor(this));

		this.navButtonFirst.setVisibility(ThemeResourceManager.isShowHelpIcon(this) ? View.VISIBLE : View.GONE);
		this.navButtonSecond.setVisibility(ThemeResourceManager.isShowCategoryIcon(this) ? View.VISIBLE : View.GONE);

	}

	private void setNavigationButtonOnClick() {
		this.navLeftButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		this.navButtonFirst.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				Intent categorySelectIntent = new Intent(BkShelfPreview.this , BkShelfTutorial.class);
//				startActivity(categorySelectIntent);

				Intent searchActivity = new Intent(BkShelfPreview.this , SearchActivity.class);
				startActivity(searchActivity);
			}
		});

		this.navButtonSecond.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int centerOfButton = (navButtonFirst.getWidth() + navButtonSecond.getWidth() + navButtonThird.getWidth()) / 2;
				int paddingFromLeft = ScreenSizeHelper.getScreenWidth() - centerOfButton;

				Intent categorySelectIntent = new Intent(BkShelfPreview.this , CategorySelectActivity.class);
				categorySelectIntent.putExtra("POINT" , paddingFromLeft);

				startActivityForResult(categorySelectIntent , REQUEST_CATEGORY_INTENT_FROM_PREVIEW);
			}
		});

		this.navButtonThird.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent librarySelectIntent = new Intent(BkShelfPreview.this , LoadedBookMainFragment.class);
				startActivity(librarySelectIntent);
			}
		});
	}

	private void initialValue() {
		this.i = getIntent();
		new BookSize(this).setCoverSize();
		((TextView) findViewById(R.id.view_detail__txt_details)).setText(StringEscapeUtils.unescapeJson(i.getStringExtra("details")));
	}

	private void loadBookDetails() {
		this.ldkPreviewLoader.SetCategoryPreviewLoader(i.getStringExtra("id_category"));
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		stopAnimationProgress();
		finish();
	}
	
	@Override
	protected void onPause() {
		stopAnimationProgress();
		super.onPause();
	}
	
	private void stopAnimationProgress(){
		
		if(this.animProgress!=null){
			this.animProgress.stop();
		}	
		
		DnLoadPdfFromUrl download = new DnLoadPdfFromUrl(this,i.getStringExtra("id_category"));
		download.clearStat();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

	    super.onWindowFocusChanged(hasFocus);

	    ImageView pb = (ImageView) findViewById(R.id.pb_cover_loading);
	    ImageView pb2 = (ImageView) findViewById(R.id.progressBar_horizontal);
	    if(pb!=null && pb2!=null){
			this.animProgress = (AnimationDrawable) pb.getDrawable();
			this.animProgress.start();
			this.animProgress = (AnimationDrawable) pb2.getDrawable();
			this.animProgress.start();
		   
	    }
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == REQUEST_CATEGORY_INTENT_FROM_PREVIEW && resultCode == RESULT_OK) {
			Intent categoryIntent = new Intent(this , BookShelfViewCategory.class);
			categoryIntent.putExtra("RESULT" , data.getExtras().getSerializable("RESULT"));
			startActivity(categoryIntent);
		}
	}
}
