package com.ghbank.bookshelf.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by androiddev on 11/01/2017.
 */

public class ImageViewBook extends ImageView{

    public ImageViewBook(Context context) {
        super(context);
    }

    public ImageViewBook(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewBook(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ImageViewBook(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        int height = (width * 200) / 167;
        setMeasuredDimension(width,height);

    }

}
