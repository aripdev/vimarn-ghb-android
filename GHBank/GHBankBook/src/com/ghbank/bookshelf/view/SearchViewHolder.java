package com.ghbank.bookshelf.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghbank.bookshelf.R;

/**
 * Created by androiddev on 11/01/2017.
 */

public class SearchViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgBook;
    public ImageView btnItem;
    public TextView textName;

    public SearchViewHolder(View itemView) {
        super(itemView);
        imgBook = (ImageView)itemView.findViewById(R.id.imgBook);
        btnItem = (ImageView)itemView.findViewById(R.id.btnItem);
        textName = (TextView)itemView.findViewById(R.id.textName);
    }

}
