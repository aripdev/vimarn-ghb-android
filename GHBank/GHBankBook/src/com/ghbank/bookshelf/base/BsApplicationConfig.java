package com.ghbank.bookshelf.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Debug;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;

import com.arip.it.library.MuPDFActivity;
import com.arip.it.library.core.model.Magazine;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDevice;
import com.ghbank.bookshelf.utils.UtSharePreferences;


@ReportsCrashes(formKey = "", mailTo = "webmaster@arip.co.th", customReportContent = {ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
        ReportField.ANDROID_VERSION,
        ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT},
        mode = ReportingInteractionMode.TOAST)
public class BsApplicationConfig extends Application {
    public static final String SUBSCRIPTION_YEAR_KEY = "yearlysubscription";
    public static final String SUBSCRIPTION_MONTHLY_KEY = "monthlysubscription";

	private static final String TAG = "PdfReaderApplication";
	private static final String PATH_SEPARATOR = "/";
	
	private static String baseUrl;

//	public static ImageLoader imageLoader;

	@Override
	public void onCreate() {
		super.onCreate();
		// Get Device Resolution
		DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics(); 
		UtDevice.setWidth(dm.widthPixels < dm.heightPixels ? dm.widthPixels
				: dm.heightPixels);
		UtDevice.setHeight(dm.widthPixels < dm.heightPixels ? dm.heightPixels
				: dm.widthPixels);
		Debug.getNativeHeapAllocatedSize();
        ACRA.init(this);
        initImageLoader(getApplicationContext());
        UtConfig.SERVER_CONFIG = UtSharePreferences.getPrefServerConfig(getApplicationContext());
      

    }

	 public static void startPDFActivity(Context context, String filePath, String title, boolean showThumbnails){
         try{
                 Uri uri = Uri.parse(filePath);
                 Intent intent = new Intent(context,MuPDFActivity.class);
                 intent.setAction(Intent.ACTION_VIEW);
                 intent.setData(uri);
                 intent.putExtra(Magazine.FIELD_TITLE, title);
                 intent.putExtra(MuPDFActivity.SHOW_THUMBNAILS_EXTRA, showThumbnails);
                 context.startActivity(intent);
         } catch (Exception e) {
                 Log.e(TAG,"Problem with starting PDF-activity, path: "+filePath,e);
         }

	 }

	 public static void initImageLoader(Context context) {
			// This configuration tuning is custom. You can tune every option, you may tune some of them,
			// or you can create default configuration by
			//  ImageLoaderConfiguration.createDefault(this);
			// method.
			@SuppressWarnings("deprecation")
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
					.threadPriority(Thread.NORM_PRIORITY - 2)
					.denyCacheImageMultipleSizesInMemory()
					.discCacheFileNameGenerator(new Md5FileNameGenerator())
					.tasksProcessingOrder(QueueProcessingType.FIFO)
					.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
					.threadPoolSize(10)
					.memoryCache(new WeakMemoryCache())
//					.writeDebugLogs() // Remove for release app
					.build();
			// Initialize ImageLoader with configuration.
			ImageLoader.getInstance().init(config);
//			imageLoader = ImageLoader.getInstance();
		}
	 

	public static boolean thereIsConnection(Context context) {
		
//		if (SystemHelper.isEmulator(context)) {
//			return true;
//		}
		
		ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null) {
			return false;
		}
		if (!i.isConnected()) {
			return false;
		}
		if (!i.isAvailable()) {
			return false;
		}
		return true;
	}

	public static String getClientName(Context context){
		return context.getResources().getString(R.string.client_name);
	}
	
	public static String getMagazineName(Context context){
		return context.getResources().getString(R.string.magazine_name);
	}

    public static String getServiceName(Context context){
        return context.getResources().getString(R.string.service_name);
    }
	
	public static String getUrlString(String fileName){
		return PATH_SEPARATOR + fileName;
	}
	
	public static String getUrlString(Context context, String fileName){
		return BsApplicationConfig.getClientName(context) + PATH_SEPARATOR 
		+ BsApplicationConfig.getMagazineName(context) + PATH_SEPARATOR + fileName;
	}

	public static String getYearlySubsCode(Context context){
		return context.getResources().getString(R.string.yearly_subs_code);
	}

	public static String getMonthlySubsCode(Context context){
		return context.getResources().getString(R.string.monthly_subs_code);
	}
	
	public static boolean isEnableCodeSubs(Context context){
		return context.getResources().getBoolean(R.bool.enable_code_subs);
	}

    public static boolean isEnableUsernamePasswordLogin(Context context){
        return context.getResources().getBoolean(R.bool.enable_username_password_login);
    }


	public static String getAmazonServerUrl(){
		return baseUrl;
	}

    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
   }


}
