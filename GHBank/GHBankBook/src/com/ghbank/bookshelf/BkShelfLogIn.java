package com.ghbank.bookshelf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.service.SvLogin;
import com.ghbank.bookshelf.utils.UtConfig;
import com.ghbank.bookshelf.utils.UtDesign;
import com.ghbank.bookshelf.utils.Utils;

import group.aim.framework.helper.ScreenSizeHelper;

public class BkShelfLogIn extends FragmentActivity {
	private Handler handler;
    private FrameLayout lnLogin;
    private ImageView imgUserLogin;
   
    private TextView btnLogin;
    private TextView btnForgot;
    private TextView txtRegister;
    private EditText edtUsername;
    private EditText edtPassword;
	private ImageView buttonSkip;
    
    private AQuery aQuery;

	private void initialView() {

		this.getActionBar().hide();

		this.lnLogin = (FrameLayout) findViewById(R.id.view_lay_login);
		this.imgUserLogin = (ImageView) findViewById(R.id.img_user_login);

		this.btnLogin = (TextView) findViewById(R.id.txt_login);
		this.btnForgot = (TextView) findViewById(R.id.txt_forgot);
		this.txtRegister = (TextView) findViewById(R.id.txt_register);
		this.edtUsername = (EditText) findViewById(R.id.edt_username);
		this.edtPassword = (EditText) findViewById(R.id.edt_password);
		this.buttonSkip = (ImageView) findViewById(R.id.button_skip);

		this.lnLogin.setVisibility(View.VISIBLE);
		this.buttonSkip.setVisibility(View.VISIBLE);
	}

	private void initialValue() {
		this.aQuery = new AQuery(this);

		// Set background
		Drawable bg = ThemeResourceManager.getBackgroundFromType(2);
		if(bg != null){
			lnLogin.setBackground(bg);
		}

		int fontColor = ThemeResourceManager.getFontColor(this);
		Drawable userIcon = ThemeResourceManager.getUserIcon();
		Drawable passwordIcon = ThemeResourceManager.getPasswordIcon();
		Drawable logoDrawable = ThemeResourceManager.getLogoIcon();

		if(userIcon != null){
			Bitmap bitmap = ((BitmapDrawable) userIcon).getBitmap();
			Drawable resizeDrawable = new BitmapDrawable(this.getResources(), Bitmap.createScaledBitmap(bitmap, 40, 40, true));
			this.edtUsername.setCompoundDrawablesWithIntrinsicBounds(resizeDrawable, null, null, null);
			bitmap.recycle();
		}

		if(passwordIcon != null) {
			Drawable img = Drawable.createFromPath(UtConfig.DESIGN_PATH_THEME+UtDesign.PASSWORD_ICON);
			Bitmap bitmap = ((BitmapDrawable) img).getBitmap();
			Drawable resizeDrawable = new BitmapDrawable(this.getResources(), Bitmap.createScaledBitmap(bitmap, 40, 40, true));
			this.edtPassword.setCompoundDrawablesWithIntrinsicBounds(resizeDrawable, null, null, null);
			bitmap.recycle();
		}

		if(logoDrawable != null)
			this.imgUserLogin.setImageDrawable(logoDrawable);

		this.edtUsername.getLayoutParams().width = ScreenSizeHelper.getScreenWidth() / 2;
		this.edtPassword.getLayoutParams().width = ScreenSizeHelper.getScreenWidth() / 2;

		this.btnForgot.setVisibility(ThemeResourceManager.isShowForgotPasswordButton(this) ? View.VISIBLE : View.GONE);
		this.txtRegister.setVisibility(ThemeResourceManager.isShowRegisterButton(this) ? View.VISIBLE : View.GONE);

		this.btnLogin.setText(Html.fromHtml("<html><body><u>" + getString(R.string.txt_login) + "</u></body></html>"));
		this.btnForgot.setText(Html.fromHtml("<html><body><u>" + getString(R.string.txt_forgot) + "</u></body></html>"));
		this.txtRegister.setText(Html.fromHtml("<html><body><u>" + getString(R.string.txt_register) + "</u></body></html>"));

		this.btnLogin.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		this.btnForgot.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		this.txtRegister.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		this.edtUsername.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));
		this.edtPassword.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(this));

		this.btnLogin.setTextColor(fontColor);
		this.btnForgot.setTextColor(fontColor);
		this.txtRegister.setTextColor(fontColor);
		this.edtUsername.setTextColor(fontColor);
		this.edtPassword.setTextColor(fontColor);



		this.edtUsername.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				InputMethodManager keyboard = (InputMethodManager)
						getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.hideSoftInputFromWindow(edtUsername.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
			}
		},0);

		this.edtPassword.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				InputMethodManager keyboard = (InputMethodManager)
						getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.hideSoftInputFromWindow( edtPassword.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
			}
		},0);

		this.edtUsername.clearFocus();
		this.edtPassword.clearFocus();
	}

	private void initialAction() {
		this.btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if((edtUsername != null && edtUsername.length() > 0) && (edtUsername != null && edtUsername.length() > 0) ){
					new SvLogin().login(BkShelfLogIn.this, aQuery, edtUsername, edtPassword);
				}else{
					Toast.makeText(BkShelfLogIn.this, "Please completed all fields", Toast.LENGTH_LONG).show();
				}
			}
		});

		this.txtRegister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelfLogIn.this, BkShelfRegister.class);
				startActivity(i);
			}
		});

		this.btnForgot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelfLogIn.this, BkShelfForgotPassword.class);
				startActivity(i);
			}
		});

		this.buttonSkip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(BkShelfLogIn.this, BookshelfStartActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				finish();
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			}
		});
	}

    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

        this.initialView();
		this.initialValue();
		this.initialAction();

		Utils.hideKeyboard(BkShelfLogIn.this);
    }
}
