package com.ghbank.bookshelf.connection;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.utils.UtUtilities;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;


public class CnInternet {

	public static boolean AlertInternetConnection(Activity ac )
    {
    	if( UtUtilities.isNetworkConnectings(ac) )return false;
    	AlertDialog.Builder ab = new AlertDialog.Builder(ac);
    	ab.setTitle(ac.getString(R.string.dg_internet_connection));
    	ab.setMessage(ac.getString(R.string.dg_internet_connection_fail));
    	ab.setNeutralButton(ac.getString(R.string.dg_ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
		
				dialog.dismiss();
			}
		});
    	ab.show();
    	return true;
    }
	
}
