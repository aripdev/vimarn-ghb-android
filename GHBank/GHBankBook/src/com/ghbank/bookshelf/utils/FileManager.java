package com.ghbank.bookshelf.utils;

import java.io.File;

/**
 * Created by androiddev on 20/01/2017.
 */

public class FileManager {

    private static final String TAG = FileManager.class.getSimpleName();

    public static void createDirIfNotExists(String path) {
        File file = new File(path);
        NLog.d(TAG,path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                NLog.e(TAG, "Problem creating  folder");
            }else {
                NLog.d(TAG,"This folder create completed.");
            }
        }else {
            NLog.w(TAG,"This folder is exists");
        }
    }
}
