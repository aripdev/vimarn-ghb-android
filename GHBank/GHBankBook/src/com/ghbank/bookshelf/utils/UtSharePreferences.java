package com.ghbank.bookshelf.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ghbank.bookshelf.session.SnPreferenceVariable;


public class UtSharePreferences {

	public static String getPrefStringColor(Activity ac){
		String color = null;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.THEME_FONT_COLOR, "").length()>0){
			color = sPref.getString(SnPreferenceVariable.THEME_FONT_COLOR, "");
		}else{
			color = UtConfig.DEFAULT_FONT_COLOR;
		}
		return color;
	}

	public static String getPrefBackgroundCategoryLeftColor(Activity ac){
		String color = null;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_LEFT, "").length()>0){
			color = sPref.getString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_LEFT, "");
		}else{
			color = UtConfig.DEFAULT_BACKFROUND_CATEGORY_LEFT_COLOR;
		}
		return color;
	}

	public static String getPrefBackgroundCategoryRightColor(Activity ac){
		String color = null;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_RIGHT, "").length()>0){
			color = sPref.getString(SnPreferenceVariable.THEME_BACKGROUND_CATEGORY_RIGHT, "");
		}else{
			color = UtConfig.DEFAULT_BACKFROUND_CATEGORY_RIGHT_COLOR;
		}
		return color;
	}

	public static boolean getPrefTutorialFirstInstall(Activity ac){
		boolean firstStart = true;

		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.DISPLAY_TUTORIAL, "").length()>0 && sPref.getString(SnPreferenceVariable.DISPLAY_TUTORIAL, "").equalsIgnoreCase("false")){
			firstStart = false;
		}
		return firstStart;
	}

	public static String getPrefServerConfig(Context ac){
		String server = null;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.SERVER_CONFIG, "").length()>0){
			server = sPref.getString(SnPreferenceVariable.SERVER_CONFIG, "");
		}else{
			server = UtConfig.SERVER_DEFAULT;
		}
		return server;
	}

	public static String getPrefServerExamConfig(Context ac){
		String server = null;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		if(sPref!=null && sPref.getString(SnPreferenceVariable.SERVER_EXAM_CONFIG, "").length()>0){
			server = sPref.getString(SnPreferenceVariable.SERVER_EXAM_CONFIG, "");
		}else{
			server = UtConfig.SERVER_EXAM;
		}
		return server;
	}

	public static void setServerConfig(Activity ac, String serverip){
		SharedPreferences sPref =  ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		SharedPreferences.Editor sPrefEditor = sPref.edit();
		sPrefEditor.putString( SnPreferenceVariable.SERVER_CONFIG, serverip);
		sPrefEditor.commit();
	}

	public static void setServerExamConfig(Activity ac, String serverip){
		SharedPreferences sPref =  ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		SharedPreferences.Editor sPrefEditor = sPref.edit();
		sPrefEditor.putString( SnPreferenceVariable.SERVER_EXAM_CONFIG, serverip);
		sPrefEditor.commit();
	}

	public static boolean getPrefStatusServer(Context ac){
		boolean server = false;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		server = sPref.getBoolean(SnPreferenceVariable.STATUS_SELECT_SERVER,false);

		return server;
	}

	public static void setStatusServer(Activity ac, boolean status){
		SharedPreferences sPref =  ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		SharedPreferences.Editor sPrefEditor = sPref.edit();
		sPrefEditor.putBoolean( SnPreferenceVariable.STATUS_SELECT_SERVER, status);
		sPrefEditor.commit();
	}

	public static boolean getPrefStatusUpdateServer(Context ac){
		boolean server = false;
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		server = sPref.getBoolean(SnPreferenceVariable.STATUS_UPDATE_SERVER,false);

		return server;
	}

	public static void setStatusUpdateServer(Activity ac, boolean status){
		SharedPreferences sPref =  ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		SharedPreferences.Editor sPrefEditor = sPref.edit();
		sPrefEditor.putBoolean( SnPreferenceVariable.STATUS_UPDATE_SERVER, status);
		sPrefEditor.commit();
	}

	public static String getPrefServerName(Context ac){
		String name = "";
		SharedPreferences sPref = ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		name = sPref.getString(SnPreferenceVariable.SERVER_NAME,UtConfig.SERVER_DEFAULT_NAME);

		return name;
	}

	public static void setPrefServerName(Activity ac, String servername){
		SharedPreferences sPref =  ac.getSharedPreferences(SnPreferenceVariable.PF_ARIP, ac.MODE_PRIVATE );
		SharedPreferences.Editor sPrefEditor = sPref.edit();
		sPrefEditor.putString( SnPreferenceVariable.SERVER_NAME, servername);
		sPrefEditor.commit();
	}
}
