package com.ghbank.bookshelf.utils;

import android.util.Log;

/*
 * Created by androiddev on 05/01/2017.
 */

public class NLog {

    public static final boolean DEBUGGING_MODE = false;

    public static void i(String tag, String msg){
        if(DEBUGGING_MODE){
            Log.i(tag,msg);
        }
    }
    public static void w(String tag, String msg){
        if(DEBUGGING_MODE){
            Log.w(tag,msg);
        }
    }
    public static void d(String tag, String msg){
        if(DEBUGGING_MODE){
            Log.d(tag,msg);
        }
    }
    public static void e(String tag, String msg){
        if(DEBUGGING_MODE){
            Log.e(tag,msg);
        }
    }
}
