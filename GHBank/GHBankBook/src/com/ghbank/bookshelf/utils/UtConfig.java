package com.ghbank.bookshelf.utils;

import android.os.Environment;

public class UtConfig {
	public static final int AD_DURATION = 3000;
	public static final String SERVER_DEFAULT = "http://epub.arip.co.th/contents/index.php?"; //Default
	public static final String SERVER_DEFAULT_NAME = "epub.arip.co.th"; //Default
	public static final String SERVER_EXAM = "http://203.148.174.2/library/kmitl/exam/content.php?";

	//203.148.174.2
	public static String SERVER_CONFIG = SERVER_DEFAULT; //Default
	//	public static String LINK = "http://"+getServerConfig()+"/contents/index.php?";
//	public static final String LIBRARY_CODE = "0010"; //ARiP
//	public static final String LIBRARY_CODE = "1010"; //RSU
//	public static final String LIBRARY_CODE = "2020"; //Test
//	public static final String LIBRARY_CODE = "1020"; //AU
//	public static final String LIBRARY_CODE = "2030"; //AOT
//	public static final String LIBRARY_CODE = "1040"; //SiamU
// 	public static final String LIBRARY_CODE = "1030"; //GSC
 	public static final String LIBRARY_CODE = "2040"; //GHBank

	public static String USERNAME = "";
	public static String NICKNAME = "";
	public static String GROUP = "0";
	public static String USERKEY = "";
	public static String PLATFORM = "and";
	public static String DESIGN_VERSION = "version1.0";
	public static String DESIGN_PATH = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/";
	public static String HELP_PATH = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/help/";
	public static String DESIGN_PATH_ZIP = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/zip/";
	public static String DESIGN_PATH_THEME = Environment.getExternalStorageDirectory()+"/Android/data/com.ghbank.bookshelf/theme/";
	public static String DEFAULT_FONT_COLOR = "#ffffff";
	public static String DEFAULT_BACKFROUND_CATEGORY_LEFT_COLOR = "#5e5e5e";
	public static String DEFAULT_BACKFROUND_CATEGORY_RIGHT_COLOR = "#e6e6e6";

	public static final String FILE_TYPE_PNG = ".png";
	public static final String FILE_TYPE_JPG = ".jpg";

	// http://epub.arip.co.th/contents/images/app/Books_shelf.png
	public static final String SHELF_BG = "http://epub.arip.co.th/contents/images/app/shelf_bar_ghbank.png";
}
