package com.ghbank.bookshelf.utils;

public class UtDevice {
	private static int width;
	private static int height;

	public static int getWidth() {
		return width;
	}

	public static void setWidth( int width ) {
		if ( UtDevice.width == 0 )
			UtDevice.width = width;
	}

	public static int getHeight() {
		return height;
	}

	public static void setHeight( int height ) {
		if ( UtDevice.height == 0 )
			UtDevice.height = height;
	}
}
