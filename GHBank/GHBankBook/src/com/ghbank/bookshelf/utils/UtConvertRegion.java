package com.ghbank.bookshelf.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class UtConvertRegion {
	public static String ConvertToHTML( String text ) {
		String ret = text;
		try {
			byte[] b = text.getBytes( "UTF8" );
			int dig = 0;
			int maxByte = 256;
			String hexDig;
			char percent = '%';
			StringBuilder html = new StringBuilder();
			for ( int i = 0; i < b.length; i++ ) {
				dig = ( int ) b[i] < 0 ? maxByte + ( int ) b[i] : ( int ) b[i];
				hexDig = Integer.toHexString( dig ).toUpperCase();
				html.append( percent ).append( hexDig );
			}
			ret = html.toString();
		} catch ( UnsupportedEncodingException ex ) {}
		return ret;
	}

	public static String convertStreamToString( InputStream is ) {
		BufferedReader reader = new BufferedReader( new InputStreamReader( is ) );
		StringBuilder sb = new StringBuilder();
		// String result = "";
		String line = null;
		try {
			while ( ( line = reader.readLine() ) != null ) {
				sb.append( line + "\n" );
				// result += ( line + "\n" );
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		} catch ( Exception e ) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		// return sb.toString();
		return sb.toString();
	}
	
}
