package com.ghbank.bookshelf.utils;

import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by androiddev on 22/02/2017.
 */

 public class ScreenUtils {

    public static String ASPECT_RATIO_4_3 = "4:3";
    public static String ASPECT_RATIO_5_3 = "5:3";
    public static String ASPECT_RATIO_8_5 = "8:5";
    public static String ASPECT_RATIO_16_9 = "16:9";

    public static int getScreenWidth(Context context){
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        Display screenSize = windowManager.getDefaultDisplay();
        Point size = new Point();
        screenSize.getRealSize(size);
        return size.x;
    }
    public static int getScreenHeight(Context context){
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display screenSize = windowManager.getDefaultDisplay();
        Point size = new Point();
        screenSize.getRealSize(size);
        return size.y;
    }

    public static String getScreenAspectRatio(Context context){
        int width = getScreenWidth(context);
        int height = getScreenHeight(context);
        float factor =  greatestCommonFactor(width,height);
        if(width > height){
            factor =  width / height;
        }
        int widthRatio = (int)(width / factor);
        int heightRatio = (int)( height / factor);

        // Show only 4:3 and 16:9
        String ratio = heightRatio + ":" + widthRatio;
        if(ratio.equals(ASPECT_RATIO_4_3)){
            return ASPECT_RATIO_4_3;
        }
        return ASPECT_RATIO_16_9;
    }
    private static int greatestCommonFactor(int width, int height) {
        return (height == 0) ? width : greatestCommonFactor(height, width % height);
    }

    public static int getDiagonal(Context context){
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi,2);
        double y = Math.pow(dm.heightPixels/dm.ydpi,2);
        return (int)Math.sqrt(x+y);
    }
}
