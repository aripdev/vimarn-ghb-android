package com.ghbank.bookshelf.utils;

public class UtDesign {

	public static String BG1 = "bg1.png";
	public static String BG2 = "bg2.png";
	public static String BG3 = "bg3.png";
	public static String BG4 = "bg4.png";
	public static String FACEBOOK_LOGIN_SMALL = "facebook_login_small.png";
	public static String FACEBOOK_LOGIN = "facebook_login.png";
	public static String USER_ICON = "form_login.png";
	public static String PASSWORD_ICON = "form_password.png";
	public static String ICON1 = "icon1.png";
	public static String ICON2 = "icon2.png";
	public static String ICON3 = "icon3.png";
	public static String ICON4 = "icon4.png";
	public static String ICON5 = "icon5.png";
	public static String ICON6 = "icon6.png";
	public static String ICON7 = "icon7.png";
	public static String ICON8 = "icon8.png";
	public static String ICON11 = "icon11.png";
	public static String ICON9 = "icon9.png";
	public static String ICON10= "icon10.png";
	public static String ICON12 = "icon12.png";
	public static String ICON13 = "icon13.png";
	public static String ICON15 = "icon15.png";
	public static String ICON40 = "icon40.png"; // Search Icon
	public static String ICON42 = "icon42.png";
	public static String ICON43 = "icon43.png";
	public static String ICON44 = "icon44.png";
	public static String LOGO1 = "logo1.png";
	public static String LOGO2 = "logo2.png";
	public static String LOGO3 = "logo3.png";
	public static String THEME_BACKGROUND1 = "theme_background_1.png";
	public static String THEME_BACKGROUND2 = "theme_background_7.png";
	public static String THEME_CLOSE1 = "theme_close1.png";
	public static String THEME_CLOSE3 = "theme_close3.png";
	public static String THEME_TRI1_1 = "theme_tri1_1.png";
	public static String THEME_TRI2_1 = "theme_tri2_1.png";
	public static String STAR1 = "star1.png";
	public static String STAR2 = "star2.png";
	public static String STAR3 = "star3.png";
	public static String SMALL_ICON_7 = "theme_background_1.png";
	
}
