package com.ghbank.bookshelf.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;

public class UtUtilities {


	public static boolean isNetworkConnectings(Activity activity)
	{
		final ConnectivityManager connMgr = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if( wifi != null && wifi.isConnected() )
		{
			Log.d("wifi", wifi.isConnected()+"");
			return true;
		}
		if( mobile != null && mobile.isConnected() )
		{
			Log.d("mobile", mobile.isConnected()+"");
			return true;
		}
		return false;
	}

	public static int getScreenWidth(Context mContext){
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int width = displaymetrics.widthPixels;
		return width;
	}

	public static int getScreenHeight(Context mContext){
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		return height;
	}

	public float getScreenDpi(Context mContext){
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		float scaledDpi = displaymetrics.scaledDensity;
		return scaledDpi;
	}

	public static float convertDpToPixel(float dp, Context context){
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	public static String getSourceFile(String path){
		String result = "";
		if(path != null && path.endsWith("mp4")){
			result = ".mp4";
		}else if(path != null && path.endsWith("jpg")){
			result = ".jpg";
		}else if(path != null && path.endsWith("png")){
			result = ".png";
		}else if(path != null && path.endsWith("bmp")){
			result = ".bmp";
		}else if(path != null && path.endsWith("mp3")){
			result = ".mp3";
		}else if(path != null && path.endsWith("wav")){
			result = ".wav";
		}
		return result;
	}

	public static Bitmap decodeSampledBitmapFromResource(String pathName) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();

		BitmapFactory.decodeFile(pathName, options);

		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inDither = true;
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options,UtDevice.getWidth(), UtDevice.getHeight());

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resourceId) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();

		BitmapFactory.decodeResource(res, resourceId, options);

		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inDither = true;
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options,UtDevice.getWidth(), UtDevice.getHeight());

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resourceId, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
}
