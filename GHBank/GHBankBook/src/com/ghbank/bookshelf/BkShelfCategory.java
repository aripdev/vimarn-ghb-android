package com.ghbank.bookshelf;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;

import com.ghbank.bookshelf.adapter.AdtCategoryExpanableAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BkShelfCategory extends FragmentActivity{

	private AdtCategoryExpanableAdapter listAdapter;
	private ExpandableListView expListView;
	private List<String> listDataHeader;
	private HashMap<String, List<String>> listDataChild;
	private ImageView imgHome;

    
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
			setContentView(R.layout.lay_category);
			
			final ActionBar actionBar = getActionBar();
			actionBar.hide();
			
			Initial();
			
			// preparing list data
	        prepareListData();
	 
	        listAdapter = new AdtCategoryExpanableAdapter(this, listDataHeader, listDataChild);
	 
	        // setting list adapter
	        expListView.setAdapter(listAdapter);
	        
	        imgHome.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
	        
	        expListView.setOnChildClickListener(new OnChildClickListener() {
				
				@Override
				public boolean onChildClick(ExpandableListView parent, View v,
						int groupPosition, int childPosition, long id) {
				
					Intent i = new Intent(BkShelfCategory.this, BkShelfByCategory.class);
					i.putExtra("id_category",listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).split(":")[1]);
					i.putExtra("name_category", listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).split(":")[0]);
					i.putExtra("name_thumbnail_path", childPosition+"");
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i); 
					// TODO Auto-generated method stub
//					Toast.makeText(BkShelfCategory.this,listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).split(":")[1], Toast.LENGTH_LONG).show();
					return false;
				}
			});
	        
	 }
	 
	 
	 private void Initial(){
//			aq = new AQuery(this);
		 expListView = (ExpandableListView) findViewById(R.id.exp_subject);
		 imgHome = (ImageView) findViewById(R.id.btn_category_home);
		 
	 }
	 
	 /*
	     * Preparing the list data
	     */
	    private void prepareListData() {
	    	
	    	String bType0 = "คณิตศาสตร์";
	    	String bType1 = "วิทยาศาสตร์";
	    	String bType2 = "สังคม ศาสนา และวัฒนธรรม";
	    	String bType3 = "ภาษาไทย";
	    	String bType4 = "ภาษาต่างประเทษ (ภาษาอังกฤษ)";
	    	String bType5 = "ภาษาต่างประเทศ (จีน เกาหลี ญี่ปุ่น)";
	    		    	
	        listDataHeader = new ArrayList<String>();
	        listDataChild = new HashMap<String, List<String>>();
	 
	        // Adding child data
	        listDataHeader.add("หนังสือ ม.๑");
	        listDataHeader.add("หนังสือ ม.๒");
	        listDataHeader.add("หนังสือ ม.๓");
	        listDataHeader.add("หนังสือทั้งหมด");
	 
	        // Adding child data
	        List<String> bookM1 = new ArrayList<String>();
	        bookM1.add("หนังสือ ม.๑ ทั้งหมด"+":146");
	        bookM1.add(bType0+":149");
	        bookM1.add(bType1+":150");
	        bookM1.add(bType2+":151");
	        bookM1.add(bType3+":152");
	        bookM1.add(bType4+":153");
	        bookM1.add(bType5+":164");
	 
	        List<String> bookM2 = new ArrayList<String>();
	        bookM2.add("หนังสือ ม.๒ ทั้งหมด"+":147");
	        bookM2.add(bType0+":154");
	        bookM2.add(bType1+":155");
	        bookM2.add(bType2+":156");
	        bookM2.add(bType3+":157");
	        bookM2.add(bType4+":158");
	        bookM2.add(bType5+":165");
	 
	        List<String> bookM3 = new ArrayList<String>();
	        bookM3.add("หนังสือ ม.๓ ทั้งหมด"+":148");
	        bookM3.add(bType0+":159");
	        bookM3.add(bType1+":160");
	        bookM3.add(bType2+":161");
	        bookM3.add(bType3+":162");
	        bookM3.add(bType4+":163");
	        bookM3.add(bType5+":166");
	        
	        List<String> bookAll = new ArrayList<String>();
	        bookAll.add("หนังสือทั้งหมด"+":0");
	 
	        listDataChild.put(listDataHeader.get(0), bookM1); // Header, Child data
	        listDataChild.put(listDataHeader.get(1), bookM2);
	        listDataChild.put(listDataHeader.get(2), bookM3);
	        listDataChild.put(listDataHeader.get(3), bookAll);
	    }
}
