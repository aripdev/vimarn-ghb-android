package com.ghbank.bookshelf.session;

public class SnPreferenceVariable {
	public static final String PF_ARIP = "pf_arip";
	public static final String USER_NAME = "pf_user_name";
	public static final String NICK_NAME = "pf_nick_name";
	public static final String GROUP = "pf_group";
	public static final String SESSION_ID = "pf_session_id";
	public static final String USER_KEY = "pf_user_key";
	public static final String DESIGN_STATUS_COMPLETE = "pf_design_complete";
	public static final String DESIGN_REVISION = "pf_design_revision";
	public static final String DISPLAY_TUTORIAL = "pf_display_tutorial";
	public static final String SERVER_CONFIG = "pf_server_config";
	public static final String SERVER_EXAM_CONFIG = "pf_server_exam_config";
	public static final String STATUS_SELECT_SERVER = "pf_status_select_server";
	public static final String STATUS_UPDATE_SERVER = "pf_status_update_server";
	public static final String SERVER_NAME = "pf_server_name";

	public static final String THEME_ID = "pf_theme_id";
	public static final String THEME_PLACEHOLDER_COLOR = "pf_placeholder_color";
	public static final String THEME_COLOR2 = "pf_theme_color2";
	public static final String THEME_COLOR = "pf_theme_color";
	public static final String THEME_FONT_COLOR = "pf_theme_font_color";
	public static final String THEME_NAME = "pf_theme_name";
	public static final String THEME_BACKGROUND_CATEGORY_LEFT = "pf_theme_background_category_left";
	public static final String THEME_BACKGROUND_CATEGORY_RIGHT = "pf_theme_background_category_right";

	//Add new
	public static final String THEME_TOTAL_FILE = "pf_theme_total_file";
	public static final String THEME_SHOW_LOGIN = "pf_theme_show_login";
	public static final String THEME_HOME_TYPE = "pf_theme_home_type";
	public static final String THEME_SHOW_BUTTON_01 = "pf_theme_button_01";
	public static final String THEME_SHOW_BUTTON_02 = "pf_theme_button_02";
	public static final String THEME_SHOW_BUTTON_03 = "pf_theme_button_03";
	public static final String THEME_SHOW_BUTTON_04 = "pf_theme_button_04";
	public static final String THEME_SHOW_BUTTON_05 = "pf_theme_button_05";
	public static final String THEME_SHOW_BUTTON_06 = "pf_theme_button_06";
	public static final String THEME_SHOW_BUTTON_07 = "pf_theme_button_07";
	public static final String THEME_SHOW_BUTTON_08 = "pf_theme_button_08";
	public static final String THEME_SHOW_BUTTON_09 = "pf_theme_button_09";
	public static final String THEME_SHOW_BUTTON_10 = "pf_theme_button_10";
	public static final String THEME_SHOW_BUTTON_11 = "pf_theme_button_11";
	public static final String THEME_SHOW_BUTTON_12 = "pf_theme_button_12";
	public static final String THEME_SHOW_BUTTON_13 = "pf_theme_button_13";

	public static final String THEME_TAB_1_TITLE = "pf_theme_tab_1_title";
	public static final String THEME_TAB_2_TITLE = "pf_theme_tab_2_title";
	public static final String THEME_TAB_3_TITLE = "pf_theme_tab_3_title";

	public static final String IS_USER_LOGIN = "pf_user_login";
}
