package com.ghbank.bookshelf.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.ghbank.bookshelf.BkShelfLogIn;
import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.change.helper.ThemeResourceManager;
import com.ghbank.bookshelf.fonts.FcustomFonts;
import com.ghbank.bookshelf.image.ImageDisplayOptions;
import com.ghbank.bookshelf.service.SvReview;
import com.ghbank.bookshelf.session.SnPreferenceVariable;
import com.ghbank.bookshelf.utils.UtDevice;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class PopupReview extends BaseAdapter {
	private static final String TAG = "PopupReview";
	
	private static String DB_PATH;
	private Activity activity;
	private PopupWindow popupWindow;
	private ImageView imgClose;
	private ListView listReview;
	private TextView btnSendReview;
	private View popupView;
	private AQuery aQuery;
	private String categoryId;
	private String rating, ebookId;
	private EditText title, detail;
	private EditText edt_title, edt_write_review;
	public static String id,name,summary,datetime,cover;
	private LinearLayout layReview;
	private ImageView imgArrow;
	private RelativeLayout layBody;
	private LinearLayout previewBookContainer;
	
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	
//	public PopupReview(Activity activity, String categoryId){
//		
//		activity = activity;
//		aQuery = new AQuery(activity);
//		categoryId = categoryId;
//		initial();
//	}
//

	private void initialView() {
		this.popupView = LayoutInflater.from(this.activity).inflate(R.layout.view_review, null);
		this.previewBookContainer = (LinearLayout) this.popupView.findViewById(R.id.view_preview__detail_book);
		this.layReview = (LinearLayout) this.popupView.findViewById(R.id.lay_review);
		this.imgArrow = (ImageView) this.popupView.findViewById(R.id.view_detail__arrow_book);;
		this.layBody = (RelativeLayout) this.popupView.findViewById(R.id.view_preview__features_body);
		this.imgClose = (ImageView) this.popupView.findViewById(R.id.img_cut);
		this.listReview = (ListView) this.popupView.findViewById(R.id.list_review);
		this.btnSendReview = (TextView) this.popupView.findViewById(R.id.btn_send_review);
		this.edt_title = (EditText) this.popupView.findViewById(R.id.edt_title);
		this.edt_write_review = (EditText) this.popupView.findViewById(R.id.edt_write_review);
	}

	private void initialValue() {
		this.popupWindow = new PopupWindow(this.popupView, UtDevice.getWidth(),  UtDevice.getHeight());
		this.popupWindow.showAtLocation(this.popupView, Gravity.CENTER, 0, 0);
		this.popupWindow.setTouchable(true);
		this.popupWindow.setFocusable(true);
		this.popupWindow.setOutsideTouchable(false);
		this.popupWindow.update();

		Drawable bgDrawable = ThemeResourceManager.getBackground1(activity);
		Drawable pointerIcon = ThemeResourceManager.getPointerTriangleIcon();

		if(bgDrawable != null)
			this.layReview.setBackground(bgDrawable);

		if(pointerIcon != null)
			this.imgArrow.setImageDrawable(pointerIcon);

		this.previewBookContainer.setBackground(ThemeResourceManager.getSmallIconCategoryBackground());
		this.layBody.setBackground(ThemeResourceManager.getSmallIconCategoryBackground());
		this.edt_title.setTextColor(ThemeResourceManager.getFontColor(this.activity));
		this.edt_write_review.setTextColor(ThemeResourceManager.getFontColor(this.activity));
	}

	private void initialActon() {
		imgClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dismiss();
			}
		});

		btnSendReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent  i = null;
				final SharedPreferences sPref = activity.getSharedPreferences(SnPreferenceVariable.PF_ARIP, activity.MODE_PRIVATE );

				if( sPref != null && sPref.getString(SnPreferenceVariable.SESSION_ID, "").length() > 0){
					if((edt_title!=null && edt_title.length() > 0) && (edt_write_review!=null && edt_write_review.length()>0) ){
						title = edt_title;
						detail = edt_write_review;
						new SvReview().sendReview(activity, aQuery, popupView, listReview, PopupReview.this.categoryId, title, detail, rating, ebookId);

					}else{
						Toast.makeText(activity, "Please completed all fields", Toast.LENGTH_LONG).show();
					}
				}else{
					i = new Intent(activity, BkShelfLogIn.class);
					activity.startActivity(i);
				}

			}
		});
	}
	
	public void initial(Activity activity, String categoryId){
		this.activity = activity;
		this.aQuery = new AQuery(activity);
		this.categoryId = categoryId;
        
	    this.initialView();
		this.initialValue();
		this.initialActon();
	}
	
	public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
		popupWindow.setOnDismissListener(listener);  
	}
	
	public boolean isPopupReviewShow(){
		return popupWindow != null ? popupWindow.isShowing():false;
	}

	public void show(JSONObject json){
		
		setReviewByJsonObject(json); 
		
		if(popupWindow!=null && !popupWindow.isShowing()){
			popupWindow.showAsDropDown(popupView);		
			}
	}
	
	public void dismiss(){
		if(popupWindow!=null && popupWindow.isShowing()){
			popupWindow.dismiss();
		}
	}
	
	private void setReviewByJsonObject(JSONObject json){

    	try {
//    		 final JSONObject jsonReview = json;
			if (json.getString("status").equals("1")) {
   					name = json.getString("name");
   					if(UtDevice.getHeight()>=1920){
   						cover = json.getJSONArray("cover_new").getJSONObject(2).getString("url");
   					}else if(UtDevice.getHeight()>=1280){
   						cover = json.getJSONArray("cover_new").getJSONObject(1).getString("url");
   					}else if(UtDevice.getHeight()<1280){
   						cover = json.getJSONArray("cover_new").getJSONObject(0).getString("url");
   					}
   					summary = json.getString("summary");
   					datetime = json.getString("datetime");
   					ebookId = json.getString("id");
   					String size = json.getString("size");
   					String ratingCount = json.getString("rating_count");
   					float ratingAverage = Float.parseFloat(json.getString("rating_average"));
   					String messageError = json.getString("message_error");
   					String statusType = json.getString("status");

	   				TextView txt_description = (TextView) popupView.findViewById(R.id.view_detail__txt_title);
	   				TextView txt_rating = (TextView) popupView.findViewById(R.id.view_detail__txt_rating);
   					TextView txt_date = (TextView) popupView.findViewById(R.id.view_detail__txt_date);
   					TextView txt_facebook_friend = (TextView) popupView.findViewById(R.id.txt_facebook_friend);
   					TextView txt_header_facebook = (TextView) popupView.findViewById(R.id.txt_header_facebook);
   					TextView txt_facebook_like_status = (TextView) popupView.findViewById(R.id.txt_facebook_like_status);
   					TextView btn_facebook_like = (TextView) popupView.findViewById(R.id.btn_facebook_like);
   					TextView txt_tap_to_rate = (TextView) popupView.findViewById(R.id.txt_tap_to_rate);
   					TextView txt_header_book = (TextView) popupView.findViewById(R.id.view_detail__txt_header_book);
   					TextView btn_send_review = (TextView) popupView.findViewById(R.id.btn_send_review);
   					RatingBar rating_review = (RatingBar) popupView.findViewById(R.id.ratingBar1);
   					
   					edt_title = (EditText) popupView.findViewById(R.id.edt_title);
   					edt_write_review = (EditText) popupView.findViewById(R.id.edt_write_review);
   			
   					ImageView img_star1 = (ImageView) popupView.findViewById(R.id.img_star1);
   					ImageView img_star2 = (ImageView) popupView.findViewById(R.id.img_star2);
   					ImageView img_star3 = (ImageView) popupView.findViewById(R.id.img_star3);
   					ImageView img_star4 = (ImageView) popupView.findViewById(R.id.img_star4);
   					ImageView img_star5 = (ImageView) popupView.findViewById(R.id.img_star5);
   					
   					img_star1.setImageResource(StarRating.setDisplay(ratingAverage, 1));
   					img_star2.setImageResource(StarRating.setDisplay(ratingAverage, 2));
   					img_star3.setImageResource(StarRating.setDisplay(ratingAverage, 3));
   					img_star4.setImageResource(StarRating.setDisplay(ratingAverage, 4));
   					img_star5.setImageResource(StarRating.setDisplay(ratingAverage, 5));
   					
   					txt_description.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_rating.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_date.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_facebook_friend.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_header_facebook.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_facebook_like_status.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					btn_facebook_like.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_tap_to_rate.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					txt_header_book.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					btn_send_review.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));

					txt_description.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_rating.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_date.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_facebook_friend.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_header_facebook.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_facebook_like_status.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_tap_to_rate.setTextColor(ThemeResourceManager.getFontColor(activity));
					txt_header_book.setTextColor(ThemeResourceManager.getFontColor(activity));

   					edt_title.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					edt_write_review.setTypeface(FcustomFonts.getTypeFaceHelveticaXBlk(activity));
   					
   				
   					edt_title.postDelayed(new Runnable() {

   				        @Override
   				        public void run() {
   				            // TODO Auto-generated method stub
   				            InputMethodManager keyboard = (InputMethodManager)
   				            activity.getSystemService(Context.INPUT_METHOD_SERVICE);
   				            keyboard.hideSoftInputFromWindow( edt_title.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
   				        }
   				    },0);
   					edt_write_review.postDelayed(new Runnable() {

   				        @Override
   				        public void run() {
   				            // TODO Auto-generated method stub
   				            InputMethodManager keyboard = (InputMethodManager)
   				            activity.getSystemService(Context.INPUT_METHOD_SERVICE);
   				            keyboard.hideSoftInputFromWindow( edt_write_review.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
   				        }
   				    },0);
   					
   				
   					
   					try{
	   					edt_title.setText(!json.isNull("review_current")?json.getJSONArray("review_current").getJSONObject(0).getString("title"):"");
	   					edt_write_review.setText(!json.isNull("review_current")?json.getJSONArray("review_current").getJSONObject(0).getString("edt_write_review"):"");
	   					rating_review.setRating(!json.isNull("review_current")?Float.parseFloat(json.getJSONArray("review_current").getJSONObject(0).getString("rating")):0);
   					}catch (Exception e) {
   						 Log.d(TAG,"onLoadTitleAndWriteReview = "+e);
   					}
   					
   					btn_send_review.setText(Html.fromHtml("<html><body><u>"+ activity.getString(R.string.txt_send_review)+"</u></body></html>"));

//   					ProgressBar pgb_download = (ProgressBar) activity.findViewById(R.id.pgb_download);
//	   				TextView txt_book_size = (TextView) popupView.findViewById(R.id.txt_book_size);
   					ImageView img_cover_detail = (ImageView) popupView.findViewById(R.id.img_cover_detail);
//   					ImageView img_review = (ImageView) activity.findViewById(R.id.img_review);
   					final ImageView pb = (ImageView) popupView.findViewById(R.id.pb_cover_loading);
   					
   					txt_description.setText(name);
   					txt_rating.setText("("+ratingCount+" Ratings)");
//   					txt_book_size.setText(size+"m");
   					
   					edt_title.setOnFocusChangeListener(new OnFocusChangeListener() {
   					
				        public void onFocusChange(View v, boolean hasFocus) {
				            if (hasFocus == true){
//				            	displayKeyboard(v);

				            }
				        }
					});
   					
   		
   					edt_write_review.setOnFocusChangeListener(new OnFocusChangeListener() {
   					
				        public void onFocusChange(View v, boolean hasFocus) {
				            if (hasFocus == true){
//				            	displayKeyboard(v);

				            }
				        }
					});

   					rating_review.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
						
						@Override
						public void onRatingChanged(RatingBar arg0, float ratings, boolean arg2) {
							// TODO Auto-generated method stub
							rating = String.valueOf(ratings);
						}
					});
			
       				Date date;
					try {
						date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime);
						String formattedDate = new SimpleDateFormat("dd MMM yyyy").format(date);	
						txt_date.setText(formattedDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    
//					img_cover_detail.getLayoutParams().height = BookSize.book_cover_heigth-50;
//					img_cover_detail.getLayoutParams().width = BookSize.book_cover_width-50;
					
					
					new SvReview().setReviewLoader(activity, aQuery, popupView, listReview, categoryId);
					
					
   					imageLoader.displayImage(cover, img_cover_detail, ImageDisplayOptions.getImageOptionCategory(), new SimpleImageLoadingListener(){
   					
   					@Override
   					public void onLoadingStarted(String imageUri, View view) {
   						pb.setVisibility(View.VISIBLE);
   					}

   					@Override
   					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
   						String message = null;
   						switch (failReason.getType()) {
   							case IO_ERROR:
   								message = "Input/Output error";
   								break;
   							case DECODING_ERROR:
   								message = "Image can't be decoded";
   								break;
   							case NETWORK_DENIED:
   								message = "Downloads are denied";
   								break;
   							case OUT_OF_MEMORY:
   								message = "Out Of Memory error";
   								break;
   							case UNKNOWN:
   								message = "Unknown error";
   								break;
   						}
   						Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

   						pb.setVisibility(View.GONE);
   					}

   					@Override
   					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
   						pb.setVisibility(View.GONE);
   					}
   				});

			}
			else{
//				Toast.makeText(aQuery.getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
	}

	private void displayKeyboard(View v){
		InputMethodManager inputMgr = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        inputMgr.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
	}
	
	private void forceHideKeyboard(Activity activity){
		if (activity.getCurrentFocus() != null) {
			InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}


	
	
}
