package com.ghbank.bookshelf.custom;

import com.ghbank.bookshelf.R;
import com.ghbank.bookshelf.utils.UtDevice;
import android.app.Activity;
import android.content.res.Configuration;



public class BookSize {

	public static int book_cover_width;
	public static int book_cover_heigth;
	private int book_cover_width_vertical = 3;
	private int book_cover_width_horizontal = 4;
	private int book_cover_heigth_vertical = 2;
	private int book_cover_heigth_horizontal = 3;
	
	public static int book_nomal_width;
	public static int book_nomal_heigth;
	private int book_nomal_width_vertical = 4;
	private int book_nomal_width_horizontal = 5;
	private int book_nomal_heigth_vertical = 3;
	private int book_nomal_heigth_horizontal = 4;
	
	private Configuration config;
	
	public BookSize(Activity ac){
		Initial(ac);
	}
	
	private void Initial(Activity ac){
       	
       	book_cover_width_vertical = (int) ac.getResources().getInteger(R.integer.book_cover_width_vertical);
       	book_cover_width_horizontal = (int) ac.getResources().getInteger(R.integer.book_cover_width_horizontal);
       	book_cover_heigth_vertical = (int) ac.getResources().getInteger(R.integer.book_cover_heigth_vertical);
       	book_cover_heigth_horizontal = (int) ac.getResources().getInteger(R.integer.book_cover_heigth_horizontal);
       	
       	book_nomal_width_vertical = (int) ac.getResources().getInteger(R.integer.book_width_vertical);
       	book_nomal_width_horizontal = (int) ac.getResources().getInteger(R.integer.book_width_horizontal);
       	book_nomal_heigth_vertical = (int) ac.getResources().getInteger(R.integer.book_heigth_vertical);
       	book_nomal_heigth_horizontal = (int) ac.getResources().getInteger(R.integer.book_heigth_horizontal);
       	
        config = ac.getResources().getConfiguration();
    		
	}
	
	public void setCoverSize(){
		if (config.orientation == Configuration.ORIENTATION_PORTRAIT){

			book_cover_width = UtDevice.getWidth()/book_cover_width_vertical;
			book_cover_heigth= UtDevice.getWidth()/book_cover_heigth_vertical;
		}else{
   		
   		 	book_cover_width = UtDevice.getWidth()/book_cover_width_horizontal;
   		 	book_cover_heigth= UtDevice.getWidth()/book_cover_heigth_horizontal;
		}
	}
	
	public void setNormalSize(){
		if (config.orientation == Configuration.ORIENTATION_PORTRAIT){

			book_nomal_width = UtDevice.getWidth()/book_nomal_width_vertical;
			book_nomal_heigth= UtDevice.getWidth()/book_nomal_heigth_vertical;
		}else{
   		
			book_nomal_width = UtDevice.getWidth()/book_nomal_width_horizontal;
			book_nomal_heigth= UtDevice.getWidth()/book_nomal_heigth_horizontal;
		}
	}
}
