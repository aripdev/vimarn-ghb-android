package com.ghbank.bookshelf.custom;

import com.ghbank.bookshelf.R;


public class StarRating {
	public static int setDisplay(float average, int position_star){
		int res = 0;
		
		switch (position_star) {
		case 1:
			if(average>=1){
				res = R.drawable.ip_star_full;
			}else if(average>=0.5f){
				res = R.drawable.ip_star_half;
			}else{
				res = R.drawable.ip_star_blank;
			}
			break;
		case 2:
			if(average>=2){
				res = R.drawable.ip_star_full;
			}else if(average>=1.5f){
				res = R.drawable.ip_star_half;
			}else{
				res = R.drawable.ip_star_blank;
			}
			break;
		case 3:
			if(average>=3){
				res = R.drawable.ip_star_full;
			}else if(average>=2.5f){
				res = R.drawable.ip_star_half;
			}else{
				res = R.drawable.ip_star_blank;
			}
			break;
		case 4:
			if(average>=4){
				res = R.drawable.ip_star_full;
			}else if(average>=3.5f){
				res = R.drawable.ip_star_half;
			}else{
				res = R.drawable.ip_star_blank;
			}
			break;
		case 5:
			if(average>=5){
				res = R.drawable.ip_star_full;
			}else if(average>=4.5f){
				res = R.drawable.ip_star_half;
			}else{
				res = R.drawable.ip_star_blank;
			}
			break;

		default:
			break;
		}
		return res;
	}
	

}
