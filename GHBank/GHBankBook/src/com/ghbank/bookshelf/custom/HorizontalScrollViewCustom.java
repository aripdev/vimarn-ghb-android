package com.ghbank.bookshelf.custom;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class HorizontalScrollViewCustom extends HorizontalScrollView {

	@SuppressWarnings("unused")
	private Runnable scrollerTask;
	@SuppressWarnings("unused")
	private int initialPosition;

	@SuppressWarnings("unused")
	private int newCheck = 100;
	@SuppressWarnings("unused")
	private static final String TAG = "HorizontalScrollViewCustom";

	public interface OnScrollStoppedListener {
		void onScrollStopped();
	}

	@SuppressWarnings("unused")
	private OnScrollStoppedListener onScrollStoppedListener;

	public HorizontalScrollViewCustom(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	Timer ntimer = new Timer();
	MotionEvent event;

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		checkAgain();
		super.onScrollChanged(l, t, oldl, oldt);
	}

	public void checkAgain() {
		try {
			ntimer.cancel();
			ntimer.purge();
		} catch (Exception e) {
		}
		ntimer = new Timer();
		ntimer.schedule(new TimerTask() {

			@Override
			public void run() {

				if (event.getAction() == MotionEvent.ACTION_UP) {
					// ScrollView Stopped Scrolling and Finger is not on the
					// ScrollView
				} else {
					// ScrollView Stopped Scrolling But Finger is still on the
					// ScrollView
					checkAgain();
				}
			}
		}, 100);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		this.event = event;
		return super.onTouchEvent(event);
	}
}