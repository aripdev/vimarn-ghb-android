package com.arip.it.library;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.arip.it.library.base.BaseActivity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExamSound extends BaseActivity {

	private String TAG = "Ch. ExamSound";
	public String Path, Total, Section, Type, Id, Sign;

	public int sec, ExamTotal;
	private String idNext, typeNext, target;

	public float x1, x2;

	String soundPath;
	String soundId;
	String soundSection;
	String soundTotal;
	String soundType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_sound);


		Bundle sound = getIntent().getExtras();
		soundPath = sound.getString("path");
		soundId = sound.getString("id");
		soundSection = sound.getString("section");
		soundTotal = sound.getString("total");
		soundType = sound.getString("type");

		String S_path = soundPath + "sound/" + soundId + ".txt";
		final String S_section = soundPath + "exam.txt";

		final String S_sound = soundPath + "sound/" + soundId + "/";

		Log.d("TAG", "S_picture : " + S_sound);

		final ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((line = examR.readLine()) != null) {
				if (line.length() > 0) {
					y.add(line);
				}
			}

			for (int i = 0; i < y.size(); i++)
				Log.d("TAG", "READ in Sound : " + y.get(i));


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		final RadioButton radioButton1 = (RadioButton) findViewById(R.id.sound_c1);
		radioButton1.setText(y.get(5));

		final RadioButton radioButton2 = (RadioButton) findViewById(R.id.sound_c2);
		radioButton2.setText(y.get(9));

		final RadioButton radioButton3 = (RadioButton) findViewById(R.id.sound_c3);
		radioButton3.setText(y.get(13));

		final RadioButton radioButton4 = (RadioButton) findViewById(R.id.sound_c4);
		radioButton4.setText(y.get(17));

		ImageView imgSound = (ImageView) findViewById(R.id.sound_play);
		imgSound.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				final MediaPlayer mp = new MediaPlayer();
				try {
					mp.setDataSource(S_sound + y.get(1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					mp.prepare();
				} catch (IOException e) {
					e.printStackTrace();
				}
				mp.start();
				//mp.pause();
				//mp.stop();

				ImageView imgPause = (ImageView) findViewById(R.id.imagePause);
				imgPause.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {

						mp.pause();
						//mp.stop();
						return false;
					}
				});
				return false;
			}
		});

		ImageView soundBtn = (ImageView) findViewById(R.id.sound_btn_answer);
		soundBtn.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				String x = "1";

				ImageView imgSound = (ImageView) findViewById(R.id.img_sound_ans);

				if (radioButton1.isChecked() && y.get(5).equals(x)) {
					imgSound.setImageResource(R.drawable.r);
				} else if (radioButton2.isChecked() && y.get(9).equals(x)) {
					imgSound.setImageResource(R.drawable.r);
				} else if (radioButton3.isChecked() && y.get(13).equals(x)) {
					imgSound.setImageResource(R.drawable.r);
				} else if (radioButton4.isChecked() && y.get(17).equals(x)) {
					imgSound.setImageResource(R.drawable.r);
				} else {
					imgSound.setImageResource(R.drawable.w);
				}

				return false;
			}
		});
	}
}


	/*
		
		Path = getIntent().getExtras().getString("path");
		Total = getIntent().getExtras().getString("total");
		Section = getIntent().getExtras().getString("section");
		Type = getIntent().getExtras().getString("type");
		Id = getIntent().getExtras().getString("id");
		Sign = getIntent().getExtras().getString("sign");
		
		ExamTotal = Integer.parseInt(Total.toString());
		
		Log.e(TAG,"path:"+Path);
		Log.e(TAG, "Total:"+Total+" section:"+Section+" ID:"+Type+" type:"+Id);
		
		if(Sign.equalsIgnoreCase("+")){
			overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
		}
		else overridePendingTransition(R.anim.slide_right_in, R.anim.hold);
		setContentView(R.layout.lay_exam_sound);
		
	}
	
	public int sectionNow(){
		String s = Section;
		Log.d(TAG, "sectionNow : "+s);
		s = replaceAll(s,"section","");
		Log.d(TAG, "sectionNow : "+s);
		int sect = Integer.parseInt(s.toString());
		return sect;
	}
	
	public void goIncrease(){
		
		Log.d(TAG, "goIncrease");
		
		int sect = sectionNow()+1;
		
		
		if(sect>=ExamTotal){
			this.finish();
		}
		else{
			
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
	    	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
	    	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
	    	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
	    	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
	    	intent.putExtra("total", Total);
	    	intent.putExtra("section", target);
	    	intent.putExtra("type", typeNext);
	    	intent.putExtra("id", idNext);
	    	intent.putExtra("sign","+");
	    	
	    	Log.e(TAG, "+++Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
	    	
	    	this.finish();
	    	this.startActivity(intent);
			
			
			
			
		}
		
		
		
		
	}
	
	public void goDecrease(){
		
		Log.d(TAG, "goDecrease");
		
		int sect = sectionNow()-1;
		
		
		if(sect<0){
			this.finish();
		}
		else{
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
        	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
        	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
        	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
        	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
        	intent.putExtra("total", Total);
        	intent.putExtra("section", target);
        	intent.putExtra("type", typeNext);
        	intent.putExtra("id", idNext);
        	intent.putExtra("sign","-");
        	
        	Log.e(TAG, "---Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
        	
        	this.finish();
        	this.startActivity(intent);
			
			
			
		}
		
		
		
	}
	
	public void findSection(int sect){
		
		target="section"+sect;
		
    	try{
			
			FileReader ex = new FileReader(Path+"exam.txt");
			BufferedReader examR = new BufferedReader(ex);
			
			String line = "";
			while ((line = examR.readLine()) != null) {
			    // do something with the line you just read, e.g.
				Log.d(TAG,"temp : "+line);
				if(line.equalsIgnoreCase(target)){
	     			typeNext = String.valueOf( examR.readLine() );	
	     			idNext = String.valueOf( examR.readLine() );
	     			Log.d(TAG,"typeNext : "+typeNext);
	     			Log.d(TAG,"idNext : "+idNext);
	     			break;
	     		}
			   
			}
			
	     	
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
	}
	
	public boolean onTouchEvent(MotionEvent touchevent) 
    {
		
		
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		//Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();  // deprecated
		//int height = display.getHeight();  // deprecated
		
		
		float sensitive=width/2;
		
    	switch (touchevent.getAction())
    	{
    		// when user first touches the screen we get x and y coordinate
    		case MotionEvent.ACTION_DOWN: 
    		{
    			x1 = touchevent.getX();
    			
    			break;
    		}
    		case MotionEvent.ACTION_UP: 
    		{
    			x2 = touchevent.getX();
    			
    			//if left to right sweep event on screen
                if (x1 < x2 && (x2-x1)>=sensitive) 
               	{
                	Log.d("puy","L - R");
                	
                	goDecrease();
                	
                	
                	
                }
                                         
                // if right to left sweep event on screen
                if (x1 > x2 && (x1-x2)>=sensitive)
                {
                	Log.d("puy","R - L");
                	
                	goIncrease();
                }
                
                break;
    		}
    	}
    	return false;
	}
	
	 private String replaceAll(String source, String pattern, String replacement) {
	        if (source == null) {
	            return "";
	        }
	        StringBuffer sb = new StringBuffer();
	        int index;
	        int patIndex = 0;
	        while ((index = source.indexOf(pattern, patIndex)) != -1) {
	            sb.append(source.substring(patIndex, index));
	            sb.append(replacement);
	            patIndex = index + pattern.length();
	        }
	        sb.append(source.substring(patIndex));
	        return sb.toString();
	    }
	
}
*/