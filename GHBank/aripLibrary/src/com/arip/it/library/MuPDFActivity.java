package com.arip.it.library;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.arip.it.library.base.BaseActivity;
import com.arip.it.library.core.lib.util.DeviceSize;
import com.arip.it.library.core.lib.util.PDFParser;
import com.arip.it.library.core.model.ContentItem;
import com.arip.it.library.core.model.Magazine;
import com.arip.it.library.core.task.TinySafeAsyncTask;
import com.arip.it.library.core.util.FileManage;
import com.arip.it.library.core.view.TwoWayView;
import com.arip.it.library.core.view.TwoWayView.Orientation;
import com.arip.it.library.database.DatabaseManager;
import com.arip.it.library.db.BookmarkHandler;
import com.arip.it.library.model.ShareLink;
import com.artifex.mupdf.LinkInfo;
import com.artifex.mupdf.LinkInfoExternal;
import com.artifex.mupdf.MediaHolder;
import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.MuPDFPageAdapter;
import com.artifex.mupdf.MuPDFPageView;
import com.artifex.mupdf.OutlineItem;
import com.artifex.mupdf.PDFPreviewPagerAdapter;
import com.artifex.mupdf.domain.OutlineActivityData;
import com.artifex.mupdf.domain.SearchTaskResult;
import com.artifex.mupdf.view.DocumentReaderView;
import com.artifex.mupdf.view.ReaderView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class MuPDFActivity extends BaseActivity {
    private static final String TAG = "MuPDFActivity";

    //      private static final int SEARCH_PROGRESS_DELAY = 200;
    @SuppressWarnings("unused")
    private static final int WAIT_DIALOG = 0;
    private static final String FILE_NAME = "FileName";

    private static final int START_BILLING_ACTIVITY = 100;
    private static final int START_OUTLINE_ACTIVITY = 101;
    static final int START_GALLERY_ACTIVITY = 102;

    public static final String SHOW_THUMBNAILS_EXTRA = "show_thumbnails";

    private MuPDFCore core;
    private String fileName;
    private int mOrientation;

    //      private int          mPageSliderRes;
    private boolean buttonsVisible;
    private boolean mTopBarIsSearch;

    //      private WeakReference<SearchTask> searchTask;
    @SuppressWarnings("unused")
    private ProgressDialog dialog;

    private AlertDialog.Builder alertBuilder;
    private static ReaderView docView;
    private View buttonsView;
    private EditText mPasswordView;
    private TextView mFilenameView;
    @SuppressWarnings("unused")
    private ImageButton mSearchButton;
    @SuppressWarnings("unused")
    private ImageButton mCancelButton;
    private ImageButton mLectureButton;
    private ImageButton mOutlineButton;
    private ImageButton mGalleryButton;
    private ImageButton mMenuButton;
    private ImageButton mEmailButton;
    private ImageButton mBookmarkButton;
    private ViewSwitcher mTopBarSwitcher;
    @SuppressWarnings("unused")
    private ImageButton mSearchBack;
    @SuppressWarnings("unused")
    private ImageButton mSearchFwd;
    private EditText mSearchText;
    //private SearchTaskResult mSearchTaskResult;
    @SuppressWarnings("unused")
    private final Handler mHandler = new Handler();
    private FrameLayout mPreviewBarHolder;
    private TwoWayView mPreview;
    private PDFPreviewPagerAdapter pdfPreviewPagerAdapter;
    private MuPDFPageAdapter mDocViewAdapter;
    private SparseArray<LinkInfoExternal[]> linkOfDocument;
    @SuppressWarnings("unused")
    private LinearLayout playerLayout;
    private Point mPreviewSize;
    private BookmarkHandler bookmarkHanlder;
    public static String bookidRef;
    private boolean addBookmark;

    public static Bitmap bMapLecture;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.w(TAG, TAG + "  onCreate");
        alertBuilder = new AlertDialog.Builder(this);

        core = getMuPdfCore(savedInstanceState);

        if (core == null) {
            finish();
            return;
        }

        mOrientation = getResources().getConfiguration().orientation;

        if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            core.setDisplayPages(2);
        } else {
            core.setDisplayPages(1);
        }

        if (bookmarkHanlder == null) {
            bookmarkHanlder = new BookmarkHandler(this);
            bookmarkHanlder.open();
        }
        createUI(savedInstanceState);
    }

    private void requestPassword(final Bundle savedInstanceState) {
        mPasswordView = new EditText(this);
        mPasswordView.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        mPasswordView.setTransformationMethod(new PasswordTransformationMethod());

        AlertDialog alert = alertBuilder.create();
        alert.setTitle(R.string.enter_password);
        alert.setView(mPasswordView);
        alert.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (core.authenticatePassword(mPasswordView.getText().toString())) {
                            createUI(savedInstanceState);
                        } else {
                            requestPassword(savedInstanceState);
                        }
                    }
                });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alert.show();
    }

    @SuppressWarnings("deprecation")
    private MuPDFCore getMuPdfCore(Bundle savedInstanceState) {
        MuPDFCore core = null;
        if (core == null) {
            core = (MuPDFCore) getLastNonConfigurationInstance();

            if (savedInstanceState != null && savedInstanceState.containsKey(FILE_NAME)) {
                fileName = savedInstanceState.getString(FILE_NAME);
            }
        }
        if (core == null) {
            Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                if (uri.toString().startsWith("content://media/external/file")) {
                    // Handle view requests from the Transformer Prime's file manager
                    // Hopefully other file managers will use this same scheme, if not
                    // using explicit paths.
                    Cursor cursor = getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
                    if (cursor.moveToFirst()) {
                        uri = Uri.parse(cursor.getString(0));
                    }
                }

                File fileToOpen = new File(uri.getPath());
                if (!fileToOpen.exists())
                    return null;

                core = openFile(Uri.decode(uri.getEncodedPath()));
                SearchTaskResult.recycle();
            }
            if (core != null && core.needsPassword()) {
                requestPassword(savedInstanceState);
                return null;
            }
        }
        if (core == null) {
            AlertDialog alert = alertBuilder.create();

            alert.setTitle(R.string.open_failed);
            alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dismiss),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alert.show();
            return null;
        }
        return core;
    }

    private void createUI(Bundle savedInstanceState) {
        if (core == null)
            return;
        // Now create the UI.
        // First create the document view making use of the ReaderView's internal
        // gesture recognition

        docView = new DocumentReaderView(this, linkOfDocument) {
            ActivateAutoLinks mLinksActivator = null;

            @Override
            protected void onMoveToChild(View view, int i) {
                Log.d(TAG, "onMoveToChild id = " + i);

//                              if(core.getDisplayPages() == 1)
//                                      mPreview.scrollToPosition(i);
//                              else
//                                      mPreview.scrollToPosition(((i == 0) ? 0 : i * 2 - 1));

                if (core == null) {
                    return;
                }
                MuPDFPageView pageView = (MuPDFPageView) docView.getDisplayedView();
                if (pageView != null) {
                    pageView.cleanRunningLinkList();
                }
                super.onMoveToChild(view, i);
                if (mLinksActivator != null)
                    mLinksActivator.cancel(true);
                mLinksActivator = new ActivateAutoLinks(pageView);
                mLinksActivator.safeExecute(i);

//                              core.setDisplayPages(2);

                //Set Page view currently
                setCurrentlyViewedPreview();


//                              EasyTracker.getInstance().setContext(MuPDFActivity.this);
                if (core.getDisplayPages() == 2) {
                    @SuppressWarnings("unused")
                    int actualPageNumber = (i * 2) - 1;
                    if (i > 0) {
//                                      EasyTracker.getTracker().sendView(
//                                                      "PDFReader/" + FilenameUtils.getBaseName(fileName) + "/page"
//                                                                      + (actualPageNumber + 1));
                    }
                    if (i + 1 < docView.getAdapter().getCount()) {
//                                      EasyTracker.getTracker().sendView(
//                                                      "PDFReader/" + FilenameUtils.getBaseName(fileName) + "/page"
//                                                                      + (actualPageNumber + 2));
                    }
                } else {
//                                      EasyTracker.getTracker().sendView(
//                                                      "PDFReader/" + FilenameUtils.getBaseName(fileName) + "/page" + (i + 1));
                }
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                if (!isShowButtonsDisabled()) {
                    hideButtons();
                }
                return super.onScroll(e1, e2, distanceX, distanceY);
            }

            @Override
            protected void onContextMenuClick() {
                if (!buttonsVisible) {
                    showButtons();
                } else {
                    hideButtons();
                }
            }

            @Override
            protected void onBuy(String path) {
//                              MuPDFActivity.this.onBuy(path);
            }


        };
        mDocViewAdapter = new MuPDFPageAdapter(this, core);
        docView.setAdapter(mDocViewAdapter);
        // Make the buttons overlay, and store all its
        // controls in variables
        makeButtonsView();

        if (mPreviewSize == null) {
            mPreviewSize = new Point();
            int padding = getResources().getDimensionPixelSize(
                    R.dimen.page_preview_size);
            PointF mPageSize = core.getSinglePageSize(0);
            float scale = mPageSize.y / mPageSize.x;
            mPreviewSize.x = (int) ((float) padding / scale);
            mPreviewSize.y = padding;

            DeviceSize.setWidth(mPreviewSize.x);
            DeviceSize.setHeight(mPreviewSize.y);
        }
        // Set the magazine title text
        String title = getIntent().getStringExtra(Magazine.FIELD_TITLE);
        String ibookId = getIntent().getStringExtra(Magazine.FIELD_BOOK_ID);
        if (title != null) {
            mFilenameView.setText(title);
        } else {
            mFilenameView.setText(fileName);
        }

        if (ibookId != null) {
            bookidRef = ibookId;
        }

        String contentPath = core.getFileDirectory() + "/content.txt";
        File f = new File(contentPath);

        try {
            if (f.exists()) {

                BufferedReader fis = new BufferedReader(new FileReader(contentPath));
                StringBuffer fileContent = new StringBuffer("");
                String line;
                ArrayList<ContentItem> dContent = new ArrayList<ContentItem>();

                while ((line = fis.readLine()) != null) {
                    fileContent.append(line);
                    dContent.add(new ContentItem(line.split(",")[0], Integer.parseInt(line.split(",")[1])));
                }
                fis.close();

                ContentItem.getDataContent().clear();
                ContentItem.setDataContent(dContent);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (f.exists()) {
            mOutlineButton.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    try {
                        OutlineItem outline[] = new OutlineItem[ContentItem.getDataContent().size()];
                        for (int contentIndex = 0; contentIndex < ContentItem.getDataContent().size(); contentIndex++) {
                            outline[contentIndex] = new OutlineItem(ContentItem.getDataContent().get(contentIndex).getJumpto(),
                                    ContentItem.getDataContent().get(contentIndex).getDescription(),
                                    ContentItem.getDataContent().get(contentIndex).getJumpto());
                        }

                        if (outline != null) {
                            OutlineActivityData.get().items = outline;
                            Intent intent = new Intent(MuPDFActivity.this, OutlineActivity.class);
                            startActivityForResult(intent, START_OUTLINE_ACTIVITY);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            mOutlineButton.setVisibility(View.GONE);
        }

//              if (core.hasOutline()) {
//                      mOutlineButton.setOnClickListener(new View.OnClickListener() {
//                              public void onClick(View v) {
        @SuppressWarnings("unused")
        OutlineItem outline[] = core.getOutline();
//                                      if (outline != null) {
//                                              OutlineActivityData.get().items = outline;
//                                              Intent intent = new Intent(MuPDFActivity.this, OutlineActivity.class);
//                                              startActivityForResult(intent, START_OUTLINE_ACTIVITY);
//                                      }
//                              }
//                      });
//              } else {
//                      mOutlineButton.setVisibility(View.GONE);
//              }

        mLectureButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                hideButtons();


                Handler handler;
                long timeDelay = 1000; //1 seconds
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
//  			            	ByteArrayOutputStream stream = new ByteArrayOutputStream();

                        View v1 = mPreview.getRootView();
                        v1.setDrawingCacheEnabled(true);
                        bMapLecture = Bitmap.createBitmap(v1.getDrawingCache());
//  		  					Bitmap bTest = Bitmap.createBitmap(v1.getDrawingCache());
//  		  					bTest.compress(Bitmap.CompressFormat.PNG, 80, stream);		
//  		  					byte[] byteArray = stream.toByteArray();
//  		  					
//  			            	
//  			            	Bundle bundle = new Bundle();
//  			            	bundle.putByteArray("bitmap", byteArray);
//  			            	bundle.putString("type", "SingleImage");

                        Intent intent = new Intent(MuPDFActivity.this, LectureActivity.class);
//  			            	intent.putExtra("image",bundle);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        v1.setDrawingCacheEnabled(false);
                    }
                }, timeDelay);

            }
        });

        mEmailButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String rootPath = core.getFileDirectory();
                String[] strFind = core.getFileDirectory().split("/");
                int size = strFind.length;
                if (size > 0) {
                    String bookId = strFind[size - 1];
                    int pageIndex = docView.getDisplayedViewIndex();
                    Log.w(TAG, "File path : " + rootPath);
                    Log.w(TAG, "Book id : " + bookId);
                    Log.w(TAG, "Page index : " + pageIndex);
                    ShareLink shareLink = new DatabaseManager(MuPDFActivity.this).getShareLink(bookId, pageIndex);
                    if (shareLink != null) {
                        new ProvideEmailIntent(bookId, shareLink.imageUrl, pageIndex).execute();
                    }
                }
            }
        });
        mBookmarkButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (addBookmark) {
                    addBookmark = false;
                    mBookmarkButton.setImageResource(R.drawable.ic_bookmark_unselected);

                    if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {

                        bookmarkHanlder.deleteBookmark(bookidRef, ((docView.getDisplayedViewIndex() + 1) * 2 - 1) + "");
                    } else {
                        bookmarkHanlder.deleteBookmark(bookidRef, (docView.getDisplayedViewIndex() + 1) + "");
                    }

                } else {
                    String imgPath;
                    addBookmark = true;
                    mBookmarkButton.setImageResource(R.drawable.ic_bookmark_selected);


                    if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                        imgPath = core.getFileDirectory() + "/previewcache/" + ((docView.getDisplayedViewIndex() == 0) ? 0 : docView.getDisplayedViewIndex() * 2) + ".jpg";
                        bookmarkHanlder.addBookmark(bookidRef, ((docView.getDisplayedViewIndex() + 1) * 2 - 1) + "", mFilenameView.getText().toString(), imgPath, "1");
                    } else {
                        imgPath = core.getFileDirectory() + "/previewcache/" + docView.getDisplayedViewIndex() + ".jpg";
                        bookmarkHanlder.addBookmark(bookidRef, (docView.getDisplayedViewIndex() + 1) + "", mFilenameView.getText().toString(), imgPath, "1");
                    }

                }


            }
        });
        mGalleryButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MuPDFActivity.this, GalleryActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("bundlecore", core);
                intent.putExtras(b);
                startActivityForResult(intent, START_GALLERY_ACTIVITY);
            }
        });

        mMenuButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                PopupMenu popup = new PopupMenu(MuPDFActivity.this, v);
                popup.getMenuInflater().inflate(R.menu.menu_tool, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Intent intent = new Intent(MuPDFActivity.this, BookmarkActivity.class);
                        if (item.getTitle() == getString(com.arip.it.library.R.string.menu_lecture)) {
                            intent = new Intent(MuPDFActivity.this, LectureActivity.class);
                        } else if (item.getTitle() == getString(com.arip.it.library.R.string.menu_bookmark)) {

                            String imgPath;
                            intent = new Intent(MuPDFActivity.this, BookmarkActivity.class);

                            if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                                imgPath = core.getFileDirectory() + "/previewcache/" + ((docView.getDisplayedViewIndex() == 0) ? 0 : docView.getDisplayedViewIndex() * 2) + ".jpg";
                                intent.putExtra("bookid", bookidRef);
                                intent.putExtra("pagenum", ((docView.getDisplayedViewIndex() + 1) * 2 - 1) + "");
                                intent.putExtra("desc", mFilenameView.getText().toString());
                                intent.putExtra("imgpath", imgPath);

                            } else {
                                imgPath = core.getFileDirectory() + "/previewcache/" + docView.getDisplayedViewIndex() + ".jpg";
                                intent.putExtra("bookid", bookidRef);
                                intent.putExtra("pagenum", (docView.getDisplayedViewIndex() + 1) + "");
                                intent.putExtra("desc", mFilenameView.getText().toString());
                                intent.putExtra("imgpath", imgPath);
                            }

                        }
                        startActivityForResult(intent, START_OUTLINE_ACTIVITY);

                        return true;
                    }
                });

                popup.show();
            }
        });

        // Reinstate last state if it was recorded
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        int orientation = prefs.getInt("orientation", mOrientation);
//              int pageNum = prefs.getInt("page"+fileName, 0);
        SharedPreferences prefsCore = getSharedPreferences("arip", Context.MODE_PRIVATE);
        int checkOpen = prefsCore.getInt("arip_session", 0);
        int pageNum = 0;
        if (checkOpen == 1) {
            pageNum = 0;
            SharedPreferences.Editor edit = prefsCore.edit();
            edit.putInt("arip_session", 0);
            edit.commit();
        } else {

            pageNum = prefs.getInt("page" + fileName, 0);
        }

        if (orientation == mOrientation)
            docView.setDisplayedViewIndex(pageNum);
        else {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                docView.setDisplayedViewIndex((pageNum + 1) / 2);
            } else {
                docView.setDisplayedViewIndex((pageNum == 0) ? 0 : pageNum * 2 - 1);
            }
        }

        // Give preview thumbnails time to appear before showing bottom bar
        if (savedInstanceState == null
                || !savedInstanceState.getBoolean("ButtonsHidden", false)) {
            mPreview.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showButtons();
                        }
                    });
                }
            }, 250);
        }

        // Stick the document view and the buttons overlay into a parent view
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(docView);
        layout.addView(buttonsView);
//              layout.setBackgroundResource(R.drawable.tiled_background);
        //layout.setBackgroundResource(R.color.canvas);
        layout.setBackgroundColor(Color.BLACK);
        setContentView(layout);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == START_BILLING_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
        if (requestCode == START_OUTLINE_ACTIVITY && resultCode >= 0) {
            resultCode -= 1;
            resultCode = resultCode < 0 ? 0 : resultCode;
            if (core.getDisplayPages() == 2) {
                resultCode = (resultCode + 1) / 2;
            }
            docView.setDisplayedViewIndex(resultCode);
        } else if (requestCode == START_GALLERY_ACTIVITY && resultCode >= 0) {

            if (core.getDisplayPages() == 2) {
                resultCode = (resultCode + 1) / 2;
            }
            docView.setDisplayedViewIndex(resultCode);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public Object onRetainNonConfigurationInstance() {
        MuPDFCore mycore = core;
        core = null;
        return mycore;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (fileName != null && docView != null) {
            outState.putString("FileName", fileName);

            // Store current page in the prefs against the file name,
            // so that we can pick it up each time the file is loaded
            // Other info is needed only for screen-orientation change,
            // so it can go in the bundle
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("page" + fileName, docView.getDisplayedViewIndex());
            edit.putInt("orientation", mOrientation);
            edit.commit();
        }

        if (!buttonsVisible)
            outState.putBoolean("ButtonsHidden", true);

        if (mTopBarIsSearch)
            outState.putBoolean("SearchMode", true);
    }

    @Override
    protected void onPause() {
        super.onPause();

        killSearch();

        if (fileName != null && docView != null) {
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("page" + fileName, docView.getDisplayedViewIndex());
            edit.putInt("orientation", mOrientation);
            edit.commit();
        }
    }

    @Override
    public void onDestroy() {
        if (core != null) {
            core.onDestroy();
        }
        core = null;

        super.onDestroy();
    }

    void showButtons() {
        if (core == null) {
            return;
        }

        if (!buttonsVisible) {
            buttonsView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            buttonsVisible = true;
            // Update page number text and slider
//                      final int index = docView.getDisplayedViewIndex();
//                      mPageSlider.setMax((core.countPages()-1)*mPageSliderRes);
//                      mPageSlider.setProgress(index*mPageSliderRes);
            int position = 0;

            if (mOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                position = (docView.getDisplayedViewIndex() + 1) * 2 - 1;
            } else {
                position = docView.getDisplayedViewIndex() + 1;
            }

            if (bookmarkHanlder.check(bookidRef, position + "")) {
                mBookmarkButton.setImageResource(R.drawable.ic_bookmark_selected);
                addBookmark = true;
            } else {
                mBookmarkButton.setImageResource(R.drawable.ic_bookmark_unselected);
                addBookmark = false;
            }

            if (mTopBarIsSearch) {
                mSearchText.requestFocus();
                showKeyboard();
            }

            Animation anim = new TranslateAnimation(0, 0, -mTopBarSwitcher.getHeight(), 0);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mTopBarSwitcher.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
            mTopBarSwitcher.startAnimation(anim);

            // Don't show thumbnail if not requested
            if (getIntent() != null && !getIntent().getBooleanExtra(SHOW_THUMBNAILS_EXTRA, true)) {
                return;
            }

            // Update listView position
            setCurrentlyViewedPreview();
            anim = new TranslateAnimation(0, 0, mPreviewBarHolder.getHeight(), 0);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mPreviewBarHolder.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
//                                      int page = docView.getCurrentPage();
//                                      if(core.getDisplayPages() == 1)
//                                              mPreview.scrollToPosition(docView.getCurrentPage());
//                                      else
//                                              mPreview.scrollToPosition((page == 0) ? 0 : page * 2 - 1);
                }
            });
            mPreviewBarHolder.startAnimation(anim);
        }
    }

    void hideButtons() {
        if (buttonsVisible) {
            buttonsView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            buttonsVisible = false;
            hideKeyboard();

            Animation anim = new TranslateAnimation(0, 0, 0, -mTopBarSwitcher.getHeight());
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    mTopBarSwitcher.setVisibility(View.INVISIBLE);
                }
            });
            mTopBarSwitcher.startAnimation(anim);

            // Don't show thumbnail if not requested
            if (getIntent() != null && !getIntent().getBooleanExtra(SHOW_THUMBNAILS_EXTRA, true)) {
                return;
            }

            anim = new TranslateAnimation(0, 0, 0, this.mPreviewBarHolder.getHeight());
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mPreviewBarHolder.setVisibility(View.INVISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
            mPreviewBarHolder.startAnimation(anim);

//                      if(bookmarkHanlder.check(bookidRef, (docView.getDisplayedViewIndex()+1)+"")){
//                    	  mBookmarkButton.setImageResource(R.drawable.ic_bookmark_selected);
//                    	  addBookmark = true;
//                      }else{
//                    	  mBookmarkButton.setImageResource(R.drawable.ic_bookmark_unselected);
//                    	  addBookmark = false;
//                      }
        }
    }

    void searchModeOn() {
        if (!mTopBarIsSearch) {
            mTopBarIsSearch = true;
            //Focus on EditTextWidget
            mSearchText.requestFocus();
            showKeyboard();
            mTopBarSwitcher.showNext();
        }
    }

    void searchModeOff() {
        if (mTopBarIsSearch) {
            mTopBarIsSearch = false;
            hideKeyboard();
            mTopBarSwitcher.showPrevious();
            SearchTaskResult.recycle();
            // Make the ReaderView act on the change to mSearchTaskResult
            // via overridden onChildSetup method.
            docView.resetupChildren();
        }
    }

    void makeButtonsView() {

        buttonsView = getLayoutInflater().inflate(R.layout.buttons, null);
        mFilenameView = (TextView) buttonsView.findViewById(R.id.docNameText);
        mPreviewBarHolder = (FrameLayout) buttonsView.findViewById(R.id.PreviewBarHolder);

        mPreview = new TwoWayView(this);
        mPreview.setOrientation(Orientation.HORIZONTAL);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, -1);
        mPreview.setLayoutParams(lp);
        pdfPreviewPagerAdapter = new PDFPreviewPagerAdapter(this, core);
        mPreview.setAdapter(pdfPreviewPagerAdapter);
        mPreview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> pArg0, View pArg1,
                                    int position, long id) {
                hideButtons();
                docView.setDisplayedViewIndex((int) id);
            }
        });
        mPreviewBarHolder.addView(mPreview);
        mSearchButton = (ImageButton) buttonsView.findViewById(R.id.searchButton);
        mCancelButton = (ImageButton) buttonsView.findViewById(R.id.cancel);
        mLectureButton = (ImageButton) buttonsView.findViewById(R.id.lectureButton);
        mOutlineButton = (ImageButton) buttonsView.findViewById(R.id.outlineButton);
        mGalleryButton = (ImageButton) buttonsView.findViewById(R.id.galleryButton);
        mMenuButton = (ImageButton) buttonsView.findViewById(R.id.menuButton);
        mBookmarkButton = (ImageButton) buttonsView.findViewById(R.id.bookmarkButton);
        mEmailButton = (ImageButton) buttonsView.findViewById(R.id.emailButton);
        mTopBarSwitcher = (ViewSwitcher) buttonsView.findViewById(R.id.switcher);
        mSearchBack = (ImageButton) buttonsView.findViewById(R.id.searchBack);
        mSearchFwd = (ImageButton) buttonsView.findViewById(R.id.searchForward);
        mSearchText = (EditText) buttonsView.findViewById(R.id.searchText);
        mTopBarSwitcher.setVisibility(View.INVISIBLE);
        mPreviewBarHolder.setVisibility(View.INVISIBLE);


//              if(bookmarkHanlder.check(bookidRef, (docView.getDisplayedViewIndex()+1)+"")){
//            	  mBookmarkButton.setImageResource(R.drawable.ic_bookmark_selected);
//              }else{
//            	  mBookmarkButton.setImageResource(R.drawable.ic_bookmark_unselected);
//              }

    }

    public static void setDisplayedByIndex(int id) {
        docView.setDisplayedViewIndex(id);
    }

    void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.showSoftInput(mSearchText, 0);
    }

    void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
    }

    void killSearch() {
    }

    void search(int direction) {
        hideKeyboard();
        if (core == null)
            return;
        killSearch();

        final int increment = direction;
        @SuppressWarnings("unused")
        final int startIndex = SearchTaskResult.get() == null ? docView.getDisplayedViewIndex() : SearchTaskResult.get().pageNumber + increment;
    }

//      @Override
//      public boolean onSearchRequested() {
//              if (buttonsVisible && mTopBarIsSearch) {
//                      hideButtons();
//              } else {
//                      showButtons();
//                      searchModeOn();
//              }
//              return super.onSearchRequested();
//      }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (buttonsVisible && !mTopBarIsSearch) {
            hideButtons();
        } else {
            showButtons();
            searchModeOff();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private MuPDFCore openFile(String path) {
        int lastSlashPos = path.lastIndexOf('/');
        fileName = new String(lastSlashPos == -1
                ? path
                : path.substring(lastSlashPos + 1));
        Log.d(TAG, "Trying to open " + path);
        PDFParser linkGetter = new PDFParser(path);
        linkOfDocument = linkGetter.getLinkInfo();

        try {
            core = new MuPDFCore(path);
            // New file: drop the old outline data
            OutlineActivityData.set(null);
        } catch (Exception e) {
            Log.e(TAG, "get core failed", e);
            return null;
        }
        return core;
    }

//      private void onBuy(String path) {
//              Log.d(TAG, "onBuy event path = " + path);
//              MagazineManager magazineManager = new MagazineManager(getContext());
//              Magazine magazine = magazineManager.findByFileName(path, Magazine.TABLE_DOWNLOADED_MAGAZINES);
//              if (null != magazine) {
//                      Intent intent = new Intent(getContext(), BillingActivity.class);
//                      intent
//                              .putExtra(BillingActivity.FILE_NAME_KEY, magazine.getFileName())
//                              .putExtra(BillingActivity.TITLE_KEY, magazine.getTitle())
//                              .putExtra(BillingActivity.SUBTITLE_KEY, magazine.getSubtitle());
//                      startActivityForResult(intent, START_BILLING_ACTIVITY);
//              }
//      }

    private Context getContext() {
        return this;
    }

    private void setCurrentlyViewedPreview() {
        int i = docView.getDisplayedViewIndex();
        if (core.getDisplayPages() == 2) {
            i = (i * 2) - 1;
        }
        pdfPreviewPagerAdapter.setCurrentlyViewing(i);
        centerPreviewAtPosition(i);
    }

    public void centerPreviewAtPosition(int position) {
        if (mPreview.getChildCount() > 0) {
            View child = mPreview.getChildAt(0);
            // assume all children the same width
            int childMeasuredWidth = child.getMeasuredWidth();

            if (childMeasuredWidth > 0) {
                if (core.getDisplayPages() == 2) {
                    mPreview.setSelectionFromOffset(position,
                            (mPreview.getWidth() / 2) - (childMeasuredWidth));
                } else {
                    mPreview.setSelectionFromOffset(position,
                            (mPreview.getWidth() / 2)
                                    - (childMeasuredWidth / 2));
                }
            } else {
                Log.e("centerOnPosition", "childMeasuredWidth = 0");
            }
        } else {
            Log.e("centerOnPosition", "childcount = 0");
        }
    }

    private class ActivateAutoLinks extends TinySafeAsyncTask<Integer, Void, ArrayList<LinkInfoExternal>> {
        private MuPDFPageView pageView;// = (MuPDFPageView) docView.getDisplayedView();

        public ActivateAutoLinks(MuPDFPageView pParent) {
            pageView = pParent;
        }

        @Override
        protected ArrayList<LinkInfoExternal> doInBackground(Integer... params) {
            int page = params[0].intValue();
            Log.d(TAG, "Page = " + page);
            if (null != core) {
                LinkInfo[] links = core.getPageLinks(page);
                if (null == links) {
                    return null;
                }
                ArrayList<LinkInfoExternal> autoLinks = new ArrayList<LinkInfoExternal>();
                for (LinkInfo link : links) {
                    if (link instanceof LinkInfoExternal) {
                        LinkInfoExternal currentLink = (LinkInfoExternal) link;

                        if (null == currentLink.url) {
                            continue;
                        }
                        Log.d(TAG, "checking link for autoplay: " + currentLink.url);

                        if (currentLink.isMediaURI()) {
                            if (currentLink.isAutoPlay()) {
                                autoLinks.add(currentLink);
                            }
                        }

                        if (currentLink.isVedioURI()) {
                            if (currentLink.isAutoPlay()) {
                                autoLinks.add(currentLink);
                            }
                        }
                    }
                }
                return autoLinks;
            }
            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<LinkInfoExternal> autoLinks) {
            if (isCancelled() || autoLinks == null) {
                return;
            }
            docView.post(new Runnable() {
                public void run() {
                    for (LinkInfoExternal link : autoLinks) {
                        if (pageView != null && null != core) {
                            String basePath = core.getFileDirectory();
                            MediaHolder mediaHolder = new MediaHolder(getContext(), link, basePath);
                            pageView.addMediaHolder(mediaHolder, link.url);
                            pageView.addView(mediaHolder);
                            mediaHolder.setVisibility(View.VISIBLE);
                            mediaHolder.requestLayout();
                        }
                    }
                }
            });
        }
    }

    public static void clearBitmapLecture() {
        if (bMapLecture != null) {
            bMapLecture.recycle();
        }
    }

    public void openMail(String imagePath, String imageUrl) {
        try {

            File file = new File(imagePath);
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("application/image");
            //emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,subject);
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, imageUrl);
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            startActivity(Intent.createChooser(emailIntent, "Send"));

        } catch (Exception e) {
            Log.e(TAG, "openMailApp error : " + e.getMessage());
        }
    }

    private class ProvideEmailIntent extends AsyncTask<Void, Void, String> {
        final String PATH = Environment.getExternalStorageDirectory() + "/Android/data/com.ghbank.bookshelf/share/image/";
        String imageUrl;
        String bookId;
        int pageIndex;
        ProgressDialog progressDialog;

        public ProvideEmailIntent(String bookId, String imageUrl, int pageIndex) {
            this.imageUrl = imageUrl;
            this.bookId = bookId;
            this.pageIndex = pageIndex;
            progressDialog = new ProgressDialog(MuPDFActivity.this);
            progressDialog.setMessage("Please wait ...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Bitmap bitmap = FileManage.getBitmapFromURL(imageUrl);
            if (bitmap != null) {
                return FileManage.saveImageFile(PATH, "s" + bookId + pageIndex + ".png", bitmap);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String imagePath) {
            super.onPostExecute(imagePath);
            progressDialog.dismiss();
            if (imagePath != null) {
                openMail(imagePath, imageUrl);
            }
        }
    }

}