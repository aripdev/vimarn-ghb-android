package com.arip.it.library.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by androiddev on 08/02/2017.
 */

public class ShareLink implements Parcelable{

    public String bookId;
    public String imageUrl;
    public int pageIndex;

    public ShareLink(String bookId, String imageUrl, int pageIndex){
        this.bookId = bookId;
        this.imageUrl = imageUrl;
        this.pageIndex = pageIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bookId);
        dest.writeString(this.imageUrl);
        dest.writeInt(this.pageIndex);
    }

    protected ShareLink(Parcel in) {
        this.bookId = in.readString();
        this.imageUrl = in.readString();
        this.pageIndex = in.readInt();
    }

    public static final Creator<ShareLink> CREATOR = new Creator<ShareLink>() {
        @Override
        public ShareLink createFromParcel(Parcel source) {
            return new ShareLink(source);
        }

        @Override
        public ShareLink[] newArray(int size) {
            return new ShareLink[size];
        }
    };
}
