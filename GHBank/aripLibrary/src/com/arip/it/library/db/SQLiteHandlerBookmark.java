package com.arip.it.library.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHandlerBookmark extends SQLiteOpenHelper{
	private static final String DATABASE_NAME = "bookmark.db";
	private static final int DATABASE_VERSION = 3;
	
	  public static final String TABLE_BOOKMARK = "bookmark";
	  public static final String COLUMN_ID = "_id";
	  public static final String COLUMN_BOOK_ID = "_bookid";
	  public static final String COLUMN_PAGE_NUM = "_pagenum";
	  public static final String COLUMN_TITLE = "_title";
	  public static final String COLUMN_IMG_PATH = "_imagepath";
	  public static final String COLUMN_REVISION = "_revision";
	  
	  private static final String DATABASE_CREATE = "create table "
		      + TABLE_BOOKMARK + "(" + COLUMN_ID + " integer primary key autoincrement, " 
			  + COLUMN_BOOK_ID + " text not null,"
		      + COLUMN_PAGE_NUM + " text not null," 
			  + COLUMN_TITLE + " text not null," 
		      + COLUMN_IMG_PATH + " text not null,"
			  + COLUMN_REVISION  + " text not null" + " );";
	
	public SQLiteHandlerBookmark(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKMARK);
	    onCreate(database);
		
	}

}

