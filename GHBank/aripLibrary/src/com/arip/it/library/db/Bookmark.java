package com.arip.it.library.db;

public class Bookmark {
	  private long id;
	  private String bookid;
	  private String pagenumber;
	  private String title;
	  private String imagepath;
	  private String revision;
	  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getPagenumber() {
		return pagenumber;
	}
	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	  
	  
}
