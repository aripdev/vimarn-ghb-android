package com.arip.it.library.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.androidquery.AQuery;

public class BookmarkHandler {
	@SuppressWarnings("unused")
	private Context mContext;
	@SuppressWarnings("unused")
	private AQuery aq;
	private SQLiteDatabase database;
	private SQLiteHandlerBookmark sqliteHandler;
	private String[] allColumns = { 
			SQLiteHandlerBookmark.COLUMN_BOOK_ID,
			SQLiteHandlerBookmark.COLUMN_PAGE_NUM,
			SQLiteHandlerBookmark.COLUMN_TITLE ,
			SQLiteHandlerBookmark.COLUMN_IMG_PATH ,
			SQLiteHandlerBookmark.COLUMN_REVISION};

	
	@SuppressWarnings("unused")
	private  String SELECT_BOOKMARK  = "SELECT * FROM"+ SQLiteHandlerBookmark.TABLE_BOOKMARK +"WHERE " +
			 SQLiteHandlerBookmark.COLUMN_BOOK_ID +"= ? AND" + SQLiteHandlerBookmark.COLUMN_PAGE_NUM+"NOT IN (?)";
	
	public BookmarkHandler(Context context){
		sqliteHandler = new SQLiteHandlerBookmark(context);
		aq = new AQuery(context);
		mContext = context;
	}
	public boolean isOpen(){
		boolean result = false;
			result = sqliteHandler.getWritableDatabase().isOpen();
		return result;
	}
	
	public void open(){
		database = sqliteHandler.getWritableDatabase();
	}
	
	public void close() {
		sqliteHandler.close();
	}
	
	public List<Bookmark> getAllBookmark(String bookid) {
	    List<Bookmark> bookmarker = new ArrayList<Bookmark>();
	    List<Bookmark> bookmarkerSelect = new ArrayList<Bookmark>();
	    Cursor cursor = database.query(SQLiteHandlerBookmark.TABLE_BOOKMARK,
	        allColumns,null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	Bookmark bookmark = cursorToBookmark(cursor);
	    	bookmarker.add(bookmark);
	    	cursor.moveToNext();
	    }
	    cursor.close();
//	    close();
	    
	    for (int i = 0; i < bookmarker.size(); i++) {
			if (bookmarker.get(i).getBookid().equals(bookid)) {
			
				bookmarkerSelect.add(bookmarkerSelect.size(), bookmarker.get(i));
			}
		}
	    
	  
	    return bookmarkerSelect;
	  }
	
//	public void updateBookmark(String id,String revision){
//		ContentValues args = new ContentValues();
//		args.put(SQLiteHandler.COLUMN_REVISION, revision);
//		database.update(SQLiteHandler.TABLE_BOOK, args, SQLiteHandler.COLUMN_ID + "=" + id, null);
//	}
	
//	public List<Book> sort(int type)
//	{
//		List<Book> favorites = new ArrayList<Book>();
//		Cursor cursor = null;
//		switch (type) {
//			case 1:	cursor = database.query(SQLiteHandler.TABLE_BOOK, allColumns, null, null, null, null, SQLiteHandler.COLUMN_TITLE);
//				break;
//	
//			default: cursor = database.query(SQLiteHandler.TABLE_BOOK, allColumns, null, null, null, null, null);
//				break;
//		}
//		cursor.moveToFirst();
//		while (!cursor.isAfterLast()) {
//	    	Book favorite = cursorToFavorite(cursor);
//	    	favorites.add(favorite);
//	    	cursor.moveToNext();
//	    }
//	    cursor.close();
//	    close();
//	    return favorites;
//	}
	
	public Boolean check(String book_id, String page_number){
		boolean statBookmark = false;
	    List<Bookmark> bookmarker = new ArrayList<Bookmark>();
	    List<Bookmark> bookmarkerById = new ArrayList<Bookmark>();
//		String[] column = {SQLiteHandlerBookmark.COLUMN_BOOK_ID, SQLiteHandlerBookmark.COLUMN_PAGE_NUM};
	    Cursor cursor = database.query(SQLiteHandlerBookmark.TABLE_BOOKMARK,
	    		allColumns, null, null, null, null, null);
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	Bookmark bookmark = cursorToBookmark(cursor);
	    	bookmarker.add(bookmark);
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
	    if(bookmarker!=null){
		    for (int i = 0; i < bookmarker.size(); i++) {
				if (bookmarker.get(i).getBookid().equals(book_id)) {
				
					bookmarkerById.add(bookmarkerById.size(), bookmarker.get(i));
				}
			}
	    }
	    
	    if(bookmarkerById!=null && bookmarkerById.size()>0){
	    	statBookmark = false;
		    for (int i = 0; i < bookmarkerById.size(); i++) {
		    	if(bookmarkerById.get(i).getPagenumber().equals(page_number)){
		    		statBookmark = true;
		    	}
			}
	    }
	    return statBookmark;
	}
	
//	public String getPDF(String id){
//	    List<Book> favorites = new ArrayList<Book>();
//	    Cursor cursor = database.query(SQLiteHandler.TABLE_BOOK,
//	    		allColumns, null, null, null, null, null);
//	    cursor.moveToFirst();
//	    while (!cursor.isAfterLast()) {
//	    	Book favorite = cursorToFavorite(cursor);
//	    	favorites.add(favorite);
//	    	cursor.moveToNext();
//	    }
//	    cursor.close();
//	    
//	    for (int i = 0; i < favorites.size(); i++) {
//			if (favorites.get(i).getId().equals(id)) {
//				return favorites.get(i).getCategory();			
//			}
//		}
//		return null;
//	}
	
	public void addBookmark(final String book_id, final String page_number,String title, String imagePath, String revision){
		final ContentValues values = new ContentValues();
		values.put(SQLiteHandlerBookmark.COLUMN_BOOK_ID, book_id);
		values.put(SQLiteHandlerBookmark.COLUMN_PAGE_NUM, page_number);
		values.put(SQLiteHandlerBookmark.COLUMN_TITLE, title);
		values.put(SQLiteHandlerBookmark.COLUMN_IMG_PATH, imagePath);
		values.put(SQLiteHandlerBookmark.COLUMN_REVISION, revision);
		
		if( book_id == null || book_id.isEmpty())				Log.e(SQLiteHandlerBookmark.COLUMN_BOOK_ID, book_id);
		if( page_number == null || page_number.isEmpty())					Log.e(SQLiteHandlerBookmark.COLUMN_PAGE_NUM, page_number);
		if( title == null || title.isEmpty())			Log.e(SQLiteHandlerBookmark.COLUMN_TITLE, title);
		if( imagePath == null || imagePath.isEmpty())			Log.e(SQLiteHandlerBookmark.COLUMN_IMG_PATH, imagePath);
		if( revision == null || revision.isEmpty())		Log.e(SQLiteHandlerBookmark.COLUMN_REVISION, revision);

		database.insert(SQLiteHandlerBookmark.TABLE_BOOKMARK, null, values);
		
//		final ProgressDialog pd = ProgressDialog.show(mContext, null, "Saving");
//		File ext = Environment.getExternalStorageDirectory();
//		File target = new File(ext, "Android/data/"+mContext.getPackageName()+"/cache/"+String.valueOf(image.hashCode()));              
//
//		aq.download(image, target, new AjaxCallback<File>(){
//		        
//		        public void callback(String url, File file, AjaxStatus status) {
//		                pd.dismiss();
//		                if(file != null){
//		            			values.put(SQLiteHandler.COLUMN_IMAGE, file.getAbsolutePath());
//		            				database.insert(SQLiteHandler.TABLE_BOOK, null, values);
//			                        Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();         			
//		                }
//		        }
//		        
//		});
	}
	
	public void updateBookmark(String book_id , String page_number, String title){
		ContentValues val = new ContentValues();
		val.put( SQLiteHandlerBookmark.COLUMN_TITLE, title);
		
		  database.update(SQLiteHandlerBookmark.TABLE_BOOKMARK, val, SQLiteHandlerBookmark.COLUMN_BOOK_ID
			        + " = " + book_id +" AND "+SQLiteHandlerBookmark.COLUMN_PAGE_NUM
			        + " = " + page_number, null);
		  }
	
	public void deleteBookmark(String book_id , String page_number) {
	    //System.out.println("Comment deleted with id: " + id);
	    database.delete(SQLiteHandlerBookmark.TABLE_BOOKMARK, SQLiteHandlerBookmark.COLUMN_BOOK_ID
	        + " = " + book_id +" AND "+SQLiteHandlerBookmark.COLUMN_PAGE_NUM
	        + " = " + page_number, null);
	  }
	
	@SuppressWarnings("unused")
	private Bookmark cursorToBookmarkCheck(Cursor cursor){
	    Bookmark bookmarker = new Bookmark();
	    bookmarker.setPagenumber(cursor.getString(0));
	    return bookmarker;
	}
	
	
	 private Bookmark cursorToBookmark(Cursor cursor) {
		 	Bookmark bookmarker = new Bookmark();
		 	bookmarker.setBookid(cursor.getString(0));
		    bookmarker.setPagenumber(cursor.getString(1));
		    bookmarker.setTitle(cursor.getString(2));
		    bookmarker.setImagepath(cursor.getString(3));
		    bookmarker.setRevision(cursor.getString(4));
		    return bookmarker;
		  }
}

