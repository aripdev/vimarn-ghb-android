package com.arip.it.library;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by werawanp on 04/03/2016.
 */
public class ExamChoice2 extends FragmentActivity {

    String choice2Path;
    String choice2Total;
    String choice2Id;
    String choice2Type;
    String choice2Sec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_exam_choice);
    }
}
/*
        Bundle bundle = getIntent().getExtras();
        choice2Path = bundle.getString("path");
        choice2Total = bundle.getString("total");
        choice2Id = bundle.getString("id");
        choice2Type = bundle.getString("type");
        choice2Sec = bundle.getString("section");

        String S_path = choice2Path + "choice/" + choice2Id + ".txt";
        final String S_section = choice2Path + "exam.txt";

        ArrayList<String> y = new ArrayList<String>();
        String line = null;

        try {
            FileReader ex = new FileReader(S_path);
            BufferedReader examR = new BufferedReader(ex);

            while ((line = examR.readLine()) != null) {
                if (line.length() > 0) {
                    y.add(line);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (y.size() == 9) {

            TextView txt1 = (TextView) findViewById(R.id.only_question);
            txt1.setText(y.get(0));

            RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
            radioButton1.setText(y.get(5));

            RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
            radioButton2.setText(y.get(8));
            RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
            radioButton3.setVisibility(View.GONE);

            RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
            radioButton4.setVisibility(View.GONE);

        }
        else if (y.size() > 10) {

            TextView txt1 = (TextView) findViewById(R.id.only_question);
            txt1.setText(y.get(0));

            RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
            radioButton1.setText(y.get(5));

            RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
            radioButton2.setText(y.get(8));

            RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
            radioButton3.setText(y.get(11));

            RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
            radioButton4.setText(y.get(14));
        }

        else {

            TextView txt1 = (TextView) findViewById(R.id.only_question);
            txt1.setText(y.get(0));

            RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
            radioButton1.setText(y.get(5));

            RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
            radioButton2.setText(y.get(8));

            RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
            radioButton3.setText(y.get(11));

            RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
            radioButton4.setText(y.get(14));
        }
        Button btnAns = (Button) findViewById(R.id.choice_btn_answer);
        btnAns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findSection();
            }

            private void findSection() {

                String xLine = "";
                ArrayList<String> a = new ArrayList<String>();

                try {
                    FileReader ay = new FileReader(S_section);
                    BufferedReader examR = new BufferedReader(ay);

                    while ((xLine = examR.readLine()) != null) {
                        if (xLine.length() > 0) {
                            a.add(xLine);
                        }
                    }

                    examR.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Intent examChoice2;

                if (a.get(8).equalsIgnoreCase("choice")) examChoice2 = new Intent(ExamChoice2.this, ExamChoice.class);
                else if (a.get(8).equalsIgnoreCase("match")) examChoice2 = new Intent(ExamChoice2.this, ExamMatch.class);
                else if (a.get(8).equalsIgnoreCase("fill")) examChoice2 = new Intent(ExamChoice2.this, ExamFill.class);
                else if (a.get(8).equalsIgnoreCase("sort")) examChoice2 = new Intent(ExamChoice2.this, ExamSort.class);
                else examChoice2 = new Intent(ExamChoice2.this, ExamSound.class);

                examChoice2.putExtra("id", a.get(9));
                examChoice2.putExtra("path", choice2Path);
                examChoice2.putExtra("total", a.get(0));
                examChoice2.putExtra("section", a.get(7));
                examChoice2.putExtra("type", a.get(8));

                startActivity(examChoice2);

            }
        });
    }


}
*/
