package com.arip.it.library;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.arip.it.library.base.BaseActivity;
import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.PDFPreviewGalleryAdapter;


public class GalleryActivity extends BaseActivity{
	
	private GridView grdView;
	private PDFPreviewGalleryAdapter pdfPreviewPagerAdapter;
	private MuPDFCore core;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery);
		grdView = (GridView)findViewById(R.id.gridview);
		Bundle bn = new Bundle();
		
		try{
			if(getIntent().getExtras()!=null){
				bn = getIntent().getExtras();
				core = (MuPDFCore)bn.getSerializable("bundlecore");
				
				
				pdfPreviewPagerAdapter = new PDFPreviewGalleryAdapter(this, core);
				grdView.setAdapter(pdfPreviewPagerAdapter);
			}
			
			grdView.setOnItemClickListener(new OnItemClickListener() {
	
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
					// TODO Auto-generated method stub
					setResult(position);
					finish();
				}
			});
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		setResult(-1);
		
		super.onBackPressed();
		
	}

}
