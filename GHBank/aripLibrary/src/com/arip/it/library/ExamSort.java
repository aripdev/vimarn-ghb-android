package com.arip.it.library;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExamSort extends Activity implements OnGestureListener {

	GestureDetector gestureDetector;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;

	String sortPath;
	String sortId;
	String sortTotal;
	String sortSection;
	String sortType;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_sort);

		gestureDetector = new GestureDetector(this, this);

		Bundle sort = getIntent().getExtras();
		sortPath = sort.getString("path");
		sortId = sort.getString("id");
		sortTotal = sort.getString("total");
		sortSection = sort.getString("section");
		sortType = sort.getString("type");

		String S_path = sortPath + "sort/" + sortId + ".txt";
		final String S_section = sortPath + "exam.txt";

		//drag
		TextView sort1 = (TextView) findViewById(R.id.sort_quest_1);
		sort1.setOnTouchListener(new ChoiceTouchListener());

		//drop
		TextView sort2 = (TextView) findViewById(R.id.sort_drag_1);
		sort2.setOnDragListener(new ChoiceDragListener());



		final ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((line = examR.readLine()) != null) {
				if (line.length() > 0) {
					y.add(line);
				}
			}

			for (int i = 0; i < y.size(); i++)
				Log.d("TAG", "READ in Sort : " + y.get(i));


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		TextView txt1 = (TextView) findViewById(R.id.sort_quest_1);
		txt1.setText(y.get(2));

		TextView txt2 = (TextView) findViewById(R.id.sort_quest_2);
		txt2.setText(y.get(5));

		TextView txt3 = (TextView) findViewById(R.id.sort_quest_3);
		txt3.setText(y.get(8));

		RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.realative_sort_ans);
		relativeLayout1.setVisibility(View.GONE);


		ImageView sortAns = (ImageView) findViewById(R.id.sort_btn_answer);
		sortAns.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.realative_sort_ans);
				relativeLayout.setVisibility(View.VISIBLE);

				TextView ans1 = (TextView) findViewById(R.id.sort_show_ans_1);
				ans1.setText(y.get(11));

				TextView ans2 = (TextView) findViewById(R.id.sort_show_ans_2);
				ans2.setText(y.get(14));

				TextView ans3 = (TextView) findViewById(R.id.sort_show_ans_3);
				ans3.setText(y.get(17));
			}
		});

	}
/*
	ImageView img1 = (ImageView) findViewById(R.id.sort_quest_3);
	img1.setOnLongClickListener(myOnLongClickListener);
	area1.addView(img1);

	@Override
	public boolean onLongClick(View v){
		ClipData data = ClipData.newPlainText("", "");
		View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
		v.startDrag(data, shadowBuilder, v, 0);

		return true;
	}
	*/


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gestureDetector.onTouchEvent(event))
			return true;

		else
			return false;

	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		// TODO Auto-generated method stub

		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				ArrayList<String> a = new ArrayList<>();
				String intLine = null;

				try {
					FileReader ex = new FileReader(sortPath + "exam.txt");
					BufferedReader examR = new BufferedReader(ex);

					while ((intLine = examR.readLine()) != null){
						if (intLine.length() > 0){
							a.add(intLine);
						}

					}

					for (int i = 0; i < a.size(); i++)
						Log.d("TAG", "READ in Sort : " + a.get(i));

					examR.close();
				}catch (FileNotFoundException e){
					e.printStackTrace();
				}catch (IOException e){
					e.printStackTrace();
				}

				Intent examSort;
				if (a.get(14).equalsIgnoreCase("choice")) examSort = new Intent(ExamSort.this, ExamChoice2.class);
				else if (a.get(14).equalsIgnoreCase("match")) examSort = new Intent(ExamSort.this, ExamMatch.class);
				else if (a.get(14).equalsIgnoreCase("fill")) examSort = new Intent(ExamSort.this, ExamFill.class);
				else if (a.get(14).equalsIgnoreCase("sort")) examSort = new Intent(ExamSort.this, ExamSort.class);
				else examSort = new Intent(ExamSort.this, ExamSound.class);

				examSort.putExtra("path", sortPath);
				examSort.putExtra("total", a.get(0));
				examSort.putExtra("section", a.get(13));
				examSort.putExtra("type", a.get(14));
				examSort.putExtra("id", a.get(15));

				startActivity(examSort);



				//Toast.makeText(ExamSort.this, "<---- Left Swipe", Toast.LENGTH_SHORT).show();

			}  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				Intent intent = new Intent(ExamSort.this, ExamFill.class);

				startActivity(intent);

				//Toast.makeText(ExamSort.this, "----> Right Swipe", Toast.LENGTH_SHORT).show();

			}

		} catch (Exception e) {
			// nothing
		}

		return true;
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onDown", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		Toast.makeText(ExamSort.this, "onLongPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onScroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onShowPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onSingleTapUp", Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lay_exam_choice, menu);
		return true;
	}

	private class ChoiceTouchListener implements View.OnTouchListener {

		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){

				//drag
				ClipData data = ClipData.newPlainText("", "");
				View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

				//drag touch
				view.startDrag(data, shadowBuilder, view, 0);

				return true;
			}

			else {
				return false;
			}
		}
	}

	private class ChoiceDragListener implements View.OnDragListener {

		@Override
		public boolean onDrag(View v, DragEvent dragEvent) {
			switch (dragEvent.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					//no action necessary
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					//no action necessary
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					//no action necessary
					break;
				case DragEvent.ACTION_DROP:
					//handle the dragged view being dropped over a drop view
					View view = (View) dragEvent.getLocalState();
					//stop displaying the view where it was before it was dragged
					view.setVisibility(View.INVISIBLE);
					//view dragged item is being dropped on
					TextView dropTarget = (TextView) v;
					//view being dragged and dropped
					TextView dropped = (TextView) view;
					//update the text in the target view to reflect the data being dropped
					dropTarget.setText(dropped.getText());
					//make it bold to highlight the fact that an item has been dropped
					dropTarget.setTypeface(Typeface.DEFAULT_BOLD);
					//if an item has already been dropped here, there will be a tag
					Object tag = dropTarget.getTag();
					//if there is already an item here, set it back visible in its original place
					if (tag != null) {
						//the tag is the view id already dropped here
						int existingID = (Integer) tag;
						//set the original view visible again
						findViewById(existingID).setVisibility(View.VISIBLE);
					}
					//set the tag in the target view being dropped on - to the ID of the view being dropped
					dropTarget.setTag(dropped.getId());

					break;
				case DragEvent.ACTION_DRAG_ENDED:
					//no action necessary
					break;
				default:
					break;
			}
			return true;
		}
	}
}
/*
		Path = getIntent().getExtras().getString("path");
		Total = getIntent().getExtras().getString("total");
		Section = getIntent().getExtras().getString("section");
		Type = getIntent().getExtras().getString("type");
		Id = getIntent().getExtras().getString("id");
		Sign = getIntent().getExtras().getString("sign");
		
		ExamTotal = Integer.parseInt(Total.toString());
		
		Log.e(TAG,"path:"+Path);
		Log.e(TAG, "Total:"+Total+" section:"+Section+" ID:"+Type+" type:"+Id);
		
		if(Sign.equalsIgnoreCase("+")){
			overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
		}
		else overridePendingTransition(R.anim.slide_right_in, R.anim.hold);
		setContentView(R.layout.lay_exam_sort);
		
	}
	
	public int sectionNow(){
		String s = Section;
		Log.d(TAG, "sectionNow : "+s);
		s = replaceAll(s,"section","");
		Log.d(TAG, "sectionNow : "+s);
		int sect = Integer.parseInt(s.toString());
		return sect;
	}
	
	public void goIncrease(){
		
		Log.d(TAG, "goIncrease");
		
		int sect = sectionNow()+1;
		
		
		if(sect>=ExamTotal){
			this.finish();
		}
		else{
			
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
	    	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
	    	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
	    	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
	    	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
	    	intent.putExtra("total", Total);
	    	intent.putExtra("section", target);
	    	intent.putExtra("type", typeNext);
	    	intent.putExtra("id", idNext);
	    	intent.putExtra("sign","+");
	    	
	    	Log.e(TAG, "+++Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
	    	
	    	this.finish();
	    	this.startActivity(intent);
			
			
			
			
		}
		
		
		
		
	}
	
	public void goDecrease(){
		
		Log.d(TAG, "goDecrease");
		
		int sect = sectionNow()-1;
		
		
		if(sect<0){
			this.finish();
		}
		else{
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
        	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
        	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
        	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
        	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
        	intent.putExtra("total", Total);
        	intent.putExtra("section", target);
        	intent.putExtra("type", typeNext);
        	intent.putExtra("id", idNext);
        	intent.putExtra("sign","-");
        	
        	Log.e(TAG, "---Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
        	
        	this.finish();
        	this.startActivity(intent);
			
			
			
		}
		
		
		
	}
	
	public void findSection(int sect){
		
		target="section"+sect;
		
    	try{
			
			FileReader ex = new FileReader(Path+"exam.txt");
			BufferedReader examR = new BufferedReader(ex);
			
			String line = "";
			while ((line = examR.readLine()) != null) {
			    // do something with the line you just read, e.g.
				Log.d(TAG,"temp : "+line);
				if(line.equalsIgnoreCase(target)){
	     			typeNext = String.valueOf( examR.readLine() );	
	     			idNext = String.valueOf( examR.readLine() );
	     			Log.d(TAG,"typeNext : "+typeNext);
	     			Log.d(TAG,"idNext : "+idNext);
	     			break;
	     		}
			   
			}
			
	     	
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
	}
	
	public boolean onTouchEvent(MotionEvent touchevent) 
    {
		
		
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		//Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();  // deprecated
		//int height = display.getHeight();  // deprecated
		
		
		float sensitive=width/2;
		
    	switch (touchevent.getAction())
    	{
    		// when user first touches the screen we get x and y coordinate
    		case MotionEvent.ACTION_DOWN: 
    		{
    			x1 = touchevent.getX();
    			
    			break;
    		}
    		case MotionEvent.ACTION_UP: 
    		{
    			x2 = touchevent.getX();
    			
    			//if left to right sweep event on screen
                if (x1 < x2 && (x2-x1)>=sensitive) 
               	{
                	Log.d("puy","L - R");
                	
                	goDecrease();
                	
                	
                	
                }
                                         
                // if right to left sweep event on screen
                if (x1 > x2 && (x1-x2)>=sensitive)
                {
                	Log.d("puy","R - L");
                	
                	goIncrease();
                }
                
                break;
    		}
    	}
    	return false;
	}
	
	 private String replaceAll(String source, String pattern, String replacement) {
	        if (source == null) {
	            return "";
	        }
	        StringBuffer sb = new StringBuffer();
	        int index;
	        int patIndex = 0;
	        while ((index = source.indexOf(pattern, patIndex)) != -1) {
	            sb.append(source.substring(patIndex, index));
	            sb.append(replacement);
	            patIndex = index + pattern.length();
	        }
	        sb.append(source.substring(patIndex));
	        return sb.toString();
	    }
	
	
}
*/