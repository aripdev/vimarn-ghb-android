package com.arip.it.library;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.arip.it.library.base.BaseActivity;


public class WebActivity extends BaseActivity{
	
    private static final String TAG = "WebActivity";
	public String Link;
	public ProgressDialog progressDialog;
    private boolean isLoadFinished = false;
	private WebView WebViw;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up, R.anim.hold);
		setContentView(R.layout.lay_web);
		
		Link = getIntent().getExtras().getString("uri");
		
		Log.d(TAG, "link : "+Link);
		final Activity activity = this;

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("please wait...");
        progressDialog.show();

		this.WebViw = (WebView) findViewById(R.id.webview);
		WebViw.getSettings().setJavaScriptEnabled(true);
        WebViw.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		WebViw.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {              
                view.loadUrl(url);
                return true;
            }

            public void onLoadResource (WebView view, String url) {
//                if (progressDialog == null && !isLoadFinished) {
//                    progressDialog = new ProgressDialog(activity);
//                    progressDialog.setMessage("please wait...");
//                    progressDialog.show();
//                }
            }
            public void onPageFinished(WebView view, String url) {
                if(progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                        isLoadFinished = true;
                    }
                }
            }
        }); 
		WebViw.loadUrl(Link);
	}

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (this.WebViw != null)
            this.WebViw.destroy();
        super.onDestroy();
    }

    /*
	
    private class SafeWebView extends WebViewClient{
    	
    	
    	
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
			return true;
		}
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			
			super.onPageStarted(view, url, favicon);
				//pd = ProgressDialog.show(view.getContext(), null, "Loading", true, true);

		//	if(pd.isShowing()!=true)
			pd = ProgressDialog.show(view.getContext(), null, "Loading", true, true);
			
			
			//pd.dismiss();
			

				
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			
			super.onPageFinished(view, url);
			
			//while(pd.isShowing())pd.dismiss();
			while(pd.isShowing()==true)pd.dismiss();
			
		}		
		
			
	}
    
    @Override
    protected void onPause() {
    	super.onPause();
        overridePendingTransition(R.anim.hold, R.anim.slide_down);
    }*/
    
   
 

}
