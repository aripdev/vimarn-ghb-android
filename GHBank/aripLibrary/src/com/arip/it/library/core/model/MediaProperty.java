package com.arip.it.library.core.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by androiddev on 20/01/2017.
 */

public class MediaProperty implements Parcelable{

    public String playIconUrl;
    public String thumbnailUrl;
    public boolean areaVisible;
    public boolean thumbnailVisible;
    public boolean iconVisible;
    public boolean autoPlay;

    public MediaProperty(){
        areaVisible = false;
        thumbnailVisible = false;
        iconVisible = false;
        playIconUrl = "";
        thumbnailUrl = "";
        autoPlay = false;
    }

    public MediaProperty(boolean areaVisible, boolean thumbnailVisible, boolean iconVisible,
                         boolean autoPlay, String playIconUrl, String thumbnailUrl){
        this.areaVisible = areaVisible;
        this.thumbnailVisible = thumbnailVisible;
        this.iconVisible = iconVisible;
        this.playIconUrl = playIconUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.autoPlay = autoPlay;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(playIconUrl);
        dest.writeString(thumbnailUrl);
        dest.writeByte(areaVisible ? (byte) 1 : (byte) 0);
        dest.writeByte(thumbnailVisible ? (byte) 1 : (byte) 0);
        dest.writeByte(iconVisible ? (byte) 1 : (byte) 0);
        dest.writeByte(autoPlay ? (byte) 1 : (byte) 0);
    }

    protected MediaProperty(Parcel in) {
        playIconUrl = in.readString();
        thumbnailUrl = in.readString();
        areaVisible = in.readByte() != 0;
        thumbnailVisible = in.readByte() != 0;
        iconVisible = in.readByte() != 0;
        autoPlay = in.readByte() != 0;
    }

    public static final Creator<MediaProperty> CREATOR = new Creator<MediaProperty>() {
        @Override
        public MediaProperty createFromParcel(Parcel source) {
            return new MediaProperty(source);
        }

        @Override
        public MediaProperty[] newArray(int size) {
            return new MediaProperty[size];
        }
    };
}
