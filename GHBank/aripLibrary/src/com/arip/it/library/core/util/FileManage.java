package com.arip.it.library.core.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by androiddev on 09/02/2017.
 */

public class FileManage {

    private static final String TAG = FileManage.class.getSimpleName();

    public static void createDirIfNotExists(String path) {
        File file = new File(path);
        Log.d(TAG,path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e(TAG, "Problem creating  folder");
            }else {
                Log.d(TAG,"This folder create completed.");
            }
        }else {
            Log.w(TAG,"This folder is exists");
        }
    }

    public static String  saveImageFile(String path, String fileName, Bitmap bitmap) {
        String filePath = null;
        if(bitmap == null)
            return null;
        try {
            createDirIfNotExists(path);
            File file = new File(path, fileName);
            if(file.exists()){
                file.delete();
            }
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            filePath = file.getAbsolutePath();
            fOut.flush();
            fOut.close();
            Log.i(TAG,"Save file completed.");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filePath;
    }

    public static Bitmap loadImageFromPath(String path) {
        File imgFile = new  File(path);
        if(imgFile.exists()){
            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return null;
    }

    public static Bitmap getBitmapFromURL(String requestUrl) {
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
