package com.arip.it.library.core.model;

/**
 * Created by narztiizzer on 12/6/2016 AD.
 */

public class SlideItem {

    private String page_id;
    private String insert_id;
    private String path;
    private String name;

    public SlideItem() {

    }

    public SlideItem(String page_id,String insert_id, String path,String name) {
        this.page_id = page_id;
        this.insert_id = insert_id;
        this.path = path;
        this.name = name;
    }

    public String getPage_id() {
        return page_id;
    }

    public void setPage_id(String page_id) {
        this.page_id = page_id;
    }

    public String getInsert_id() {
        return insert_id;
    }

    public void setInsert_id(String insert_id) {
        this.insert_id = insert_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}