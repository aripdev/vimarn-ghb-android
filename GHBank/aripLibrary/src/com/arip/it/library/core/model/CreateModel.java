package com.arip.it.library.core.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by androiddev on 25/01/2017.
 */

public class CreateModel {

    private static final String TAG = CreateModel.class.getSimpleName();

    public static List<Category> categories(String jsonString) {
        if (jsonString == null) {
            return null;
        }

        try {
            JSONObject object = new JSONObject(jsonString);
            if (object.getString("status").equals("1")) {
                JSONArray dataArray = object.getJSONArray("data");
                int size = dataArray.length();
                if (size > 0) {

                    List<Category> root = new ArrayList<>();
                    for (int i = 0; i < size; i++) {

                        // Level 1
                        JSONObject rootObject = dataArray.getJSONObject(i);
                        String id = rootObject.getString("id");
                        String name = rootObject.getString("name");
                        String icon = rootObject.getString("image");

                        // Level 2 and 3
                        List<Category> parent = null;
                        if (!rootObject.isNull("subcategory")) {
                            parent = getSubCategory(rootObject.getJSONArray("subcategory"));
                        }
                        root.add(new Category(id,name,icon,parent));
                    }
                    return root;
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "categories : create category model error.");
            e.printStackTrace();
        }

        return null;
    }

    private static List<Category> getSubCategory(JSONArray jsonArray) {

        try {
            int size = jsonArray.length();

            if (size > 0) {
                // Level 2
                List<Category> list = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    String id = object.getString("id");
                    String name = object.getString("name");
                    String icon = object.getString("image");

                    // Level 3...N
                    List<Category> child = null;
                    if(!object.isNull("subcategory")){
                        child = getSubCategory(object.getJSONArray("subcategory"));
                    }

                    list.add(new Category(id,name,icon,child));
                }
                return list;
            }
        } catch (JSONException e) {
            Log.e(TAG,"getSubCategory : get sub category error.");
            e.printStackTrace();
        }


        return null;
    }
}
