package com.arip.it.library.core.model;

import java.util.ArrayList;

public class ContentItem {
	private String description;
	private int jumpto;
	private static ArrayList<ContentItem> dataContent = new ArrayList<ContentItem>();
	
	public ContentItem(String description, int jumpto){
		this.description = description;
		this.jumpto = jumpto;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getJumpto() {
		return jumpto;
	}

	public void setJumpto(int jumpto) {
		this.jumpto = jumpto;
	}

	public static ArrayList<ContentItem> getDataContent() {
		return dataContent;
	}

	public static void setDataContent(ArrayList<ContentItem> dataContent) {
		ContentItem.dataContent = dataContent;
	}

}
