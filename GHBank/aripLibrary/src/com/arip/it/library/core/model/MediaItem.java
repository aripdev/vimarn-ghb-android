package com.arip.it.library.core.model;

import java.util.ArrayList;

public class MediaItem {

	private String pageId;
	private int insert_id;
	private int codinateX;
	private int codinateY;
	private int codinateWidth;
	private int codinateHight;
	private String pathMedia;
	private String nameMedia;
	private String pathMediaImagePlay;
	private String pathMediaImagePause;
	private String pdfType;
	private String pdfEffect;
	private String pdfOption;
	private String pdfTypeGalleryCoun;
	private ArrayList<SlideItem> slideRaw;
	private int check;
	private boolean isShowIcon;
	private MediaProperty property;

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, String pdfTypeGalleryCoun,
					 String tColor,String bColor,String image,String content,MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		//Log.d("puy","PDF Option : "+pdfOption);
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
		this.property = property;


//		this.tColor=tColor;
//		this.bColor=bColor;
//		this.image=image;
//		if(image!="")this.pathMedia=image;
//		this.content=content;
	}

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, String pdfTypeGalleryCoun,MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
		this.property = property;
	}

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, boolean isShowIcon,MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		this.isShowIcon = isShowIcon;
		this.property = property;
	}

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, String pdfTypeGalleryCoun, int check,MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
		this.check = check;
		this.property = property;
	}

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, String pdfTypeGalleryCoun,ArrayList<SlideItem> slide,
					 MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
		this.slideRaw = slide;
		this.check = check;
		this.property = property;
	}

	public MediaItem(String pageId, int codinateX, int codinateY, int codinateWidth, int codinateHight,
					 String pathMedia, String nameMedia, String pathMediaImagePlay, String pathMediaImagePause,
					 String pdfType, String pdfEffect, String pdfOption, String pdfTypeGalleryCoun,ArrayList<SlideItem> slide,int insert_id,
					 MediaProperty property){
		this.pageId = pageId;
		this.codinateX = codinateX;
		this.codinateY = codinateY;
		this.codinateWidth = codinateWidth;
		this.codinateHight = codinateHight;
		this.pathMedia = pathMedia;
		this.nameMedia = nameMedia;
		this.pathMediaImagePlay = pathMediaImagePlay;
		this.pathMediaImagePause = pathMediaImagePause;
		this.pdfType = pdfType;
		this.pdfEffect = pdfEffect;
		this.pdfOption = pdfOption;
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
		this.slideRaw = slide;
		this.check = check;
		this.insert_id = insert_id;
		this.property = property;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public int getCodinateX() {
		return codinateX;
	}

	public void setCodinateX(int codinateX) {
		this.codinateX = codinateX;
	}

	public int getCodinateY() {
		return codinateY;
	}

	public void setCodinateY(int codinateY) {
		this.codinateY = codinateY;
	}

	public int getCodinateWidth() {
		return codinateWidth;
	}

	public void setCodinateWidth(int codinateWidth) {
		this.codinateWidth = codinateWidth;
	}

	public int getCodinateHight() {
		return codinateHight;
	}

	public void setCodinateHight(int codinateHight) {
		this.codinateHight = codinateHight;
	}

	public String getPathMedia() {
		return pathMedia;
	}

	public void setPathMedia(String pathMedia) {
		this.pathMedia = pathMedia;
	}

	public String getNameMedia() {
		return nameMedia;
	}

	public void setNameMedia(String nameMedia) {
		this.nameMedia = nameMedia;
	}

	public String getPathMediaImagePlay() {
		return pathMediaImagePlay;
	}

	public void setPathMediaImagePlay(String pathMediaImagePlay) {
		this.pathMediaImagePlay = pathMediaImagePlay;
	}

	public String getPathMediaImagePause() {
		return pathMediaImagePause;
	}

	public void setPathMediaImagePause(String pathMediaImagePause) {
		this.pathMediaImagePause = pathMediaImagePause;
	}


	public String getPdfType() {
		return pdfType;
	}


	public void setPdfType(String pdfType) {
		this.pdfType = pdfType;
	}


	public String getPdfEffect() {
		return pdfEffect;
	}


	public void setPdfEffect(String pdfEffect) {
		this.pdfEffect = pdfEffect;
	}


	public String getPdfOption() {
		return pdfOption;
	}


	public void setPdfOption(String pdfOption) {
		this.pdfOption = pdfOption;
	}


	public String getPdfTypeGalleryCount() {
		return pdfTypeGalleryCoun;
	}


	public void setPdfTypeGalleryCoun(String pdfTypeGalleryCoun) {
		this.pdfTypeGalleryCoun = pdfTypeGalleryCoun;
	}


	public ArrayList<SlideItem> getSlideRaw() {
		return slideRaw;
	}


	public void setSlideRaw(ArrayList<SlideItem> slideRaw) {
		this.slideRaw = slideRaw;
	}

	public int getInsert_id() {
		return insert_id;
	}

	public void setInsert_id(int insert_id) {
		this.insert_id = insert_id;
	}

	public int getCheck() {
		return check;
	}

	public void setCheck(int check) {
		this.check = check;
	}

	public boolean isShowIcon() {
		return isShowIcon;
	}

	public void setShowIcon(boolean showIcon) {
		isShowIcon = showIcon;
	}

	public void setProperty(MediaProperty property){
		this.property = property;
	}
	public MediaProperty getProperty(){
		return property;
	}
}

