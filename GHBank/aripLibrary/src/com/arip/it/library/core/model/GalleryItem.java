package com.arip.it.library.core.model;

public class GalleryItem {

	private String pageid;
	private String title;
	private String path;
	private String name;
	private int check;
	private int group;
	private int number;

	public GalleryItem(){

	}

	public GalleryItem(String pageid,String insert,String title, String path, String name){
		this.pageid = pageid;
		this.title = title;
		this.path = path;
		this.name = name;
	}

	public GalleryItem(String pageid,String title, String path, String name,int check){
		this.pageid = pageid;
		this.title = title;
		this.path = path;
		this.name = name;
		this.check = check;
	}

	public GalleryItem(String pageid,String title, String path, String name){
		this.pageid = pageid;
		this.title = title;
		this.path = path;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPageid() {
		return pageid;
	}

	public void setPageid(String pageid) {
		this.pageid = pageid;
	}

	public int getCheck() {
		return check;
	}

	public void setCheck(int check) {
		this.check = check;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
