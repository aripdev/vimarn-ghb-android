package com.arip.it.library.core.model;


public class MediaSeekItem {
	private static int position;
	private static boolean isPaused;
	public static int getPosition() {
		return position;
	}
	public static void setPosition(int position) {
		MediaSeekItem.position = position;
	}
	public static boolean isPaused() {
		return isPaused;
	}
	public static void setPaused(boolean isPaused) {
		MediaSeekItem.isPaused = isPaused;
	}
	
	
}
