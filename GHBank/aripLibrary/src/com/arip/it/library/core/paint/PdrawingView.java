package com.arip.it.library.core.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.arip.it.library.LectureActivity;

public class PdrawingView extends View {
	public Bitmap mBitmap;
	private Bitmap mBitmapResource;

	public Canvas currentCanvas;
	public Paint p;

	public int canvasWidth;
	public int canvasHeight;

	private LectureActivity m;

	public PdrawingView(Context context, LectureActivity mainActivity, Bitmap bitmap) {
		super(context);
		this.m = mainActivity;
		mBitmapResource = bitmap;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (mBitmap == null) {
			canvasWidth = canvas.getWidth();
			canvasHeight = canvas.getHeight();
//			mBitmap = Bitmap.createBitmap(canvasWidth, canvasHeight,
//					Bitmap.Config.RGB_565);
			mBitmap = Bitmap.createBitmap(mBitmapResource);

			currentCanvas = new Canvas(mBitmap);
			p = new Paint();
//			p.setColor(m.backgroundColor);
			p.setColor(Color.TRANSPARENT);
			p.setAntiAlias(true);
		}

		currentCanvas.drawPaint(p);
//		currentCanvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
		currentCanvas.setBitmap(mBitmap);
		mBitmapResource.recycle();
		for (int i = 0; i < m.visibleDrawables.size(); i++) {
			m.visibleDrawables.get(i).draw(currentCanvas);
		}
//		canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
		canvas.drawBitmap(mBitmap, 0.0f, 0.0f, null);
	}

	protected Bitmap getBitmap() {
		return mBitmap;
	}

	public void clearImage(){

		if(mBitmap == null)
			mBitmap.recycle();

		mBitmap = Bitmap.createBitmap(mBitmapResource);
	}
}