package com.arip.it.library.core.lib.util;

public class DeviceSize {
	private static int width;
	private static int height;

	public static int getWidth() {
		return width;
	}

	public static void setWidth( int width ) {
		if ( DeviceSize.width == 0 )
			DeviceSize.width = width;
	}

	public static int getHeight() {
		return height;
	}

	public static void setHeight( int height ) {
		if ( DeviceSize.height == 0 )
			DeviceSize.height = height;
	}
}
