package com.arip.it.library.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by androiddev on 25/01/2017.
 */

public class Category implements Parcelable{

    public String categoryId;
    public String categoryName;
    public String categoryIconUrl;
    public List<Category> subCategory;

    public Category(String categoryId,String categoryName, String categoryIconUrl,
                    List<Category> subCategory){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryIconUrl = categoryIconUrl;
        this.subCategory = subCategory;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.categoryId);
        dest.writeString(this.categoryName);
        dest.writeString(this.categoryIconUrl);
        dest.writeTypedList(this.subCategory);
    }

    protected Category(Parcel in) {
        this.categoryId = in.readString();
        this.categoryName = in.readString();
        this.categoryIconUrl = in.readString();
        this.subCategory = in.createTypedArrayList(Category.CREATOR);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
