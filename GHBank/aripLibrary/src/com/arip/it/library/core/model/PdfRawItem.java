package com.arip.it.library.core.model;

import java.util.ArrayList;

public class PdfRawItem {
	private String pdfPageId;
	private String pdfName;
	private String pdfPath;
	private String pdfRootPath;
	private String height;
	private String width;
	private String multiplier;
	private ArrayList<MediaItem> media;

	public PdfRawItem(String pdfPageId, String pdfName, String pdfPath,  String pdfRootPath, String multiplier, ArrayList<MediaItem> media){
		this.pdfPageId = pdfPageId;
		this.pdfName = pdfName;
		this.pdfPath = pdfPath;
		this.pdfRootPath = pdfRootPath;
		this.multiplier = multiplier;
		this.media = media;

	}

	public PdfRawItem(String pdfPageId, String pdfName, String pdfPath,  String pdfRootPath, String multiplier, ArrayList<MediaItem> media , String width , String height){
		this.pdfPageId = pdfPageId;
		this.pdfName = pdfName;
		this.pdfPath = pdfPath;
		this.pdfRootPath = pdfRootPath;
		this.multiplier = multiplier;
		this.media = media;
		this.width = width;
		this.height = height;
	}

	public String getPdfPageId() {
		return pdfPageId;
	}

	public void setPdfPageId(String pdfPageId) {
		this.pdfPageId = pdfPageId;
	}

	public String getPdfName() {
		return pdfName;
	}

	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getPdfRootPath() {
		return pdfRootPath;
	}

	public void setPdfRootPath(String pdfRootPath) {
		this.pdfRootPath = pdfRootPath;
	}

	public String getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(String multiplier) {
		this.multiplier = multiplier;
	}

	public ArrayList<MediaItem> getMedia() {
		return media;
	}

	public void setMedia(ArrayList<MediaItem> media) {
		this.media = media;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}
}
