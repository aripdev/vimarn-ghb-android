package com.arip.it.library;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExamFill extends Activity implements OnGestureListener {

	GestureDetector gd;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;

	String fillPath;
	String fillId;
	String fillSection;
	String fillTotal;
	String fillType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_fill);

		gd = new GestureDetector(this, this);

		Bundle fill = getIntent().getExtras();
		fillPath = fill.getString("path");
		fillId = fill.getString("id");
		fillSection = fill.getString("section");
		fillTotal = fill.getString("total");
		fillType = fill.getString("type");

		String S_path = fillPath + "fill/" + fillId +".txt";
		String S_section = fillPath + "exam.txt";

		final ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((line = examR.readLine()) != null) {
				if (line.length() > 0){
					y.add(line);
				}
			}

			for (int i = 0; i < y.size(); i++)
				Log.d("TAG", "READ in Fill : " + y.get(i));

			examR.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		TextView txt1 = (TextView) findViewById(R.id.fill_quest_1);
		txt1.setText(y.get(4));

		TextView txt2 = (TextView) findViewById(R.id.fill_quest_3);
		txt2.setText(y.get(28));

		final RadioButton radioButton1 = (RadioButton) findViewById(R.id.c1_c1);
		radioButton1.setText(y.get(11));

		final RadioButton radioButton2 = (RadioButton) findViewById(R.id.c1_c2);
		radioButton2.setText(y.get(15));

		final RadioButton radioButton3 = (RadioButton) findViewById(R.id.c1_c3);
		radioButton3.setText(y.get(19));

		final RadioButton radioButton4 = (RadioButton) findViewById(R.id.c1_c4);
		radioButton4.setText(y.get(23));


		Button img = (Button) findViewById(R.id.fill_btn_answer);
		img.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String x = "1";

				ImageView imgFill1 = (ImageView) findViewById(R.id.img_fill_answer);

				if (radioButton1.isChecked() && y.get(12).equals(x)){
					imgFill1.setImageResource(R.drawable.r);
				}

				else if (radioButton2.isChecked() && y.get(16).equals(x)){
					imgFill1.setImageResource(R.drawable.r);
				}

				else if (radioButton3.isChecked() && y.get(20).equals(x)){
					imgFill1.setImageResource(R.drawable.r);
				}

				else if (radioButton4.isChecked() && y.get(24).equals(x)){
					imgFill1.setImageResource(R.drawable.r);
				}

				else {
					imgFill1.setImageResource(R.drawable.w);
				}

			}
		});

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gd.onTouchEvent(event))
			return true;
		else
			return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		// TODO Auto-generated method stub

		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				ArrayList<String> a = new ArrayList<>();
				String intLine = null;

				try {
					FileReader ex = new FileReader(fillPath + "exam.txt");
					BufferedReader examR = new BufferedReader(ex);

					while ((intLine = examR.readLine()) != null){
						if (intLine.length() > 0){
							a.add(intLine);
						}

					}

					examR.close();
				}catch (FileNotFoundException e){
					e.printStackTrace();
				}catch (IOException e){
					e.printStackTrace();
				}

				Intent fillMatch;
				if (a.get(11).equalsIgnoreCase("choice")) fillMatch = new Intent(ExamFill.this, ExamChoice.class);
				else if (a.get(11).equalsIgnoreCase("match")) fillMatch = new Intent(ExamFill.this, ExamMatch.class);
				else if (a.get(11).equalsIgnoreCase("fill")) fillMatch = new Intent(ExamFill.this, ExamFill.class);
				else if (a.get(11).equalsIgnoreCase("sort")) fillMatch = new Intent(ExamFill.this, ExamSort.class);
				else fillMatch = new Intent(ExamFill.this, ExamSound.class); //แก้ type ลองใช้กับ sound

				fillMatch.putExtra("path", fillPath);
				fillMatch.putExtra("total", a.get(0));
				fillMatch.putExtra("section", a.get(10));
				fillMatch.putExtra("type", a.get(11));
				fillMatch.putExtra("id", a.get(12));

				startActivity(fillMatch);




			}  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				onBackPressed();
/*
				Intent intFill = new Intent(ExamFill.this, ExamSound.class);
				startActivity(intFill);
*/
				Toast.makeText(ExamFill.this, "----> Right Swipe", Toast.LENGTH_SHORT).show();

			}

		} catch (Exception e) {
			// nothing
		}

		return true;
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onDown", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		Toast.makeText(ExamFill.this, "onLongPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onScroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onShowPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onSingleTapUp", Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lay_exam_choice, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
	/*
		Path = getIntent().getExtras().getString("path");
		Total = getIntent().getExtras().getString("total");
		Section = getIntent().getExtras().getString("section");
		Type = getIntent().getExtras().getString("type");
		Id = getIntent().getExtras().getString("id");
		Sign = getIntent().getExtras().getString("sign");
		
		ExamTotal = Integer.parseInt(Total.toString());
		
		Log.e(TAG,"path:"+Path);
		Log.e(TAG, "Total:"+Total+" section:"+Section+" ID:"+Type+" type:"+Id);
		
		if(Sign.equalsIgnoreCase("+")){
			overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
		}
		else overridePendingTransition(R.anim.slide_right_in, R.anim.hold);
		setContentView(R.layout.lay_exam_fill);
		
	}
	
	public int sectionNow(){
		String s = Section;
		Log.d(TAG, "sectionNow : "+s);
		s = replaceAll(s,"section","");
		Log.d(TAG, "sectionNow : "+s);
		int sect = Integer.parseInt(s.toString());
		return sect;
	}
	
	public void goIncrease(){
		
		Log.d(TAG, "goIncrease");
		
		int sect = sectionNow()+1;
		
		
		if(sect>=ExamTotal){
			this.finish();
		}
		else{
			
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
	    	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
	    	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
	    	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
	    	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
	    	intent.putExtra("total", Total);
	    	intent.putExtra("section", target);
	    	intent.putExtra("type", typeNext);
	    	intent.putExtra("id", idNext);
	    	intent.putExtra("sign","+");
	    	
	    	Log.e(TAG, "+++Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
	    	
	    	this.finish();
	    	this.startActivity(intent);
			
			
			
			
		}
		
		
		
		
	}
	
	public void goDecrease(){
		
		Log.d(TAG, "goDecrease");
		
		int sect = sectionNow()-1;
		
		
		if(sect<0){
			this.finish();
		}
		else{
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
        	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
        	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
        	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
        	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
        	intent.putExtra("total", Total);
        	intent.putExtra("section", target);
        	intent.putExtra("type", typeNext);
        	intent.putExtra("id", idNext);
        	intent.putExtra("sign","-");
        	
        	Log.e(TAG, "---Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
        	
        	this.finish();
        	this.startActivity(intent);
			
			
			
		}
		
		
		
	}
	
	public void findSection(int sect){
		
		target="section"+sect;
		
    	try{
			
			FileReader ex = new FileReader(Path+"exam.txt");
			BufferedReader examR = new BufferedReader(ex);
			
			String line = "";
			while ((line = examR.readLine()) != null) {
			    // do something with the line you just read, e.g.
				Log.d(TAG,"temp : "+line);
				if(line.equalsIgnoreCase(target)){
	     			typeNext = String.valueOf( examR.readLine() );	
	     			idNext = String.valueOf( examR.readLine() );
	     			Log.d(TAG,"typeNext : "+typeNext);
	     			Log.d(TAG,"idNext : "+idNext);
	     			break;
	     		}
			   
			}
			
	     	
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
	}
	
	public boolean onTouchEvent(MotionEvent touchevent) 
    {
		
		
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		//Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();  // deprecated
		//int height = display.getHeight();  // deprecated
		
		
		float sensitive=width/2;
		
    	switch (touchevent.getAction())
    	{
    		// when user first touches the screen we get x and y coordinate
    		case MotionEvent.ACTION_DOWN: 
    		{
    			x1 = touchevent.getX();
    			
    			break;
    		}
    		case MotionEvent.ACTION_UP: 
    		{
    			x2 = touchevent.getX();
    			
    			//if left to right sweep event on screen
                if (x1 < x2 && (x2-x1)>=sensitive) 
               	{
                	Log.d("puy","L - R");
                	
                	goDecrease();
                	
                	
                	
                }
                                         
                // if right to left sweep event on screen
                if (x1 > x2 && (x1-x2)>=sensitive)
                {
                	Log.d("puy","R - L");
                	
                	goIncrease();
                }
                
                break;
    		}
    	}
    	return false;
	}
	
	 private String replaceAll(String source, String pattern, String replacement) {
	        if (source == null) {
	            return "";
	        }
	        StringBuffer sb = new StringBuffer();
	        int index;
	        int patIndex = 0;
	        while ((index = source.indexOf(pattern, patIndex)) != -1) {
	            sb.append(source.substring(patIndex, index));
	            sb.append(replacement);
	            patIndex = index + pattern.length();
	        }
	        sb.append(source.substring(patIndex));
	        return sb.toString();
	    }
	
}
*/