package com.arip.it.library;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExamMatch extends Activity implements OnGestureListener {

	GestureDetector gd;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;

	String matchPath;
	String matchTotal;
	String matchId;
	String matchType;
	String matchSec;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_match);

		gd = new GestureDetector(this, this);

		Bundle match = getIntent().getExtras();
		matchPath = match.getString("path");
		matchTotal = match.getString("total");
		matchId = match.getString("id");
		matchType = match.getString("type");
		matchSec = match.getString("section");

		String S_path = matchPath + "match/" + matchId + ".txt";
		final String S_section = matchPath + "exam.txt";
		String S_picture = matchPath + "match/" + matchId + "/";

		Log.d("TAG", "S_picture : " + S_picture);

		final ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((line = examR.readLine()) != null) {
				if (line.length() > 0) {
					y.add(line);
				}
			}

			for (int i = 0; i < y.size(); i++)
				Log.d("TAG", "Read in Match : " + y.get(i));

			examR.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.d("TAG", "Path ID : " + S_path);

		final TextView txt1 = (TextView) findViewById(R.id.txt_match_right_data1);
		txt1.setText(y.get(14));

		TextView txt2 = (TextView) findViewById(R.id.txt_match_right_data2);
		txt2.setText(y.get(18));

		Bitmap bmp1 = BitmapFactory.decodeFile(S_picture + y.get(3));
		ImageView img1 = (ImageView) findViewById(R.id.img_match_1);
		img1.setImageBitmap(bmp1);

		Bitmap bmp = BitmapFactory.decodeFile(S_picture + y.get(8));
		ImageView img = (ImageView) findViewById(R.id.img_match_2);
		img.setImageBitmap(bmp);

		final EditText edt1 = (EditText) findViewById(R.id.txt_match_left_1);
		final EditText edt2 = (EditText) findViewById(R.id.txt_match_left_2);



		final ImageView imgMatch1 = (ImageView) findViewById(R.id.img_match_left_1);
		imgMatch1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String answer1 = edt1.getText().toString();

				ImageView imgMatch2 = (ImageView) findViewById(R.id.img_match_r);

				if (answer1.equals(y.get(5))){
					imgMatch2.setImageResource(R.drawable.r);
				}

				else {
					imgMatch2.setImageResource(R.drawable.w);
				}

			}
		});

		ImageView imageMatch3 = (ImageView) findViewById(R.id.img_match_left_2);
		imageMatch3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String answer2 = edt2.getText().toString();

				ImageView imagMatch4 = (ImageView) findViewById(R.id.img_match_w);

				if (answer2.equals(y.get(10))){
					imagMatch4.setImageResource(R.drawable.r);
				}

				else {
					imagMatch4.setImageResource(R.drawable.w);
				}
			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gd.onTouchEvent(event))
			return true;
		else
			return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		// TODO Auto-generated method stub

		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				ArrayList<String> a = new ArrayList<>();
				String intLine = null;

				try {
					FileReader ex = new FileReader(matchPath + "exam.txt");
					BufferedReader examR = new BufferedReader(ex);

					while ((intLine = examR.readLine()) != null){
						if (intLine.length() > 0){
							a.add(intLine);
						}

					}

					examR.close();
				}catch (FileNotFoundException e){
					e.printStackTrace();
				}catch (IOException e){
					e.printStackTrace();
				}

				Intent examMatch;
				if (a.get(8).equalsIgnoreCase("choice")) examMatch = new Intent(ExamMatch.this, ExamChoice.class);
				else if (a.get(8).equalsIgnoreCase("match")) examMatch = new Intent(ExamMatch.this, ExamMatch.class);
				else if (a.get(8).equalsIgnoreCase("fill")) examMatch = new Intent(ExamMatch.this, ExamFill.class);
				else if (a.get(8).equalsIgnoreCase("sort")) examMatch = new Intent(ExamMatch.this, ExamSort.class);
				else examMatch = new Intent(ExamMatch.this, ExamSound.class);

				examMatch.putExtra("path", matchPath);
				examMatch.putExtra("total", a.get(0));
				examMatch.putExtra("section", a.get(7));
				examMatch.putExtra("type", a.get(8));
				examMatch.putExtra("id", a.get(9));

				startActivity(examMatch);




			}  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				onBackPressed();

				Toast.makeText(ExamMatch.this, "----> Right Swipe", Toast.LENGTH_SHORT).show();

			}

		} catch (Exception e) {
			// nothing
		}

		return true;
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onDown", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		Toast.makeText(ExamMatch.this, "onLongPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onScroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onShowPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onSingleTapUp", Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lay_exam_choice, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
/*
		TextView txt3 = (TextView) findViewById(R.id.txt_match_left_1);
		txt3.setText(y.get(13));

		TextView txt4 = (TextView) findViewById(R.id.txt_match_left_2);
		txt4.setText(y.get(17));

		Bitmap bmp1 = BitmapFactory.decodeFile(S_picture + y.get(3));
		ImageView img1 = (ImageView) findViewById(R.id.img_match_1);
		img1.setImageBitmap(bmp1);

		Bitmap bmp = BitmapFactory.decodeFile(S_picture + y.get(8));
		ImageView img = (ImageView) findViewById(R.id.img_match_2);
		img.setImageBitmap(bmp);
	}
}
/*
		Bundle match = getIntent().getExtras();
		matchPath = match.getString("path");
		matchTotal = match.getString("total");
		matchId = match.getString("id");
		matchType = match.getString("type");
		matchSec = match.getString("section");

		String S_path = matchPath + "match/" + matchId + ".txt";
		final String S_section = matchPath + "exam.txt";
		String S_picture = matchPath + "match/" + matchId +"/";

		ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((line = examR.readLine()) != null){
				if (line.length() > 0) {
					y.add(line);
				}
			}

			examR.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.d("TAG", "Path ID : " + S_path);

		TextView txt1 = (TextView) findViewById(R.id.txt_match_right_data1);
		txt1.setText(y.get(14));

		TextView txt2 = (TextView) findViewById(R.id.txt_match_right_data2);
		txt2.setText(y.get(18));
/*
		TextView txt3 = (TextView) findViewById(R.id.txt_match_left_1);
		txt3.setText(y.get(13));

		TextView txt4 = (TextView) findViewById(R.id.txt_match_left_2);
		txt4.setText(y.get(17));

		Bitmap bmp1 = BitmapFactory.decodeFile(S_picture + y.get(3));
		ImageView img1 = (ImageView) findViewById(R.id.img_match_1);
		img1.setImageBitmap(bmp1);

		Bitmap bmp = BitmapFactory.decodeFile(S_picture + y.get(8));
		ImageView img = (ImageView) findViewById(R.id.img_match_2);
		img.setImageBitmap(bmp);

		Log.d("TAG", "IMAGE : " + S_picture + y.get(8));
		Log.d("TAG", "IMAGE : " + S_picture + y.get(3));

		ImageView imgBtn = (ImageView) findViewById(R.id.match_btn_answer);
		imgBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findSection();
			}

			private void findSection() {

				String xLine = null;
				ArrayList<String> a = new ArrayList<String>();

				try {
					FileReader ex = new FileReader(S_section);
					BufferedReader examR = new BufferedReader(ex);

					while ((xLine = examR.readLine()) != null) {

						if (xLine.length() > 0 ){
							a.add(xLine);
						}

					}

					examR.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}



			}
		});




	}





}
		
/*
		Path = getIntent().getExtras().getString("path");
		Total = getIntent().getExtras().getString("total");
		Section = getIntent().getExtras().getString("section");
		Type = getIntent().getExtras().getString("type");
		Id = getIntent().getExtras().getString("id");
		Sign = getIntent().getExtras().getString("sign");
		
		ExamTotal = Integer.parseInt(Total.toString());
		
		Log.e(TAG,"path:"+Path);
		Log.e(TAG, "Total:"+Total+" section:"+Section+" ID:"+Type+" type:"+Id);
		
		if(Sign.equalsIgnoreCase("+")){
			overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
		}
		else overridePendingTransition(R.anim.slide_right_in, R.anim.hold);
		setContentView(R.layout.lay_exam_match);
		
	}
	
	public int sectionNow(){
		String s = Section;
		Log.d(TAG, "sectionNow : "+s);
		s = replaceAll(s,"section","");
		Log.d(TAG, "sectionNow : "+s);
		int sect = Integer.parseInt(s.toString());
		return sect;
	}
	
	public void goIncrease(){
		
		Log.d(TAG, "goIncrease");
		
		int sect = sectionNow()+1;
		
		
		if(sect>=ExamTotal){
			this.finish();
		}
		else{
			
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
	    	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
	    	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
	    	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
	    	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
	    	intent.putExtra("total", Total);
	    	intent.putExtra("section", target);
	    	intent.putExtra("type", typeNext);
	    	intent.putExtra("id", idNext);
	    	intent.putExtra("sign","+");
	    	
	    	Log.e(TAG, "+++Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
	    	
	    	this.finish();
	    	this.startActivity(intent);
			
			
			
			
		}
		
		
		
		
	}
	
	public void goDecrease(){
		
		Log.d(TAG, "goDecrease");
		
		int sect = sectionNow()-1;
		
		
		if(sect<0){
			this.finish();
		}
		else{
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
        	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
        	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
        	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
        	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
        	intent.putExtra("total", Total);
        	intent.putExtra("section", target);
        	intent.putExtra("type", typeNext);
        	intent.putExtra("id", idNext);
        	intent.putExtra("sign","-");
        	
        	Log.e(TAG, "---Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
        	
        	this.finish();
        	this.startActivity(intent);
			
			
			
		}
		
		
		
	}
	
	public void findSection(int sect){
		
		target="section"+sect;
		
    	try{
			
			FileReader ex = new FileReader(Path+"exam.txt");
			BufferedReader examR = new BufferedReader(ex);
			
			String line = "";
			while ((line = examR.readLine()) != null) {
			    // do something with the line you just read, e.g.
				Log.d(TAG,"temp : "+line);
				if(line.equalsIgnoreCase(target)){
	     			typeNext = String.valueOf( examR.readLine() );	
	     			idNext = String.valueOf( examR.readLine() );
	     			Log.d(TAG,"typeNext : "+typeNext);
	     			Log.d(TAG,"idNext : "+idNext);
	     			break;
	     		}
			   
			}
			
	     	
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
	}
	
	public boolean onTouchEvent(MotionEvent touchevent) 
    {
		
		
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		//Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();  // deprecated
		//int height = display.getHeight();  // deprecated
		
		
		float sensitive=width/2;
		
    	switch (touchevent.getAction())
    	{
    		// when user first touches the screen we get x and y coordinate
    		case MotionEvent.ACTION_DOWN: 
    		{
    			x1 = touchevent.getX();
    			
    			break;
    		}
    		case MotionEvent.ACTION_UP: 
    		{
    			x2 = touchevent.getX();
    			
    			//if left to right sweep event on screen
                if (x1 < x2 && (x2-x1)>=sensitive) 
               	{
                	Log.d("puy","L - R");
                	
                	goDecrease();
                	
                	
                	
                }
                                         
                // if right to left sweep event on screen
                if (x1 > x2 && (x1-x2)>=sensitive)
                {
                	Log.d("puy","R - L");
                	
                	goIncrease();
                }
                
                break;
    		}
    	}
    	return false;
	}
	
	 private String replaceAll(String source, String pattern, String replacement) {
	        if (source == null) {
	            return "";
	        }
	        StringBuffer sb = new StringBuffer();
	        int index;
	        int patIndex = 0;
	        while ((index = source.indexOf(pattern, patIndex)) != -1) {
	            sb.append(source.substring(patIndex, index));
	            sb.append(replacement);
	            patIndex = index + pattern.length();
	        }
	        sb.append(source.substring(patIndex));
	        return sb.toString();
	    }
	
	
}
*/