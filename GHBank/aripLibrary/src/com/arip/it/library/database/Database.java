package com.arip.it.library.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.longevitysoft.android.xml.plist.PListXMLHandler.TAG;

/**
 * Created by androiddev on 08/02/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "app.database.book";
    private static final int    DATABASE_VERSION = 1;

    public static final String TB_SHARE_LINK = "tb_share_link";
    public static final String COL_ID = "_id";
    public static final String COL_BOOK_ID = "_book_id";
    public static final String COL_IMAGE_URL = "_image_url";
    public static final String COL_PAGE_INDEX = "_page_index";


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTableShareLink(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAllTableAndNewCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAllTableAndNewCreate(db);
    }

    private void createTableShareLink(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TB_SHARE_LINK + " (" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL_BOOK_ID + " TEXT," +
                COL_IMAGE_URL + " TEXT," +
                COL_PAGE_INDEX + " TEXT" +
                ");");
        Log.d(TAG, "Create table " + TB_SHARE_LINK + " completed.");
    }


    private void dropAllTableAndNewCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TB_SHARE_LINK);
        onCreate(db);
    }

}
