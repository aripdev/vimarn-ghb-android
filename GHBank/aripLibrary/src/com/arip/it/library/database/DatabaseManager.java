package com.arip.it.library.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.arip.it.library.model.ShareLink;

/**
 * Created by androiddev on 08/02/2017.
 */

public class DatabaseManager {

    private static final String TAG = DatabaseManager.class.getSimpleName();
    private Database database;

    public DatabaseManager(Context context) {
        database = new Database(context);
    }

    public boolean addShareLink(ShareLink shareLink) {
        SQLiteDatabase writeDatabase = database.getWritableDatabase();
        boolean result = false;
        try {
            ContentValues values = new ContentValues();
            values.put(Database.COL_BOOK_ID, shareLink.bookId);
            values.put(Database.COL_IMAGE_URL, shareLink.imageUrl);
            values.put(Database.COL_PAGE_INDEX, shareLink.pageIndex);
            writeDatabase.insert(Database.TB_SHARE_LINK, null, values);
            result = true;
        } catch (SQLiteException e) {
            Log.e(TAG, "Add share link to database error");
            e.printStackTrace();
        } finally {
            writeDatabase.close();
        }
        return result;
    }

    public ShareLink getShareLink(String bookId, int pageIndex) {
        SQLiteDatabase readDatabase = database.getReadableDatabase();
        ShareLink shareLink = null;
        try {
            final String sql = "SELECT * FROM " + Database.TB_SHARE_LINK +
                    " WHERE " + Database.COL_PAGE_INDEX + "='" + pageIndex + "'" +
                    " AND " + Database.COL_BOOK_ID + "='" + bookId + "'";
            Cursor cursor = readDatabase.rawQuery(sql, null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    String id = cursor.getString(cursor.getColumnIndex(Database.COL_BOOK_ID));
                    String imageUrl = cursor.getString(cursor.getColumnIndex(Database.COL_IMAGE_URL));
                    int page = cursor.getInt(cursor.getColumnIndex(Database.COL_PAGE_INDEX));
                    shareLink = new ShareLink(id, imageUrl, page);
                }
                cursor.close();
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Get share from database error");
            e.printStackTrace();
        }
        return shareLink;
    }
}
