package com.arip.it.library;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.arip.it.library.db.Bookmark;
import com.arip.it.library.db.BookmarkHandler;
import com.artifex.mupdf.BookmarkAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BookmarkActivity extends FragmentActivity{

	private static BookmarkHandler bookmarkHandler;
	private AQuery aq;
	private BookmarkAdapter bookmarkAdt;
	private ListView lstBookmark;
	private String bookid,pageNumber,imgPath,txtDescription;
	private EditText edtBookmark;
	private TextView btnAdd;
	private TextView btnCancel;
	private ImageView imgBookmark;
	private List<Bookmark> data;
	private String tmpPageNumber;
	private String tmpTitle;
	private LinearLayout lyBookmark;
	public static int positionHighlight = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.bookmark);
		Intent i = getIntent();
		aq = new AQuery(BookmarkActivity.this);

		if(bookmarkHandler == null){
			bookmarkHandler = new BookmarkHandler(aq.getContext());
			bookmarkHandler.open();
		}



		if(i.getStringExtra("bookid")!=null){
			bookid = i.getStringExtra("bookid");
		}

		if(i.getStringExtra("pagenum")!=null){
			pageNumber = i.getStringExtra("pagenum");
			tmpPageNumber = i.getStringExtra("pagenum");
		}else{
			pageNumber = "1";
			tmpPageNumber = "1";
		}

		Initial();

		if(i.getStringExtra("desc")!=null){
			txtDescription = i.getStringExtra("desc");

		}else{
			txtDescription = "";
		}

//	        if(i.getStringExtra("pagenum")!=null){
//	        	pageNumber = i.getStringExtra("pagenum");
//	        	
//	        	if(bookmarkHandler.check(bookid, pageNumber+"")){
//	        		if(data!=null && data.size()>0){
//	        			for(int position = 0;  position< data.size(); position++){
//	        				if(data.get(position).getPagenumber().equals(pageNumber)){
//	        					edtBookmark.setText(data.get(position).getTitle().toString());
//	        				}
//	        			}
//	        		}
////	        		edtBookmark.setText(txtDescription);
//	        	}
//	        }else{
//	        	pageNumber = "1";
//	        }

		if(i.getStringExtra("imgpath")!=null){
			imgPath = i.getStringExtra("imgpath");
		}else{
			imgPath = "";
		}

		lstBookmark.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});


	}

	private void Initial(){
		lyBookmark = (LinearLayout)findViewById(R.id.lay_edit_bookmark);
		lstBookmark = (ListView)findViewById(R.id.lst_bookmark);
		edtBookmark = (EditText)findViewById(R.id.edt_txt_bookmark);
		btnAdd = (TextView)findViewById(R.id.btn_add_bookmark);
		btnCancel = (TextView)findViewById(R.id.btn_cancel_bookmark);
		imgBookmark = (ImageView)findViewById(R.id.img_add_bookmark);


		new LoadSavedBookmark().execute();

		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(bookmarkHandler.check(bookid, pageNumber+"")){
					bookmarkHandler.updateBookmark(bookid, pageNumber, edtBookmark.getText().toString());


				}else{
					bookmarkHandler.addBookmark(bookid, pageNumber, edtBookmark.getText().toString(), imgPath, "1");
				}
				positionHighlight = -1;
				new LoadSavedBookmark().execute();
				lyBookmark.setVisibility(View.GONE);
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(btnAdd.getText().toString().equalsIgnoreCase(getString(R.string.bk_update))){
					pageNumber = tmpPageNumber;
					if(!bookmarkHandler.check(bookid, pageNumber+"")){
						btnAdd.setText(getString(R.string.bk_add));
					}else{
						edtBookmark.setText(tmpTitle);
						finish();
						positionHighlight = -1;
					}

				}else{
					finish();
					positionHighlight = -1;
				}

			}
		});



		edtBookmark.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				InputMethodManager keyboard = (InputMethodManager)
						getSystemService(Context.INPUT_METHOD_SERVICE);
				keyboard.hideSoftInputFromWindow( edtBookmark.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);

			}
		},0);

//		lstBookmark.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				// TODO Auto-generated method stub
//				setResult(data.get(position).getTitle()!=null?Integer.parseInt(data.get(position).getTitle().toString()):0);
//				finish();
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				// TODO Auto-generated method stub
//				
//			}
//		});

		lstBookmark.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// TODO Auto-generated method stub
				setResult(data.get(position).getPagenumber()!=null?Integer.parseInt(data.get(position).getPagenumber().toString())-1:0);
				finish();
			}
		});
	}

	public void showMunuEdit(){
		lyBookmark.setVisibility(View.VISIBLE);
	}


	public void setFocusUpdateTextDescription(String title,String pagenum){
		edtBookmark.setText(title);
		edtBookmark.setSelectAllOnFocus(true);

		pageNumber = pagenum;
		btnAdd.setText(getString(R.string.bk_update));

	}
	public void refreshBookmark(){
		new LoadSavedBookmark().execute();
	}

	private class LoadSavedBookmark extends AsyncTask<String, Void, ArrayList<HashMap<String, String>>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//				if(bookmarkHandler ==null){
//					bookmarkHandler = new BookmarkHandler(aq.getContext());
//					bookmarkHandler.open();
//				}
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... params) {

			data = bookmarkHandler.getAllBookmark(bookid);
			ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
			for (int i = 0; i < data.size(); i++) {
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("bookid", data.get(i).getBookid());
				hash.put("pagenum", data.get(i).getPagenumber());
				hash.put("title", data.get(i).getTitle());
				hash.put("imagepath", data.get(i).getImagepath());
				hash.put("revision",data.get(i).getRevision());
				arrayList.add(hash);
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			super.onPostExecute(result);
//				GridView grid =  (GridView)findViewById(R.id.grd_book_category_detail);      
//	   			grid.setExpanded(true);      

			try{
				if(bookmarkHandler.check(bookid, pageNumber+"")){
					if(data!=null && data.size()>0){
						for(int position = 0;  position< data.size(); position++){
							if(data.get(position).getPagenumber().equals(pageNumber)){
								edtBookmark.setText(data.get(position).getTitle().toString());

								if(tmpPageNumber!=null && tmpPageNumber.equalsIgnoreCase(pageNumber)){
									tmpTitle = data.get(position).getTitle().toString();
								}

								btnAdd.setText(getString(R.string.bk_update));
							}
						}
					}
				}
			}catch (Exception e){

			}

			bookmarkAdt = new BookmarkAdapter(BookmarkActivity.this, result);
			lstBookmark.setAdapter(bookmarkAdt);

//			        fileGarbage();

		}

	}
}
