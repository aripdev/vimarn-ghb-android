package com.arip.it.library;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class ExamChoice extends Activity implements OnGestureListener {

	GestureDetector gd;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;

	TextView txtView2;

	String choicePath;
	String choiceTotal;
	String choiceSection;
	String choiceType;
	String choiceId;

	String Total;
	int choiceX;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_choice);

		gd = new GestureDetector(this, this);

		Bundle choice = getIntent().getExtras();
		choicePath = choice.getString("path");
		choiceTotal = choice.getString("total");
		choiceSection = choice.getString("section");
		choiceType = choice.getString("type");
		choiceId = choice.getString("id");

		choiceX = choice.getInt("x");

		Log.d("TAG", "Choice X : " + choiceX);
		//รับค่าบรรทัดมา








		String S_path = choicePath + "choice/" + choiceId + ".txt";
		final String S_section = choicePath + "exam.txt";

		final ArrayList<String> y = new ArrayList<String>();
		String line = null;

		try {
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			Total = String.valueOf( examR.readLine() );

			while ((line = examR.readLine()) != null) {
				if (line.length() > 0) {
					y.add(line);
				}
			}

			for (int i = 0; i < y.size(); i++)
				Log.d("TAG", "READ in Choice : " + y.get(i));



		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String z = y.get(1);

		Log.d("TAG", "A :" + z);



		if (y.size() == 9) {

			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(y.get(0));

			RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(y.get(5));

			RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
			radioButton2.setText(y.get(8));
			RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
			radioButton3.setVisibility(View.GONE);

			RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
			radioButton4.setVisibility(View.GONE);

		}
		else if (y.size() > 10) {

			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(y.get(0));

			final RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(y.get(5));

			RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
			radioButton2.setText(y.get(8));

			RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
			radioButton3.setText(y.get(11));

			RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
			//radioButton4.setText(y.get(14));

			final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.choice_groupradio);

			final Button imageView1 = (Button) findViewById(R.id.choice_btn_answer);
			imageView1.setOnClickListener(new View.OnClickListener() {


				@Override
				public void onClick(View v) {
					int selectId = radioGroup.getCheckedRadioButtonId();

					ImageView img = (ImageView) findViewById(R.id.choice_img_r);

					if (radioButton1.isChecked() && y.get(1).equalsIgnoreCase(y.get(7))) {
						//ImageView img = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.r);
					} else if (radioButton1.isChecked() && y.get(1).equalsIgnoreCase(y.get(4))) {
						//ImageView img2 = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.r);
					} else if (radioButton1.isChecked() && y.get(1).equalsIgnoreCase(y.get(10))) {
						//ImageView img3 = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.r);
					} else if (radioButton1.isChecked() && y.get(1).equalsIgnoreCase(y.get(13))) {
						//ImageView img4 = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.r);
					} else if (radioButton1.isChecked() && y.get(1).equals(y.get(4))) {
						//ImageView img5 = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.w);
					} else {
						radioButton1.isChecked();
						//ImageView img7 = (ImageView) findViewById(R.id.choice_img_r);
						img.setImageResource(R.drawable.w);
					}


				}
			});


		}

		else {

			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(y.get(0));

			RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(y.get(5));

			RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
			radioButton2.setText(y.get(8));

			RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
			radioButton3.setText(y.get(11));

			RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
			//radioButton4.setText(y.get(14));
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (gd.onTouchEvent(event))
			return true;
		else
			return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
						   float velocityY) {
		// TODO Auto-generated method stub

		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				ArrayList<String> a = new ArrayList<>();
				String intLine = null;

				try {
					FileReader ex = new FileReader(choicePath + "exam.txt");
					BufferedReader examR = new BufferedReader(ex);

					while ((intLine = examR.readLine()) != null){
						if (intLine.length() > 0){
							a.add(intLine);
						}

					}

					for (int i = 0; i < a.size(); i++)
						Log.d("TAG", "READ in Choice : " + a.get(i));

					examR.close();
				}catch (FileNotFoundException e){
					e.printStackTrace();
				}catch (IOException e){
					e.printStackTrace();
				}

				Intent examChoice;
				if (a.get(5).equalsIgnoreCase("choice")) examChoice = new Intent(ExamChoice.this, ExamChoice2.class);
				else if (a.get(5).equalsIgnoreCase("match")) examChoice = new Intent(ExamChoice.this, ExamMatch.class);
				else if (a.get(5).equalsIgnoreCase("fill")) examChoice = new Intent(ExamChoice.this, ExamFill.class);
				else if (a.get(5).equalsIgnoreCase("sort")) examChoice = new Intent(ExamChoice.this, ExamSort.class);
				else examChoice = new Intent(ExamChoice.this, ExamSound.class);

				examChoice.putExtra("path", choicePath);
				examChoice.putExtra("total", a.get(0));
				examChoice.putExtra("section", a.get(4));
				examChoice.putExtra("type", a.get(5));
				examChoice.putExtra("id", a.get(6));

				startActivity(examChoice);



				Toast.makeText(ExamChoice.this, "<---- Left Swipe", Toast.LENGTH_SHORT).show();

			}  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				onBackPressed();
/*
				Intent intent = new Intent(ExamChoice.this, ExamFill.class);

				startActivity(intent);

				Toast.makeText(ExamChoice.this, "----> Right Swipe", Toast.LENGTH_SHORT).show();
*/
			}

		} catch (Exception e) {
			// nothing
		}

		return true;
	}

	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onDown", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		Toast.makeText(ExamChoice.this, "onLongPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onScroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onShowPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onSingleTapUp", Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lay_exam_choice, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}
/*
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExamChoice  extends Activity implements OnGestureListener{

	GestureDetector gd;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;
	public String Path, Total, Section, Type, Id, Sign;

	/*private String TAG="Ch. ExamChoice";

	
	public int sec,ExamTotal;
	private String idNext,typeNext,target;
	
	public float x1,x2;
	
	public TextView question;


	public ImageView Right,Wrong;
	
	public String S_path,S_question,S_picture,S_answer,S_choice,ans_choice;
	public int S_totalChoice;


	int S_totalChoice;
	public RadioButton c1, c2, c3, c4, ans;
	public RadioGroup RG;
	public Button answer;
	public String examId;
	public String examSec;
	public String examPath;
	public String examTotal;
	public String examType;

	ViewPager pager;
	MyPageAdapter adapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_exam_choice);

		Bundle bundle = getIntent().getExtras();
		examId = bundle.getString("id");
		examPath = bundle.getString("path");
		examTotal = bundle.getString("total");
		examType = bundle.getString("type");
		examSec = bundle.getString("section");

		Log.d("TAG", "ID : " + examId + " Path : " + examPath);

		final String S_path = examPath + "choice/" + examId + ".txt";
		final String S_section = examPath + "exam.txt";

		ArrayList<String> a = new ArrayList<String>();
		String xLine = null;
		int lineNumber = 0;

		try {

			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

			while ((xLine = examR.readLine()) != null) {
				if (xLine.length() > 0) {
					a.add(xLine);
				}

				lineNumber++;
			}

			for (int i = 0; i < a.size(); i++)
				Log.d("TAG", "Test" + a.get(i));
			Log.d("TAG", "Line : " + a.size());

			examR.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//รับ id ที่ถูกส่งมาแล้ว
		Log.d("TAG", "S_path : " + S_path);
		Log.d("TAG", "S_section : " + S_section);
		Log.d("TAG", "Line : " + lineNumber);

		if (a.size() == 9) {

			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(a.get(0));

			RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(a.get(5));

			RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
			radioButton2.setText(a.get(8));

			RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
			radioButton3.setVisibility(View.GONE);

			RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
			radioButton4.setVisibility(View.GONE);
		} else if (a.size() > 10) {
			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(a.get(0));

			RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(a.get(5));

			RadioButton radioButton2 = (RadioButton) findViewById(R.id.choice_c2);
			radioButton2.setText(a.get(8));

			RadioButton radioButton3 = (RadioButton) findViewById(R.id.choice_c3);
			radioButton3.setText(a.get(11));

			RadioButton radioButton4 = (RadioButton) findViewById(R.id.choice_c4);
			radioButton4.setText(a.get(14));
		} else {
			TextView txt1 = (TextView) findViewById(R.id.only_question);
			txt1.setText(a.get(0));

			RadioButton radioButton1 = (RadioButton) findViewById(R.id.choice_c1);
			radioButton1.setText(a.get(5));
		}

		gd = new GestureDetector(this, this);
	}

	public boolean onTouch(MotionEvent event) {
		if (gd.onTouchEvent(event))
			return true;
		else
			return false;

	}

	public boolean onFling(MotionEvent e1, MotionEvent e2,float velocityX, float velocityY) {

		try {

			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

				Toast.makeText(ExamChoice.this, "<---- Left Swipe", Toast.LENGTH_SHORT).show();

			}  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {


				Toast.makeText(ExamChoice.this, "----> Right Swipe", Toast.LENGTH_SHORT).show();

				String line = "";
				ArrayList<String> y = new ArrayList<String>();

				try {
					FileReader ex = new FileReader(examPath + "exam.txt");
					BufferedReader examR = new BufferedReader(ex);

					while ((line = examR.readLine()) != null) {
						if (line.length() > 0) {
							y.add(line);
						}
					}

					for (int i = 0; i < y.size(); i++)
						Log.d("TAG", "Test : " + y.get(i));

					examR.close();

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}


				Intent examChoice;

				if (y.get(5).equalsIgnoreCase("choice"))
					examChoice = new Intent(ExamChoice.this, ExamChoice2.class);
				else if (y.get(5).equalsIgnoreCase("match"))
					examChoice = new Intent(ExamChoice.this, ExamMatch.class);
				else if (y.get(5).equalsIgnoreCase("fill"))
					examChoice = new Intent(ExamChoice.this, ExamFill.class);
				else if (y.get(5).equalsIgnoreCase("sort"))
					examChoice = new Intent(ExamChoice.this, ExamSort.class);
				else
					examChoice = new Intent(ExamChoice.this, ExamSound.class);

				examChoice.putExtra("id", y.get(6));
				examChoice.putExtra("path", examPath);
				examChoice.putExtra("section", y.get(4));
				examChoice.putExtra("total", y.get(0));
				examChoice.putExtra("type", y.get(5));


				startActivity(examChoice);

			}
	} catch (Exception e){

		}

		return true;
	}
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onDown", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		Toast.makeText(ExamChoice.this, "onLongPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
							float distanceY) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onScroll", Toast.LENGTH_SHORT).show();
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onShowPress", Toast.LENGTH_SHORT).show();
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		//Toast.makeText(MainActivity.this, "onSingleTapUp", Toast.LENGTH_SHORT).show();
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.lay_exam_choice, menu);
		return true;
	}

}




/*
		Button btnAns = (Button) findViewById(R.id.choice_btn_answer);
		btnAns.setOnClickListener(new View.OnClickListener() {
		*/
/*
		public void onTouch(view v) {

										  String line = "";
										  ArrayList<String> y = new ArrayList<String>();

										  try {
											  FileReader ex = new FileReader(S_section);
											  BufferedReader examR = new BufferedReader(ex);

											  while ((line = examR.readLine()) != null) {
												  if (line.length() > 0) {
													  y.add(line);
												  }
											  }

											  for (int i = 0; i < y.size(); i++)
												  Log.d("TAG", "Test : " + y.get(i));

											  examR.close();

										  } catch (FileNotFoundException e) {
											  e.printStackTrace();
										  } catch (IOException e) {
											  e.printStackTrace();
										  }

										  Intent examChoice;

										  if (y.get(5).equalsIgnoreCase("choice"))
											  examChoice = new Intent(ExamChoice.this, ExamChoice2.class);
										  else if (y.get(5).equalsIgnoreCase("match"))
											  examChoice = new Intent(ExamChoice.this, ExamMatch.class);
										  else if (y.get(5).equalsIgnoreCase("fill"))
											  examChoice = new Intent(ExamChoice.this, ExamFill.class);
										  else if (y.get(5).equalsIgnoreCase("sort"))
											  examChoice = new Intent(ExamChoice.this, ExamSort.class);
										  else
											  examChoice = new Intent(ExamChoice.this, ExamSound.class);

										  examChoice.putExtra("id", y.get(6));
										  examChoice.putExtra("path", examPath);
										  examChoice.putExtra("section", y.get(4));
										  examChoice.putExtra("total", y.get(0));
										  examChoice.putExtra("type", y.get(5));


										  startActivity(examChoice);

										  overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);


									  }
								  */

		//);
	//}













/*==============================================================*/








/* ทำเป็น Object
			private String choiceSection;
			private String choiceType;
			private String choiceId;

			public void findSection() {


				String line = null;
				ArrayList<T> y = new ArrayList<T>();

				try {
					FileReader ex = new FileReader(S_section);
					BufferedReader examR = new BufferedReader(ex);

					examR.readLine();

					while ((line = examR.readLine()) != null) {

						if (line.length() > 0) {

							y.add(new T(choiceSection, choiceType, choiceId));

						}

					}

					for (int i = 0; i < y.size(); i++)
						Log.d("TAG", "Test + " + y.get(i).choiceSection + y.get(i).choiceType + y.get(i).choiceId);

					examR.close();


				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}




			}
*/






/* ทำเป็น Object
	private class T {
		public String choiceSection;
		public String choiceType;
		public String choiceId;
		public T(String choiceId, String choiceType, String choiceSection) {
			this.choiceSection = choiceSection;
			this.choiceType = choiceType;
			this.choiceId = choiceId;
		}
	}
	*/





/*
		Bundle bundle = getIntent().getExtras();
		examId = bundle.getStringArrayList("id");
		examPath = bundle.getString("path");
		//examTotal = bundle.getString("total");
		//examType = bundle.getString("type");
		//examSec = bundle.getString("section");

		String S_path = examPath + "choice/" + examId + ".txt";
		String S_question = null, S_choice = null, S_section = null, S_id = null, S_text = null;

		//int ExamTotal;
		ArrayList<String> a = new ArrayList<>();
		String xLine = null;
		int lineNumber = 0;
		try {

			//FileReader ex = new FileReader(S_path);

			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);

//			S_question = examR.readLine();




			while ((xLine = examR.readLine()) != null) {
				if (xLine.length() > 0) {
					a.add(xLine);
				}
				lineNumber++;
			}



			for (int i = 0; i < a.size(); i++)
				Log.d("TAG", "Test" + a.get(i));

			examR.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


		if (lineNumber == 14){
			RadioButton txt3 = (RadioButton) findViewById(R.id.choice_c3);
			txt3.setText(a.get(11));
		}

		TextView txtQuestion = (TextView) findViewById(R.id.only_question);
		txtQuestion.setText(a.get(0));

		RadioButton txt1 = (RadioButton) findViewById(R.id.choice_c1);
		txt1.setText(a.get(5));

		RadioButton txt2 = (RadioButton) findViewById(R.id.choice_c2);
		txt2.setText(a.get(8));
/*
		Button btnAns =(Button) findViewById(R.id.choice_btn_answer);
		btnAns.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findSection();
			}
		});


		Log.d("TAG", "path:" + examPath);
		Log.d("TAG", "ID:" + examId + "total:" + examTotal + "type:" + examType + "sec:" + examSec);
		Log.d("TAG", "S_path:" + S_path);
		Log.d("TAG", "S_Choice" + S_choice);
	}
/*
	private void findSection() {

		ArrayList<String> b = new ArrayList<>();
		String bLine = null;

		try {

			//FileReader ex = new FileReader(S_path);

			FileReader ex = new FileReader(examId + ".txt");
			BufferedReader examR = new BufferedReader(ex);

//			S_question = examR.readLine();


			while ((bLine = examR.readLine()) != null) {
				if (bLine.length() > 0) {
					b.add(bLine);
				}

			}

			for (int i = 0; i < b.size(); i++)
				Log.d("TAG", "Test" + b.get(i));

			examR.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}*/

/*
	private void findSection(int sect) {
		examSec = "section" + sect;

		try {

			FileReader ex = new FileReader(Path + "exam.txt");
			BufferedReader examR = new BufferedReader(ex);

			String line = "";
			while ((line = examR.readLine()) != null) {
				// do something with the line you just read, e.g.
				Log.d("TAG", "temp : " + line);
				if (line.equalsIgnoreCase(examSec)) {
					examType = String.valueOf(examR.readLine());
					examId = String.valueOf(examR.readLine());
					Log.d("TAG", "typeNext : " + examType);
					Log.d("TAG", "idNext : " + examId);
					break;
				}

			}


			examR.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		findSection(sect);

		Intent intent = null;

		if (examType.equalsIgnoreCase("choice")) intent = new Intent(this, ExamChoice.class);
		else if (examType.equalsIgnoreCase("sound")) intent = new Intent(this, ExamSound.class);
		else if (examType.equalsIgnoreCase("match")) intent = new Intent(this, ExamMatch.class);
		else if (examType.equalsIgnoreCase("sort")) intent = new Intent(this, ExamSort.class);
		else intent = new Intent(this, ExamFill.class);

		intent.putExtra("path", examPath);
		intent.putExtra("total", examTotal);
		intent.putExtra("section", examSec);
		intent.putExtra("type", examType);
		intent.putExtra("id", examId);
		intent.putExtra("sign", "-");

		Log.e("TAG", "---Total:" + examTotal + " section:" + examSec + " ID:" + examId + " type:" + examType);

		this.finish();
		this.startActivity(intent);

	}
	}


	//getChoice(S_totalChoice);

/*
	private void getChoice(int n) {

		Bundle bundle = getIntent().getExtras();
		String examId = bundle.getString("id");
		String examPath = bundle.getString("path");
		String examTotal = bundle.getString("total");
		String examType = bundle.getString("type");
		String examSec = bundle.getString("section");

		String S_path = examPath + "choice/" + examId + ".txt";

		String c_txt = null, c_id = null, c_pic = null;

		try {

			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);


			String line = "";
			String temp = "";
			int ttt = 0;

			while ((line = examR.readLine()) != null) {
				// do something with the line you just read, e.g.
				Log.d("TAG", "temp : " + line);
				if (line.startsWith("choice")) {

					//temp = replaceAll(line,"choice","");
					//ttt = Integer.parseInt(temp.toString());

					c_id = examR.readLine();
					c_txt = examR.readLine();
					c_pic = examR.readLine();

					setChoice(ttt+1,c_id,c_txt,c_pic);

					//if (ttt == (n - 1)) break;

				}

			}

			examR.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RadioButton choice_c1 = (RadioButton) findViewById(R.id.choice_c1);
		choice_c1.setText(c_txt);
	}

	private void setChoice(int n, String c_id, String c_txt, String c_pic) {

		String S_answer = null, ans_choice;

		c1 = (RadioButton)findViewById(R.id.choice_c1);
		c2 = (RadioButton)findViewById(R.id.choice_c2);
		c3 = (RadioButton)findViewById(R.id.choice_c3);
		c4 = (RadioButton)findViewById(R.id.choice_c4);
		RG = (RadioGroup)findViewById(R.id.choice_groupradio);
		RG.clearCheck();

		answer = (Button)findViewById(R.id.choice_btn_answer);

		if(n==1){
			c1.setText(c_txt);
		}
		else if(n==2){
			c2.setText(c_txt);
		}
		else if(n==3){
			c3.setText(c_txt);
			c3.setVisibility(View.VISIBLE);
		}
		else if(n==4){
			c4.setText(c_txt);
			c4.setVisibility(View.VISIBLE);
		}

		if(c_id.equalsIgnoreCase(S_answer))ans_choice=c_txt;

	}
}

	/*
		if(Sign.equalsIgnoreCase("+")){
			overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
		}
		else overridePendingTransition(R.anim.slide_right_in, R.anim.hold);

		
		initial();
		
		
		
		answer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				int selectedId = RG.getCheckedRadioButtonId();
				ans = (RadioButton) findViewById(selectedId);
				ans.getText().toString();
				
			}
		});
		
	}
	
	
	public void getData(){
		
		S_path = Path+"choice/"+Id+".txt";
		
		try{
			
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);
			
			S_question = examR.readLine();
			S_picture = examR.readLine();
			S_choice = examR.readLine();
			
			S_totalChoice = Integer.parseInt(S_choice.toString());
			
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		getChoice(S_totalChoice);
		
		question.setText(S_question);
		question.setVisibility(View.VISIBLE);
		
	
	}
	
	
	public void getChoice(int n){
		
		try{
			
			FileReader ex = new FileReader(S_path);
			BufferedReader examR = new BufferedReader(ex);
			
			
			String line = "";
			String temp = "";
			int ttt=0;
			
			while ((line = examR.readLine()) != null) {
			    // do something with the line you just read, e.g.
				Log.d(TAG,"temp : "+line);
				if(line.startsWith("choice")){
					
					temp = replaceAll(line,"choice","");
					ttt = Integer.parseInt(temp.toString());
					
					String c_id = examR.readLine();
					String c_txt = examR.readLine();
					String c_pic = examR.readLine();
					
					setChoice(ttt+1,c_id,c_txt,c_pic);
					
					if(ttt==(n-1))break;
	     			
	     		}
			   
			}
			
	     	
	     	examR.close();
			
		}
		 catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
	}
	
	public void setChoice(int n,String c_id,String c_txt,String c_pic){
		
		if(n==1){
			c1.setText(c_txt);
		}
		else if(n==2){
			c2.setText(c_txt);
		}
		else if(n==3){
			c3.setText(c_txt);
			c3.setVisibility(View.VISIBLE);
		}
		else if(n==4){
			c4.setText(c_txt);
			c4.setVisibility(View.VISIBLE);
		}
		
		if(c_id.equalsIgnoreCase(S_answer))ans_choice=c_txt;
		
	}
	
	
	public void initial(){
		
		question = (TextView)findViewById(R.id.choice_question);
		question.setVisibility(View.GONE);
		//question.setVisibility(View.VISIBLE);
		
		c1 = (RadioButton)findViewById(R.id.choice_c1);
		c2 = (RadioButton)findViewById(R.id.choice_c2);
		c3 = (RadioButton)findViewById(R.id.choice_c3);
		c4 = (RadioButton)findViewById(R.id.choice_c4);
		c3.setVisibility(View.GONE);
		c4.setVisibility(View.GONE);
		
		RG = (RadioGroup)findViewById(R.id.choice_groupradio);
		RG.clearCheck();
		
		answer = (Button)findViewById(R.id.choice_btn_answer);
		
	}
	
	public int sectionNow(){
		String s = Section;
		Log.d(TAG, "sectionNow : "+s);
		s = replaceAll(s,"section","");
		Log.d(TAG, "sectionNow : "+s);
		int sect = Integer.parseInt(s.toString());
		return sect;
	}
	
	public void goIncrease(){
		
		Log.d(TAG, "goIncrease");
		
		int sect = sectionNow()+1;
		
		
		if(sect>=ExamTotal){
			this.finish();
		}
		else{
			
			
			findSection(sect);
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
	    	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
	    	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
	    	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
	    	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
	    	intent.putExtra("total", Total);
	    	intent.putExtra("section", target);
	    	intent.putExtra("type", typeNext);
	    	intent.putExtra("id", idNext);
	    	intent.putExtra("sign","+");
	    	
	    	Log.e(TAG, "+++Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
	    	
	    	this.finish();
	    	this.startActivity(intent);
			
			
			
			
		}
		
		
		
		
	}

	public void goDecrease(){
		
		Log.d(TAG, "goDecrease");
		
		int sect = sectionNow()-1;
		
		
		if(sect<0){
			this.finish();
		}
		else{
			
			findSection();
			
			Intent intent = null;
			
			if(typeNext.equalsIgnoreCase("choice"))intent = new Intent(this, ExamChoice.class);
        	else if(typeNext.equalsIgnoreCase("sound"))intent = new Intent(this, ExamSound.class);
        	else if(typeNext.equalsIgnoreCase("match"))intent = new Intent(this, ExamMatch.class);
        	else if(typeNext.equalsIgnoreCase("sort"))intent = new Intent(this, ExamSort.class);
        	else intent = new Intent(this, ExamFill.class);
			
			intent.putExtra("path", Path);
        	intent.putExtra("total", Total);
        	intent.putExtra("section", target);
        	intent.putExtra("type", typeNext);
        	intent.putExtra("id", idNext);
        	intent.putExtra("sign","-");
        	
        	Log.e(TAG, "---Total:"+Total+" section:"+target+" ID:"+idNext+" type:"+typeNext);
        	
        	this.finish();
        	this.startActivity(intent);
			

			
		}
		
		
		
	}



	public void findSection(int sect) {

		examSec = "section" + sect;

		try {

			FileReader ex = new FileReader(Path + "exam.txt");
			BufferedReader examR = new BufferedReader(ex);

			String line = "";
			while ((line = examR.readLine()) != null) {
				// do something with the line you just read, e.g.
				Log.d("TAG", "temp : " + line);
				if (line.equalsIgnoreCase(examSec)) {
					examType = String.valueOf(examR.readLine());
					examId = String.valueOf(examR.readLine());
					Log.d("TAG", "typeNext : " + examType);
					Log.d("TAG", "idNext : " + examId);
					break;
				}

			}


			examR.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		findSection(sect);

		Intent intent = null;

		if (examType.equalsIgnoreCase("choice")) intent = new Intent(this, ExamChoice.class);
		else if (examType.equalsIgnoreCase("sound")) intent = new Intent(this, ExamSound.class);
		else if (examType.equalsIgnoreCase("match")) intent = new Intent(this, ExamMatch.class);
		else if (examType.equalsIgnoreCase("sort")) intent = new Intent(this, ExamSort.class);
		else intent = new Intent(this, ExamFill.class);

		intent.putExtra("path", examPath);
		intent.putExtra("total", examTotal);
		intent.putExtra("section", examSec);
		intent.putExtra("type", examType);
		intent.putExtra("id", examId);
		intent.putExtra("sign", "-");

		Log.e("TAG", "---Total:" + examTotal + " section:" + examSec + " ID:" + examId + " type:" + examType);

		this.finish();
		this.startActivity(intent);


	}}
	/*
	
	public boolean onTouchEvent(MotionEvent touchevent) 
    {
		
		
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		//Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();  // deprecated
		//int height = display.getHeight();  // deprecated
		
		
		float sensitive=width/2;
		
    	switch (touchevent.getAction())
    	{
    		// when user first touches the screen we get x and y coordinate
    		case MotionEvent.ACTION_DOWN: 
    		{
    			x1 = touchevent.getX();
    			
    			break;
    		}
    		case MotionEvent.ACTION_UP: 
    		{
    			x2 = touchevent.getX();
    			
    			//if left to right sweep event on screen
                if (x1 < x2 && (x2-x1)>=sensitive) 
               	{
                	Log.d("puy","L - R");
                	
                	goDecrease();
                	
                	
                	
                }
                                         
                // if right to left sweep event on screen
                if (x1 > x2 && (x1-x2)>=sensitive)
                {
                	Log.d("puy","R - L");
                	
                	goIncrease();
                }
                
                break;
    		}
    	}
    	return false;
	}
	
	 private String replaceAll(String source, String pattern, String replacement) {
	        if (source == null) {
	            return "";
	        }
	        StringBuffer sb = new StringBuffer();
	        int index;
	        int patIndex = 0;
	        while ((index = source.indexOf(pattern, patIndex)) != -1) {
	            sb.append(source.substring(patIndex, index));
	            sb.append(replacement);
	            patIndex = index + pattern.length();
	        }
	        sb.append(source.substring(patIndex));
	        return sb.toString();
	    }
	
*/


