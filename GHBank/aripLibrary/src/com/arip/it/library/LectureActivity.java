package com.arip.it.library;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;

import com.arip.it.library.core.paint.PcanvasGestureListener;
import com.arip.it.library.core.paint.PcanvasTouchListener;
import com.arip.it.library.core.paint.PdrawingView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.PathShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LectureActivity extends FragmentActivity {

	public PdrawingView myView;
	public OnTouchListener touchListener;
	public GestureDetector gdt;

	public ArrayList<Drawable> visibleDrawables = new ArrayList<Drawable>();
	public Shape currentShape = new OvalShape();
	public ShapeDrawable currentDrawable = new ShapeDrawable(currentShape);
	public Path currentPath;

	static int defaultColor = Color.BLACK;
	static int defaultRadius = 10;
//	static int defaultBackground = Color.WHITE;
	static int defaultBackground;
	
	public static enum ShapeType {
		BRUSH, ERASER, OVAL, RECTANGLE
	}
	
	public int color = defaultColor;
	public int brushRadius = defaultRadius;
	public int eraseRadius = defaultRadius;
	public int backgroundColor = defaultBackground;
	public ShapeType currShape = ShapeType.BRUSH;
	
	
//	private LinearLayout btnWant;
//	private LinearLayout btnDelete;
//	private LinearLayout btnFrame;
//	private LinearLayout btnDraw;
//	
//	private ImageView imgColor1;
//	private ImageView imgColor2;
//	private ImageView imgColor3;
//	private ImageView imgColor4;
//	private ImageView imgColor5;
//	private ImageView imgEraser;
	
	@SuppressWarnings("unused")
	private Drawable resultPhoto;
	private Bitmap resultPhotoBitmap;
	@SuppressWarnings("unused")
	private boolean setFrameSelect = false;
	
	@SuppressWarnings("unused")
	private Bundle extras;
	protected RelativeLayout layout;
	
	private ProgressDialog dgLoading;
	private TextView btnCancel;
	private TextView btnDone;
	
	@SuppressWarnings("unused")
	private ImageView btnPencil;
	@SuppressWarnings("unused")
	private ImageView btnHighLight;
	private ImageView btnRubber;
	@SuppressWarnings("unused")
	private ImageView btnColor;
	@SuppressWarnings("unused")
	private ImageView imgColorSelect;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.lay_lecture_pdf);
       
		
		layout = (RelativeLayout) findViewById(R.id.fram_draw);
		btnCancel = (TextView) findViewById(R.id.btnCancel);
		btnDone = (TextView) findViewById(R.id.btnDone);
		
		btnPencil  = (ImageView) findViewById(R.id.btnPencil);
		btnHighLight = (ImageView) findViewById(R.id.btnHighLight);
		btnRubber = (ImageView) findViewById(R.id.btnRubber);
		btnColor = (ImageView) findViewById(R.id.btnColor);
		imgColorSelect = (ImageView) findViewById(R.id.imgColor);
		
		
		if(MuPDFActivity.bMapLecture!=null){
//			Bundle extras = getIntent().getExtras();
//			Bitmap bmp = (Bitmap) extras.getParcelable("image");
			myView = new PdrawingView(this, LectureActivity.this,MuPDFActivity.bMapLecture);
			// disable hardware acceleration
			myView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			
			
			
			gdt = new GestureDetector(this, new PcanvasGestureListener(LectureActivity.this));
			touchListener = new PcanvasTouchListener(LectureActivity.this, gdt);
			myView.setOnTouchListener(touchListener);
			
			layout.addView(myView);
//			MuPDFActivity.clearBitmapLecture();
		}
//		imgEraser.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//
//				DialogForClearPaint();
//				
//			}
//		});

		btnDone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		btnRubber.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PopupMenu popup = new PopupMenu(LectureActivity.this, v);
		        popup.getMenuInflater().inflate(R.menu.menu_lecture, popup.getMenu());
		        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
		            public boolean onMenuItemClick(MenuItem item) {
		            	
		            	if(item.getTitle() == getString(com.arip.it.library.R.string.menu_erase)) {
		            		
		            	 }else if(item.getTitle() == getString(com.arip.it.library.R.string.menu_erase_all)) {
		            		 
			             } 
		            	
		                return true;
		            }
		        });

		        popup.show();
			}
		});
    }
    
    @SuppressWarnings("unused")
	private void setBrushShapeType(){
    	currShape = ShapeType.BRUSH;
    }
    
    @SuppressWarnings("unused")
	private void setEraserShapeType(){
    	currShape = ShapeType.ERASER;
    	eraseRadius = defaultRadius;
    }
    
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) 
	    {
	    case android.R.id.home: 
	        onBackPressed();
	        break;

	    default:
	        return super.onOptionsItemSelected(item);
	    }
		
		return true;
	}
	
	public void setCurrentShape() {
		switch (currShape) {
		case BRUSH:
			setPath(false);
			break;
		case ERASER:
			setPath(true);
			break;
		case OVAL:
			currentShape = new OvalShape();
			currentDrawable = new ShapeDrawable(currentShape);
			break;
		case RECTANGLE:
			currentShape = new RectShape();
			currentDrawable = new ShapeDrawable(currentShape);
			break;
		}
	}
		private void setPath(boolean erase) {
			currentPath = new Path();
			currentShape = new PathShape(currentPath, myView.canvasWidth,myView.canvasHeight);
			currentDrawable = new ShapeDrawable(currentShape);
			currentDrawable.setBounds(0, 0, myView.canvasWidth, myView.canvasHeight);

			Paint curr = currentDrawable.getPaint();
			curr.setColor(color);
			curr.setStyle(Paint.Style.STROKE);
			curr.setStrokeWidth(brushRadius);
			curr.setStrokeJoin(Paint.Join.ROUND);
			curr.setStrokeCap(Paint.Cap.ROUND);
	        curr.setAntiAlias(true);
	        curr.setDither(true);
			
	        if (erase == true) {
				curr.setStrokeWidth(eraseRadius);
	        	curr.setColor(backgroundColor);
	        }
		}
		
	@SuppressWarnings("unused")
	private class CreateImageFileTaskProcess extends AsyncTask<URL, Integer, Long> {
		
		
		
		protected Long doInBackground( URL... urls ) {
			Long ret = this.createProcess();

			return ret;
		}

		protected void onProgressUpdate( Integer... progress ) {

		}

		protected void onPostExecute( Long result ) {
		

			try {
				if(resultPhotoBitmap!=null)
					resultPhotoBitmap.recycle();
				
				dgLoading.dismiss();
			
	        	finish();
	        	overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
				
			} catch ( Exception e ) {
				// TODO: handle exception
			}

		}

		private long createProcess() {
			long ret = 0;
			
			
			try {
				( new File( Environment.getExternalStorageDirectory() + "/WantedPhotoGallery/squareImage" ) ).mkdir();
				
				Uri mImageSaveOnSD = Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ "/WantedPhotoGallery/squareImage",
								   "tmp_photo_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
		
				File f = new File(mImageSaveOnSD.getPath());
				f.createNewFile();
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
				resultPhotoBitmap.compress(CompressFormat.JPEG, 100, bos);
				byte[] bitmapdata = bos.toByteArray();

				//write the bytes in file
				@SuppressWarnings("resource")
				FileOutputStream fos = new FileOutputStream(f);
				fos.write(bitmapdata);
//				WTDphotoBitmapData.setImageSource(result);
				
			} catch ( Exception ex ) {}

			return ret;
		}
	}		
	
	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth, float newHeight) {   
		if(bitmapToScale == null)
		    return null;
		//get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix, true);  
		}

	public AlertDialog DialogForTurnOff() {

			AlertDialog.Builder dgBuilder = new AlertDialog.Builder( this );
			dgBuilder.setTitle("Delete");
			dgBuilder.setMessage( "Do you want to delete photo");
			dgBuilder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick( DialogInterface dialog, int which ) { // String
							finish();
							dialog.dismiss();
							
						}
					} ).setNegativeButton( "Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick( DialogInterface dialog, int id ) {
							dialog.dismiss();
						}
					} );

			dgBuilder.create();
			return dgBuilder.show();
		}
	 
	 public AlertDialog DialogForClearPaint() {

			AlertDialog.Builder dgBuilder = new AlertDialog.Builder( this );
			dgBuilder.setTitle("Delete");
			dgBuilder.setMessage( "Do you want to delete paint");
			dgBuilder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick( DialogInterface dialog, int which ) { // String
							visibleDrawables.clear();
							myView.clearImage();
							myView.invalidate();
							dialog.dismiss();
							
						}
					} ).setNegativeButton( "Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick( DialogInterface dialog, int id ) {
							dialog.dismiss();
						}
					} );

			dgBuilder.create();
			return dgBuilder.show();
		}
	 
	 
	
}


