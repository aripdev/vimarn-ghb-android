package com.artifex.mupdf;

public class OutlineItem {
	public  int level;
	public  String title;
	public  int page;

	public OutlineItem(int level, String title, int page) {
		this.level = level;
		this.title = title;
		this.page = page;
	}

}
