package com.artifex.mupdf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arip.it.library.BookmarkActivity;
import com.arip.it.library.R;
import com.arip.it.library.core.lib.util.DeviceSize;
import com.arip.it.library.db.BookmarkHandler;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class BookmarkAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<HashMap<String, String>> d;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
//	private BookmarkHandler bookmarkHanlder;
	private final SparseArray<Bitmap> mBitmapCache = new SparseArray<Bitmap>();
//	private String mPath;
	private Bitmap mLoadingBitmap;
	
	
	public BookmarkAdapter(Context context, ArrayList<HashMap<String, String>> data) {
		mContext = context;
		d = data;
		
		mLoadingBitmap = BitmapFactory.decodeResource(
				mContext.getResources(), R.drawable.darkdenim3);
//		if(bookmarkHanlder==null){
//            bookmarkHanlder = new BookmarkHandler(mContext);
//            bookmarkHanlder.open();
//        }
	}
	
	@Override
	  public int getCount() {
	   return d.size();
	  }

	  @Override
	  public Object getItem(int position) {
	   return d.get(position);
	  }

	  @Override
	  public long getItemId(int position) {
	   return position;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		LayoutInflater inflater;
		  if (convertView==null) {
				 inflater =LayoutInflater.from(mContext);
				 convertView = inflater.inflate(R.layout.row_bookmark, null);
	        	 holder = new ViewHolder();
	        	 holder.txtPageNum = (TextView)convertView.findViewById(R.id.txt_bookmark_number);
	        	 holder.txtTitle = (TextView)convertView.findViewById(R.id.txt_bookmark_title);
	        	 holder.btnEdit = (ImageView)convertView.findViewById(R.id.btn_bookmark_edit);
	        	 holder.btnDelete = (ImageView)convertView.findViewById(R.id.btn_bookmark_delete);
	        	 holder.img = (ImageView)convertView.findViewById(R.id.img_bookmark_image);
	        	 convertView.setTag(holder);
		  }	  
		  else{
	        	holder =(ViewHolder)convertView.getTag();
		  }
		  
		  holder.btnEdit.setOnClickListener(new OnClickListener() {
			
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 InputMethodManager keyboard = (InputMethodManager)
					 mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		             keyboard.showSoftInput(holder.btnEdit , InputMethodManager.SHOW_IMPLICIT);	
		             
		             BookmarkActivity bk = (BookmarkActivity)mContext;
		             bk.setFocusUpdateTextDescription(holder.txtTitle.getText().toString(),holder.txtPageNum.getText().toString());
		            
		             BookmarkActivity.positionHighlight = position;
		             bk.refreshBookmark();
		             
		             bk.showMunuEdit();
				}
		  });
		  
		  if(position==  BookmarkActivity.positionHighlight){
			  holder.txtTitle.setBackgroundResource(R.drawable.bg_highlight_text);
		  }else{
			  holder.txtTitle.setBackgroundColor(Color.parseColor("#00000000"));
		  }
		  
		  holder.btnDelete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					confirmDelete(position);
				}
		  });
		  
		  
		  holder.img.setLayoutParams(new LinearLayout.LayoutParams(DeviceSize.getWidth(), DeviceSize.getHeight()));
		  holder.txtPageNum.setText(d.get(position).get("pagenum"));
		  holder.txtTitle.setText(d.get(position).get("title"));
//		  mPath = d.get(position).get("imagepath");
//		  imageLoader.displayImage(d.get(position).get("imagepath"), holder.img, ImageDisplayOptions.getImageOption());
		  drawPageImageView(holder, position);

	   return convertView;
	  }
	  
	   static class ViewHolder{
		   TextView txtPageNum;
		   TextView txtTitle;
	       ImageView btnEdit;
	       ImageView btnDelete;
	       ImageView img;
	    }
	   

		private void drawPageImageView(ViewHolder holder, int position) {
			if (cancelPotentialWork(holder, position)) {
				final BitmapWorkerTask task = new BitmapWorkerTask(holder, position);
				final AsyncDrawable asyncDrawable = new AsyncDrawable(
						mContext.getResources(), mLoadingBitmap, task);
				holder.img.setImageDrawable(asyncDrawable);
				task.execute();
			}
		}

		public static boolean cancelPotentialWork(ViewHolder holder, int position) {
			final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(holder.img);

			if (bitmapWorkerTask != null) {
				final int bitmapPosition = bitmapWorkerTask.position;
				if (bitmapPosition != position) {
					// Cancel previous task
					bitmapWorkerTask.cancel(true);
				} else {
					// The same work is already in progress
					return false;
				}
			}
			// No task associated with the ImageView, or an existing task was
			// cancelled
			return true;
		}
	   private void confirmDelete(final int position){
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
	        builder.setCancelable(true);
	        builder.setTitle(mContext.getString(R.string.bk_delete_confirm));
	        builder.setInverseBackgroundForced(true);
	        builder.setPositiveButton(mContext.getString(R.string.bk_delete_confirm_yes),
	                new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog,
	                            int which) {
	    					BookmarkHandler bookmarkHandler = new BookmarkHandler(mContext);
	    					bookmarkHandler.open();
	    					bookmarkHandler.deleteBookmark(d.get(position).get("bookid"), d.get(position).get("pagenum"));
	    					BookmarkActivity bk = (BookmarkActivity)mContext;
	    					bk.refreshBookmark();

	                    }
	                });
	        builder.setNegativeButton(mContext.getString(R.string.bk_delete_confirm_no),
	                new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog,
	                            int which) {
	                        dialog.dismiss();
	                    }
	                });
	        AlertDialog alert = builder.create();
	        alert.show();
		}
	   
	   class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {

			private final WeakReference<ViewHolder> viewHolderReference;
			private int position;

			public BitmapWorkerTask(ViewHolder holder, int position) {
				viewHolderReference = new WeakReference<ViewHolder>(holder);
				this.position = position;
			}

			@Override
			protected Bitmap doInBackground(Integer... params) {
//				if (mPreviewSize == null) {
//					mPreviewSize = new Point();
//					int padding = mContext.getResources().getDimensionPixelSize(
//							R.dimen.page_preview_size);
//					PointF mPageSize = mCore.getSinglePageSize(0);
//					float scale = mPageSize.y / mPageSize.x;
//					DeviceSize.getWidth() = (int) ((float) padding / scale);
//					mPreviewSize.y = padding;
//				}
				Bitmap lq = null;
				lq = getCachedBitmap(position);
				mBitmapCache.put(position, lq);
				return lq;
			}

			@Override
			protected void onPostExecute(Bitmap bitmap) {
				if (isCancelled()) {
					bitmap = null;
				}

				if (viewHolderReference != null && bitmap != null) {
					final ViewHolder holder = viewHolderReference.get();
					if (holder != null) {
						final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(holder.img);
						if (this == bitmapWorkerTask && holder != null) {
							holder.img.setImageBitmap(bitmap);
							
						}
					}
				}
			}
		}

		private Bitmap getCachedBitmap(int position) {
			String mCachedBitmapFilePath = d.get(position).get("imagepath");
//			String mCachedBitmapFilePath = mPath;
			File mCachedBitmapFile = new File(mCachedBitmapFilePath);
			Bitmap lq = null;
			try {
				if (mCachedBitmapFile.exists() && mCachedBitmapFile.canRead()) {
//					Log.d(TAG, "page " + position + " found in cache");
					lq = BitmapFactory.decodeFile(mCachedBitmapFilePath);
					return lq;
				}
			} catch (Exception e) {
				e.printStackTrace();
				// some error with cached file,
				// delete the file and get rid of bitmap
				mCachedBitmapFile.delete();
				lq = null;
			}
			if (lq == null) {
				lq = Bitmap.createBitmap(DeviceSize.getWidth(), DeviceSize.getHeight(),
						Bitmap.Config.ARGB_8888);
//				mCore.drawSinglePage(position, lq, DeviceSize.getWidth(),DeviceSize.getHeight());
				try {
					lq.compress(CompressFormat.JPEG, 50, new FileOutputStream(
							mCachedBitmapFile));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					mCachedBitmapFile.delete();
				}
			}
			return lq;
		}
		
		static class AsyncDrawable extends BitmapDrawable {
			private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

			public AsyncDrawable(Resources res, Bitmap bitmap,
					BitmapWorkerTask bitmapWorkerTask) {
				super(res, bitmap);
				bitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(
						bitmapWorkerTask);
			}

			public BitmapWorkerTask getBitmapWorkerTask() {
				return bitmapWorkerTaskReference.get();
			}
		}

		private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
			if (imageView != null) {
				final Drawable drawable = imageView.getDrawable();
				if (drawable instanceof AsyncDrawable) {
					final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
					return asyncDrawable.getBitmapWorkerTask();
				}
			}
			return null;
		}

}
	   

