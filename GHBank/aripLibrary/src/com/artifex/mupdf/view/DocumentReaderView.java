package com.artifex.mupdf.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.arip.it.library.ExamChoice;
import com.arip.it.library.ExamFill;
import com.arip.it.library.ExamMatch;
import com.arip.it.library.ExamSort;
import com.arip.it.library.ExamSound;
import com.arip.it.library.R;
import com.arip.it.library.SlideShowActivity;
import com.arip.it.library.VideoActivity;
import com.arip.it.library.WebActivity;
import com.artifex.mupdf.LinkInfoExternal;
import com.artifex.mupdf.MuPDFPageView;
import com.artifex.mupdf.PageView;
import com.artifex.mupdf.domain.SearchTaskResult;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public abstract class DocumentReaderView extends ReaderView {
    private static final String TAG = "DocumentReaderView";

    private enum LinkState {
        DEFAULT, HIGHLIGHT, INHIBIT
    }

    private static int tapPageMargin = 70;

    private LinkState linkState = LinkState.DEFAULT;

    private boolean showButtonsDisabled;

    public DocumentReaderView(Context context,
                              SparseArray<LinkInfoExternal[]> pLinkOfDocument) {
        super(context, pLinkOfDocument);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        tapPageMargin = (int) (getWidth() * .1);
        Log.w(TAG,"new width : " + w);
        Log.w(TAG,"new height : " + h);
        Log.w(TAG,"old width : " + oldw);
        Log.w(TAG,"old height : " + oldh);
        Log.w(TAG,"tapPageMargin : " + tapPageMargin);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        Log.w(TAG,TAG + " onSingleTapConfirmed");
        Log.w(TAG,"Link linkState : " + linkState);
        if (!showButtonsDisabled) {
            int linkPage = -1;
            String linkString = null;

            if (linkState != LinkState.INHIBIT) {
                MuPDFPageView pageView = (MuPDFPageView) getDisplayedView();
                if (pageView != null) {
                    linkPage = pageView.hitLinkPage(e.getX(), e.getY());
                    linkString = pageView.hitLinkUri(e.getX(),  e.getY());
                }
            }

            if (linkPage != -1) {
                // block pageView from sliding to next page
                noAutomaticSlide = true;
                Log.d(TAG,"linkPage ="+ linkPage);
                setDisplayedViewIndex(linkPage);
            } else if (linkString != null) {
                // start intent with url as linkString
                openLink(linkString);
            } else {
                if (e.getX() < tapPageMargin) {
                    Log.d(TAG, "moveToPrevious");
                    super.moveToPrevious();
                } else if (e.getX() > super.getWidth() - tapPageMargin) {
                    Log.d(TAG, "moveToNext");
                    super.moveToNext();
                } else {
                    onContextMenuClick();
                }
            }
        }
        return super.onSingleTapUp(e);
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector d) {
        // Disabled showing the buttons until next touch.
        // Not sure why this is needed, but without it
        // pinch zoom can make the buttons appear
        showButtonsDisabled = true;
        return super.onScaleBegin(d);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.w(TAG,TAG + " onTouchEvent");
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            showButtonsDisabled = false;
        }
        return super.onTouchEvent(event);
    }

    public boolean isShowButtonsDisabled() {
        return showButtonsDisabled;
    }

    abstract protected void onContextMenuClick();
    abstract protected void onBuy(String path);

    //        protected void onChildSetup(int i, View v) {
//            if (SearchTaskResult.get() != null && SearchTaskResult.get().pageNumber == i)
//                    ((PageView)v).setSearchBoxes(SearchTaskResult.get().searchBoxes);
//            else
//                    ((PageView)v).setSearchBoxes(null);
//
//            ((PageView)v).setLinkHighlighting(mLinkState == LinkState.HIGHLIGHT);
//    }

    @Override
    protected void onMoveToChild(View view, int i) {

//            mPageNumberView.setText(String.format("%d/%d", i+1, core.countPages()));
//            mPageSlider.setMax((core.countPages()-1) * mPageSliderRes);
//            mPageSlider.setProgress(i * mPageSliderRes);
        if (SearchTaskResult.get() != null && SearchTaskResult.get().pageNumber != i) {
            SearchTaskResult.recycle();
            resetupChildren();
        }
    }

    @Override
    protected void onSettle(View v) {
        ((PageView)v).addHq(true);
    }

    @Override
    protected void onUnsettle(View v) {
        ((PageView)v).removeHq();
    }

    @Override
    protected void onNotInUse(View v) {
        ((PageView)v).releaseResources();
    }

    /**
     * @param linkString - url to open
     */
    private void openLink(String linkString) {
        Log.d(TAG, "!openLink " + linkString);
        String storage = Environment.getExternalStorageDirectory()+"";

        Uri uri = Uri.parse(linkString);
        String warect = uri.getQueryParameter("warect");
        Log.d("warect","warect :"+warect);

        Boolean isFullScreen = warect != null && warect.equals("full");
        if(linkString.startsWith("http://localhost/")) {
            // display local content

            // get the current page view
            String path = uri.getPath();
            Log.d(TAG, "localhost path = " + path);
            if(path == null)
                return;

            if(path.endsWith("jpg") || path.endsWith("png") || path.endsWith("bmp")) {
                // start image slideshow
                Intent intent = new Intent(getContext(), SlideShowActivity.class);
                intent.putExtra("path", path.toLowerCase());
                intent.putExtra("uri", linkString.toLowerCase());

                Log.d(TAG,"basePath = "+path+"\nuri = "+ linkString);
                //startActivity(intent);
            } if(path.endsWith("mp4") && isFullScreen) {
                // start a video player
                Intent intent = new Intent(getContext(), VideoActivity.class);
                intent.putExtra("path", path);
                intent.putExtra("uri", linkString);
                //Uri videoUri = Uri.parse("file://" + getStoragePath() + "/wind_355" + path);
//                            Intent intent = new Intent(Intent.ACTION_VIEW, videoUri);
                //startActivity(intent);
            } if(path.endsWith("mp3")) {

            } if (path.equals("html5")){
                Log.w("DocumentReader","Html5 Type clicked.");
            }
        } else if(linkString.startsWith("+exam+")) {
            String Link = linkString.replace("+exam+", "");
            Log.e("Ch.DocumentReaderView", "Link : "+Link);

            String Total = null,firstID = null,firstTYPE = null,section = null;

            ArrayList<String> read = new ArrayList<String>();
            String line = null;
            int lineNumber = 0;

            try{

                FileReader ex = new FileReader(Link + "exam.txt");
                BufferedReader examR = new BufferedReader(ex);
                /*
                Total = String.valueOf( examR.readLine() );
                section = String.valueOf( examR.readLine() );
                firstTYPE = String.valueOf( examR.readLine() );
                firstID = String.valueOf( examR.readLine() );
*/

                while ((line = examR.readLine()) != null){
                    if (line.length() > 0){
                        read.add(line);
                        lineNumber++;
                    }
                }

                for (int i = 0; i < read.size(); i++)
                    Log.d("TAG", "READ : " + read.get(i));

                examR.close();
            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                Log.v("TAG","Exam : " + read);
            }

//            Log.e("Ch Doc.", "Total:"+read.get(0)+" section:"+read.get(1)+" ID:"+read.get(3)+" type:"+read.get(2));
//            Log.d("TAG", "Link : " + Link + "exam.txt");

            Intent intent;


            if(read.get(2).equalsIgnoreCase("choice"))intent = new Intent(getContext(), ExamChoice.class);
            else if(read.get(2).equalsIgnoreCase("sound"))intent = new Intent(getContext(), ExamSound.class);
            else if(read.get(2).equalsIgnoreCase("match"))intent = new Intent(getContext(), ExamMatch.class);
            else if(read.get(2).equalsIgnoreCase("sort"))intent = new Intent(getContext(), ExamSort.class);
            else intent = new Intent(getContext(), ExamFill.class);


            intent.putExtra("path", Link);
            intent.putExtra("total", read.get(0));
            intent.putExtra("section", read.get(1));
            intent.putExtra("type", read.get(2));
            intent.putExtra("id", read.get(3));
            intent.putExtra("x", 1);
            //intent.putExtra("sign","+");

            getContext().startActivity(intent);


        } else if(linkString.startsWith("http") || linkString.startsWith("html5")) {

            if(linkString.endsWith(".pdf"))
                linkString = "https://docs.google.com/gview?url=" + linkString;

//            linkString = "http://epub.arip.co.th/authoring/files/test/test_27092016_99059/zip/ZIP_27092016_76118/index.html";

            Intent intent = new Intent(getContext(), WebActivity.class);
            // intent.putExtra("uri", linkString.toLowerCase());
            intent.putExtra("uri", linkString);
            getContext().startActivity(intent);
        }


        else if(linkString.startsWith(storage))
        {
            // custom dialog
            final Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.lay_quick_action);
            dialog.setTitle("Tips");
/*
    			FileReader cartoonFr = new FileReader();
    			BufferedReader cartoonBr = new BufferedReader(cartoonFr);
    	     	int pageNumber = Integer.valueOf( cartoonBr.readLine() );
    	     	cartoonBr.close();
    	     	*/

            // Tooltip Content
            String content="";
            String path = linkString+".txt";


            try{

                FileReader tooltip = new FileReader(path);
                BufferedReader tooltipBr = new BufferedReader(tooltip);
                content = String.valueOf( tooltipBr.readLine() );
                tooltipBr.close();

            }
            catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            //Display display = getWindowManager().getDefaultDisplay();
            int width = display.getWidth();  // deprecated
            int height = display.getHeight();  // deprecated


            ScrollView scroll = (ScrollView) dialog.findViewById(R.id.scrollView1);
            //scroll.getLayoutParams().width= (width/5)*3;
            scroll.getLayoutParams().height= height/3;


            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.scrollText);

            content  = replaceAll(content,"&lt;","<");
            Log.d(TAG, "txt : "+content);
            content = replaceAll (content, "&gt;",">");
            Log.d(TAG, "txt : "+content);

            text.setText(Html.fromHtml(content));

            //Tooltip Image
            ImageView image = (ImageView) dialog.findViewById(R.id.scrollImage);

            image.getLayoutParams().width = width/2;
            image.getLayoutParams().height = height/3;


            //image.setImageResource(R.drawable.ex_sort_1);
            File file;
            File file_jpg = new File(linkString+".jpg");
            File file_png = new File(linkString+".png");
            File file_bmp = new File(linkString+".bmp");
            if(file_jpg.exists())file=file_jpg;
            else if(file_png.exists())file=file_png;
            else if(file_bmp.exists())file=file_bmp;
            else file=null;

            if(file !=null){
                Uri link = Uri.fromFile(file);
                image.setImageURI(link);
            }
            //image.setImageDrawable(Drawable.createFromPath(linkString));

            dialog.show();
        }

        else if(linkString.startsWith("buy://localhost")) {
//                    onBuy(uri.getPath().substring(1));
        } else {
//                    WebViewActivity.startWithUrl(getContext(), uri.toString());
        }

    }



    private String replaceAll(String source, String pattern, String replacement) {
        if (source == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        int index;
        int patIndex = 0;
        while ((index = source.indexOf(pattern, patIndex)) != -1) {
            sb.append(source.substring(patIndex, index));
            sb.append(replacement);
            patIndex = index + pattern.length();
        }
        sb.append(source.substring(patIndex));
        return sb.toString();
    }

    private String getString(String content) {
        // TODO Auto-generated method stub
        return null;
    }
}